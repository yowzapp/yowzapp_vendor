package com.groundhog.vendor.android.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.groundhog.vendor.android.model.NavDrawerItem;
import com.groundhog.vendor.android.utils.PlaythoraUtility;

import java.util.Collections;
import java.util.List;

import static com.groundhog.vendor.android.utils.Config.RALEWAY_SEMIBOLD;

/**
 * Created by vaishakha on 8/9/16.
 */
public class NavigationDrawerAdapter extends RecyclerView.Adapter<NavigationDrawerAdapter.MyViewHolder> {
    List<NavDrawerItem> data = Collections.emptyList();
    private LayoutInflater inflater;
    private Context context;
    public static int selected_item = 0;

    public NavigationDrawerAdapter(Context context, List<NavDrawerItem> data) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.data = data;
        setHasStableIds(true);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(com.groundhog.vendor.android.R.layout.nav_drawer_row, parent, false); //Inflating the layout
        MyViewHolder vhItem = new MyViewHolder(v); //Creating ViewHolder and passing the object of type view
        return vhItem; // Returning the created object

    }

    public void Refersh() {
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        NavDrawerItem current = data.get(position);
        holder.title.setText(current.getTitle());

        holder.title.setTypeface((PlaythoraUtility.getFont(context, RALEWAY_SEMIBOLD)));

        if (position == selected_item) {
            holder.navView.setBackgroundColor(Color.parseColor("#f4f4f4"));
            holder.viewLine.setVisibility(View.INVISIBLE);
        } else {
            holder.navView.setBackgroundColor(Color.WHITE);
            holder.viewLine.setVisibility(View.INVISIBLE);

        }

        if (position == 0)
            holder.image.setImageResource(com.groundhog.vendor.android.R.drawable.ic_booking);

        if (position == 1)
            holder.image.setImageResource(com.groundhog.vendor.android.R.drawable.ic_venue);

        if (position == 2)
            holder.image.setImageResource(com.groundhog.vendor.android.R.drawable.ic_record);

        if (position == 3)
            holder.image.setImageResource(com.groundhog.vendor.android.R.drawable.ic_accounts);

        if (position == 4)
            holder.image.setImageResource(com.groundhog.vendor.android.R.drawable.ic_settings);

        if (position == 5)
            holder.image.setImageResource(com.groundhog.vendor.android.R.drawable.ic_logout);

        if (position == 6)
            holder.viewLine.setVisibility(View.VISIBLE);

        if (position == 7)
            holder.image.setImageResource(com.groundhog.vendor.android.R.drawable.ic_feedback);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        ImageView image;
        View viewLine;
        RelativeLayout navView;

        public MyViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(com.groundhog.vendor.android.R.id.title_text);
            image = (ImageView) itemView.findViewById(com.groundhog.vendor.android.R.id.userimage);
            viewLine = (View) itemView.findViewById(com.groundhog.vendor.android.R.id.viewLine);
            navView = (RelativeLayout) itemView.findViewById(com.groundhog.vendor.android.R.id.nav_view);
        }
    }
}
