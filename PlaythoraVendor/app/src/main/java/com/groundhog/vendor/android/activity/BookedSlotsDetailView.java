package com.groundhog.vendor.android.activity;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.groundhog.vendor.android.utils.Config;
import com.groundhog.vendor.android.utils.PlaythoraUtility;
import com.groundhog.vendor.android.utils.PreferenceManager;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

import static com.groundhog.vendor.android.utils.Config.RALEWAY_REGULAR;
import static com.groundhog.vendor.android.utils.Config.RALEWAY_SEMIBOLD;

/**
 * Created by hemanth on 29/9/16.
 */
public class BookedSlotsDetailView extends AppCompatActivity {

    Toolbar toolbar;
    String nameValue, dateValue, btiming, bStatus, bId, payMode, transId, payment;
    TextView name, date, timings, bookingStatus, bookingId, paymentMode, transactionId, paymentModeText,
            transactionIdText, cancel, confirm, call, bookedBy, dateText, timingText, statusText, bIdText;
    PreferenceManager mPref;
    int bookId;
    String num = "", venuename, groundName, cancelBooking;
    LinearLayout cancelLayout;
    TextView toolbarTitle, toolbarSubTitle, text;
    Button yes, no;
    Dialog dialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.groundhog.vendor.android.R.layout.booked_slots_detail_layout);

        initializeAllComponents();
        Intent i = getIntent();
        bookId = i.getIntExtra("BOOKINDID", 0);
        venuename = i.getStringExtra("VENUENAME");
        groundName = i.getStringExtra("GROUNDNAME");
        Log.e("venuename", venuename);
        Log.e("groundName", groundName);
        mPref = PreferenceManager.instance(this);
        toolbar = (Toolbar) findViewById(com.groundhog.vendor.android.R.id.bookedSlotsDeatilViewToolbar);
        toolbar.setNavigationIcon(com.groundhog.vendor.android.R.drawable.ic_back_arrow);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle(Html.fromHtml("<font color=\"#345560\"><big>" + venuename + "</big></font>"));
        getSupportActionBar().setSubtitle(Html.fromHtml("<font color='#345560'><small>" + groundName + "</small></font>"));

        toolbarTitle = (TextView) findViewById(com.groundhog.vendor.android.R.id.toolbar_title);
        toolbarSubTitle = (TextView) findViewById(com.groundhog.vendor.android.R.id.toolbar_sub_title);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        loadDetial(bookId);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelDialog(bookId);
            }
        });
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home = new Intent(BookedSlotsDetailView.this, HomeActivity.class);
                home.putExtra("ActivtyName", "newBooking");
                startActivity(home);
            }
        });

        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + num));
                startActivity(intent);
            }
        });

    }

    private void cancelDialog(final int bookId) {
        Log.e("popUpId", bookId + "");

        dialog = new Dialog(BookedSlotsDetailView.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(com.groundhog.vendor.android.R.layout.cancel_booking_dialog);

        yes = (Button) dialog.findViewById(com.groundhog.vendor.android.R.id.yes_btn_cancel);
        no = (Button) dialog.findViewById(com.groundhog.vendor.android.R.id.no_btn);
        text = (TextView) dialog.findViewById(com.groundhog.vendor.android.R.id.main_text);

        yes.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        no.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        text.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

                if (PlaythoraUtility.checkInternetConnection(BookedSlotsDetailView.this)) {
                    PlaythoraUtility.showProgressDialog(BookedSlotsDetailView.this);
                    AsyncHttpClient mHttpClient = new AsyncHttpClient();
                    mHttpClient.addHeader("accessToken", mPref.getAccessToken());
                    RequestParams params = new RequestParams();
                    params.add("booking_id", String.valueOf(bookId));
                    mHttpClient.post(Config.CANCEL_BOOKING, params,
                            new AsyncHttpResponseHandler() {
                                @Override
                                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                    PlaythoraUtility.hideProgressDialog();
                                    try {
                                        String s = new String(responseBody);
                                        Log.e("CANCELBOOKING", s);
                                        JSONObject object = new JSONObject(s);
                                        if (object.getInt("statusCode") == 200) {
                                            Toast.makeText(BookedSlotsDetailView.this, object.getString("message"), Toast.LENGTH_SHORT).show();
                                            Intent intent = new Intent(BookedSlotsDetailView.this, HomeActivity.class);
                                            startActivity(intent);
                                        }

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }

                                @Override
                                public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                                    PlaythoraUtility.hideProgressDialog();
                                    try {
                                        String s = new String(bytes);
                                        Log.e("RESPONSE_FAIL", s);
                                        JSONObject object = new JSONObject(s);
                                        if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                                            Toast.makeText(BookedSlotsDetailView.this, object.getString("message"), Toast.LENGTH_SHORT).show();
                                            mPref.setAccessToken("");
                                            Intent intent = new Intent(BookedSlotsDetailView.this, LoginActivity.class);
                                            startActivity(intent);
                                            finish();
                                        } else
                                            Toast.makeText(BookedSlotsDetailView.this, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        // Toast.makeText(getActivity(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                } else {
                    Toast.makeText(BookedSlotsDetailView.this, "Not connected to internet", Toast.LENGTH_LONG).show();
                }

            }
        });

        dialog.show();
    }

    private void loadDetial(int bookId) {

        if (PlaythoraUtility.checkInternetConnection(BookedSlotsDetailView.this)) {
            PlaythoraUtility.showProgressDialog(BookedSlotsDetailView.this);
            AsyncHttpClient mHttpClient = new AsyncHttpClient();
            mHttpClient.addHeader("accessToken", mPref.getAccessToken());
            Log.e("ACCESSTOKEN", mPref.getAccessToken());
            RequestParams params = new RequestParams();
            params.add("booking_id", String.valueOf(bookId));
            mHttpClient.post(Config.BOOKING_DETAIL, params,
                    new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                            PlaythoraUtility.hideProgressDialog();
                            try {
                                String s = new String(responseBody);
                                Log.e("RESPONSE", s);
                                JSONObject object = new JSONObject(s);
                                if (object.getString("status").equalsIgnoreCase("success")) {
                                    JSONObject j = object.getJSONObject("success");
                                    name.setText(j.getString("player_name"));
                                    date.setText(j.getString("date"));
                                    timings.setText(j.getString("start_time") + " - " + j.getString("end_time"));
                                    bookingId.setText(j.getInt("id") + "");

                                    if (j.getBoolean("payment_status"))
                                        bookingStatus.setText("Payment success");
                                    else bookingStatus.setText("Payment failed");

                                    paymentMode.setText(j.getString("payment_mode"));
                                    transactionId.setText(j.getString("transaction_identifier"));
                                    num = j.getString("mobile");
                                    cancelBooking = j.getBoolean("can_cancel") + "";

                                    Log.e("can_cancel", cancelBooking);

                                    if (cancelBooking.equalsIgnoreCase("true")) {
                                        cancelLayout.setVisibility(View.VISIBLE);
                                    } else {
                                        cancelLayout.setVisibility(View.GONE);
                                    }

                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                            PlaythoraUtility.hideProgressDialog();
                            try {
                                String s = new String(bytes);
                                Log.e("RESPONSE_FAIL", s);
                                JSONObject object = new JSONObject(s);
                                if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                                    Toast.makeText(BookedSlotsDetailView.this, object.getString("message"), Toast.LENGTH_SHORT).show();
                                    mPref.setAccessToken("");
                                    Intent intent = new Intent(BookedSlotsDetailView.this, LoginActivity.class);
                                    startActivity(intent);
                                    finish();
                                } else
                                    Toast.makeText(BookedSlotsDetailView.this, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                            } catch (Exception e) {
                                e.printStackTrace();
                                // Toast.makeText(getActivity(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        } else {
            Toast.makeText(BookedSlotsDetailView.this, "Not connected to internet", Toast.LENGTH_LONG).show();
        }


    }

    private void initializeAllComponents() {

        name = (TextView) findViewById(com.groundhog.vendor.android.R.id.bookedSlotsName);
        date = (TextView) findViewById(com.groundhog.vendor.android.R.id.bookedSlotsDate);
        timings = (TextView) findViewById(com.groundhog.vendor.android.R.id.bookedSlotsTimings);
        bookingStatus = (TextView) findViewById(com.groundhog.vendor.android.R.id.bookedSlotsStatus);
        bookingId = (TextView) findViewById(com.groundhog.vendor.android.R.id.bookedSlotsId);
        paymentMode = (TextView) findViewById(com.groundhog.vendor.android.R.id.bookedSlotsPaymentMode);
        transactionId = (TextView) findViewById(com.groundhog.vendor.android.R.id.bookedSlotsTransactionId);
        paymentModeText = (TextView) findViewById(com.groundhog.vendor.android.R.id.bookedSlotsPaymentModeText);
        transactionIdText = (TextView) findViewById(com.groundhog.vendor.android.R.id.bookedSlotsTransactionIdText);
        cancel = (TextView) findViewById(com.groundhog.vendor.android.R.id.bookingCancel);
        confirm = (TextView) findViewById(com.groundhog.vendor.android.R.id.bookingConfirm);
        call = (TextView) findViewById(com.groundhog.vendor.android.R.id.callCustomer);

        bookedBy = (TextView) findViewById(com.groundhog.vendor.android.R.id.bookedSlotsNameText);
        dateText = (TextView) findViewById(com.groundhog.vendor.android.R.id.bookedSlotsDateText);
        timingText = (TextView) findViewById(com.groundhog.vendor.android.R.id.bookedSlotsTimingText);
        statusText = (TextView) findViewById(com.groundhog.vendor.android.R.id.bookedSlotsStatusText);
        bIdText = (TextView) findViewById(com.groundhog.vendor.android.R.id.bookedSlotsIdText);

        cancelLayout = (LinearLayout) findViewById(com.groundhog.vendor.android.R.id.linearLayout);


        name.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_SEMIBOLD));
        date.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_SEMIBOLD));
        timings.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_SEMIBOLD));
        bookingStatus.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_SEMIBOLD));
        bookingId.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_SEMIBOLD));
        paymentMode.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_SEMIBOLD));
        transactionId.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_SEMIBOLD));
        cancel.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        confirm.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        call.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        bookedBy.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        dateText.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        timingText.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        statusText.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        bIdText.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        paymentModeText.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        transactionIdText.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));


    }
}
