package com.groundhog.vendor.android.activity;

import android.Manifest;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.groundhog.vendor.android.utils.Config;
import com.groundhog.vendor.android.utils.PlaythoraUtility;
import com.groundhog.vendor.android.utils.PreferenceManager;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.tuenti.smsradar.Sms;
import com.tuenti.smsradar.SmsListener;
import com.tuenti.smsradar.SmsRadar;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

import static com.groundhog.vendor.android.utils.Config.RALEWAY_REGULAR;

/**
 * Created by Nakul on 9/8/2016.
 */
public class VerificationActivity extends AppCompatActivity {

    EditText number, enterPin;
    TextInputLayout verifyNumberInputLayout, enterPinInputLayout;
    Button verify;
    TextView count, mobileNumber, verifyText, verifySubText;
    String phoneNum;
    CountDownTimer waitingTimer, retryTimer;
    private boolean isSMSRecevied = false;
    TextView resendCode, verifyNow;
    int position;
    PreferenceManager mPref;
    //    String otp;
    private int PERMISSION_REQUEST_CODE = 200;

    String[] permissionsSms = new String[]{
            Manifest.permission.READ_SMS,
            Manifest.permission.RECEIVE_SMS,
    };
    boolean value;
    String activityName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.groundhog.vendor.android.R.layout.verification_layout);

        initializeAllComponents();
        mPref = PreferenceManager.instance(this);
        Toolbar toolbar = (Toolbar) findViewById(com.groundhog.vendor.android.R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(com.groundhog.vendor.android.R.drawable.ic_back_arrow);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });


        if (getIntent().hasExtra("activity")) {
            activityName = getIntent().getStringExtra("activity");
        }

       /* number.setText("+91"+ " ");

        position = number.length();
        Editable editable = number.getText();
        Selection.setSelection(editable, position);*/


        number.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                verifyNumberInputLayout.setError(null);
            }
        });


        verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkForAllValidation()) {
                    phoneNum = number.getText().toString();
                    VerifyNum();
                }
            }
        });

        if (checkPermissionSms()) {
            value = true;
        }


    }

    private boolean checkPermissionSms() {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p : permissionsSms) {
            result = ContextCompat.checkSelfPermission(VerificationActivity.this, p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), PERMISSION_REQUEST_CODE);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions, int[] grantResults) {

        switch (permsRequestCode) {


            case 200:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    value = true;
                    //startTimer();

                } else {
                    value = false;
                    /*textTimeCounter.setVisibility(View.INVISIBLE);
                    textCoudntFetch.setText("Please enter otp manually");
                    textSecondLine.setVisibility(View.INVISIBLE);
                    resend.setEnabled(true);
                    resend.setTextColor(Color.parseColor("#024E8E"));*/
                    // mVerify.setEnabled(true);
                    // mOtp.setEnabled(true);
                }


                break;


        }


    }

    private void VerifyNum() {
        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            PlaythoraUtility.showProgressDialog(VerificationActivity.this);
            AsyncHttpClient mHttpClient = new AsyncHttpClient();
            RequestParams params = new RequestParams();
            params.add("mobile", phoneNum);
            params.add("id", mPref.getId());

            mHttpClient.post(Config.MOBILE_VERIFY, params,
                    new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                            PlaythoraUtility.hideProgressDialog();
                            String s = new String(responseBody);
                            Log.e("RESPONSE", s);
                            try {
                                JSONObject object = new JSONObject(s);
                                if (object.getString("status").equalsIgnoreCase("success")) {
//                                    otp = String.valueOf(object.getInt("OTP"));
                                    showDialog(phoneNum);
                                    //  Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                            PlaythoraUtility.hideProgressDialog();
                            String s = new String(bytes);
                            Log.e("mobileVerifyFailure", s);
                            JSONObject mobileVerifyFail = null;
                            try {
                                mobileVerifyFail = new JSONObject(s);
                                if (mobileVerifyFail.has("message")) {
                                    Toast.makeText(getApplicationContext(), mobileVerifyFail.getString("message"), Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(VerificationActivity.this, "Some error occured, Please try again later", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    });
        } else {

            Toast.makeText(getApplicationContext(), "Not connected to internet", Toast.LENGTH_LONG).show();

        }


    }

    private void showDialog(String phoneNum) {

        final Dialog phoneDialog = new Dialog(VerificationActivity.this);
        phoneDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        phoneDialog.setCanceledOnTouchOutside(false);
        phoneDialog.setContentView(com.groundhog.vendor.android.R.layout.verify_dialog_layout);


        count = (TextView) phoneDialog.findViewById(com.groundhog.vendor.android.R.id.verificationCoundDownTime);
        mobileNumber = (TextView) phoneDialog.findViewById(com.groundhog.vendor.android.R.id.mobileNumber);
        enterPin = (EditText) phoneDialog.findViewById(com.groundhog.vendor.android.R.id.otpId);
        resendCode = (TextView) phoneDialog.findViewById(com.groundhog.vendor.android.R.id.resendCode);
        verifyNow = (TextView) phoneDialog.findViewById(com.groundhog.vendor.android.R.id.verifyNow);
        enterPinInputLayout = (TextInputLayout) phoneDialog.findViewById(com.groundhog.vendor.android.R.id.input_layout_enter_pin);
        verifySubText = (TextView) phoneDialog.findViewById(com.groundhog.vendor.android.R.id.verifySubText);

        resendCode.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        verifyNow.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        enterPinInputLayout.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        verifySubText.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));


        mobileNumber.setText(phoneNum);
//        enterPin.setText(otp);

        if (value) {
            timerstart();
        } else {
            count.setText("Please enter the code in SMS manually");
            //  count.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
            count.setTextSize(16);
            resendCode.setEnabled(true);
        }
        phoneDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                try {
                    if (waitingTimer != null) {
                        waitingTimer.cancel();
                    }
                    SmsRadar.stopSmsRadarService(VerificationActivity.this);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        enterPin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    SmsRadar.stopSmsRadarService(VerificationActivity.this);
                    if (waitingTimer != null) {
                        waitingTimer.cancel();
                    }
                    count.setText("Please enter the code in SMS manually");
                    count.setTextSize(16);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        phoneDialog.show();

    }

    private void timerstart() {

        waitingTimer = new CountDownTimer(30000, 1000) {
            public void onTick(long millisUntilFinished) {

                if (String.valueOf(millisUntilFinished / 1000).length() == 2) {
                    count.setText("" + millisUntilFinished / 1000 + "s");
                } else {
                    count.setText("0" + millisUntilFinished / 1000 + "s");
                }

                // count.setText(millisUntilFinished / 1000 + " sec");
                count.setTextSize(35);
                resendCode.setEnabled(false);
                try {
                    ListenToIncomingSms();
                } catch (NullPointerException e) {
                    System.out.print("Caught the NullPointerException");
                }
            }

            public void onFinish() {

                if (!isSMSRecevied) {
                    retryTimer = new CountDownTimer(1000, 1000) {
                        @Override
                        public void onTick(long millisUntilFinished) {
                            if (String.valueOf(millisUntilFinished / 1000).length() == 2) {
                                count.setText("" + millisUntilFinished / 1000);
                            } else {
                                count.setText("0" + millisUntilFinished / 1000);
                            }
                            count.setTextSize(35);
                            try {
                                ListenToIncomingSms();
                            } catch (NullPointerException e) {
                                System.out.print("Caught the NullPointerException");
                            }
                        }

                        @Override
                        public void onFinish() {
                            SmsRadar.stopSmsRadarService(VerificationActivity.this);
                            // Toast.makeText(getApplicationContext(), "Did not receive any msg please try again", Toast.LENGTH_SHORT).show();
                            count.setText("Please enter the code in SMS manually");
                            // count.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
                            count.setTextSize(16);
                            resendCode.setEnabled(true);
                        }
                    };
                    retryTimer.start();
                }
            }
        };
        waitingTimer.start();

        resendCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                resendOtp();
            }
        });

        enterPin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                enterPinInputLayout.setError(null);
            }
        });

        verifyNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validation()) {
                    varifyOtp();
                }
            }
        });

    }

    public void ListenToIncomingSms() {
        SmsRadar.initializeSmsRadarService(VerificationActivity.this, new SmsListener() {
            @Override
            public void onSmsSent(Sms sms) {

            }


            @Override
            public void onSmsReceived(Sms sms) {
                Log.v("IncomingMessage", String.valueOf(sms));
                String val = String.valueOf(sms);
                if (val.contains("PLYTHR") || val.contains("PAZZOM")) {
                    if (val.contains("msg")) {
                        String id = new String(val);
                        int startindex = id.indexOf("OTP");
                        int endindex = id.indexOf("to");
                        String number = id.substring(startindex, endindex);
                        String numberOnly = number.replaceAll("[^0-9]", "");
                        Log.v("Number", numberOnly);
                        enterPin.setText(numberOnly);
                        try {
                            waitingTimer.cancel();
                        } catch (Exception e) {
                        }
                        try {
                            varifyOtp();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });
    }

    private void resendOtp() {
        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            PlaythoraUtility.showProgressDialog(VerificationActivity.this);
            AsyncHttpClient mHttpClient = new AsyncHttpClient();

            RequestParams params = new RequestParams();
            params.add("mobile", phoneNum);
            params.add("id", mPref.getId());
            mHttpClient.post(Config.RESEND_OTP, params,
                    new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                            PlaythoraUtility.hideProgressDialog();
                            String s = new String(responseBody);
                            Log.e("RESPONSE", s);
                            try {
                                JSONObject object = new JSONObject(s);
                                if (object.getString("status").equalsIgnoreCase("success")) {
                                    Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_SHORT).show();
                                    timerstart();
                                    count.setText("");
                                    count.setTextSize(35);
                                    verifyNow.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            if (validation()) {
                                                varifyOtp();
                                            }
                                        }
                                    });
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                            PlaythoraUtility.hideProgressDialog();
                            String s = new String(bytes);
                            Log.e("RESPONSEFAIL", s);
                            Toast.makeText(getApplicationContext(), "Could not register try again later", Toast.LENGTH_SHORT).show();
                        }

                    });
        } else {

            Toast.makeText(getApplicationContext(), "Not connected to internet", Toast.LENGTH_LONG).show();

        }

    }

    private void varifyOtp() {
        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            PlaythoraUtility.showProgressDialog(VerificationActivity.this);
            AsyncHttpClient mHttpClient = new AsyncHttpClient();

            RequestParams params = new RequestParams();
            params.add("id", mPref.getId());
            params.add("otp", enterPin.getText().toString());
            Log.e("OTP_PARAMS", params.toString());

            mHttpClient.post(Config.OTP_VERIFY, params,
                    new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                            PlaythoraUtility.hideProgressDialog();
                            String s = new String(responseBody);
                            Log.e("RESPONSE", s);
                            try {
                                JSONObject object = new JSONObject(s);
                                if (object.getString("status").equalsIgnoreCase("success")) {
                                    if (activityName.equalsIgnoreCase("SignUpActivity")) {
                                        mPref.setMobilVerified("true");
                                        Toast.makeText(getApplicationContext(), "Successfully verified, try login.", Toast.LENGTH_SHORT).show();
                                        Intent login = new Intent(VerificationActivity.this, LoginActivity.class);
                                        startActivity(login);
                                        finishAffinity();
                                    } else {
                                        Toast.makeText(getApplicationContext(), "Successfully verified.", Toast.LENGTH_SHORT).show();
                                        mPref.setMobilVerified("true");
                                        Intent login = new Intent(VerificationActivity.this, HomeActivity.class);
                                        startActivity(login);
                                        finishAffinity();
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                            PlaythoraUtility.hideProgressDialog();
                            String s = new String(bytes);
                            Log.e("RESPONSEFAIL", s);
                            Toast.makeText(getApplicationContext(), "Could not register try again later", Toast.LENGTH_SHORT).show();
                        }

                    });
        } else {

            Toast.makeText(getApplicationContext(), "Not connected to internet", Toast.LENGTH_LONG).show();

        }
    }

    private boolean validation() {

        if (enterPin.getText().toString().isEmpty() || enterPin.getText().length() < 6) {
            enterPinInputLayout.setError("Enter 6 digit otp");
            enterPinInputLayout.setErrorEnabled(true);
        } else {
            enterPinInputLayout.setErrorEnabled(false);
        }

        if (enterPinInputLayout.isErrorEnabled()) {
            return false;
        } else {
            return true;
        }
    }

    private boolean checkForAllValidation() {

        if (number.getText().toString().length() == 0) {
            verifyNumberInputLayout.setError("Please enter mobile number");
            verifyNumberInputLayout.setErrorEnabled(true);
        } else if (number.getText().toString().length() < 10) {
            verifyNumberInputLayout.setError("Enter 10 digit mobile number");
            verifyNumberInputLayout.setErrorEnabled(true);
        } else {
            verifyNumberInputLayout.setErrorEnabled(false);
        }

        if (verifyNumberInputLayout.isErrorEnabled()) {
            return false;
        } else {
            return true;
        }
    }

    private void initializeAllComponents() {
        number = (EditText) findViewById(com.groundhog.vendor.android.R.id.verifyNumber);
        verifyText = (TextView) findViewById(com.groundhog.vendor.android.R.id.verify_text);
        verify = (Button) findViewById(com.groundhog.vendor.android.R.id.verify_btn);
        verifyNumberInputLayout = (TextInputLayout) findViewById(com.groundhog.vendor.android.R.id.input_layout_signup_verify_number);
        verificationUI(findViewById(com.groundhog.vendor.android.R.id.verification_activity));

        //  number.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        verify.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        verifyNumberInputLayout.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        verifyText.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));

    }

    private void verificationUI(View viewById) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(viewById instanceof EditText)) {
            viewById.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    PlaythoraUtility.hideSoftKeyboard(VerificationActivity.this);
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (viewById instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) viewById).getChildCount(); i++) {
                View innerView = ((ViewGroup) viewById).getChildAt(i);
                verificationUI(innerView);
            }
        }
    }
}
