package com.groundhog.vendor.android.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.groundhog.vendor.android.utils.Config;
import com.groundhog.vendor.android.utils.PlaythoraUtility;
import com.groundhog.vendor.android.utils.PreferenceManager;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

import static com.groundhog.vendor.android.utils.Config.RALEWAY_REGULAR;

/**
 * Created by Nakul on 9/14/2016.
 */
public class ViewBankAccountActivity extends AppCompatActivity {

    TextView accName,accNumber,branchName,ifsc,bankName,accNameText,accNumberText,deleteTitle,detailText;
    String name,num,bName,ifscCode,bankValue;
    int isPrimary,accountId;
    String position;
    PreferenceManager mPref;
    Button yes,no;
    Toolbar toolbar;
    TextView title;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.groundhog.vendor.android.R.layout.view_bank_account_layout);

        toolbar = (Toolbar) findViewById(com.groundhog.vendor.android.R.id.view_account_toolbar);
        title = (TextView) findViewById(com.groundhog.vendor.android.R.id.toolbar_title);
        toolbar.setNavigationIcon(com.groundhog.vendor.android.R.drawable.ic_back_arrow);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        title.setText("View bank account");
        title.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));

      //  getSupportActionBar().setTitle(Html.fromHtml("<font color=\"white\"><small>" + "View bank account" + "</small></font>"));

        initializeAllComponents();

        Intent i = getIntent();

        name = i.getStringExtra("AccName");
        num = i.getStringExtra("AccNumber");
        bankValue = i.getStringExtra("BankName");
        bName = i.getStringExtra("BranchName");
        ifscCode = i.getStringExtra("IFSC");
        isPrimary = i.getIntExtra("isCheck",0);
        position = i.getStringExtra("position");
        accountId = i.getIntExtra("id",0);

        Log.v("Posoition",position);
        Log.e("is_primary", String.valueOf(isPrimary));
        Log.e("AccName",""+name);
        Log.e("AccNumber",""+num);
        Log.e("BankName",""+bankValue);
        Log.e("BranchName",""+bName);
        Log.e("IFSC",""+ifscCode);
        Log.e("accountId",""+isPrimary);

        viewBankAccountDetail();

    }

    private void  viewBankAccountDetail() {

        if(PlaythoraUtility.checkInternetConnection(getApplicationContext())){
            PlaythoraUtility.showProgressDialog(ViewBankAccountActivity.this);
            AsyncHttpClient httpClient = new AsyncHttpClient();
            httpClient.addHeader("accessToken",mPref.getAccessToken());

            RequestParams params = new RequestParams();
            params.add("account_id", String.valueOf(accountId));

            httpClient.post(Config.BANK_ACCOUNT_DETAIL, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    PlaythoraUtility.hideProgressDialog();

                    String s = new String(responseBody);
                     Log.e("viewBankDetail",s);

                    accName.setText(name);
                    accNumber.setText(num);
                    bankName.setText("Bank name :"+" "+bankValue);
                    branchName.setText("Branch name :"+" "+bName);
                    ifsc.setText("IFSC code :"+" "+ifscCode);

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    PlaythoraUtility.hideProgressDialog();
                    try {
                        String s = new String(responseBody);
                        Log.e("RESPO",s);
                        JSONObject object = new JSONObject(s);
                        if(object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400){
                            mPref.setAccessToken("");
                            Toast.makeText(getApplicationContext(),object.getString("message"),Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(ViewBankAccountActivity.this,LoginActivity.class);
                            startActivity(intent);
                            finish();
                        }else Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                    }catch (JSONException e){
                        e.printStackTrace();
                        //Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                    }
                }
            });

        }
    }

    private void initializeAllComponents() {
        mPref = PreferenceManager.instance(getApplicationContext());
        accName = (TextView) findViewById(com.groundhog.vendor.android.R.id.accountName);
        accNumber = (TextView) findViewById(com.groundhog.vendor.android.R.id.accountNumber);
        branchName = (TextView) findViewById(com.groundhog.vendor.android.R.id.branchName);
        ifsc = (TextView) findViewById(com.groundhog.vendor.android.R.id.ifsc);
        bankName = (TextView) findViewById(com.groundhog.vendor.android.R.id.bankName);

        accNameText = (TextView) findViewById(com.groundhog.vendor.android.R.id.account_name_text);
        accNumberText = (TextView) findViewById(com.groundhog.vendor.android.R.id.account_number_text);
        detailText = (TextView) findViewById(com.groundhog.vendor.android.R.id.bank_details_text);

        accName.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        branchName.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        ifsc.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        bankName.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        accNameText.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        accNumberText.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        detailText.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(com.groundhog.vendor.android.R.menu.menu_view_account, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case com.groundhog.vendor.android.R.id.icDelete:

                final Dialog deleteDialog= new Dialog(ViewBankAccountActivity.this);
                deleteDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                deleteDialog.setContentView(com.groundhog.vendor.android.R.layout.delete_dialog_layout);

                yes = (Button) deleteDialog.findViewById(com.groundhog.vendor.android.R.id.yes);
                no = (Button) deleteDialog.findViewById(com.groundhog.vendor.android.R.id.no);
                deleteTitle = (TextView) deleteDialog.findViewById(com.groundhog.vendor.android.R.id.main_text);

                yes.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
                no.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
                deleteTitle.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));

                yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        deleteBankAccount();
                        deleteDialog.dismiss();
                    }
                });

                no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        deleteDialog.dismiss();
                    }
                });
                deleteDialog.show();
                return true;


            case com.groundhog.vendor.android.R.id.icEdit:
                Intent edit = new Intent(ViewBankAccountActivity.this,EditBankAccountActivity.class);
                edit.putExtra("AccName",name);
                edit.putExtra("AccNumber",num);
                edit.putExtra("BankName",bankValue);
                edit.putExtra("BranchName",bName);
                edit.putExtra("IFSC",ifscCode);
                edit.putExtra("isCheck",isPrimary);
                edit.putExtra("account_id",accountId);
                edit.putExtra("position",position);
                startActivity(edit);
                return true;

            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                break;

        }
        return false;
    }

    private void deleteBankAccount() {
        if(PlaythoraUtility.checkInternetConnection(getApplicationContext())){
            PlaythoraUtility.showProgressDialog(ViewBankAccountActivity.this);
            AsyncHttpClient httpClient = new AsyncHttpClient();
            httpClient.addHeader("accessToken",mPref.getAccessToken());

            RequestParams params = new RequestParams();
            params.add("account_id", String.valueOf(accountId));
            params.add("id",mPref.getId());

            httpClient.post(Config.DELETE_BANK_ACCOUNT, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    PlaythoraUtility.hideProgressDialog();
                    String delete = new String(responseBody);
                    Log.e("DELETE",delete);
                    Toast.makeText(ViewBankAccountActivity.this,"Bank account deleted successfully",Toast.LENGTH_SHORT).show();
                    Intent deleted = new Intent(ViewBankAccountActivity.this,HomeActivity.class);
                    deleted.putExtra("ActivityName","bankAccount");
                    startActivity(deleted);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    PlaythoraUtility.hideProgressDialog();
                    try {
                        String s = new String(responseBody);
                        Log.e("RESPO",s);
                        JSONObject object = new JSONObject(s);
                        if(object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400){
                            mPref.setAccessToken("");
                            Toast.makeText(getApplicationContext(),object.getString("message"),Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(ViewBankAccountActivity.this,LoginActivity.class);
                            startActivity(intent);
                            finish();
                        }else Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                    }catch (JSONException e){
                        e.printStackTrace();
                        //Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }
}
