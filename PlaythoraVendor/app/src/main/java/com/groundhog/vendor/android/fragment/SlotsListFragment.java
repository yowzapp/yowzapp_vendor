package com.groundhog.vendor.android.fragment;

import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.groundhog.vendor.android.R;
import com.groundhog.vendor.android.model.GroundSlotsTimeModel;
import com.groundhog.vendor.android.utils.ExpandableHeightListView;

import java.sql.Time;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by vaishakha on 20/9/16.
 */
public class SlotsListFragment extends Fragment{
    View view;
    ExpandableHeightListView slotList;
    //MyListAdapter adapter;
    ArrayList<GroundSlotsTimeModel> newList,newadapterList;
    String starttime,endTimeText,price,members,ground,verified,getTime,mTime,day;
    TextInputLayout startTimeInputLayout,endTimeInputLayout,slotDurationInputLayout,slotPriceInputLayout,memberInputLayout;
    EditText slotDuration,slotPrice,slotMember;
    TextView startTime,endTime;
    Button save;
    CheckBox checkBox;

    public SlotsListFragment(String ground, String verified,String day) {
        this.ground = ground;
        this.verified = verified;
        this.day = day;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.slot_list_layout, container,
                false);

        initializeAllComponents();
        textChangeListeners();

       /* slotList.setExpanded(true);
        newList = new ArrayList<>();
        adapter = new MyListAdapter(getActivity(),newList);
        slotList.setAdapter(adapter);*/

        checkBox.setText("Disable all slots for " + day);
        endTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTime = "endTime";
                callTimePicker(mTime);
            }
        });

        startTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTime = "startTime";
                callTimePicker(mTime);
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(checkForValidation()){
                    getActivity().finish();
                }
            }
        });

        return view;

    }

    private boolean checkForValidation() {
        boolean value;

        if(startTime.getText().toString().trim().isEmpty() || endTime.getText().toString().isEmpty()){
            Toast.makeText(getActivity(),"Select time",Toast.LENGTH_SHORT).show();
            value = true;
        }else value = false;

        if(slotDuration.getText().toString().trim().isEmpty()){
            slotDurationInputLayout.setError("Please select slot duration");
            slotDurationInputLayout.setErrorEnabled(true);
        }else slotDurationInputLayout.setErrorEnabled(false);

        if(slotPrice.getText().toString().trim().isEmpty()){
            slotPriceInputLayout.setError("Please select slot price");
            slotPriceInputLayout.setErrorEnabled(true);
        }else slotPriceInputLayout.setErrorEnabled(false);

        if(slotMember.getText().toString().trim().isEmpty()){
            memberInputLayout.setError("Please select slot members");
            memberInputLayout.setErrorEnabled(true);
        }else memberInputLayout.setErrorEnabled(false);

        if(value || slotDurationInputLayout.isErrorEnabled() || slotPriceInputLayout.isErrorEnabled() || memberInputLayout.isErrorEnabled())
            return false;
        else return true;
    }

    private void callTimePicker(final String time) {
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;

        mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                String t = getTime( selectedHour, selectedMinute);
                //getTime = time.replace("AM", "am").replace("PM","pm");
                if(time.equalsIgnoreCase("startTime"))
                    startTime.setText(t);
                else endTime.setText(t);
                Log.e("TTTTTTt",mTime);
            }
        }, hour, minute, true);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }

    private String getTime(int hr, int min) {
        Time tme = new Time(hr,min,0);//seconds by default set to zero
        Format formatter;
        formatter = new SimpleDateFormat("h:mm a");
        return formatter.format(tme);
    }

    private void initializeAllComponents() {
        slotList = (ExpandableHeightListView) view.findViewById(R.id.slot_list);
        slotDurationInputLayout = (TextInputLayout) view.findViewById(R.id.input_layout_slot_duration);
        slotPriceInputLayout = (TextInputLayout) view.findViewById(R.id.input_layout_slot_price);
        memberInputLayout = (TextInputLayout) view.findViewById(R.id.input_layout_num_mem);
        startTime = (TextView) view.findViewById(R.id.start_text);
        endTime = (TextView) view.findViewById(R.id.end_text);
        slotDuration = (EditText) view.findViewById(R.id.slot_text);
        slotPrice = (EditText) view.findViewById(R.id.slot_price);
        slotMember = (EditText) view.findViewById(R.id.number_mem);
        save = (Button) view.findViewById(R.id.venue_slots_save);
        checkBox = (CheckBox) view.findViewById(R.id.check_box_disable);
    }

    private void textChangeListeners() {
       /* startTime.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                startTimeInputLayout.setError(null);
            }
        });

        endTime.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                endTimeInputLayout.setError(null);
            }
        });*/

        slotDuration.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                slotDurationInputLayout.setError(null);
            }
        });

        slotPrice.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                slotPriceInputLayout.setError(null);
            }
        });

        slotMember.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                memberInputLayout.setError(null);
            }
        });

    }


   /* @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.e("RRRRRRRRRRR", String.valueOf(resultCode) + " " + String.valueOf(requestCode));

        try {
            if (requestCode == 9) {
                starttime = data.getStringExtra("StartTime");
                endTimeText = data.getStringExtra("EndTime");
                price = data.getStringExtra("Price");
                members = data.getStringExtra("Member");
                newList.add(new GroundSlotsTimeModel(starttime, endTimeText, price, members));
                Log.e("slotTime", starttime + " " + endTime + " " + price + " " + members);
                adapter.mRefresh(newList);
            }
        }catch (Exception e){}
    }*/

    /*private class MyListAdapter extends BaseAdapter{
        private LayoutInflater inflater=null;
        Context context;

        public MyListAdapter(Context context, ArrayList<GroundSlotsTimeModel> newList) {
            this.context = context;
            newadapterList = new ArrayList<>();
            for(int i =0 ;i<newList.size();i++){
                newadapterList.add(newList.get(i));
            }
            newadapterList.add(new GroundSlotsTimeModel("","","",""));
        }

        public void mRefresh(ArrayList<GroundSlotsTimeModel> newList){
            newadapterList = new ArrayList<>();
            for(int i =0 ;i<newList.size();i++){
                newadapterList.add(newList.get(i));
            }
            newadapterList.add(new GroundSlotsTimeModel("","","",""));
            notifyDataSetChanged();
        }
        @Override
        public int getCount() {
            return newadapterList.size();
        }

        @Override
        public Object getItem(int i) {
            return i;
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(final int i, View view, ViewGroup viewGroup) {
            View rowView;
            final Holder holder=new Holder();

            inflater = ( LayoutInflater )context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.slots_list_item, null);
            holder.time = (TextView) rowView.findViewById(R.id.text_time);
            holder.price = (TextView) rowView.findViewById(R.id.text_price);
            holder.members = (TextView) rowView.findViewById(R.id.text_members);
            holder.listItemRelative = (LinearLayout) rowView.findViewById(R.id.slot_info);
            holder.addGroundslot = (RelativeLayout) rowView.findViewById(R.id.slot_add);
            holder.edit = (ImageView) rowView.findViewById(R.id.slot_edit);
            holder.clear = (ImageView) rowView.findViewById(R.id.slot_clear);

            if(newadapterList.get(i).getPrice().isEmpty()){
                holder.listItemRelative.setVisibility(View.GONE);
                holder.addGroundslot.setVisibility(View.VISIBLE);
            }else{
                holder.listItemRelative.setVisibility(View.VISIBLE);
                holder.addGroundslot.setVisibility(View.GONE);
                String[] st = newadapterList.get(i).getStartTime().split(" ");
                //String[] et = newadapterList.get(i).getEndTime().split(" ");
                String str = newadapterList.get(i).getEndTime().replace("am", "AM").replace("pm","PM");
                holder.time.setText(st[0] + " - " + str);
                holder.price.setText(newadapterList.get(i).getPrice());
                holder.members.setText(newadapterList.get(i).getMembers() + " members");

            }

            holder.addGroundslot.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, AddGroundSlotsActivity.class);
                    intent.putExtra("GroundName",ground);
                    intent.putExtra("Verified",verified);
                    getParentFragment().startActivityForResult(intent,9);
                }
            });

            holder.clear.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    newList.remove(i);
                    newadapterList.remove(i);
                    notifyDataSetChanged();
                }
            });

            holder.edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.e("POsition", String.valueOf(i));
                    Intent intent = new Intent(context, AddGroundSlotsActivity.class);
                    intent.putExtra("StartTime",newadapterList.get(i).getStartTime());
                    intent.putExtra("EndTime",newadapterList.get(i).getEndTime());
                    intent.putExtra("Price",newadapterList.get(i).getPrice());
                    intent.putExtra("Member",newadapterList.get(i).getMembers());
                    intent.putExtra("Position",String.valueOf(i));
                    intent.putExtra("GroundName",ground);
                    intent.putExtra("Verified",verified);
                    getParentFragment().startActivityForResult(intent,3);
                }
            });

            return rowView;
        }

        public class Holder
        {
            RelativeLayout addGroundslot;
            LinearLayout listItemRelative;
            TextView time,price,members;
            ImageView clear,edit;
        }
    }
*/
    }
