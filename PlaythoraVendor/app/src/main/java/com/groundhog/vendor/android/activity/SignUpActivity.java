package com.groundhog.vendor.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.groundhog.vendor.android.utils.Config;
import com.groundhog.vendor.android.utils.PlaythoraUtility;
import com.groundhog.vendor.android.utils.PreferenceManager;
import com.google.firebase.iid.FirebaseInstanceId;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

import static com.groundhog.vendor.android.utils.Config.RALEWAY_REGULAR;

/**
 * Created by Nakul on 9/8/2016.
 */
public class SignUpActivity extends AppCompatActivity{

    EditText fullName,email,password,confirmPassword;
    Button registerNow;
    TextInputLayout nameInputLayout,emailInputLayout,passwordInputLayout,confirmInputLayout;
    int id;
    ProgressBar progressBar;
    PreferenceManager mPref;
    String deviceId,mobileVerified;
    String value="true";
    TextView businessText;
    String token,android_id,msg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.groundhog.vendor.android.R.layout.signup_layout);

        initializeAllComponents();

        mPref = PreferenceManager.instance(this);
       token = FirebaseInstanceId.getInstance().getToken();

        msg = getString(com.groundhog.vendor.android.R.string.msg_token_fmt, token);
       // Toast.makeText(SignUpActivity.this, msg, Toast.LENGTH_SHORT).show();



        deviceId = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);
      //  mPref.setAccessToken(token);
        Log.e("android_id", deviceId);
        mPref.setAndroid_id(deviceId);
        Log.e("deviceId", deviceId);
        mPref.setGcmToken(token);

        Toolbar toolbar = (Toolbar) findViewById(com.groundhog.vendor.android.R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(com.groundhog.vendor.android.R.drawable.ic_back_arrow);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });

        registerNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(value.equalsIgnoreCase("true")) {
                    if (checkForAllValidation()) {
                        SignUp();
                        }
                    }
                else if(value.equalsIgnoreCase("checked")){
                    verifyEmail(true);
                }
            }
        });


        fullName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!b){
                    if (fullName.getText().toString().length() == 0 || fullName.getText().toString().trim().length() == 0) {
                        nameInputLayout.setError("Full name is required");
                        nameInputLayout.setErrorEnabled(true);
                    }else {
                        nameInputLayout.setErrorEnabled(false);
                    }
                }
            }
        });

        email.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(!b){
                    if(!android.util.Patterns.EMAIL_ADDRESS.matcher(email.getText().toString()).matches()){
                        emailInputLayout.setError("Please provide valid email address");
                        emailInputLayout.setErrorEnabled(true);
                    }else {
                        verifyEmail(false);

                    }

                }
            }
        });

        password.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(!b){
                    if (password.getText().toString().length() == 0 || password.getText().toString().contains(" ")) {
                        passwordInputLayout.setError("Password is required");
                        passwordInputLayout.setErrorEnabled(true);
                    }else {
                        passwordInputLayout.setErrorEnabled(false);
                    }
                }
            }
        });

        confirmPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {

                if(!b){
                    if (confirmPassword.getText().toString().length() == 0 || confirmPassword.getText().toString().contains(" ")) {
                        confirmInputLayout.setError("Password does not match");
                        confirmInputLayout.setErrorEnabled(true);
                    }else if(!(password.getText().toString().equals(confirmPassword.getText().toString()))){
                        confirmInputLayout.setError("Password does not match");
                        confirmInputLayout.setErrorEnabled(true);
                    } else {
                        confirmInputLayout.setErrorEnabled(false);
                    }
                }

            }
        });

        fullName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                nameInputLayout.setError(null);

            }
        });

        email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                value = "checked";
            }

            @Override
            public void afterTextChanged(Editable editable) {
                emailInputLayout.setError(null);

            }
        });

        password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                passwordInputLayout.setError(null);

            }
        });

        confirmPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                confirmInputLayout.setError(null);

            }
        });


    }

    private void verifyEmail(final boolean b) {
        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            progressBar.setVisibility(View.VISIBLE);
            AsyncHttpClient mHttpClient = new AsyncHttpClient();

            mHttpClient.get(Config.EMAIL_EXISTS + email.getText().toString()+"&type=vendor",
                    new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                            progressBar.setVisibility(View.GONE);
                            String s = new String(responseBody);
                            Log.e("RESPONSE", s);
                            try {
                                JSONObject object = new JSONObject(s);
                                if(object.getString("status").equalsIgnoreCase("success")) {
                                    emailInputLayout.setErrorEnabled(false);
                                    value = "true";
                                    if(b){
                                        if (checkForAllValidation()) {
                                            SignUp();
                                        }
                                    }
                                }
                            } catch (JSONException e) {
                                value = "false";
                                Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                            }

                        }
                        @Override
                        public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                            progressBar.setVisibility(View.GONE);
                            String s = new String(bytes);
                            Log.e("RESPONSE", s);
                            try {
                                JSONObject object = new JSONObject(s);
                                emailInputLayout.setError(object.getString("message"));
                                emailInputLayout.setErrorEnabled(true);
                                value = "false";
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                emailInputLayout.setErrorEnabled(true);
                                value = "false";
                            }

                        }

                    });
        }else{

            Toast.makeText(getApplicationContext(), "Not connected to internet", Toast.LENGTH_LONG).show();

        }
    }

    private void SignUp() {
        if(mPref.getGcmToken()==null){
            token = FirebaseInstanceId.getInstance().getToken();
            mPref.setGcmToken(token);
        }

        if(mPref.getAndroid_id()==null){
            android_id = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                    Settings.Secure.ANDROID_ID);
            Log.e("ANDROID_ID",android_id);
            mPref.setAndroid_id(android_id);
        }

        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            PlaythoraUtility.showProgressDialog(SignUpActivity.this);
            AsyncHttpClient mHttpClient = new AsyncHttpClient();

            RequestParams params= new RequestParams();
            params.add("email",email.getText().toString());
            params.add("password", password.getText().toString());
            params.add("name", fullName.getText().toString());
            params.add("gcm_token",mPref.getGcmToken());
            params.add("device_type","android");
            params.add("device_id",mPref.getAndroid_id());
            Log.e("SIGNUP_PARAMS", params.toString());

            mHttpClient.post(Config.VENDOR_REGISTER ,params,
                    new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                            PlaythoraUtility.hideProgressDialog();
                            String s = new String(responseBody);
                            Log.e("REGISTER", s);
                            try {
                                JSONObject object = new JSONObject(s);
                                id = object.getJSONObject("success").getInt("id");
                                mPref.setId(String.valueOf(id));
                                mobileVerified = String.valueOf(object.getJSONObject("success").getBoolean("mobile_verified"));
                                Log.e("mobile_verified",mobileVerified);
                                mPref.setMobilVerified(mobileVerified);

                                if(object.getString("status").equalsIgnoreCase("success")) {
                                    Intent verification = new Intent(SignUpActivity.this, VerificationActivity.class);
                                    verification.putExtra("activity","SignUpActivity");
                                    startActivity(verification);
                                    finish();
                                    Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                        @Override
                        public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                            PlaythoraUtility.hideProgressDialog();
                            String  s = new String(bytes);
                            Log.e("failure",s);
                            Toast.makeText(getApplicationContext(), "Could not register try again later", Toast.LENGTH_SHORT).show();
                        }

                    });
        }else{

            Toast.makeText(getApplicationContext(), "Not connected to internet", Toast.LENGTH_LONG).show();

        }

    }

    private boolean checkForAllValidation() {

        if (fullName.getText().toString().length() == 0) {
            nameInputLayout.setError("Full name is required");
            nameInputLayout.setErrorEnabled(true);
        }else {
            nameInputLayout.setErrorEnabled(false);
        }

        if (email.getText().toString().length() ==0) {
            emailInputLayout.setError("Please provide  valid email address");
            emailInputLayout.setErrorEnabled(true);
         }else {
            emailInputLayout.setErrorEnabled(false);
        }

        if (password.getText().toString().length() == 0) {
            passwordInputLayout.setError("Password is required");
            passwordInputLayout.setErrorEnabled(true);
        }else {
            passwordInputLayout.setErrorEnabled(false);
        }

        if (confirmPassword.getText().toString().length() == 0) {
            confirmInputLayout.setError("Confirm password is required");
            confirmInputLayout.setErrorEnabled(true);
        }else {
            confirmInputLayout.setErrorEnabled(false);
        }

        if (!(password.getText().toString().equals(confirmPassword.getText().toString()))) {
            confirmInputLayout.setError("Password does not match");
            confirmInputLayout.setErrorEnabled(true);
        }else {
            confirmInputLayout.setErrorEnabled(false);
        }


        if(!email.getText().toString().isEmpty()){
            if(!android.util.Patterns.EMAIL_ADDRESS.matcher(email.getText().toString()).matches()){
                emailInputLayout.setError("Please provide valid email address");
               emailInputLayout.setErrorEnabled(true);
            }else {
                emailInputLayout.setErrorEnabled(false);
            }

        }

        if(nameInputLayout.isErrorEnabled() || emailInputLayout.isErrorEnabled() || passwordInputLayout.isErrorEnabled() || confirmInputLayout.isErrorEnabled()){
            return false;
        }else {
            return true;
        }


    }

    private void initializeAllComponents() {

        fullName = (EditText) findViewById(com.groundhog.vendor.android.R.id.signUpFullName);
        email = (EditText) findViewById(com.groundhog.vendor.android.R.id.signupEmailId);
        password = (EditText) findViewById(com.groundhog.vendor.android.R.id.signupPassword);
        confirmPassword = (EditText) findViewById(com.groundhog.vendor.android.R.id.confirmPassword);
        registerNow = (Button) findViewById(com.groundhog.vendor.android.R.id.btn_signup);
        nameInputLayout = (TextInputLayout) findViewById(com.groundhog.vendor.android.R.id.input_layout_signup_name);
        emailInputLayout = (TextInputLayout) findViewById(com.groundhog.vendor.android.R.id.input_layout_signup_email);
        passwordInputLayout = (TextInputLayout) findViewById(com.groundhog.vendor.android.R.id.input_layout_signup_password);
        confirmInputLayout = (TextInputLayout) findViewById(com.groundhog.vendor.android.R.id.input_layout_signup_confirm_password);
        progressBar = (ProgressBar) findViewById(com.groundhog.vendor.android.R.id.progress_bar);
        signupUI(findViewById(com.groundhog.vendor.android.R.id.signup_activity));
        businessText = (TextView) findViewById(com.groundhog.vendor.android.R.id.business_text);

        fullName.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        email.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        password.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        confirmPassword.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        registerNow.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        nameInputLayout.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        emailInputLayout.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        passwordInputLayout.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        confirmInputLayout.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        businessText.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));


    }

    private void signupUI(View viewById) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(viewById instanceof EditText)) {
            viewById.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    PlaythoraUtility.hideSoftKeyboard(SignUpActivity.this);
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (viewById instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) viewById).getChildCount(); i++) {
                View innerView = ((ViewGroup) viewById).getChildAt(i);
                signupUI(innerView);
            }
        }

    }
}
