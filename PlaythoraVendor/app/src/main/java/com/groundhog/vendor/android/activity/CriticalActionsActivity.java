package com.groundhog.vendor.android.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.groundhog.vendor.android.utils.Config;
import com.groundhog.vendor.android.utils.PlaythoraUtility;
import com.groundhog.vendor.android.utils.PreferenceManager;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;

import static com.groundhog.vendor.android.utils.Config.RALEWAY_REGULAR;

/**
 * Created by vaishakha on 15/9/16.
 */
public class CriticalActionsActivity extends AppCompatActivity {
    private TextView groundName,groundVerified,deleteText,confirmText,deleteInfo;
    CheckBox checkBox;
    private String gName,verifiedText;
    Button yes,no,delete,cancle,conitnue;
    boolean value,isDisable;
    public static boolean success;
    EditText emailId;
    TextInputLayout emailInputLayout;
    PreferenceManager mPref;
    int VenueId;
    Toolbar toolbar;
    TextView title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.groundhog.vendor.android.R.layout.critical_actions_layout);

        toolbar = (Toolbar) findViewById(com.groundhog.vendor.android.R.id.critical_toolbar);
        title = (TextView) findViewById(com.groundhog.vendor.android.R.id.toolbar_title);
        toolbar.setNavigationIcon(com.groundhog.vendor.android.R.drawable.ic_back_arrow);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        title.setText("Critical actions");
        title.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));


      //  getSupportActionBar().setTitle(Html.fromHtml("<font color=\"white\"><small>" + "Critical actions" + "</small></font>"));

        initializeAllComponents();
        mPref = PreferenceManager.instance(this);
        success = false;

        Intent intent = getIntent();
        verifiedText=intent.getStringExtra("Verified");
        gName=intent.getStringExtra("GroundName");
        isDisable=intent.getBooleanExtra("IsDisable",false);
        VenueId=intent.getIntExtra("VenueId",0);

        checkBox.setChecked(isDisable);
        value = isDisable;

        groundName.setText(gName);
        if(verifiedText.equalsIgnoreCase("true")){
        }else groundVerified.setVisibility(View.GONE);


            checkBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!value) {
                        checkBox.setChecked(false);
                        showDialog();
                    }else {
                        checkBox.setChecked(false);
                        value = false;
                        disableGround(value);
                    }
                }
            });


        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteDialog();
            }
        });
    }

    private void disableGround(final boolean value) {
        JSONObject obj = makeJsonObject(value);
        Log.e("JSONOBJ",obj.toString());

        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            try {
                PlaythoraUtility.showProgressDialog(CriticalActionsActivity.this);
                AsyncHttpClient mHttpClient = new AsyncHttpClient();
                mHttpClient.addHeader("accessToken", mPref.getAccessToken());
                StringEntity se = new StringEntity(obj.toString());
                Log.e("JSONOBJ",se.toString());
                mHttpClient.post(getApplicationContext(), Config.EDIT_VENUE, se, "application/json",
                        new AsyncHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                PlaythoraUtility.hideProgressDialog();
                                try {
                                    String s = new String(responseBody);
                                    Log.e("RESPONSE", s);
                                    JSONObject object = new JSONObject(s);
                                    if (object.getString("status").equalsIgnoreCase("success")) {
                                        success =true;
                                        checkBox.setChecked(object.getJSONObject("success").getBoolean("is_disabled"));
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                }

                            }

                            @Override
                            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                                PlaythoraUtility.hideProgressDialog();
                                String s = new String(bytes);
                                Log.d("RESPONSE_FAIL", s);
                                try {
                                    JSONObject object = new JSONObject(s);
                                    if(object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400){
                                        mPref.setAccessToken("");
                                        Toast.makeText(getApplicationContext(),object.getString("message"),Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(CriticalActionsActivity.this,LoginActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }else Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();

                                } catch (JSONException e) {
                                    try {
                                        JSONObject object = new JSONObject(s);
                                        Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_SHORT).show();
                                    }catch (Exception e1){
                                        // Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                    }
                                }

                            }

                        });
            }catch (Exception e){}
        }else{

            Toast.makeText(getApplicationContext(), "Not connected to internet", Toast.LENGTH_LONG).show();

        }



    }

    private JSONObject makeJsonObject(boolean value) {
        JSONObject finalobject = new JSONObject();
        try {
            finalobject.put("is_disabled", value);
            finalobject.put("venue_id", VenueId);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("Grounds", finalobject.toString());
        return finalobject;
    }

    private void initializeAllComponents() {
        checkBox = (CheckBox) findViewById(com.groundhog.vendor.android.R.id.check_delete);
        groundName = (TextView) findViewById(com.groundhog.vendor.android.R.id.critical_ground_name) ;
        groundVerified = (TextView) findViewById(com.groundhog.vendor.android.R.id.critical_verified) ;
        delete = (Button) findViewById(com.groundhog.vendor.android.R.id.btn_delete);
        deleteText = (TextView) findViewById(com.groundhog.vendor.android.R.id.deleteText);

        groundName.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        groundVerified.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        delete.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        checkBox.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        deleteText.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));

    }

    private void deleteDialog() {
        final Dialog dialog = new Dialog(CriticalActionsActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(com.groundhog.vendor.android.R.layout.delete_confirmation_dialog);

        cancle = (Button) dialog.findViewById(com.groundhog.vendor.android.R.id.cancle_btn);
        conitnue = (Button) dialog.findViewById(com.groundhog.vendor.android.R.id.continue_btn);
        emailId = (EditText) dialog.findViewById(com.groundhog.vendor.android.R.id.email_Id);
        emailInputLayout = (TextInputLayout) dialog.findViewById(com.groundhog.vendor.android.R.id.input_layout_email);
        deleteInfo = (TextView) dialog.findViewById(com.groundhog.vendor.android.R.id.delete_info);


        cancle.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        conitnue.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        emailId.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        emailInputLayout.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        deleteInfo.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));




        emailId.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                emailInputLayout.setError(null);
            }
        });

        cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        conitnue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(checkForValidation()) {
                    dialog.dismiss();
                    deleteGround();
                }
            }
        });
        dialog.show();
    }

    private void deleteGround() {

        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            try {
                PlaythoraUtility.showProgressDialog(CriticalActionsActivity.this);
                AsyncHttpClient mHttpClient = new AsyncHttpClient();
                mHttpClient.addHeader("accessToken", mPref.getAccessToken());
                RequestParams params = new RequestParams();
                params.add("venue_id", String.valueOf(VenueId));
                params.add("email", emailId.getText().toString().trim());
                mHttpClient.post( Config.DELETE_VENUE, params,
                        new AsyncHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                PlaythoraUtility.hideProgressDialog();
                                try {
                                    String s = new String(responseBody);
                                    Log.e("RESPONSE", s);
                                    JSONObject object = new JSONObject(s);
                                    if (object.getString("status").equalsIgnoreCase("success")) {
                                        Intent i = new Intent(CriticalActionsActivity.this,HomeActivity.class);
                                        i.putExtra("ActivityName","CriticalActionsActivity");
                                        startActivity(i);
                                        finish();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                }

                            }

                            @Override
                            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                                PlaythoraUtility.hideProgressDialog();
                                String s = new String(bytes);
                                Log.d("RESPONSE_FAIL", s);
                                try {
                                    JSONObject object = new JSONObject(s);
                                    if(object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400){
                                        mPref.setAccessToken("");
                                        Toast.makeText(getApplicationContext(),object.getString("message"),Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(CriticalActionsActivity.this,LoginActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }else Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    try {
                                        JSONObject object = new JSONObject(s);
                                        Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_SHORT).show();
                                    }catch (Exception e1){
                                       // Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                    }
                                }

                            }

                        });
            }catch (Exception e){}
        }else{

            Toast.makeText(getApplicationContext(), "Not connected to internet", Toast.LENGTH_LONG).show();

        }


    }

    private boolean checkForValidation() {
        if(emailId.getText().toString().isEmpty()) {
            emailInputLayout.setError("Please enter email address");
            return false;
        }else if(!android.util.Patterns.EMAIL_ADDRESS.matcher(emailId.getText().toString()).matches()){
            emailInputLayout.setError("Please provide valid email address");
            return false;
        }else {
            emailInputLayout.setErrorEnabled(false);
            return true;
        }

    }

    private void showDialog() {

        final Dialog dialog = new Dialog(CriticalActionsActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(com.groundhog.vendor.android.R.layout.disable_confirmation_layout);

        yes = (Button) dialog.findViewById(com.groundhog.vendor.android.R.id.yes_btn) ;
        no = (Button) dialog.findViewById(com.groundhog.vendor.android.R.id.no_btn);
        confirmText = (TextView) dialog.findViewById(com.groundhog.vendor.android.R.id.confrm_text);

        yes.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        no.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        confirmText.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkBox.setChecked(false);
                value = false;
                dialog.dismiss();
            }
        });
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                value = true;
                disableGround(value);
                dialog.dismiss();
            }
        });

        dialog.show();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                break;

        }
        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent();
        setResult(1,i);
        finish();
    }
}
