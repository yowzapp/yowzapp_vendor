package com.groundhog.vendor.android.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.groundhog.vendor.android.utils.CustomViewPager;

import java.util.List;

/**
 * Created by vaishakha on 16/9/16.
 */
public class VenueSlotsFragment extends Fragment implements View.OnClickListener {
    TextView groundname,sunText,monText,tueText,wedText,thuText,friText,satText;
    CustomViewPager pager;
    View view;
    MyPagerAdapter adapter;
    String ground,verified,game;

    public VenueSlotsFragment(String ground, String verified,String game) {
        this.ground = ground;
        this.verified = verified;
        this.game  = game;
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(com.groundhog.vendor.android.R.layout.venue_slots, container,
                false);

        initializeAllComponents();
        setupListeners();

        //groundname.setText(game);
        adapter = new MyPagerAdapter(getChildFragmentManager());
        pager.setAdapter(adapter);
        pager.setPagingEnabled(false);
        pager.setCurrentItem(0);
        return view;
    }

    private void setupListeners() {
        sunText.setOnClickListener(this);
        monText.setOnClickListener(this);
        tueText.setOnClickListener(this);
        wedText.setOnClickListener(this);
        thuText.setOnClickListener(this);
        friText.setOnClickListener(this);
        satText.setOnClickListener(this);
    }

    private void initializeAllComponents() {
        //groundname = (TextView) view.findViewById(R.id.slots_gname);
        sunText = (TextView) view.findViewById(com.groundhog.vendor.android.R.id.sunday_text);
        monText = (TextView) view.findViewById(com.groundhog.vendor.android.R.id.monday_text);
        tueText = (TextView) view.findViewById(com.groundhog.vendor.android.R.id.tuesday_text);
        wedText = (TextView) view.findViewById(com.groundhog.vendor.android.R.id.wednesday_text);
        thuText = (TextView) view.findViewById(com.groundhog.vendor.android.R.id.thursday_text);
        friText = (TextView) view.findViewById(com.groundhog.vendor.android.R.id.friday_text);
        satText = (TextView) view.findViewById(com.groundhog.vendor.android.R.id.saturday_text);
        pager = (CustomViewPager) view.findViewById(com.groundhog.vendor.android.R.id.pager);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case com.groundhog.vendor.android.R.id.sunday_text:
                sunText.setBackgroundColor(getResources().getColor(com.groundhog.vendor.android.R.color.colorPrimary));
                monText.setBackgroundResource(0);
                tueText.setBackgroundResource(0);
                wedText.setBackgroundResource(0);
                thuText.setBackgroundResource(0);
                friText.setBackgroundResource(0);
                satText.setBackgroundResource(0);
                pager.setCurrentItem(0);
                break;

            case com.groundhog.vendor.android.R.id.monday_text:
                sunText.setBackgroundResource(0);
                monText.setBackgroundColor(getResources().getColor(com.groundhog.vendor.android.R.color.colorPrimary));
                tueText.setBackgroundResource(0);
                wedText.setBackgroundResource(0);
                thuText.setBackgroundResource(0);
                friText.setBackgroundResource(0);
                satText.setBackgroundResource(0);
                pager.setCurrentItem(1);
                break;

            case com.groundhog.vendor.android.R.id.tuesday_text:
                sunText.setBackgroundResource(0);
                monText.setBackgroundResource(0);
                tueText.setBackgroundColor(getResources().getColor(com.groundhog.vendor.android.R.color.colorPrimary));
                wedText.setBackgroundResource(0);
                thuText.setBackgroundResource(0);
                friText.setBackgroundResource(0);
                satText.setBackgroundResource(0);
                pager.setCurrentItem(2);
                break;

            case com.groundhog.vendor.android.R.id.wednesday_text:
                sunText.setBackgroundResource(0);
                monText.setBackgroundResource(0);
                tueText.setBackgroundResource(0);
                tueText.setBackgroundResource(0);
                wedText.setBackgroundColor(getResources().getColor(com.groundhog.vendor.android.R.color.colorPrimary));
                thuText.setBackgroundResource(0);
                friText.setBackgroundResource(0);
                satText.setBackgroundResource(0);
                pager.setCurrentItem(3);
                break;

            case com.groundhog.vendor.android.R.id.thursday_text:
                sunText.setBackgroundResource(0);
                monText.setBackgroundResource(0);
                tueText.setBackgroundResource(0);
                wedText.setBackgroundResource(0);
                thuText.setBackgroundColor(getResources().getColor(com.groundhog.vendor.android.R.color.colorPrimary));
                friText.setBackgroundResource(0);
                satText.setBackgroundResource(0);
                pager.setCurrentItem(4);
                break;

            case com.groundhog.vendor.android.R.id.friday_text:
                sunText.setBackgroundResource(0);
                monText.setBackgroundResource(0);
                tueText.setBackgroundResource(0);
                wedText.setBackgroundResource(0);
                thuText.setBackgroundResource(0);
                friText.setBackgroundColor(getResources().getColor(com.groundhog.vendor.android.R.color.colorPrimary));
                satText.setBackgroundResource(0);
                pager.setCurrentItem(5);
                break;

            case com.groundhog.vendor.android.R.id.saturday_text:
                sunText.setBackgroundResource(0);
                monText.setBackgroundResource(0);
                tueText.setBackgroundResource(0);
                wedText.setBackgroundResource(0);
                thuText.setBackgroundResource(0);
                friText.setBackgroundResource(0);
                satText.setBackgroundColor(getResources().getColor(com.groundhog.vendor.android.R.color.colorPrimary));
                pager.setCurrentItem(6);
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.e("RRRRRRRRRRR", String.valueOf(resultCode) + " " + String.valueOf(requestCode));
        List<Fragment> fragments = getChildFragmentManager().getFragments();
        if (fragments != null) {
            for (Fragment fragment : fragments) {
                fragment.onActivityResult(requestCode, resultCode, data);
            }
        }
    }

        private class MyPagerAdapter extends FragmentPagerAdapter {


        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment currentFragment = new SlotsListFragment(ground,verified,"sunday");
            switch (position) {
                case 0:
                    currentFragment = new SlotsListFragment(ground,verified,"sunday");
                    break;

                case 1:
                    currentFragment = new SlotsListFragment(ground,verified,"monday");
                    break;

                case 2:
                    currentFragment = new SlotsListFragment(ground,verified,"tuesday");
                    break;

                case 3:
                    currentFragment = new SlotsListFragment(ground,verified,"wednesday");
                    break;

                case 4:
                    currentFragment = new SlotsListFragment(ground,verified,"thursday");
                    break;

                case 5:
                    currentFragment = new SlotsListFragment(ground,verified,"friday");
                    break;

                case 6:
                    currentFragment = new SlotsListFragment(ground,verified,"saturday");
                    break;
                default:
                    break;
            }
            return currentFragment;
        }

        @Override
        public int getCount() {
            return 7;
        }
    }
}
