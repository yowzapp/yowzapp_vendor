package com.groundhog.vendor.android.model;

/**
 * Created by vaishakha on 10/9/16.
 */
public class LocationObj {
    private String locationName;
    private String lat;
    private String longg;

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLongg() {
        return longg;
    }

    public void setLongg(String longg) {
        this.longg = longg;
    }
}
