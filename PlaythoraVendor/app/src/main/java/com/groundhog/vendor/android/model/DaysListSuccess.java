package com.groundhog.vendor.android.model;

/**
 * Created by hemanth on 24/3/18.
 */

public class DaysListSuccess {
    private int id;
    private String name;
    private int data_available;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getData_available() {
        return data_available;
    }

    public void setData_available(int data_available) {
        this.data_available = data_available;
    }
}
