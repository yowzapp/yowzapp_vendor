package com.groundhog.vendor.android.model;

/**
 * Created by vaishakha on 25/1/17.
 */
public class BookingSlots {
    private String status;
    private BookingSlotSuccess success;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public BookingSlotSuccess getSuccess() {
        return success;
    }

    public void setSuccess(BookingSlotSuccess success) {
        this.success = success;
    }
}
