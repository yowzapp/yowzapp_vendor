package com.groundhog.vendor.android.model;

/**
 * Created by hemanth on 24/3/18.
 */

public class DaysDetail {
    private int id;
    private int day_id;
    private int ground_id;
    private String start_time;
    private String end_time;
    private int duration;
    private int price;
    private int total_players;
    private int apply_for_rest_days;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDay_id() {
        return day_id;
    }

    public void setDay_id(int day_id) {
        this.day_id = day_id;
    }

    public int getGround_id() {
        return ground_id;
    }

    public void setGround_id(int ground_id) {
        this.ground_id = ground_id;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getTotal_players() {
        return total_players;
    }

    public void setTotal_players(int total_players) {
        this.total_players = total_players;
    }

    public int getApply_for_rest_days() {
        return apply_for_rest_days;
    }

    public void setApply_for_rest_days(int apply_for_rest_days) {
        this.apply_for_rest_days = apply_for_rest_days;
    }
}
