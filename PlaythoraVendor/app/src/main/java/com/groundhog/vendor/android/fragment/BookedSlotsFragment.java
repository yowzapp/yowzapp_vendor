package com.groundhog.vendor.android.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.groundhog.vendor.android.activity.BookedSlotsDetailView;
import com.groundhog.vendor.android.model.BookedSlotsList;
import com.groundhog.vendor.android.model.BookedSlotsModel;
import com.groundhog.vendor.android.utils.PlaythoraUtility;

import java.util.ArrayList;
import java.util.List;

import static com.groundhog.vendor.android.utils.Config.RALEWAY_REGULAR;
import static com.groundhog.vendor.android.utils.Config.RALEWAY_SEMIBOLD;

/**
 * Created by hemanth on 29/9/16.
 */
public class BookedSlotsFragment extends Fragment {

    RecyclerView bookedSlotsRecyclerView;
    Gson gson;
    public static  ArrayList<BookedSlotsModel> bookedSlotsArrayList;
    BookedSlotsAdapter bookedSlotsAdapter;
    TextView emptyText;
    String venueName = "",mGroundName = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(com.groundhog.vendor.android.R.layout.free_slots_layout,container,false);

        emptyText = (TextView)view.findViewById(com.groundhog.vendor.android.R.id.emptytext);
        bookedSlotsRecyclerView  = (RecyclerView) view.findViewById(com.groundhog.vendor.android.R.id.free_slots_recycler_view);
        bookedSlotsRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        bookedSlotsRecyclerView.setHasFixedSize(true);


        emptyText.setTypeface(PlaythoraUtility.getFont(getActivity(),RALEWAY_REGULAR));

        return view;

    }

    public void loadBookedSlots(List<BookedSlotsList> booked_slots, String venueName, String mGroundName){
        this.venueName = venueName;
        this.mGroundName = mGroundName;
        Log.e("venueName",venueName);
        Log.e("mGroundName",mGroundName);
        Log.e("slots", String.valueOf(booked_slots.size()));
        if(booked_slots.size()==0) {
            emptyText.setText("No bookings for now");
            bookedSlotsRecyclerView.setAdapter(null);
        }
        else {
            emptyText.setText("");
            bookedSlotsAdapter = new BookedSlotsAdapter(getActivity(), bookedSlotsRecyclerView, booked_slots);
            bookedSlotsRecyclerView.setAdapter(bookedSlotsAdapter);
            bookedSlotsAdapter.notifyDataSetChanged();
        }

    }


    private class BookedSlotsAdapter extends RecyclerView.Adapter<BookedSlotsAdapter.MyViewHolder>{
        private Context mContext;
        List<BookedSlotsList> bookedSlotsArrayList;
        RecyclerView recyclerView;

        public BookedSlotsAdapter(Context mContext, RecyclerView recyclerView, List<BookedSlotsList> bookedSlotsArrayList) {
            this.mContext = mContext;
            this.bookedSlotsArrayList = bookedSlotsArrayList;
            this.recyclerView = recyclerView;
        }

        @Override
        public BookedSlotsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(com.groundhog.vendor.android.R.layout.booked_slots_item_layout,parent,false);


            return new BookedSlotsAdapter.MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(BookedSlotsAdapter.MyViewHolder holder, final int position) {

            holder.bSlotsPersonName.setText(bookedSlotsArrayList.get(position).getPlayer_name());
            holder.bSlotsTime.setText(bookedSlotsArrayList.get(position).getStart_time() + " - " + bookedSlotsArrayList.get(position).getEnd_time());
            holder.bSlotsPrice.setText("AED: " + bookedSlotsArrayList.get(position).getPrice());


           // bookedSlotsArrayList.get(position).getPayment().equals("true");

            final View itemView = holder.itemView;

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent bookedSlotsView = new Intent(getActivity(),BookedSlotsDetailView.class);
                    bookedSlotsView.putExtra("BOOKINDID",bookedSlotsArrayList.get(position).getId());
                    bookedSlotsView.putExtra("VENUENAME",venueName);
                    bookedSlotsView.putExtra("GROUNDNAME",mGroundName);
                    startActivity(bookedSlotsView);

                }
            });

        }

        @Override
        public int getItemCount() {
            return (null != bookedSlotsArrayList ? bookedSlotsArrayList.size() : 0);
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView bSlotsPersonName,bSlotsTime,bSlotsPrice;
            public MyViewHolder(View itemView) {
                super(itemView);
                bSlotsPersonName = (TextView) itemView.findViewById(com.groundhog.vendor.android.R.id.bookedSlotsPersonName);
                bSlotsTime = (TextView) itemView.findViewById(com.groundhog.vendor.android.R.id.bookedSlotsTime);
                bSlotsPrice = (TextView) itemView.findViewById(com.groundhog.vendor.android.R.id.bookedSlotsPrice);

                bSlotsPersonName.setTypeface(PlaythoraUtility.getFont(getActivity(),RALEWAY_REGULAR));
                bSlotsTime.setTypeface(PlaythoraUtility.getFont(getActivity(),RALEWAY_REGULAR));
                bSlotsPrice.setTypeface(PlaythoraUtility.getFont(getActivity(),RALEWAY_SEMIBOLD));


            }
        }



    }
}
