package com.groundhog.vendor.android.model;

/**
 * Created by vaishakha on 25/1/17.
 */
public class VenueGroundListing {
    private String name;
    private int id;
    private String cover_pic;
    private int booked_slots;
    private int free_slots;
    private String timing;
    private String sport_type;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCover_pic() {
        return cover_pic;
    }

    public void setCover_pic(String cover_pic) {
        this.cover_pic = cover_pic;
    }

    public int getBooked_slots() {
        return booked_slots;
    }

    public void setBooked_slots(int booked_slots) {
        this.booked_slots = booked_slots;
    }

    public int getFree_slots() {
        return free_slots;
    }

    public void setFree_slots(int free_slots) {
        this.free_slots = free_slots;
    }

    public String getTiming() {
        return timing;
    }

    public void setTiming(String timing) {
        this.timing = timing;
    }

    public String getSport_type() {
        return sport_type;
    }

    public void setSport_type(String sport_type) {
        this.sport_type = sport_type;
    }
}
