package com.groundhog.vendor.android.fragment;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.groundhog.vendor.android.adapter.NavigationDrawerAdapter;
import com.groundhog.vendor.android.helper.RecyclerItemClickListener;
import com.groundhog.vendor.android.model.NavDrawerItem;
import com.groundhog.vendor.android.utils.PlaythoraUtility;
import com.groundhog.vendor.android.utils.PreferenceManager;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.groundhog.vendor.android.utils.Config.RALEWAY_SEMIBOLD;

/**
 * Created by vaishakha on 8/9/16.
 */
public class NavigationDrawerFragment extends Fragment {
    private View viewFragment,containerView;
    private RecyclerView recyclerView;
    private static String[] titles = null;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private FragmentDrawerListener drawerListener;
    private static NavigationDrawerAdapter adapter;
    private Context mContext;
    private CircleImageView userImage;
    private TextView userName;
    private PreferenceManager mPref;
    private LinearLayout header;

    public void setDrawerListener(FragmentDrawerListener listener) {
        this.drawerListener = listener;
    }

    public static List<NavDrawerItem> getData() {
        List<NavDrawerItem> data = new ArrayList<>();


        // preparing navigation drawer items
        for (int i = 0; i < titles.length; i++) {
            NavDrawerItem navItem = new NavDrawerItem();
            navItem.setTitle(titles[i]);
            data.add(navItem);
        }
        return data;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // drawer labels
            titles = getActivity().getResources().getStringArray(com.groundhog.vendor.android.R.array.nav_drawer_labels);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflating view layout
        viewFragment = inflater.inflate(com.groundhog.vendor.android.R.layout.fragment_navigation_drawer, container, false);

        initializeAllComponents();


        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);
        adapter = new NavigationDrawerAdapter(getActivity(), getData());
        recyclerView.setAdapter(adapter);

        header.setOnClickListener(null);

        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(mContext, recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if(position!=6) {
                    NavigationDrawerAdapter.selected_item = position;
                    if(position!=5 && position!=7)
                        recyclerView.getAdapter().notifyDataSetChanged();
                    drawerListener.onDrawerItemSelected(view, position);
                    mDrawerLayout.closeDrawer(containerView);
                }
            }
        }));



        return viewFragment;
    }

    public DrawerLayout getmDrawerLayout() {
        return mDrawerLayout;
    }

    public View getContainerView() {
        return containerView;
    }

    public void setUp(int fragmentId, DrawerLayout drawerLayout, final Toolbar toolbar) {
        containerView = getActivity().findViewById(fragmentId);
        mDrawerLayout = drawerLayout;
        Log.e("profile",""+mPref.getProfilePic());
        if(!mPref.getProfilePic().isEmpty()) {
            Picasso.with(getActivity()).load(mPref.getProfilePic()).fit().config(Bitmap.Config.RGB_565).into(userImage);
        }
        userName.setText(mPref.getName());
        mDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, toolbar, com.groundhog.vendor.android.R.string.drawer_open, com.groundhog.vendor.android.R.string.drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getActivity().invalidateOptionsMenu();
//                Log.e("profile",""+mPref.getProfilePic());
//                if(!mPref.getProfilePic().isEmpty()) {
//                    Picasso.with(getActivity()).load(mPref.getProfilePic()).fit().config(Bitmap.Config.RGB_565).into(userImage);
//                }
//                userName.setText(mPref.getName());
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
//                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                toolbar.setAlpha(1 - slideOffset / 2);
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });
    }
    private void initializeAllComponents() {
        mPref = PreferenceManager.instance(getActivity());
        mContext = getActivity();
        recyclerView = (RecyclerView) viewFragment.findViewById(com.groundhog.vendor.android.R.id.drawerList);
        userImage = (CircleImageView) viewFragment.findViewById(com.groundhog.vendor.android.R.id.user_image);
        userName = (TextView) viewFragment.findViewById(com.groundhog.vendor.android.R.id.user_name);
        header = (LinearLayout) viewFragment.findViewById(com.groundhog.vendor.android.R.id.navigation_header);

        userName.setTypeface(PlaythoraUtility.getFont(mContext,RALEWAY_SEMIBOLD));
    }

    public interface FragmentDrawerListener {
        public void onDrawerItemSelected(View view, int position);
    }
    public static void CallRefersh() {
        adapter.Refersh();
    }

}

