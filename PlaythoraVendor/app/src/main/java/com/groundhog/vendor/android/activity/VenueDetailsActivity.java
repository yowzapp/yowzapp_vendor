package com.groundhog.vendor.android.activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.groundhog.vendor.android.model.AllAmenities;
import com.groundhog.vendor.android.utils.Config;
import com.groundhog.vendor.android.utils.PlaythoraUtility;
import com.groundhog.vendor.android.utils.PreferenceManager;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import eu.fiskur.chipcloud.ChipCloud;
import eu.fiskur.chipcloud.ChipListener;

import static com.groundhog.vendor.android.utils.Config.RALEWAY_REGULAR;
import static com.groundhog.vendor.android.utils.Config.RALEWAY_SEMIBOLD;

/**
 * Created by vaishakha on 10/9/16.
 */
public class VenueDetailsActivity extends AppCompatActivity {
    public TextView groundName, groundLocation, groundVerified, bookings, viewBooking, settings, editDetails,
            manageGrounds, venueLoc, amen, picturesText, critical;
    private ImageView groundImage, editDetailVerifyimg, locationVerifyimg, pictureVerifyimg, mngVerifyimg,
            amenitiesVerifyimg, actionVerifyimg;
    private ImageView editDetailImg, locationImg, pictureImg, mngImg, actionsImg, locationIcon, amenitiesImg;
    private String gName = "", gLocation = "", gImage = "", verifiedText = "", games = "", gDesp = "";
    String[] amenities = {}, selectedamenities = {};
    //private static String sportsType;
    private int venueId;
    int mID;
    private GridView gridView;
    ChipCloud chipCloud, dialogChipCloud;
    RelativeLayout editPicture, criticalActionsRelative, groundLocationRelative, editDetailsRelative, slotManagementRelative, viewBookings, amenitiesRelative;
    PreferenceManager mPref;
    JSONArray obj1, galleryObj, allsports, allAmenities, selectedAmenities;
    JSONObject locationObj;
    boolean locationsStatus, isDisable;
    public static ArrayList<AllAmenities> allAmenitiesModel, selectedAmenitiesModel, sendAmenities;
    Gson gson;
    ArrayList<String> list;
    int venueID;
    Dialog dialog;
    Toolbar toolbar;
    TextView title;
    String groundNameValue, date;
    int groundId;
    String groundStatus;
    Toast toast;
    //ProgressBar progressBar;
    // RelativeLayout progress_lay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.groundhog.vendor.android.R.layout.venue_details_layout);

        toolbar = (Toolbar) findViewById(com.groundhog.vendor.android.R.id.main_toolbar);
        title = (TextView) findViewById(com.groundhog.vendor.android.R.id.toolbar_title);
        toolbar.setNavigationIcon(com.groundhog.vendor.android.R.drawable.ic_back_arrow);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // getSupportActionBar().setTitle(Html.fromHtml("<font color=\"white\"><small>" + "Venue details" + "</small></font>"));

        title.setText("Venue details");
        title.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));

        initializeAllComponents();
        mPref = PreferenceManager.instance(this);

        Intent get = getIntent();
        if (get.hasExtra("IDs")) {
        /*gName = get.getStringExtra("GroundName");
        gLocation = get.getStringExtra("Location");
        gImage = get.getStringExtra("GroundImage");
        verifiedText = get.getStringExtra("Verified");
        games = get.getStringExtra("Games");*/
            mID = get.getIntExtra("IDs", 0);
        }

        VenueDetails();

        /*progress_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });*/

        viewBookings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (toast != null) {
                    toast.cancel();
                }

                if (groundStatus.equalsIgnoreCase("false")) {
                    toast = Toast.makeText(VenueDetailsActivity.this, "No booking available", Toast.LENGTH_SHORT);
                    toast.show();
                } else {
                    Intent i = new Intent(VenueDetailsActivity.this, BookingGroundSlotsActivity.class);
                    i.putExtra("VENUENAME", gName);
                    i.putExtra("VENUEID", venueId);
                    i.putExtra("GROUNDNAME", groundNameValue);
                    i.putExtra("GROUNDID", groundId);
                    i.putExtra("DATE", date);
                    startActivity(i);
                }

            }
        });

        editPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(VenueDetailsActivity.this, EditVenuePictures.class);
                intent.putExtra("Images", galleryObj.toString());
                intent.putExtra("GroundName", gName);
                intent.putExtra("VerifiedText", verifiedText);
                intent.putExtra("VENUEID", String.valueOf(venueId));
                startActivityForResult(intent, 2);
            }
        });

        criticalActionsRelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(VenueDetailsActivity.this, CriticalActionsActivity.class);
                intent.putExtra("Verified", verifiedText);
                intent.putExtra("GroundName", gName);
                intent.putExtra("IsDisable", isDisable);
                intent.putExtra("VenueId", venueId);
                startActivityForResult(intent, 4);
            }
        });

        groundLocationRelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(VenueDetailsActivity.this, EditVenueLocation.class);
                intent.putExtra("Verified", verifiedText);
                intent.putExtra("GroundName", gName);
                intent.putExtra("VenueId", venueId);
                intent.putExtra("locationsStatus", locationsStatus);
                intent.putExtra("locationsDetail", locationObj.toString());
                startActivityForResult(intent, 3);
            }
        });

        editDetailsRelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(VenueDetailsActivity.this, EditVenueDetailsActivity.class);
                intent.putExtra("Verified", verifiedText);
                intent.putExtra("GroundName", gName);
                intent.putExtra("GroundImage", gImage);
                intent.putExtra("VenueId", venueId);
                intent.putExtra("GROUNDDETAIL", obj1.toString());
                intent.putExtra("DESCRIPTION", gDesp);
                startActivityForResult(intent, 1);
            }
        });

        /*groundImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(VenueDetailsActivity.this,GalleryViewActivity.class);
                intent.putExtra("ImagePath",gImage);
                intent.putExtra("ImageCode","1");
                startActivity(intent);
            }
        });*/

        slotManagementRelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (allsports == null) {
                    return;
                }
                Intent intent = new Intent(VenueDetailsActivity.this, AddGroundsActivity.class);
                intent.putExtra("Verified", verifiedText);
                intent.putExtra("GroundName", gName);
                intent.putExtra("AllSports", allsports.toString());
                intent.putExtra("GROUNDDETAIL", obj1.toString());
                intent.putExtra("VenueId", String.valueOf(venueId));
                startActivityForResult(intent, 5);
            }
        });

        amenitiesRelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("ALLlllll", String.valueOf(amenities.length));
                Log.e("sssssss", String.valueOf(selectedamenities.length));
                Intent i = new Intent(VenueDetailsActivity.this, AminiteisList.class);
                i.putExtra("VenueId", venueId);
                i.putExtra("ALLAMENITIES", allAmenities.toString());
                i.putExtra("SELECTEDAMENITIES", selectedAmenities.toString());
                startActivityForResult(i, 6);


                //showDialog();
            }
        });
    }


    private void showDialog() {
        list = new ArrayList();
        dialog = new Dialog(VenueDetailsActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(com.groundhog.vendor.android.R.layout.amenities);
        Button save = (Button) dialog.findViewById(com.groundhog.vendor.android.R.id.save_amenities);
        dialogChipCloud = (ChipCloud) dialog.findViewById(com.groundhog.vendor.android.R.id.chip_cloud_dialog);
        ImageView cancel = (ImageView) dialog.findViewById(com.groundhog.vendor.android.R.id.amenities_cancel);
        dialogChipCloud.removeAllViews();
        new ChipCloud.Configure()
                .chipCloud(dialogChipCloud)
                .deselectedFontColor(Color.parseColor("#979797"))
                .deselectedColor(Color.parseColor("#FFFFFF"))
                .selectedColor(Color.parseColor("#40ae79"))
                .selectedFontColor(Color.parseColor("#FFFFFF"))
                .selectTransitionMS(250)
                .deselectTransitionMS(250)
                .labels(amenities)
                .mode(ChipCloud.Mode.MULTI)
                .chipListener(new ChipListener() {
                    @Override
                    public void chipSelected(int index) {

                        list.add(amenities[index]);
                        Log.e("chipSelected", String.valueOf(index));
                        for (int i = 0; i < list.size(); i++) {
                            Log.e("VVVVVVVVVVV", list.get(i));
                        }

                    }

                    @Override
                    public void chipDeselected(int index) {
                        for (int i = 0; i < list.size(); i++) {
                            if (list.get(i).equalsIgnoreCase(amenities[index]))
                                list.remove(i);
                        }

                        Log.e("chipDeselected", String.valueOf(index));

                    }
                })
                .rebuild(selectedamenities);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                postAmenities();

            }
        });
        dialog.show();
    }

    private void postAmenities() {

        JSONObject obj = makeJsonObject();
        Log.e("JSONOBJ", obj.toString());

        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            try {
                PlaythoraUtility.showProgressDialog(VenueDetailsActivity.this);
                AsyncHttpClient mHttpClient = new AsyncHttpClient();
                mHttpClient.addHeader("accessToken", mPref.getAccessToken());
                StringEntity se = new StringEntity(obj.toString());
                Log.e("JSONOBJ", se.toString());
                mHttpClient.post(getApplicationContext(), Config.EDIT_VENUE, se, "application/json",
                        new AsyncHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                PlaythoraUtility.hideProgressDialog();
                                String s = new String(responseBody);
                                Log.e("RESPONSE", s);
                                try {
                                    JSONObject object = new JSONObject(s);
                                    if (object.getString("status").equalsIgnoreCase("success")) {
                                        dialog.dismiss();
                                        VenueDetails();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                }

                            }

                            @Override
                            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                                PlaythoraUtility.hideProgressDialog();
                                String s = new String(bytes);
                                Log.d("RESPONSE_FAIL", s);
                                try {
                                    JSONObject object = new JSONObject(s);
                                    if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                                        mPref.setAccessToken("");
                                        Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(VenueDetailsActivity.this, LoginActivity.class);
                                        startActivity(intent);
                                        finish();
                                    } else
                                        Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    //Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                }

                            }

                        });
            } catch (Exception e) {
            }
        } else {

            Toast.makeText(getApplicationContext(), "Not connected to internet", Toast.LENGTH_LONG).show();

        }


    }

    private JSONObject makeJsonObject() {

        sendAmenities = new ArrayList<>();
        if (list.size() != 0) {
            for (int i = 0; i < list.size(); i++) {
                for (int j = 0; j < allAmenitiesModel.size(); j++) {
                    if (list.get(i).equalsIgnoreCase(allAmenitiesModel.get(j).getName())) {
                        sendAmenities.add(allAmenitiesModel.get(j));
                    }
                }
            }
        }

        for (int i = 0; i < sendAmenities.size(); i++) {
            Log.e("sendAmenities", sendAmenities.get(i).getName());
        }
        for (int i = 0; i < list.size(); i++) {
            Log.e("ListAmenities", list.get(i));
        }

        JSONObject jsonObject = null;
        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < sendAmenities.size(); i++) {
            jsonObject = new JSONObject();
            try {
                jsonObject.put("id", sendAmenities.get(i).getId());

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            jsonArray.put(jsonObject);
        }
        JSONObject finalobject = new JSONObject();
        try {
            finalobject.put("amenities", jsonArray);
            finalobject.put("venue_id", venueID);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("Grounds", finalobject.toString());
        return finalobject;
    }

    private void VenueDetails() {
        Log.e("accessToken", mPref.getAccessToken());
        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            try {
                PlaythoraUtility.showProgressDialog(VenueDetailsActivity.this);
//                progress_lay.setVisibility(View.VISIBLE);
                //progressBar.setVisibility(View.VISIBLE);
                //getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                //  WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                AsyncHttpClient mHttpClient = new AsyncHttpClient();
                mHttpClient.addHeader("accessToken", mPref.getAccessToken());
                final RequestParams params = new RequestParams();
                params.add("venue_id", String.valueOf(mID));
                Log.e("venue_id", mID + " &&&");
                mHttpClient.post(Config.VENUE_DETAILS, params,
                        new AsyncHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                PlaythoraUtility.hideProgressDialog();
//                                progress_lay.setVisibility(View.GONE);
                                // progressBar.setVisibility(View.GONE);
                                //getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                                try {
                                    String s = new String(responseBody);
                                    Log.e("VenueDetail", s);
                                    JSONObject object = new JSONObject(s);
                                    if (object.getString("status").equalsIgnoreCase("success")) {
                                        JSONObject obj = object.getJSONObject("success");
                                        obj1 = new JSONArray();
                                        if (obj.get("grounddetails") instanceof JSONArray) {
                                            obj1 = obj.getJSONArray("grounddetails");
                                            Log.e("OBJ", obj1.toString());
                                        }

                                        groundNameValue = obj.getString("booking_ground_name");
                                        groundId = obj.getInt("booking_ground_id");
                                        date = obj.getString("booking_date");


                                        Log.e("booking_ground_name", groundNameValue);
                                        Log.e("booking_ground_id", groundId + "");
                                        Log.e("booking_date", date);


                                        locationObj = new JSONObject();
                                        if (obj.get("locationdetails") instanceof JSONObject) {
                                            locationObj = obj.getJSONObject("locationdetails");
                                            Log.e("locationObj", locationObj.toString());
                                        }

                                        galleryObj = new JSONArray();
                                        if (obj.get("gallerydetails") instanceof JSONArray) {
                                            galleryObj = obj.getJSONArray("gallerydetails");
                                            Log.e("GALLERYOBJ", galleryObj.toString());
                                        }

                                        allsports = new JSONArray();
                                        if (obj.get("allsports") instanceof JSONArray) {
                                            allsports = obj.getJSONArray("allsports");
                                            Log.e("allsports", allsports.toString());
                                        }

                                        allAmenities = new JSONArray();
                                        allAmenitiesModel = new ArrayList<>();
                                        if (obj.get("allamenities") instanceof JSONArray) {
                                            allAmenities = obj.getJSONArray("allamenities");
                                            String all = allAmenities.toString();
                                            Log.e("allamenities", all);
                                            if (allAmenities.length() != 0) {
                                                gson = new Gson();
                                                JSONArray jsonArray = null;
                                                try {
                                                    jsonArray = new JSONArray(all);
                                                    for (int i = 0; i < jsonArray.length(); i++) {
                                                        allAmenitiesModel.add(gson.fromJson(jsonArray.get(i).toString(), AllAmenities.class));
                                                    }

                                                    amenities = new String[allAmenitiesModel.size()];
                                                    for (int i = 0; i < allAmenitiesModel.size(); i++) {
                                                        amenities[i] = allAmenitiesModel.get(i).getName();
                                                    }
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        }


                                        selectedamenities = new String[0];
                                        selectedAmenities = new JSONArray();
                                        selectedAmenitiesModel = new ArrayList<>();
                                        if (obj.get("amenities") instanceof JSONArray) {
                                            selectedAmenities = obj.getJSONArray("amenities");
                                            Log.e("amenities", selectedAmenities.toString());
                                            String selected = selectedAmenities.toString();
                                            Log.e("allamenities", selected);
                                            if (selectedAmenities.length() != 0) {
                                                Log.e("amenitiesss", selectedAmenities.toString());
                                                gson = new Gson();
                                                JSONArray jsonArray = null;
                                                try {
                                                    jsonArray = new JSONArray(selected);
                                                    for (int i = 0; i < jsonArray.length(); i++) {
                                                        selectedAmenitiesModel.add(gson.fromJson(jsonArray.get(i).toString(), AllAmenities.class));
                                                    }

                                                    selectedamenities = new String[selectedAmenitiesModel.size()];
                                                    for (int i = 0; i < selectedAmenitiesModel.size(); i++) {
                                                        selectedamenities[i] = selectedAmenitiesModel.get(i).getName();
                                                    }
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        }
                                        if (obj.getBoolean("amenities_status")) {
                                            amenitiesVerifyimg.setVisibility(View.GONE);
                                            amenitiesImg.setVisibility(View.VISIBLE);
                                        } else {
                                            amenitiesVerifyimg.setVisibility(View.VISIBLE);
                                            amenitiesImg.setVisibility(View.GONE);
                                        }
                                        gName = obj.getString("name");
                                        gLocation = obj.getString("location_name");
                                        gDesp = obj.getString("description");
                                        groundName.setText(gName);
                                        groundLocation.setText(gLocation);
                                        verifiedText = String.valueOf(obj.getBoolean("is_verified"));
                                        venueId = obj.getInt("id");
                                        gImage = obj.getString("cover_pic");
                                        locationsStatus = obj.getBoolean("locations_status");
                                        isDisable = obj.getBoolean("is_disabled");

                                        venueID = obj.getInt("id");

                                        chipCloud.removeAllViews();
                                        String sportsType = obj.getString("sports_types");
                                        String[] s1 = sportsType.split(",");
                                        Log.e("SPORTS", String.valueOf(s1));

                                        new ChipCloud.Configure()
                                                .chipCloud(chipCloud)
                                                .labels(s1)
                                                .mode(ChipCloud.Mode.MULTI)
                                                .build();


                                        if (obj.getString("sports_types").isEmpty()) {
                                            chipCloud.setVisibility(View.GONE);
                                        } else chipCloud.setVisibility(View.VISIBLE);

                                        if (gLocation.isEmpty()) {
                                            locationIcon.setVisibility(View.GONE);
                                            groundLocation.setVisibility(View.GONE);
                                        } else {
                                            locationIcon.setVisibility(View.VISIBLE);
                                            groundLocation.setVisibility(View.VISIBLE);
                                            groundLocation.setText(gLocation);
                                        }

                                        if (!obj.getString("cover_pic").isEmpty())
                                            Picasso.with(getApplicationContext()).load(obj.getString("cover_pic")).fit().centerCrop().noFade().into(groundImage);

                                        if (obj.getBoolean("is_verified")) {
                                            String colorText = "<font color=\"#40ae79\">" + "Verified!" + "</font>" + " Available on platform";
                                            groundVerified.setText(Html.fromHtml(colorText));
                                        } else groundVerified.setVisibility(View.GONE);

                                        if (obj.getString("sports_types").isEmpty()) {
                                            chipCloud.setVisibility(View.GONE);
                                        }

                                        editDetailVerifyimg.setVisibility(View.GONE);
                                        editDetailImg.setVisibility(View.VISIBLE);


                                        if (locationsStatus) {
                                            locationVerifyimg.setVisibility(View.GONE);
                                            locationImg.setVisibility(View.VISIBLE);
                                        } else {
                                            locationVerifyimg.setVisibility(View.VISIBLE);
                                            locationImg.setVisibility(View.GONE);
                                        }

                                        if (obj.getBoolean("ground_status")) {
                                            mngVerifyimg.setVisibility(View.GONE);
                                            mngImg.setVisibility(View.VISIBLE);
                                        } else {
                                            mngVerifyimg.setVisibility(View.VISIBLE);
                                            mngImg.setVisibility(View.GONE);
                                        }
                                        groundStatus = obj.getBoolean("ground_status") + "";
                                        Log.e("ground_status", groundStatus);


                                        if (obj.getBoolean("gallery_status")) {
                                            pictureVerifyimg.setVisibility(View.GONE);
                                            pictureImg.setVisibility(View.VISIBLE);
                                        } else {
                                            pictureVerifyimg.setVisibility(View.VISIBLE);
                                            pictureImg.setVisibility(View.GONE);
                                        }

                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                }

                            }

                            @Override
                            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                                PlaythoraUtility.hideProgressDialog();

                                try {
                                    String s = new String(bytes);
                                    Log.d("RESPONSE_FAIL", s);
                                    JSONObject object = new JSONObject(s);
                                    if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                                        mPref.setAccessToken("");
                                        Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(VenueDetailsActivity.this, LoginActivity.class);
                                        startActivity(intent);
                                        finish();
                                    } else
                                        Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    //Toast.makeText(getApplicationContext(), "Some error occurred please try again later.", Toast.LENGTH_SHORT).show();
                                }

                            }

                        });
            } catch (Exception e) {
            }
        } else {

            Toast.makeText(getApplicationContext(), "Not connected to internet", Toast.LENGTH_LONG).show();

        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case 1:
                if (EditVenueDetailsActivity.value)
                    VenueDetails();
                break;

            case 2:
                if (EditVenuePictures.value)
                    VenueDetails();
                break;

            case 3:
                if (EditVenueLocation.value)
                    VenueDetails();
                break;

            case 4:
                Log.e("success", String.valueOf(CriticalActionsActivity.success));
                if (CriticalActionsActivity.success)
                    VenueDetails();
                break;

            case 5:
                if (AddGroundsActivity.value)
                    VenueDetails();
                break;

            case 6:
                if (AminiteisList.value)
                    VenueDetails();
                break;

        }


    }

    private void initializeAllComponents() {
        groundName = (TextView) findViewById(com.groundhog.vendor.android.R.id.ground_details_name);
        groundLocation = (TextView) findViewById(com.groundhog.vendor.android.R.id.venue_details_location);
        groundImage = (ImageView) findViewById(com.groundhog.vendor.android.R.id.venue_details_image);
        groundVerified = (TextView) findViewById(com.groundhog.vendor.android.R.id.venue_details_verified_text);
        gridView = (GridView) findViewById(com.groundhog.vendor.android.R.id.grid_venue_details);
        chipCloud = (ChipCloud) findViewById(com.groundhog.vendor.android.R.id.chip_cloud_detail);
        editPicture = (RelativeLayout) findViewById(com.groundhog.vendor.android.R.id.pictures_relative);
        criticalActionsRelative = (RelativeLayout) findViewById(com.groundhog.vendor.android.R.id.critical_actions_relative);
        groundLocationRelative = (RelativeLayout) findViewById(com.groundhog.vendor.android.R.id.ground_location_relative);
        editDetailsRelative = (RelativeLayout) findViewById(com.groundhog.vendor.android.R.id.edit_details_relative);
        slotManagementRelative = (RelativeLayout) findViewById(com.groundhog.vendor.android.R.id.slot_management_relative);
        viewBookings = (RelativeLayout) findViewById(com.groundhog.vendor.android.R.id.booking_relative);
        editDetailVerifyimg = (ImageView) findViewById(com.groundhog.vendor.android.R.id.edit_details_img);
        editDetailImg = (ImageView) findViewById(com.groundhog.vendor.android.R.id.edit_details_image);
        locationVerifyimg = (ImageView) findViewById(com.groundhog.vendor.android.R.id.ground_location_img);
        locationImg = (ImageView) findViewById(com.groundhog.vendor.android.R.id.ground_location_image);
        pictureVerifyimg = (ImageView) findViewById(com.groundhog.vendor.android.R.id.pictures_img);
        pictureImg = (ImageView) findViewById(com.groundhog.vendor.android.R.id.pictures_image);
        mngVerifyimg = (ImageView) findViewById(com.groundhog.vendor.android.R.id.slot_management_img);
        mngImg = (ImageView) findViewById(com.groundhog.vendor.android.R.id.slot_management_image);
        actionVerifyimg = (ImageView) findViewById(com.groundhog.vendor.android.R.id.critical_actions_img);
        actionsImg = (ImageView) findViewById(com.groundhog.vendor.android.R.id.critical_actions_image);
        locationIcon = (ImageView) findViewById(com.groundhog.vendor.android.R.id.venue_details_location_image);
        amenitiesRelative = (RelativeLayout) findViewById(com.groundhog.vendor.android.R.id.amenities);
        amenitiesVerifyimg = (ImageView) findViewById(com.groundhog.vendor.android.R.id.amenities_img);
        amenitiesImg = (ImageView) findViewById(com.groundhog.vendor.android.R.id.amenities_image);

        bookings = (TextView) findViewById(com.groundhog.vendor.android.R.id.booking_text);
        viewBooking = (TextView) findViewById(com.groundhog.vendor.android.R.id.viewBookingText);
        settings = (TextView) findViewById(com.groundhog.vendor.android.R.id.text_settings);
        editDetails = (TextView) findViewById(com.groundhog.vendor.android.R.id.edit_detail);
        manageGrounds = (TextView) findViewById(com.groundhog.vendor.android.R.id.manage_ground);
        venueLoc = (TextView) findViewById(com.groundhog.vendor.android.R.id.venueLocation);
        amen = (TextView) findViewById(com.groundhog.vendor.android.R.id.amenitiesText);
        picturesText = (TextView) findViewById(com.groundhog.vendor.android.R.id.picture_text);
        critical = (TextView) findViewById(com.groundhog.vendor.android.R.id.critical_actions);
        // progressBar = (ProgressBar)findViewById(R.id.progress_bar);
        // progress_lay = (RelativeLayout)findViewById(R.id.progress_lay);

        groundName.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_SEMIBOLD));
        groundLocation.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        groundVerified.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        bookings.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_SEMIBOLD));
        viewBooking.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        settings.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_SEMIBOLD));
        editDetails.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        venueLoc.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        amen.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        picturesText.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        critical.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        manageGrounds.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                //Toast.makeText(getApplicationContext(),"Back button clicked", Toast.LENGTH_SHORT).show();
                onBackPressed();
                return true;
            default:
                break;

        }
        return false;
    }

}
