package com.groundhog.vendor.android.model;

/**
 * Created by vaishakha on 1/2/17.
 */
public class ReportModel {
    private String status;
    private ReportList success;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ReportList getSuccess() {
        return success;
    }

    public void setSuccess(ReportList success) {
        this.success = success;
    }

}
