package com.groundhog.vendor.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.groundhog.vendor.android.R;
import com.groundhog.vendor.android.utils.Config;
import com.groundhog.vendor.android.utils.PlaythoraUtility;
import com.groundhog.vendor.android.utils.PreferenceManager;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

import static com.groundhog.vendor.android.utils.Config.RALEWAY_REGULAR;

/**
 * Created by Nakul on 9/8/2016.
 */
public class ForgotPasswordActivity  extends AppCompatActivity{

    Button reset;
    EditText resetEmail;
    TextInputLayout emailBoxInputLayout;
    SweetAlertDialog dialog;
    PreferenceManager mPref;
    TextView businessText;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_layout);

        initializeAllComponents();

     /*   dialog  = new SweetAlertDialog(ForgotPasswordActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        dialog.getProgressHelper().setBarColor(getResources().getColor(R.color.colorPrimary));
        dialog.setTitleText("Loading");
        dialog.setCancelable(false);*/

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });

        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(checkForAllValidation()){

                    login();

            }
            }
        });

        resetEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                emailBoxInputLayout.setError(null);

            }
        });


    }

    private void login() {

        if(PlaythoraUtility.checkInternetConnection(getApplicationContext())){
            PlaythoraUtility.showProgressDialog(ForgotPasswordActivity.this);
            AsyncHttpClient httpClient = new AsyncHttpClient();
            RequestParams params= new RequestParams();
            params.add("email",resetEmail.getText().toString());
            Log.e("FORGOT_PASSWORD_PARAMS", resetEmail.getText().toString());
            httpClient.post(Config.FORGOT_PASSWORD, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    PlaythoraUtility.hideProgressDialog();
                    String s = new String(responseBody);
                    Log.e("FORGOT_PASSWORD_SUCCESS",s);
                    Intent login = new Intent(ForgotPasswordActivity.this,LoginActivity.class);
                    startActivity(login);
                    Toast.makeText(ForgotPasswordActivity.this,"Email sent successfully",Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    PlaythoraUtility.hideProgressDialog();
                    String failure = new String(responseBody);
                    Log.e("FORGOT_PASSWORD_FAILURE",failure);
                    JSONObject forgetPasswordFail = null;
                    try {
                        forgetPasswordFail = new JSONObject(failure);
                        if (forgetPasswordFail.has("message")) {
                            Toast.makeText(getApplicationContext(), forgetPasswordFail.getString("message"), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(ForgotPasswordActivity.this, "Some error occured, Please try again later", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });


        }else {
            Toast.makeText(ForgotPasswordActivity.this, "check your internet connection", Toast.LENGTH_LONG).show();
        }

    }

    private boolean checkForAllValidation() {

        if (resetEmail.getText().toString().length() ==0) {
            emailBoxInputLayout.setError("Email address is empty");
            emailBoxInputLayout.setErrorEnabled(true);
        }else {
            emailBoxInputLayout.setErrorEnabled(false);
        }

        if(!resetEmail.getText().toString().isEmpty()){
            if(!android.util.Patterns.EMAIL_ADDRESS.matcher(resetEmail.getText().toString()).matches()){
                emailBoxInputLayout.setError("Please provide valid email address");
                emailBoxInputLayout.setErrorEnabled(true);
            }else {
                emailBoxInputLayout.setErrorEnabled(false);
            }

        }

        if(emailBoxInputLayout.isErrorEnabled()){
            return false;
        }else {
            return true;

        }
    }

    private void initializeAllComponents() {
        mPref = PreferenceManager.instance(getApplicationContext());
        reset = (Button) findViewById(R.id.reset_btn);
        resetEmail = (EditText) findViewById(R.id.forgotEmailId);
        emailBoxInputLayout = (TextInputLayout) findViewById(R.id.input_layout_forgot_email);
        forgotUI(findViewById(R.id.forgot_activity));
        businessText = (TextView) findViewById(R.id.business_text);

        reset.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        resetEmail.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        emailBoxInputLayout.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        businessText.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
    }

    private void forgotUI(View viewById) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(viewById instanceof EditText)) {
            viewById.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    PlaythoraUtility.hideSoftKeyboard(ForgotPasswordActivity.this);
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (viewById instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) viewById).getChildCount(); i++) {
                View innerView = ((ViewGroup) viewById).getChildAt(i);
                forgotUI(innerView);
            }
        }
    }
}
