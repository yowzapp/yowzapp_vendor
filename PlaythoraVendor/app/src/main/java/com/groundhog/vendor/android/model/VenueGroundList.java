package com.groundhog.vendor.android.model;

public class VenueGroundList {
    private int id;
    private String name;
    private String start_time;
    private String end_time;
    private int slot_duration;
    private int max_member;
    private boolean is_disabled;
    private int sports_id;
    private String sports_name;
    private String slot_price;

    public String getSports_image() {
        return sports_image;
    }

    public void setSports_image(String sports_image) {
        this.sports_image = sports_image;
    }

    private String sports_image;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public int getSlot_duration() {
        return slot_duration;
    }

    public void setSlot_duration(int slot_duration) {
        this.slot_duration = slot_duration;
    }

    public int getMax_member() {
        return max_member;
    }

    public void setMax_member(int max_member) {
        this.max_member = max_member;
    }

    public boolean is_disabled() {
        return is_disabled;
    }

    public void setIs_disabled(boolean is_disabled) {
        this.is_disabled = is_disabled;
    }

    public int getSports_id() {
        return sports_id;
    }

    public void setSports_id(int sports_id) {
        this.sports_id = sports_id;
    }

    public String getSports_name() {
        return sports_name;
    }

    public void setSports_name(String sports_name) {
        this.sports_name = sports_name;
    }

    public String getSlot_price() {
        return slot_price;
    }

    public void setSlot_price(String slot_price) {
        this.slot_price = slot_price;
    }
}
