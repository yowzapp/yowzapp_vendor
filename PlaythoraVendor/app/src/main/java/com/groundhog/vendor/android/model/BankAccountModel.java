package com.groundhog.vendor.android.model;

import java.util.List;

/**
 * Created by Nakul on 9/14/2016.
 */
public class BankAccountModel {

    private String status;
    private String message;
    private List<BankAccountSuccess> success;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<BankAccountSuccess> getSuccess() {
        return success;
    }

    public void setSuccess(List<BankAccountSuccess> success) {
        this.success = success;
    }
}
