package com.groundhog.vendor.android.adapter;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.groundhog.vendor.android.model.SportsSuccessList;
import com.groundhog.vendor.android.utils.PlaythoraUtility;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import static com.groundhog.vendor.android.utils.Config.RALEWAY_REGULAR;

/**
 * Created by vaishakha on 20/12/16.
 */
public class SportsAdapter extends RecyclerView.Adapter<SportsAdapter.SportsHolder>{
    public static String sportName="",sportId="", sportImage="";
    int Id;
    ArrayList<SportsSuccessList> sports;
    private boolean[] mSelectedSports;
    Activity context;
    public SportsAdapter(Activity context, ArrayList<SportsSuccessList> sports, int id) {
        this.context = context;
        this.sports = sports;
        Id = id;
        mSelectedSports = new boolean[this.sports.size()];
        sportName="";
        sportId="";
        Log.e("LEGTH", String.valueOf(this.sports.size()));
        for(int i=0 ; i<this.sports.size();i++){
            Log.e("IDDDDDDD",Id + " & " + this.sports.get(i).getId());
            if(Id == this.sports.get(i).getId()) {
                Log.e("SPORTNAME",this.sports.get(i).getName());
                mSelectedSports[i] = true;
                sportName = this.sports.get(i).getName();
            }
            else mSelectedSports[i]=false;
        }

    }

    @Override
    public SportsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View contentView = LayoutInflater.from(parent.getContext()).inflate(com.groundhog.vendor.android.R.layout.sports_type_item,null);
        return new SportsHolder(contentView);
    }

    @Override
    public void onBindViewHolder(final SportsHolder holder, final int position) {
        holder.sportsText.setText(sports.get(position).getName());
        holder.sports_text.setText(sports.get(position).getName());
        if(null!=sports.get(position).getThumbnail() && !sports.get(position).getThumbnail().isEmpty()){
            Picasso.with(context).load(sports.get(position).getThumbnail()).fit().centerInside().config(Bitmap.Config.RGB_565).into(holder.SportImg);
        }
        if(mSelectedSports[position]){
            Picasso.with(context).load(com.groundhog.vendor.android.R.drawable.check).fit().centerInside().config(Bitmap.Config.RGB_565).into(holder.SportImg);
            sportName = sports.get(position).getName();
            sportId = String.valueOf(sports.get(position).getId());
            sportImage = sports.get(position).getThumbnail();
            Log.e("sportName",sportName + " " + sportId);
            holder.sportsText.setBackgroundResource(com.groundhog.vendor.android.R.drawable.dark_back);
            holder.sportsText.setTextColor(Color.WHITE);
            holder.sports_text.setTextColor(Color.BLACK);

        }else {
            holder.sportsText.setTextColor(Color.parseColor("#979797"));
            holder.sports_text.setTextColor(Color.parseColor("#979797"));
            holder.sportsText.setBackgroundResource(com.groundhog.vendor.android.R.drawable.white_oval_back);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PlaythoraUtility.hideSoftKeyboard(context);
                Log.e("LEGTH", String.valueOf(position));
                for (int i = 0;i<sports.size();i++){
                    mSelectedSports[i]=false;
                    Log.e("LEGTH", String.valueOf(mSelectedSports[i]));
                }
                mSelectedSports[position]=true;
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return sports.size();
    }

    public class SportsHolder extends RecyclerView.ViewHolder{
        TextView sportsText,sports_text;
        ImageView SportImg;
        public SportsHolder(View itemView) {
            super(itemView);
            sportsText = (TextView) itemView.findViewById(com.groundhog.vendor.android.R.id.text_games);
            sports_text = (TextView) itemView.findViewById(com.groundhog.vendor.android.R.id.sports_text);
            SportImg = (ImageView) itemView.findViewById(com.groundhog.vendor.android.R.id.sports_img);

            sportsText.setTypeface(PlaythoraUtility.getFont(context,RALEWAY_REGULAR));
            sports_text.setTypeface(PlaythoraUtility.getFont(context,RALEWAY_REGULAR));
        }
    }
}