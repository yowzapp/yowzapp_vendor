package com.groundhog.vendor.android.model;

/**
 * Created by Nakul on 9/15/2016.
 */
public class UsersModel {

    private  String userName;
    private  String id;
    private  String role;
    private  String image;
    private  String mobNumber;
    private  String email;
    private  String venue;

    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public String getMobNumber() {
        return mobNumber;
    }

    public void setMobNumber(String mobNumber) {
        this.mobNumber = mobNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public UsersModel(String userName, String id, String role, String image, String mobNumber, String email,String venue) {
        this.userName = userName;
        this.id = id;
        this.role = role;
        this.image = image;
        this.mobNumber = mobNumber;
        this.email = email;
        this.venue = venue;
    }



    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
