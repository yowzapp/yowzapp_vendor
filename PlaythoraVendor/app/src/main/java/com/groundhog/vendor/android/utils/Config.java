package com.groundhog.vendor.android.utils;

/**
 * Created by hemanth on 1/12/16.
 */

public class Config {

//    public static final String PLAYTHORA_VENDOR_BASEURL = "http://54.202.83.129/api";
    public static final String PLAYTHORA_VENDOR_BASEURL = "https://dev.yowzapp.co/api";
   // public static final String PLAYTHORA_VENDOR_BASEURL = "http://192.168.1.17:9000/api";

    //new server



    public static final String LOGIN = PLAYTHORA_VENDOR_BASEURL+"/login?type=vendor";
    public static final String LOGOUT = PLAYTHORA_VENDOR_BASEURL+"/logout";
    public static final String FORGOT_PASSWORD = PLAYTHORA_VENDOR_BASEURL+"/forgotpassword";

    public static final String RESET_PASSWORD = PLAYTHORA_VENDOR_BASEURL+"/resetPassword";
    public static final String ADD_BANK_ACCOUNT = PLAYTHORA_VENDOR_BASEURL+"/vendor/addBankAccount";
    public static final String BANK_ACCOUNT_LIST = PLAYTHORA_VENDOR_BASEURL+"/vendor/bankAccountList";
    public static final String BANK_ACCOUNT_DETAIL = PLAYTHORA_VENDOR_BASEURL+"/vendor/bankAccountDetails";
    public static final String EDIT_BANK_ACCOUNT = PLAYTHORA_VENDOR_BASEURL+"/vendor/editBankAccount";
    public static final String DELETE_BANK_ACCOUNT = PLAYTHORA_VENDOR_BASEURL+"/vendor/deleteBankAccount";

    public static final String EMAIL_EXISTS = PLAYTHORA_VENDOR_BASEURL+"/emailExits?email=";
    public static final String VENDOR_REGISTER = PLAYTHORA_VENDOR_BASEURL+"/vendorRegister";
    public static final String MOBILE_VERIFY = PLAYTHORA_VENDOR_BASEURL+"/mobileVerification";
    public static final String RESEND_OTP = PLAYTHORA_VENDOR_BASEURL+"/resendOtp";
    public static final String OTP_VERIFY = PLAYTHORA_VENDOR_BASEURL+"/otpVerification";
    public static final String USER_DETAIL = PLAYTHORA_VENDOR_BASEURL+"/getUser";
    public static final String EDIT_USER = PLAYTHORA_VENDOR_BASEURL+"/editUser";
    public static final String SPORTS_LIST = PLAYTHORA_VENDOR_BASEURL+"/sportsList";
    public static final String CREATE_VENUE = PLAYTHORA_VENDOR_BASEURL+"/vendor/createVenue";
    public static final String UPLOAD_GROUND_IMAGE = PLAYTHORA_VENDOR_BASEURL+"/vendor/uploadImage";
    public static final String VENUE_LIST = PLAYTHORA_VENDOR_BASEURL+"/vendor/venuesList";
    public static final String VENUE_DETAILS = PLAYTHORA_VENDOR_BASEURL+"/vendor/venueDetails";
    public static final String EDIT_VENUE = PLAYTHORA_VENDOR_BASEURL+"/vendor/editVenue";
    public static final String ADD_VENUE_IMAGE = PLAYTHORA_VENDOR_BASEURL+"/vendor/addVenueImage";
    public static final String DELETE_VENUE_IMAGE = PLAYTHORA_VENDOR_BASEURL+"/vendor/deleteVenueImage";
    public static final String DELETE_VENUE = PLAYTHORA_VENDOR_BASEURL+"/vendor/deleteVenue";
    public static final String ADD_GROUND = PLAYTHORA_VENDOR_BASEURL+"/vendor/addGround";
    public static final String EDIT_GROUND = PLAYTHORA_VENDOR_BASEURL+"/vendor/editGround";
    public static final String DELETE_GROUND = PLAYTHORA_VENDOR_BASEURL+"/vendor/deleteGround";
    public static final String NEW_BOOK = PLAYTHORA_VENDOR_BASEURL+"/vendor/newBooking";
    public static final String VENUE_GROUND = PLAYTHORA_VENDOR_BASEURL+"/vendor/venue/grounds";
    public static final String VENUE_GROUND_DETAIL = PLAYTHORA_VENDOR_BASEURL+"/player/groundDetails";
    public static final String GROUND_BOOK = PLAYTHORA_VENDOR_BASEURL+"/vendor/venue/book";
    public static final String LATEST_BOOKING = PLAYTHORA_VENDOR_BASEURL+"/vendor/bookings/latest";
    public static final String BOOKIND_SORT = PLAYTHORA_VENDOR_BASEURL+"/vendor/slots/all";
    public static final String BOOKING_DETAIL = PLAYTHORA_VENDOR_BASEURL+"/vendor/slot/detail";
    public static final String CANCEL_BOOKING = PLAYTHORA_VENDOR_BASEURL+"/vendor/slot/cancel";
    public static final String NOTIFICATION_LIST = PLAYTHORA_VENDOR_BASEURL + "/player/notificationList";
    public static final String GET_USER = PLAYTHORA_VENDOR_BASEURL+"/getUser";
    public static final String NOTIFY_READ = PLAYTHORA_VENDOR_BASEURL+"/player/notificationRead";
    public static final String REPORTS = PLAYTHORA_VENDOR_BASEURL+"/vendor/reports";


    public static final String DAYS_RANGE = PLAYTHORA_VENDOR_BASEURL+"/vendor/ground/dayRange/";
    public static final Object DAYS_RANGE_DETAIL =PLAYTHORA_VENDOR_BASEURL + "/vendor/ground/dayRangeDetail/";
    public static final String DAYS_RANGE_SAVE =PLAYTHORA_VENDOR_BASEURL + "/vendor/ground/dayRangeSave/";

    public static final String RALEWAY_REGULAR = "fonts/Raleway-Regular.ttf";
    public static final String RALEWAY_BOLD = "fonts/Raleway-Bold.ttf";
    public static final String RALEWAY_SEMIBOLD = "fonts/Raleway-SemiBold.ttf";

}
