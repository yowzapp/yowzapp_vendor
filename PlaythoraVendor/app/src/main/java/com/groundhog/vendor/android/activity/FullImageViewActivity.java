package com.groundhog.vendor.android.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.groundhog.vendor.android.utils.TouchImageView;
import com.squareup.picasso.Picasso;

import java.io.IOException;

/**
 * Created by vaishakha on 16/9/16.
 */
public class FullImageViewActivity extends AppCompatActivity {
    String imagePath,ImageCode;
    TouchImageView touchImageView;
    ImageView cancle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.groundhog.vendor.android.R.layout.full_image_layout);
//        touchImageView = (TouchImageView) findViewById(R.id.fitImageFull);
        cancle = (ImageView) findViewById(com.groundhog.vendor.android.R.id.cancle_img);

        Intent intent = getIntent();
        imagePath = intent.getStringExtra("ImagePath");
        ImageCode = intent.getStringExtra("ImageCode");

        cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        try {
            if(ImageCode.equalsIgnoreCase("2"))
            {
                try {
                    ExifInterface exif = new ExifInterface(imagePath);
                    int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
                    int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                    Log.e("ROTATE",orientation+"");
                    Bitmap bm;
                    int rotation=0;
                    if(rotation==0){
                        if(orientation==6){
                            rotation=90;
                        }
                        if(orientation==3){
                            rotation=180;
                        }
                        if(orientation==8){
                            rotation=270;
                        }
                        if(orientation==4){
                            rotation=180;
                        }

                    }
                    Log.e("ooooooo", String.valueOf(currentapiVersion));
                    Matrix matrix = new Matrix();
                    matrix.postRotate(rotation);
                    bm = Bitmap.createScaledBitmap(BitmapFactory.decodeFile(imagePath), 100, 100, true);
                    bm = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), matrix, true);
                    touchImageView.setImageBitmap(bm);


                } catch (IOException e) {
                    e.printStackTrace();
                }catch (OutOfMemoryError e){
                    e.printStackTrace();
                } catch (NullPointerException e){
                    Log.e("MMMMMMMMMMMMM", "Null");
                    Toast.makeText(getApplicationContext(),"Couldnt fetch the image try again.", Toast.LENGTH_SHORT).show();

                }catch (IllegalArgumentException e){
                    Toast.makeText(getApplicationContext(),"Couldnt fetch the image try again.", Toast.LENGTH_SHORT).show();

                }
            } else Picasso.with(FullImageViewActivity.this).load(imagePath).into(touchImageView);
        }catch (Exception e){
            e.printStackTrace();
        }

    }
}
