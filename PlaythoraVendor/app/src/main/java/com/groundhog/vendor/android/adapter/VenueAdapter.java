package com.groundhog.vendor.android.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.groundhog.vendor.android.model.VenueModel;

import java.util.ArrayList;

/**
 * Created by Nakul on 9/21/2016.
 */
public class VenueAdapter  extends BaseAdapter{
    
    private Context mContext;
    ArrayList<VenueModel> venueModelsArrayList ;
    private LayoutInflater inflater=null;
    private boolean[] isCheck;
    boolean b=false;
    String check;



    public VenueAdapter(Context context, ArrayList<VenueModel> venueModelsArrayList, String venueValue) {
        
        this.mContext = context;
        this.venueModelsArrayList = venueModelsArrayList;
        isCheck = new boolean[venueModelsArrayList.size()];
        check=venueValue;

      /*  for (int i=0;i<venueModelsArrayList.size();i++){
            isCheck[i]=false;
        }*/

    }

    @Override
    public int getCount() {
        return venueModelsArrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }
    
    public class Holder{
        TextView venueText;
        ImageView imgVenue;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        View row = view;
        Holder holder=new Holder();

        if(row==null){
            inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(com.groundhog.vendor.android.R.layout.venue_dialog_item_layout, null);

            holder.venueText = (TextView) row.findViewById(com.groundhog.vendor.android.R.id.dialogVenueText);
            holder.imgVenue = (ImageView) row.findViewById(com.groundhog.vendor.android.R.id.venueImage);
            final Holder finalHolder = holder;

            row.setTag(holder);

        }else {
            holder = (Holder) row.getTag();
        }

        holder.venueText.setText(venueModelsArrayList.get(i).getGroundName());

        if(check.equals(venueModelsArrayList.get(i).getGroundName())){
            holder.imgVenue.setVisibility(View.VISIBLE);
        }else {
            holder.imgVenue.setVisibility(View.GONE);
        }

       /* if(isCheck[i]){
            holder.imgVenue.setVisibility(View.VISIBLE);
        }else {
            holder.imgVenue.setVisibility(View.GONE);
        }*/

       /* holder.venueText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!isCheck[i]){
                    Log.v("isCheck", String.valueOf(isCheck[i]));
                    for(int j=0;j<venueModelsArrayList.size();j++){
                        isCheck[j]=false;
                    } isCheck[i]=true;
                    notifyDataSetChanged();
                }

            }
        });*/

        return row;
    }
}
