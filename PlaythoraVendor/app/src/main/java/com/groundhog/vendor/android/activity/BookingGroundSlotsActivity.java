package com.groundhog.vendor.android.activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.astuetz.PagerSlidingTabStrip;
import com.google.gson.Gson;
import com.groundhog.vendor.android.R;
import com.groundhog.vendor.android.fragment.BookedSlotsFragment;
import com.groundhog.vendor.android.fragment.FreeSlotsFragment;
import com.groundhog.vendor.android.model.BookingSlots;
import com.groundhog.vendor.android.model.VenueGroundList;
import com.groundhog.vendor.android.model.VenueListItems;
import com.groundhog.vendor.android.utils.Config;
import com.groundhog.vendor.android.utils.PlaythoraUtility;
import com.groundhog.vendor.android.utils.PreferenceManager;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import cz.msebera.android.httpclient.Header;

import static com.groundhog.vendor.android.utils.Config.RALEWAY_REGULAR;
import static com.groundhog.vendor.android.utils.Config.RALEWAY_SEMIBOLD;

/**
 * Created by hemanth on 29/9/16.
 */

public class BookingGroundSlotsActivity extends AppCompatActivity {

    PagerSlidingTabStrip tabStrip;
    MyPagerAdapter myPagerAdapter;
    ViewPager viewPager;
    Toolbar toolbar;
    LinearLayout mTabsLinearLayout;
    ImageView previous, next;
    TextView todayText, toolbarTitle, mGround, noVenue;
    RelativeLayout todayCalender;
    String toDate, date;
    Calendar calendar;
    int year, month, day;
    Dialog dialog;
    VenueAdapter adapter;
    ListView dialogListView, groundListview;
    ProgressBar dialogProgressBar, groundProgressBar;
    ArrayList<VenueListItems> venueListItems;
    ArrayList<VenueGroundList> venueGroundItems;
    int venueId, groundId;
    PreferenceManager mPref;
    Gson gson;
    VenueGroundAdapter groundAdapter;
    String venueName, mGroundName, mDate = "", rawDate = "";
    BookingSlots bookingSlots;
    BookedSlotsFragment bookedSlotsFragment;
    FreeSlotsFragment freeSlotsFragment;
    ProgressBar progressBar;
    int slotId;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.booking_ground_slots_layout);

        initializeAllComponents();

        mPref = PreferenceManager.instance(this);

        venueName = "Select Venue";
        mGroundName = "Select Ground";
        todayText.setText("Today");

        updateTodayDate();

        progressBar = (ProgressBar) findViewById(com.groundhog.vendor.android.R.id.book_progress_bar);
        toolbar = (Toolbar) findViewById(com.groundhog.vendor.android.R.id.bookingGroundSlotsToolbar);
        toolbarTitle = (TextView) findViewById(com.groundhog.vendor.android.R.id.toolbar_title);
        mGround = (TextView) findViewById(com.groundhog.vendor.android.R.id.ground_drop_down);
        toolbarTitle.setVisibility(View.VISIBLE);

        toolbarTitle.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        mGround.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));

        toolbar.setNavigationIcon(com.groundhog.vendor.android.R.drawable.ic_back_arrow);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        //getSupportActionBar().setTitle(Html.fromHtml("<font color=\"white\"><big>" + "Select Venue" + "</big></font>"));
        //getSupportActionBar().setSubtitle(Html.fromHtml("<font color='#ffffff'><small>" + "sachin" + "</small></font>"));


        if (getIntent().hasExtra("VENUENAME")) {
            venueName = getIntent().getStringExtra("VENUENAME");
            mGroundName = getIntent().getStringExtra("GROUNDNAME");
            venueId = getIntent().getIntExtra("VENUEID", 0);
            groundId = getIntent().getIntExtra("GROUNDID", 0);
            mDate = getIntent().getStringExtra("DATE");
            String[] split = mDate.split("-");
            todayText.setText(split[2] + "/" + split[1] + "/" + split[0]);
            loadBookingDetail();
        }

        Log.e("venueName", venueName);
        Log.e("mGroundName", mGroundName);
        Log.e("venueId", venueId + "");
        Log.e("groundId", groundId + "");
        Log.e("mDate", mDate + "");

        //2017-01-25
        toolbarTitle.setText(venueName);
        mGround.setText(mGroundName);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);

        todayCalender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar myCalendar = Calendar.getInstance();
                DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        myCalendar.set(Calendar.YEAR, year);
                        myCalendar.set(Calendar.MONTH, monthOfYear);
                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                        Log.e("selectedYear", String.valueOf(year));
                        Log.e("selectedMonth", String.valueOf(monthOfYear));
                        Log.e("selectedDate", String.valueOf(dayOfMonth));

                        String tempCheckDate;
                        if ((monthOfYear + 1) < 10) {
//                                toDate = dayOfMonth + "/" + "0" + (monthOfYear + 1) + "/" + year;
//                                mDate = year + "-" + "0" + (monthOfYear + 1) + "-" + dayOfMonth;
                            if (dayOfMonth < 10) {
                                toDate = "0" + dayOfMonth + "/" + "0" + (monthOfYear + 1) + "/" + year;
                                mDate = year + "-" + "0" + (monthOfYear + 1) + "-" + "0" + dayOfMonth;
                                tempCheckDate = year + "-" + "0" + (monthOfYear + 1) + "-" + "0" + (dayOfMonth + 1);
                            } else {
                                toDate = dayOfMonth + "/" + "0" + (monthOfYear + 1) + "/" + year;
                                mDate = year + "-" + "0" + (monthOfYear + 1) + "-" + dayOfMonth;
                                tempCheckDate = year + "-" + "0" + (monthOfYear + 1) + "-" + (dayOfMonth + 1);
                            }
                        } else {
//                                toDate = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
//                                mDate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                            if (dayOfMonth < 10) {
                                toDate = "0" + dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
                                mDate = year + "-" + (monthOfYear + 1) + "-" + "0" + dayOfMonth;
                                tempCheckDate = year + "-" + (monthOfYear + 1) + "-" + "0" + (dayOfMonth + 1);
                            } else {
                                toDate = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
                                mDate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                                tempCheckDate = year + "-" + (monthOfYear + 1) + "-" + (dayOfMonth + 1);
                            }
                        }

                        Log.e("toDate", toDate);
                        Log.e("mDate", mDate);
                        if (checkDate(tempCheckDate)) {
                            todayText.setText(toDate);
                            String[] rawDate = toDate.split("/");
                            mDate = rawDate[2] + "-" + rawDate[1] + "-" + rawDate[0];

                            Log.e("mDate", mDate);

                            if (groundId != 0) {
                                loadBookingDetail();
                            }
                        } else {
                            Toast.makeText(BookingGroundSlotsActivity.this, "Booking details are only available for upcoming days", Toast.LENGTH_SHORT).show();
                        }
                    }
                };

                DatePickerDialog dpDialog = new DatePickerDialog(BookingGroundSlotsActivity.this, com.groundhog.vendor.android.R.style.DialogTheme, listener, year, month, day);
                dpDialog.show();

            }
        });

        toolbarTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showVenueList(venueId);
            }
        });

        mGround.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (venueId != 0)
                    showGroundList(groundId);
                else
                    Toast.makeText(getApplicationContext(), "Please select venue", Toast.LENGTH_SHORT).show();
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String dt = todayText.getText().toString();
                Calendar cal = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
                if (dt.equalsIgnoreCase("today")) {
                    dt = df.format(cal.getTime());

                }

                String formattedDate;

                try {
                    cal.setTime(df.parse(dt));
                    cal.add(Calendar.DATE, 1);
                    formattedDate = df.format(cal.getTime());

                    Log.v("NEXT DATE : ", formattedDate);
                    todayText.setText(formattedDate);
                    String[] rawDate = formattedDate.split("/");
                    mDate = rawDate[2] + "-" + rawDate[1] + "-" + rawDate[0];
                    if (groundId != 0) {
                        loadBookingDetail();
                    }

                } catch (ParseException e) {
                    e.printStackTrace();
                }


            }
        });


        previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String dt1 = todayText.getText().toString();
                Calendar calend = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");

                if (dt1.equalsIgnoreCase("today")) {
                    dt1 = df.format(calend.getTime());
                }

                String formattedDate = df.format(calendar.getTime());
                try {
                    calend.setTime(df.parse(dt1));
                    calend.add(Calendar.DATE, -1);
                    formattedDate = df.format(calend.getTime());

                    Log.v("NEXT DATE : ", formattedDate);
                    todayText.setText(formattedDate);
                    String[] rawDate = formattedDate.split("/");
                    mDate = rawDate[2] + "-" + rawDate[1] + "-" + rawDate[0];

                    if (groundId != 0) {
                        loadBookingDetail();
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }


            }
        });

        myPagerAdapter = new MyPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(myPagerAdapter);

        float density = BookingGroundSlotsActivity.this.getResources().getDisplayMetrics().density;
        float px = 14 * density;
        int dp = (int) px;
        tabStrip.setTextSize(dp);
        tabStrip.setTypeface(null, Typeface.NORMAL);
        tabStrip.getChildAt(0).getLayoutParams().width = 200;
        tabStrip.setTextColor(getResources().getColor(com.groundhog.vendor.android.R.color.black));
        tabStrip.setIndicatorColor(getResources().getColor(com.groundhog.vendor.android.R.color.white));
        tabStrip.setIndicatorHeight(0);

        tabStrip.setDividerColor(R.color.white);
//        tabStrip.setAllCaps(true);
        tabStrip.setShouldExpand(true);


        tabStrip.setViewPager(viewPager);
        final int pageMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources().getDisplayMetrics());
        viewPager.setPageMargin(pageMargin);
        setUpTabStrip();

        tabStrip.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {


            }

            @Override
            public void onPageSelected(int position) {
                for (int i = 0; i < mTabsLinearLayout.getChildCount(); i++) {
                    TextView tv = (TextView) mTabsLinearLayout.getChildAt(i);
                    if (i == position) {
                        tv.setTextColor(ContextCompat.getColor(getApplicationContext(), com.groundhog.vendor.android.R.color.white));
                        tv.setBackgroundResource(com.groundhog.vendor.android.R.drawable.profile_background);

                    } else {
                        tv.setTextColor(ContextCompat.getColor(getApplicationContext(), com.groundhog.vendor.android.R.color.text_one));
                        tv.setBackgroundResource(com.groundhog.vendor.android.R.drawable.booking_slots_deselected_background);

                    }
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private boolean checkDate(String selectedDate) {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String currentDate = sdf.format(c.getTime());
//        String getMyTime2 = sdf.format(getMyTime);

        Log.d("currentDate", currentDate);
        Log.d("selectedDate", selectedDate);

        // getCurrentDateTime: 05/23/2016 18:49 PM

        if (currentDate.compareTo(selectedDate) < 0) {
            Log.e("Return", "Today date is less than Slots date");
            return true;
        } else {
            Log.d("Return", "Today date is more than Slots date");
            return false;
        }
    }

    private void updateTodayDate() {
        String dt = todayText.getText().toString();
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        if (dt.equalsIgnoreCase("today")) {
            dt = df.format(cal.getTime());

        }

        String formattedDate;

        try {
            cal.setTime(df.parse(dt));
            cal.add(Calendar.DATE, 0);
            formattedDate = df.format(cal.getTime());

            Log.v("NEXT DATE : ", formattedDate);
            todayText.setText(formattedDate);
            String[] rawDate = formattedDate.split("/");
            mDate = rawDate[2] + "-" + rawDate[1] + "-" + rawDate[0];
//            if(groundId!=0){
//                loadBookingDetail();
//            }

        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    private void loadBookingDetail() {

        if (PlaythoraUtility.checkInternetConnection(BookingGroundSlotsActivity.this)) {
            try {
                progressBar.setVisibility(View.VISIBLE);
                AsyncHttpClient mHttpClient = new AsyncHttpClient();
                mHttpClient.addHeader("accessToken", mPref.getAccessToken());
                Log.e("ACCESSTOKEN", mPref.getAccessToken());
                RequestParams params = new RequestParams();
                params.add("ground_id", String.valueOf(groundId));
                params.add("date", String.valueOf(mDate));
                Log.e("PARAMS", params + "");

                mHttpClient.post(Config.BOOKIND_SORT, params,
                        new AsyncHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                progressBar.setVisibility(View.GONE);
                                try {
                                    String s = new String(responseBody);
                                    Log.e("RESPONSE_BOOKING", s);
                                    JSONObject object = new JSONObject(s);
                                    if (object.getString("status").equalsIgnoreCase("success")) {
                                        slotId = object.getJSONObject("success").getInt("slot_id");
                                        Log.e("slot_id", slotId + "");
                                        bookingSlots = new BookingSlots();
                                        gson = new Gson();
                                        bookingSlots = gson.fromJson(s, BookingSlots.class);
                                        freeSlotsFragment.loadSlots(bookingSlots.getSuccess().getFree_slots(), toolbarTitle.getText().toString(), venueId, mGround.getText().toString(), groundId, todayText.getText().toString(), slotId);
                                        bookedSlotsFragment.loadBookedSlots(bookingSlots.getSuccess().getBooked_slots(), venueName, mGroundName);


                                        //Checking Back button visibility
                                        if (checkDate(String.valueOf(mDate))) {
                                            previous.setVisibility(View.VISIBLE);
                                        } else {
                                            previous.setVisibility(View.INVISIBLE);
                                        }

                                   /* if(object.getJSONArray("success").length()!=0){
                                        if(object.get("success") instanceof JSONArray) {
                                            *//*JSONArray jsonArray = object.getJSONArray("success");
                                            venueGroundItems = new ArrayList<>();
                                            gson = new Gson();
                                            for(int i = 0;i<jsonArray.length();i++){
                                                venueGroundItems.add(gson.fromJson(jsonArray.get(i).toString(),VenueGroundList.class));
                                            }
                                            groundAdapter = new VenueGroundAdapter(BookingGroundSlotsActivity.this,venueGroundItems);
                                            groundListview.setAdapter(groundAdapter);*//*
                                        }
                                    }*/
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                                progressBar.setVisibility(View.GONE);
                                try {
                                    String s = new String(bytes);
                                    Log.e("RESPONSE_FAIL", s);
                                    JSONObject object = new JSONObject(s);
                                    if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                                        Toast.makeText(BookingGroundSlotsActivity.this, object.getString("message"), Toast.LENGTH_SHORT).show();
                                        mPref.setAccessToken("");
                                        Intent intent = new Intent(BookingGroundSlotsActivity.this, LoginActivity.class);
                                        startActivity(intent);
                                        finish();
                                    } else
                                        Toast.makeText(BookingGroundSlotsActivity.this, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    // Toast.makeText(getActivity(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
                progressBar.setVisibility(View.GONE);
            }
        } else {
            Toast.makeText(BookingGroundSlotsActivity.this, "Not connected to internet", Toast.LENGTH_LONG).show();
        }


    }

    private void showGroundList(int groundId) {


        final Dialog dialog = new Dialog(BookingGroundSlotsActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(com.groundhog.vendor.android.R.layout.venue_list_dialog);

        TextView title = (TextView) dialog.findViewById(com.groundhog.vendor.android.R.id.venue_text);
        title.setText("Select ground");

        title.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_SEMIBOLD));
        groundListview = (ListView) dialog.findViewById(com.groundhog.vendor.android.R.id.venue_list);
        groundProgressBar = (ProgressBar) dialog.findViewById(com.groundhog.vendor.android.R.id.dialog_progress);

        LoadGround(groundId);

        groundListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mGround.setText(venueGroundItems.get(position).getName());
                BookingGroundSlotsActivity.this.groundId = venueGroundItems.get(position).getId();
                //bDate.setText("");
                //bSlots.setText("");
                //startArrayList = new ArrayList<>();
                //endArrayList = new ArrayList<>();

                loadBookingDetail();

                dialog.dismiss();
            }
        });

        dialog.show();


    }

    private void LoadGround(final int ground_id) {
        if (PlaythoraUtility.checkInternetConnection(BookingGroundSlotsActivity.this)) {
            groundProgressBar.setVisibility(View.VISIBLE);
            AsyncHttpClient mHttpClient = new AsyncHttpClient();
            mHttpClient.addHeader("accessToken", mPref.getAccessToken());
            Log.e("ACCESSTOKEN", mPref.getAccessToken());
            RequestParams params = new RequestParams();
            params.add("venue_id", String.valueOf(venueId));
            mHttpClient.post(Config.VENUE_GROUND, params,
                    new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                            groundProgressBar.setVisibility(View.GONE);
                            try {
                                String s = new String(responseBody);
                                Log.e("RESPONSE", s);
                                JSONObject object = new JSONObject(s);
                                if (object.getString("status").equalsIgnoreCase("success")) {
                                    if (object.getJSONArray("success").length() != 0) {
                                        if (object.get("success") instanceof JSONArray) {
                                            JSONArray jsonArray = object.getJSONArray("success");
                                            venueGroundItems = new ArrayList<>();
                                            gson = new Gson();
                                            for (int i = 0; i < jsonArray.length(); i++) {
                                                venueGroundItems.add(gson.fromJson(jsonArray.get(i).toString(), VenueGroundList.class));
                                            }
                                            groundAdapter = new VenueGroundAdapter(BookingGroundSlotsActivity.this, venueGroundItems, ground_id);
                                            groundListview.setAdapter(groundAdapter);
                                        }
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                            groundProgressBar.setVisibility(View.GONE);
                            try {
                                String s = new String(bytes);
                                Log.e("RESPONSE_FAIL", s);
                                JSONObject object = new JSONObject(s);
                                if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                                    Toast.makeText(BookingGroundSlotsActivity.this, object.getString("message"), Toast.LENGTH_SHORT).show();
                                    mPref.setAccessToken("");
                                    Intent intent = new Intent(BookingGroundSlotsActivity.this, LoginActivity.class);
                                    startActivity(intent);
                                    finish();
                                } else
                                    Toast.makeText(BookingGroundSlotsActivity.this, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                            } catch (Exception e) {
                                e.printStackTrace();
                                // Toast.makeText(getActivity(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        } else {
            Toast.makeText(BookingGroundSlotsActivity.this, "Not connected to internet", Toast.LENGTH_LONG).show();
        }

    }

    private void showVenueList(int venue_Id) {
        dialog = new Dialog(BookingGroundSlotsActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(com.groundhog.vendor.android.R.layout.venue_list_dialog);

        dialogListView = (ListView) dialog.findViewById(com.groundhog.vendor.android.R.id.venue_list);
        dialogProgressBar = (ProgressBar) dialog.findViewById(com.groundhog.vendor.android.R.id.dialog_progress);
        noVenue = (TextView) dialog.findViewById(R.id.no_venue);

        TextView textView = (TextView) dialog.findViewById(com.groundhog.vendor.android.R.id.venue_text);
        textView.setText("Select Venue");
        textView.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_SEMIBOLD));
        noVenue.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_SEMIBOLD));


        loadVenueList();

        dialogListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                try {
                    toolbarTitle.setText(venueListItems.get(position).getName());
                    BookingGroundSlotsActivity.this.venueId = venueListItems.get(position).getId();
                    mGround.setText("Select ground");

                    groundId = 0;
                    //bGround.setText("");
                    //bDate.setText("");
                    //bSlots.setText("");
                    //startArrayList = new ArrayList<>();
                    //endArrayList = new ArrayList<>();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void loadVenueList() {
        if (PlaythoraUtility.checkInternetConnection(BookingGroundSlotsActivity.this)) {
            try {
                dialogProgressBar.setVisibility(View.VISIBLE);
                AsyncHttpClient mHttpClient = new AsyncHttpClient();
                mHttpClient.addHeader("accessToken", mPref.getAccessToken());
                Log.e("ACCESSTOKEN", mPref.getAccessToken());
                mHttpClient.get(Config.NEW_BOOK,
                        new AsyncHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                dialogProgressBar.setVisibility(View.GONE);
                                try {
                                    String s = new String(responseBody);
                                    Log.e("RESPONSE", s);
                                    JSONObject object = new JSONObject(s);
                                    if (object.getString("status").equalsIgnoreCase("success")) {
                                        if (object.getJSONArray("venueList").length() != 0) {
                                            if (object.get("venueList") instanceof JSONArray) {
                                                JSONArray jsonArray = object.getJSONArray("venueList");
                                                venueListItems = new ArrayList<>();
                                                gson = new Gson();
                                                for (int i = 0; i < jsonArray.length(); i++) {
                                                    venueListItems.add(gson.fromJson(jsonArray.get(i).toString(), VenueListItems.class));
                                                }
                                                if (!venueListItems.isEmpty()) {
                                                    adapter = new VenueAdapter(BookingGroundSlotsActivity.this, venueListItems, venueId);
                                                    dialogListView.setAdapter(adapter);
                                                    noVenue.setVisibility(View.GONE);
                                                } else {
                                                    dialogListView.setAdapter(null);
                                                    noVenue.setVisibility(View.VISIBLE);
                                                }
                                            }
                                        } else {
                                            dialogListView.setAdapter(null);
                                            noVenue.setVisibility(View.VISIBLE);
                                        }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                                dialogProgressBar.setVisibility(View.GONE);
                                try {
                                    String s = new String(bytes);
                                    Log.e("RESPONSE_FAIL", s);
                                    JSONObject object = new JSONObject(s);
                                    if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                                        Toast.makeText(BookingGroundSlotsActivity.this, object.getString("message"), Toast.LENGTH_SHORT).show();
                                        mPref.setAccessToken("");
                                        Intent intent = new Intent(BookingGroundSlotsActivity.this, LoginActivity.class);
                                        startActivity(intent);
                                        finish();
                                    } else
                                        Toast.makeText(BookingGroundSlotsActivity.this, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    // Toast.makeText(getActivity(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
                dialogProgressBar.setVisibility(View.GONE);
            }
        } else {
            Toast.makeText(BookingGroundSlotsActivity.this, "Not connected to internet", Toast.LENGTH_LONG).show();
        }
    }


    public static class VenueAdapter extends BaseAdapter {
        Context context;
        List<VenueListItems> list;
        private boolean[] mCheckedState;

        public VenueAdapter(Context context, List<VenueListItems> list, int venueId) {
            this.context = context;
            this.list = list;
            mCheckedState = new boolean[list.size()];

            for (int i = 0; i < list.size(); i++) {
                mCheckedState[i] = false;
            }

            for (int i = 0; i < list.size(); i++) {
                if (venueId == list.get(i).getId())
                    mCheckedState[i] = true;
            }
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            convertView = inflater.inflate(com.groundhog.vendor.android.R.layout.venue_list_item, parent, false);

            CallHolder holder = new CallHolder();
            holder.venueText = (TextView) convertView.findViewById(com.groundhog.vendor.android.R.id.venue_list_item);
            holder.image = (ImageView) convertView.findViewById(com.groundhog.vendor.android.R.id.selected_image);
            holder.relativeLayout = (RelativeLayout) convertView.findViewById(com.groundhog.vendor.android.R.id.item_view);

            holder.venueText.setText(list.get(position).getName());

            holder.venueText.setTypeface(PlaythoraUtility.getFont(context, RALEWAY_REGULAR));

            if (mCheckedState[position])
                holder.image.setVisibility(View.VISIBLE);
            else holder.image.setVisibility(View.INVISIBLE);

            holder.image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    for (int i = 0; i < list.size(); i++) {
                        mCheckedState[i] = false;
                    }
                    mCheckedState[position] = true;
                    notifyDataSetChanged();
                }
            });


            return convertView;
        }


        static class CallHolder {
            TextView venueText;
            ImageView image;
            RelativeLayout relativeLayout;
        }
    }

    public static class VenueGroundAdapter extends BaseAdapter {
        Context context;
        List<VenueGroundList> list;
        private boolean[] mCheckedState;

        public VenueGroundAdapter(Context context, List<VenueGroundList> list, int groundId) {
            this.context = context;
            this.list = list;
            mCheckedState = new boolean[list.size()];

            for (int i = 0; i < list.size(); i++) {
                mCheckedState[i] = false;
            }

            for (int i = 0; i < list.size(); i++) {
                if (groundId == list.get(i).getId())
                    mCheckedState[i] = true;
            }
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            convertView = inflater.inflate(com.groundhog.vendor.android.R.layout.venue_list_item, parent, false);

            CallHolder holder = new CallHolder();
            holder.venueText = (TextView) convertView.findViewById(com.groundhog.vendor.android.R.id.venue_list_item);
            holder.image = (ImageView) convertView.findViewById(com.groundhog.vendor.android.R.id.selected_image);
            holder.relativeLayout = (RelativeLayout) convertView.findViewById(com.groundhog.vendor.android.R.id.item_view);

            holder.venueText.setText(list.get(position).getName());

            holder.venueText.setTypeface(PlaythoraUtility.getFont(context, RALEWAY_REGULAR));

            if (mCheckedState[position])
                holder.image.setVisibility(View.VISIBLE);
            else holder.image.setVisibility(View.INVISIBLE);

            holder.image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    for (int i = 0; i < list.size(); i++) {
                        mCheckedState[i] = false;
                    }
                    mCheckedState[position] = true;
                    notifyDataSetChanged();
                }
            });

            return convertView;
        }


        static class CallHolder {
            TextView venueText;
            RelativeLayout relativeLayout;
            ImageView image;
        }
    }


    private void initializeAllComponents() {

        previous = (ImageView) findViewById(com.groundhog.vendor.android.R.id.previousDate);
        next = (ImageView) findViewById(com.groundhog.vendor.android.R.id.nextDate);
        todayText = (TextView) findViewById(com.groundhog.vendor.android.R.id.todayDate);
        todayCalender = (RelativeLayout) findViewById(com.groundhog.vendor.android.R.id.calenderRelativeLayout);
        viewPager = (ViewPager) findViewById(com.groundhog.vendor.android.R.id.slotViewPager);
        tabStrip = (PagerSlidingTabStrip) findViewById(com.groundhog.vendor.android.R.id.slotsTab);
    }


    private void setUpTabStrip() {
        //your other customizations related to tab strip...blahblah
        // Set first tab selected
        mTabsLinearLayout = ((LinearLayout) tabStrip.getChildAt(0));
        for (int i = 0; i < mTabsLinearLayout.getChildCount(); i++) {
            TextView tv = (TextView) mTabsLinearLayout.getChildAt(i);

            if (i == 0) {
                tv.setTextColor(ContextCompat.getColor(getApplicationContext(), com.groundhog.vendor.android.R.color.white));
                tv.setBackgroundResource(com.groundhog.vendor.android.R.drawable.profile_background);
            } else {
                tv.setTextColor(ContextCompat.getColor(getApplicationContext(), com.groundhog.vendor.android.R.color.text_one));
                tv.setBackgroundResource(com.groundhog.vendor.android.R.drawable.booking_slots_deselected_background);
            }
        }
    }


    public class MyPagerAdapter extends FragmentPagerAdapter {

        private final String[] Titles = {"BOOKED", "FREE"};

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return Titles[position];
        }

        @Override
        public int getCount() {
            return Titles.length;
        }

        @Override
        public Fragment getItem(int position) {

            if (position == 0) {
                bookedSlotsFragment = new BookedSlotsFragment();
                return bookedSlotsFragment;
            } else {
                freeSlotsFragment = new FreeSlotsFragment();
                return freeSlotsFragment;
            }


        }
    }
}
