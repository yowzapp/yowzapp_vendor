package com.groundhog.vendor.android.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.groundhog.vendor.android.model.ImageModel;
import com.groundhog.vendor.android.utils.HackyProblematicViewGroup;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import uk.co.senab.photoview.PhotoView;
import uk.co.senab.photoview.PhotoViewAttacher;

/**
 * Created by vaishakha on 16/9/16.
 */
public class GalleryViewActivity extends AppCompatActivity {
    TextView totalCount,currentCount;
    HackyProblematicViewGroup viewPager;
    ViewPagerAdapter pageradapter;
    int position;
    ArrayList<ImageModel> list;
    Context context;
    ImageView cancle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.groundhog.vendor.android.R.layout.gallery_viewpager);

        context = getApplicationContext();
        totalCount = (TextView) findViewById(com.groundhog.vendor.android.R.id.total_images);
        currentCount = (TextView) findViewById(com.groundhog.vendor.android.R.id.current_image);
        viewPager = (HackyProblematicViewGroup) findViewById(com.groundhog.vendor.android.R.id.pager);
        cancle = (ImageView) findViewById(com.groundhog.vendor.android.R.id.cancle_img);

        Intent intent = getIntent();
        if(intent.hasExtra("POSITION")){
            list = EditVenuePictures.imagesArray;
            position = intent.getIntExtra("POSITION",1);
            Log.e("POSITION", String.valueOf(position));
        }
        pageradapter = new ViewPagerAdapter(context,list);
        viewPager.setAdapter(pageradapter);
        viewPager.setCurrentItem(position);

        currentCount.setText((position+1) +"");
        totalCount.setText(list.size()+"");
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                currentCount.setText((position+1)+"");
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }

    public class ViewPagerAdapter extends PagerAdapter{
        Context context;
        LayoutInflater mLayoutInflater;
        ArrayList<ImageModel> list;
        String imageUrl;
        PhotoViewAttacher mAttacher;

        public ViewPagerAdapter(Context context, ArrayList<ImageModel> list) {
            this.list = list;
            this.context = context;
            mLayoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }


        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }


        @Override
        public Parcelable saveState() {
            return null;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            /*View itemView = mLayoutInflater.inflate(R.layout.full_image_layout, container, false);

            final ImageView imageView = (ImageView) itemView.findViewById(R.id.pager_image);

            //imageView.setImageBitmap(BitmapFactory.decodeFile(list.get(position).getImage()));
            Picasso.with(context).load(list.get(position).getImage()).config(Bitmap.Config.RGB_565).into(imageView);
            mAttacher = new PhotoViewAttacher(imageView);
*/
            final PhotoView photoView = new PhotoView(context);
            Picasso.with(context).load(list.get(position).getImage()).config(Bitmap.Config.RGB_565).into(photoView);

            container.addView(photoView);

            return photoView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }
    }
}
