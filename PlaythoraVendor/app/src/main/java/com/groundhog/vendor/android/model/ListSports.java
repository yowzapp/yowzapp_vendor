package com.groundhog.vendor.android.model;

/**
 * Created by vaishakha on 14/9/16.
 */
public class ListSports {
    private String name;
    private String sportsName;
    private String id;

    public ListSports(String sportsName) {
        this.sportsName = sportsName;
    }

    public ListSports(String sportsName, String name) {
        this.name = name;
        this.sportsName = sportsName;
    }

    public ListSports(String sportsName, String id,String name) {
        this.name = name;
        this.sportsName = sportsName;
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSportsName() {
        return sportsName;
    }

    public void setSportsName(String sportsName) {
        this.sportsName = sportsName;
    }
}
