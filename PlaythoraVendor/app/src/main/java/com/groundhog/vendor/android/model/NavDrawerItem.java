package com.groundhog.vendor.android.model;

/**
 * Created by vaishakha on 8/9/16.
 */
public class NavDrawerItem {
    private boolean showNotify;
    private String title;
    private String image;


    public NavDrawerItem() {

    }

    public NavDrawerItem(boolean showNotify, String title,String image) {
        this.showNotify = showNotify;
        this.title = title;
        this.image = image;
    }

    public boolean isShowNotify() {

        return showNotify;
    }

    public void setShowNotify(boolean showNotify) {
        this.showNotify = showNotify;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
