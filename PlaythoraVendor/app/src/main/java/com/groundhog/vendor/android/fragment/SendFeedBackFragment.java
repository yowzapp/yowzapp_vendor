package com.groundhog.vendor.android.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by vaishakha on 22/9/16.
 */
public class SendFeedBackFragment  extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(com.groundhog.vendor.android.R.layout.send_feed_back_layout, container,
                false);
        return view;
    }
}