package com.groundhog.vendor.android.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.groundhog.vendor.android.activity.NewBookingActivity;
import com.groundhog.vendor.android.model.FreeSlotsList;
import com.groundhog.vendor.android.model.FreeSlotsModel;
import com.groundhog.vendor.android.utils.PlaythoraUtility;

import java.util.ArrayList;
import java.util.List;

import static com.groundhog.vendor.android.utils.Config.RALEWAY_REGULAR;
import static com.groundhog.vendor.android.utils.Config.RALEWAY_SEMIBOLD;

/**
 * Created by hemanth on 29/9/16.
 */
public class FreeSlotsFragment extends Fragment {

    RecyclerView freeSlotsRecyclerView;
    Gson gson;
    public static ArrayList<FreeSlotsModel> freeSlotsArrayList;
    FreeSlotsAdapter freeSlotsAdapter;
    TextView emptyText;
    String venueName = "",mGroundName = "",date;
    int venID,groundID,slotId;
    ArrayList<String> startArrayList = new ArrayList<>();
    ArrayList<String> endArrayList = new ArrayList<>();
    ArrayList<Integer> slotIdArrayList = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(com.groundhog.vendor.android.R.layout.free_slots_layout,container,false);


        emptyText = (TextView)view.findViewById(com.groundhog.vendor.android.R.id.emptytext);
        freeSlotsRecyclerView  = (RecyclerView) view.findViewById(com.groundhog.vendor.android.R.id.free_slots_recycler_view);
        freeSlotsRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        freeSlotsRecyclerView.setHasFixedSize(true);

        emptyText.setTypeface(PlaythoraUtility.getFont(getActivity(),RALEWAY_REGULAR));
        return view;


    }

    public void loadSlots(List<FreeSlotsList> free_slots, String venueName, int venueId, String mGroundName, int groundId, String s, int slotId){

        this.venueName = venueName;
        this.mGroundName = mGroundName;
        this.venID = venueId;
        this.groundID = groundId;
        this.date = s;
        this.slotId = slotId;
        Log.e("venueName",venueName);
        Log.e("mGroundName",mGroundName);
        Log.e("venueId",venueId+"");
        Log.e("groundId",groundId+"");
        Log.e("date",s);
        Log.e("freeSlots", String.valueOf(free_slots.size()));
        Log.e("slotId",slotId+"");

        if(free_slots.size()==0) {
            emptyText.setText("No free slots for now");
            freeSlotsRecyclerView.setAdapter(null);
        }
        else {
            emptyText.setText("");
            freeSlotsAdapter = new FreeSlotsAdapter(getActivity(), freeSlotsRecyclerView, free_slots);
            freeSlotsRecyclerView.setAdapter(freeSlotsAdapter);
            freeSlotsAdapter.notifyDataSetChanged();
        }
    }

    private class FreeSlotsAdapter extends RecyclerView.Adapter<FreeSlotsFragment.FreeSlotsAdapter.MyViewHolder>{
        private Context mContext;
        List<FreeSlotsList> freeSlotsModelArrayList;
        RecyclerView recyclerView;

        public FreeSlotsAdapter(Context mContext, RecyclerView recyclerView, List<FreeSlotsList> freeSlotsArrayList) {
            this.mContext = mContext;
            this.freeSlotsModelArrayList = freeSlotsArrayList;
            this.recyclerView = recyclerView;
        }

        @Override
        public FreeSlotsFragment.FreeSlotsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(com.groundhog.vendor.android.R.layout.free_slots_item_layout,parent,false);


            return new FreeSlotsFragment.FreeSlotsAdapter.MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(FreeSlotsFragment.FreeSlotsAdapter.MyViewHolder holder, final int position) {
               holder.fSlotsTime.setText(freeSlotsModelArrayList.get(position).getStart_time() + " - " + freeSlotsModelArrayList.get(position).getEnd_time());
               holder.fSlotsPrice.setText("AED: " + freeSlotsModelArrayList.get(position).getSlot_price());
               startArrayList = new ArrayList<>();
               endArrayList = new ArrayList<>();
               slotIdArrayList = new ArrayList<>();
          /*  for(int i=0; 0<freeSlotsModelArrayList.size();i++){
                startArrayList.add(freeSlotsModelArrayList.get(position).getStart_time());
                endArrayList.add(freeSlotsModelArrayList.get(position).getEnd_time());
            }*/

            final View itemView = holder.itemView;

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startArrayList.add(freeSlotsModelArrayList.get(position).getStart_time());
                    endArrayList.add(freeSlotsModelArrayList.get(position).getEnd_time());
                    slotIdArrayList.add(freeSlotsModelArrayList.get(position).getSlot_id());

                    Intent intent = new Intent(mContext, NewBookingActivity.class);
                    intent.putExtra("VENUENAME",venueName);
                    intent.putExtra("VENUEID",venID);
                    intent.putExtra("GROUNDNAME",mGroundName);
                    intent.putExtra("GROUNDID",groundID);
                    intent.putExtra("DATE",date);
                    intent.putExtra("SlotsCount",1);
                    intent.putExtra("SlotsId",slotId);
                    intent.putStringArrayListExtra("startArrayList",startArrayList);
                    intent.putStringArrayListExtra("endArrayList",endArrayList);
                    intent.putIntegerArrayListExtra("slotIdArrayList",slotIdArrayList);
                    startActivity(intent);
                }
            });

        }

        @Override
        public int getItemCount() {
            return (null != freeSlotsModelArrayList ? freeSlotsModelArrayList.size() : 0);
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView  fSlotsTime,fSlotsPrice;
            public MyViewHolder(View itemView) {
                super(itemView);

                fSlotsTime = (TextView) itemView.findViewById(com.groundhog.vendor.android.R.id.freeSlotsTime);
                fSlotsPrice = (TextView) itemView.findViewById(com.groundhog.vendor.android.R.id.freeSlotsPrice);

                fSlotsPrice.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_SEMIBOLD));
                fSlotsTime.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_REGULAR));

            }
        }



    }
}
