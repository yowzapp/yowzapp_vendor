package com.groundhog.vendor.android.model;

/**
 * Created by vaishakha on 2/12/16.
 */
public class VenueSportsList {
    private String name;
    private String sports_name;
    private int id;
    private int sports_id;

    public VenueSportsList(String name, int id) {
        this.name = name;
        this.id = id;
    }

    public VenueSportsList(String sports_name, int sports_id,String groundname) {
        this.sports_name = sports_name;
        this.sports_id = sports_id;
        this.name = groundname;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSports_name() {
        return sports_name;
    }

    public void setSports_name(String sports_name) {
        this.sports_name = sports_name;
    }

    public int getSports_id() {
        return sports_id;
    }

    public void setSports_id(int sports_id) {
        this.sports_id = sports_id;
    }
}
