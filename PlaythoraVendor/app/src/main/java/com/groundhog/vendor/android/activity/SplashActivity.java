package com.groundhog.vendor.android.activity;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.groundhog.vendor.android.utils.PreferenceManager;

/**
 * Created by hemanth on 1/2/17.
 */

public class SplashActivity extends Activity {

    PreferenceManager mpref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.groundhog.vendor.android.R.layout.splash_layout);
        mpref = PreferenceManager.instance(getApplicationContext());
        NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        nm.cancelAll();

        Thread timeThread = new Thread(){
            public void run() {
                try {
                    sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    Log.e("token",mpref.getAccessToken());
                    if (!mpref.getAccessToken().isEmpty()) {
                        Log.e("mobileVerified",mpref.getMobilVerified());
                        if (mpref.getMobilVerified().equals("false")) {
                            Intent intent = new Intent(SplashActivity.this, VerificationActivity.class);
                            startActivity(intent);
                            finish();
                        } else {
                            Intent intent = new  Intent(SplashActivity.this,HomeActivity.class);
                            startActivity(intent);
                            finish();
                        }

                    } else {
                        Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                        startActivity(intent);
                        finish();
                    }

                }
            }
        };
        timeThread.start();

    }
}
