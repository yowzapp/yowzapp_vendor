package com.groundhog.vendor.android.model;

/**
 * Created by vaishakha on 22/12/16.
 */
public class AllAmenities {
    private int id;
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
