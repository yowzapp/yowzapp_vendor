package com.groundhog.vendor.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.groundhog.vendor.android.R;
import com.groundhog.vendor.android.adapter.SlotsAdapter;
import com.groundhog.vendor.android.model.DaysListDetailModel;
import com.groundhog.vendor.android.utils.Config;
import com.groundhog.vendor.android.utils.PlaythoraUtility;
import com.groundhog.vendor.android.utils.PreferenceManager;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;

import static com.groundhog.vendor.android.utils.Config.RALEWAY_REGULAR;

public class EditGroundSlotDetailsActivity extends AppCompatActivity implements View.OnClickListener {
    RecyclerView recyclerSlots;
    SlotsAdapter slotsAdapter;
    TextView toolbarTitle;
    TextView textAddTimeRange;
    String response = "{\n" +
            "\t\"status\": \"success\",\n" +
            "\t\"statusCode\": 200,\n" +
            "\t\"message\": \"Ground Day Range Details\",\n" +
            "\t\"success\": {\n" +
            "\t\t\"delete\": \"1,2,3\",\n" +
            "\t\t\"details\": [{\n" +
            "\t\t\t\t\"apply_for_rest_days\": 1,\n" +
            "\t\t\t\t\"id\": 1,\n" +
            "\t\t\t\t\"day_id\": 1,\n" +
            "\t\t\t\t\"ground_id\": 39,\n" +
            "\t\t\t\t\"start_time\": \"07:00 AM\",\n" +
            "\t\t\t\t\"end_time\": \"09:00 AM\",\n" +
            "\t\t\t\t\"duration\": 30,\n" +
            "\t\t\t\t\"price\": 1000,\n" +
            "\t\t\t\t\"total_players\": 12\n" +
            "\t\t\t},\n" +
            "\t\t\t{\n" +
            "\t\t\t\t\"apply_for_rest_days\": 1,\n" +
            "\t\t\t\t\"id\": 2,\n" +
            "\t\t\t\t\"day_id\": 1,\n" +
            "\t\t\t\t\"ground_id\": 39,\n" +
            "\t\t\t\t\"start_time\": \"10:00 AM\",\n" +
            "\t\t\t\t\"end_time\": \"12:00 PM\",\n" +
            "\t\t\t\t\"duration\": 60,\n" +
            "\t\t\t\t\"price\": 500,\n" +
            "\t\t\t\t\"total_players\": 5\n" +
            "\t\t\t},\n" +
            "\t\t\t{\n" +
            "\t\t\t\t\"apply_for_rest_days\": 1,\n" +
            "\t\t\t\t\"id\": 3,\n" +
            "\t\t\t\t\"day_id\": 1,\n" +
            "\t\t\t\t\"ground_id\": 39,\n" +
            "\t\t\t\t\"start_time\": \"01:00 PM\",\n" +
            "\t\t\t\t\"end_time\": \"03:00 PM\",\n" +
            "\t\t\t\t\"duration\": 90,\n" +
            "\t\t\t\t\"price\": 1000,\n" +
            "\t\t\t\t\"total_players\": 10\n" +
            "\t\t\t},\n" +
            "\t\t\t{\n" +
            "\t\t\t\t\"apply_for_rest_days\": 1,\n" +
            "\t\t\t\t\"id\": 4,\n" +
            "\t\t\t\t\"day_id\": 1,\n" +
            "\t\t\t\t\"ground_id\": 39,\n" +
            "\t\t\t\t\"start_time\": \"04:00 PM\",\n" +
            "\t\t\t\t\"end_time\": \"06:00 PM\",\n" +
            "\t\t\t\t\"duration\": 30,\n" +
            "\t\t\t\t\"price\": 2000,\n" +
            "\t\t\t\t\"total_players\": 4\n" +
            "\t\t\t}\n" +
            "\t\t]\n" +
            "\t}\n" +
            "}";
    private String verified, ground, games, venueId;
    private Toolbar toolbar;
    String groundID, dayID;
    PreferenceManager mPref;
    DaysListDetailModel daysListDetailModel;
    Gson gson;
    private Button buttonEditSlotSave;
    TextView save;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_ground_slot_details);
        mPref = PreferenceManager.instance(EditGroundSlotDetailsActivity.this);
        initializeViews();
        getIntentDatas();


        setClickListeners();
        if (getIntent().hasExtra("dayString")) {
            toolbarTitle.setText(getIntent().getStringExtra("dayString"));
            dayID = getIntent().getStringExtra("dayID");
            groundID = getIntent().getStringExtra("groundID");
        }
        save = (TextView) findViewById(R.id.save);


        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(slotsAdapter!=null) {
                    if(checkForValidation()) {
                        saveSlots();
                    }else {
                        PlaythoraUtility.showToast(EditGroundSlotDetailsActivity.this,"Please fill all the details");
                    }
                }
            }
        });

        populateDetail();
    }



    private void populateDetail() {

        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            PlaythoraUtility.showProgressDialog(EditGroundSlotDetailsActivity.this);
            AsyncHttpClient mHttpClient = new AsyncHttpClient();
            mHttpClient.addHeader("accessToken", mPref.getAccessToken());
            Log.e("groundID", groundID+"");
            Log.e("dayID", dayID+"");
            mHttpClient.get(Config.DAYS_RANGE_DETAIL + groundID + "/" + dayID, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    PlaythoraUtility.hideProgressDialog();
                    String response = new String(responseBody);
                    Log.e("DAYS_RANGE_DETAIL", response);
                    try {
                        JSONObject object = new JSONObject(response);
                        daysListDetailModel = new DaysListDetailModel();
                        gson = new Gson();
                        daysListDetailModel = gson.fromJson(response, DaysListDetailModel.class);

                        slotsAdapter = new SlotsAdapter(EditGroundSlotDetailsActivity.this, response,save);
                        recyclerSlots.setAdapter(slotsAdapter);

                        if(slotsAdapter!=null) {
                            if (slotsAdapter.getDetailsArray().length() != 0) {
                                save.setVisibility(View.VISIBLE);
                            }else {
                                save.setVisibility(View.GONE);
                            }
                        }


                    } catch (JSONException e) {
                        Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                    } catch (IllegalArgumentException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                    PlaythoraUtility.hideProgressDialog();

                    try {
                        String s = new String(bytes);
                        Log.e("DAYS_RANGE_DETAIL_FAIL", s);
                        JSONObject object = new JSONObject(s);
                        if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                            mPref.setAccessToken("");
                            Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(EditGroundSlotDetailsActivity.this, LoginActivity.class);
                            startActivity(intent);
                            finish();
                        } else
                            Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                        // Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                    }

                }

            });
        } else {
            Toast.makeText(getApplicationContext(), "Not connected to internet", Toast.LENGTH_LONG).show();

        }
    }



    private boolean checkForValidation() {

        boolean b = true;
        try {
            JSONArray jsonArray = slotsAdapter.getDetailsArray();
            for (int i = 0; i < jsonArray.length(); i++) {
                if (jsonArray.getJSONObject(i).getString("start_time").isEmpty()) {
                    Log.e("val_START",jsonArray.getJSONObject(i).getString("start_time"));
                    b=false;
                    return b;
                }else if(jsonArray.getJSONObject(i).getString("end_time").isEmpty()){
                    Log.e("val_END",jsonArray.getJSONObject(i).getString("end_time"));
                    b=false;
                    return b;
                }else if(jsonArray.getJSONObject(i).getInt("duration")==0){
                    Log.e("val_DURATION",jsonArray.getJSONObject(i).getInt("duration")+"");
                    b=false;
                    return b;
                }else if(jsonArray.getJSONObject(i).getInt("price")==0){
                    Log.e("val_PRICE",jsonArray.getJSONObject(i).getInt("price")+"");
                    b=false;
                    return b;
                }else if(jsonArray.getJSONObject(i).getInt("total_players")==0){
                    Log.e("val_PRICE",jsonArray.getJSONObject(i).getInt("total_players")+"");
                    b=false;
                    return b;
                }else {
                    b=true;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return b;

    }

    private void getIntentDatas() {
        try {
            Intent intent = getIntent();
            verified = intent.getStringExtra("Verified");
            ground = intent.getStringExtra("GroundName");
            games = intent.getStringExtra("AllSports");
            venueId = intent.getStringExtra("VenueId");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setClickListeners() {
        textAddTimeRange.setOnClickListener(this);
        buttonEditSlotSave.setOnClickListener(this);
    }

    private void initializeViews() {
        toolbar = (Toolbar) findViewById(R.id.venuSlotsToolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbarTitle = (TextView) findViewById(R.id.toolbar_title);
        recyclerSlots = (RecyclerView) findViewById(R.id.recycler_slot);
        textAddTimeRange = (TextView) findViewById(R.id.text_add_time_range);
        buttonEditSlotSave = (Button) findViewById(R.id.button_edit_slot_save);

        toolbarTitle.setTypeface(PlaythoraUtility.getFont(EditGroundSlotDetailsActivity.this, RALEWAY_REGULAR));
        textAddTimeRange.setTypeface(PlaythoraUtility.getFont(EditGroundSlotDetailsActivity.this, RALEWAY_REGULAR));
        buttonEditSlotSave.setTypeface(PlaythoraUtility.getFont(EditGroundSlotDetailsActivity.this, RALEWAY_REGULAR));

        recyclerSlots.setNestedScrollingEnabled(false);
        recyclerSlots.setLayoutManager(new LinearLayoutManager(EditGroundSlotDetailsActivity.this, LinearLayoutManager.VERTICAL, false));

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
           /* case R.id.button_edit_slot_save:
                saveSlots();
                break;*/
            case R.id.text_add_time_range:
                // need to check
                slotsAdapter.addNewTimeRange(save);
                break;
            default:
                break;
        }
    }

    private void saveSlots() {
        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            PlaythoraUtility.showProgressDialog(EditGroundSlotDetailsActivity.this);
            AsyncHttpClient mHttpClient = new AsyncHttpClient();
            mHttpClient.addHeader("accessToken", mPref.getAccessToken());

            StringEntity stringEntity = null;
            try {
                stringEntity = new StringEntity(prepareJSONData());
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            mHttpClient.post(EditGroundSlotDetailsActivity.this, Config.DAYS_RANGE_SAVE, stringEntity, "application/json", new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    PlaythoraUtility.hideProgressDialog();
                    String response = new String(responseBody);
                    Log.e("DAYS_RANGE_SAVE", response);
                    Toast.makeText(EditGroundSlotDetailsActivity.this, "Slots saved successfully", Toast.LENGTH_SHORT).show();

                    Intent intent = getIntent();
                    intent.putExtra("groundID",groundID);
                    setResult(10,intent);
                    finish();

                }

                @Override
                public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                    PlaythoraUtility.hideProgressDialog();

                    try {
                        String s = new String(bytes);
                        Log.e("DAYS_RANGE_DETAIL_FAIL", s);
                        JSONObject object = new JSONObject(s);
                        if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                            mPref.setAccessToken("");
                            Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(EditGroundSlotDetailsActivity.this, LoginActivity.class);
                            startActivity(intent);
                            finish();
                        } else
                            Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                        // Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                    }

                }

            });
        } else {
            Toast.makeText(getApplicationContext(), "Not connected to internet", Toast.LENGTH_LONG).show();
        }
    }

    private String prepareJSONData() {
        try {
            int detailsArrayLength = slotsAdapter.getDetailsArray().length();
            JSONObject requestParams = new JSONObject();
            requestParams.put("ground_id", Integer.parseInt(groundID));

//            requestParams.put("delete", "");
            //requestParams.put("details", slotsAdapter.getDetailsArray());

            if (!slotsAdapter.getDeletedSlots().equals("")) {
                requestParams.put("delete", slotsAdapter.getDeletedSlots());
            }
            for (int i = 0; i < detailsArrayLength; i++) {
                slotsAdapter.getDetailsArray().getJSONObject(i).put("day_id", Integer.parseInt(dayID));
            }
            requestParams.put("details", slotsAdapter.getDetailsArray());

            Log.e("STRINGENTITY", requestParams.toString());
            return requestParams.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = getIntent();
                intent.putExtra("groundID",groundID);
                setResult(10,intent);
                finish();
                return true;
            default:
                break;
        }
        return false;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Intent intent = getIntent();
            intent.putExtra("groundID",groundID);
            setResult(10,intent);
            finish();
            return true;
        }
        return false;
    }
}