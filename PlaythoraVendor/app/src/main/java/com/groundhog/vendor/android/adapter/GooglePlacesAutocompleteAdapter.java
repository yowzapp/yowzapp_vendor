package com.groundhog.vendor.android.adapter;

/**
 * Created by vaishakha on 22/9/16.
 */
import android.app.Activity;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

public class GooglePlacesAutocompleteAdapter extends ArrayAdapter implements Filterable {
    private static final String LOG_TAG = "Google Places Autocomplete";
    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String OUT_JSON = "/json";
    private static final String API_KEY = "AIzaSyCPKwNMVuBU070GTZBWimbfI8hVUNM9ojY";
    private ArrayList<String> resultList;
    private Activity context = null;
    public static ArrayList<String> placeID = null;
    public GooglePlacesAutocompleteAdapter(Activity context, int textViewResourceId) {
        super(context, textViewResourceId);
        this.context = context;
    }


    @Override
    public int getCount() {
        if(resultList != null)
            return resultList.size();
        else
            return 0;
    }

    @Override
    public String getItem(int index) {
        return resultList.get(index);
    }
    /*@Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;

        //if (convertView == null) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        //if (position != (resultList.size() - 1)) {
        Log.e("REEEEE",resultList.size()+"");
        view = inflater.inflate(R.layout.loc_list_row_item, null);
        TextView autocompleteTextView = (TextView) view.findViewById(R.id.txt_mem_name);
        autocompleteTextView.setText(resultList.get(position));
        // }
        // else
        //     view = inflater.inflate(R.layout.autocomplete_google_logo, null);

        //else {
        //    view = convertView;
        //}

      *//*  if (position != (resultList.size() - 1)) {

        }
        else {
            ImageView imageView = (ImageView) view.findViewById(R.id.imageView);
            // not sure what to do <img draggable="false" class="emoji" alt="😀" src="http://s.w.org/images/core/emoji/72x72/1f600.png">
        }*//*

        return view;
    }*/

    public ArrayList<String> autocomplete(String input) {
        ArrayList<String> resultList = null;
        ArrayList<String> descriptionList = null;
        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);

            sb.append("?key=" + API_KEY);
            sb.append("&types=geocode");
            sb.append("&location=12.9545163,77.3500463");
            sb.append("&radius=50000");
            sb.append("&components=country:in");
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));

            URL url = new URL(sb.toString());
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            // Create a JSON object hierarchy from the results
            Log.d("yo",jsonResults.toString());
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

            // Extract the Place descriptions from the results
            placeID = new ArrayList<String>(predsJsonArray.length());
            resultList = new ArrayList(predsJsonArray.length());
            descriptionList = new ArrayList(predsJsonArray.length());

            for (int i = 0; i < predsJsonArray.length(); i++) {
                if(predsJsonArray.getJSONObject(i).getJSONArray("types").toString().contains("political")) {
                    resultList.add(predsJsonArray.getJSONObject(i).toString());
                    if(predsJsonArray.getJSONObject(i).getString("description").endsWith(", India")){
                        String s=predsJsonArray.getJSONObject(i).getString("description").substring(0,predsJsonArray.getJSONObject(i).getString("description").length()-7);
                        descriptionList.add(s);
                    }else{
                        descriptionList.add(predsJsonArray.getJSONObject(i).getString("description"));
                    }

                    placeID.add(predsJsonArray.getJSONObject(i).getString("place_id"));
                }
            }
            // saveArray(resultList.toArray(new String[resultList.size()]), "predictionsArray", getContext());
        } catch (JSONException e) {
            Log.e(LOG_TAG, "Cannot process JSON results", e);
        }

        return descriptionList;
    }


    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                final FilterResults filterResults = new FilterResults();
                if (constraint != null) {
                    // Retrieve the autocomplete results.
                    resultList = autocomplete(constraint.toString());

                    // Assign the data to the FilterResults
                    context.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            filterResults.values = resultList;
                            filterResults.count = resultList.size();
                        }
                    });

                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    // setImageVisibility();

                    //LocationActivity.locList.setVisibility(View.VISIBLE);
                    try {
                        context.runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                notifyDataSetChanged();
                                //LocationActivity.noLoc.setVisibility(View.GONE);
                                //LocationActivity.locaProgress.setVisibility(View.GONE);
                            }
                        });
                        Thread.sleep(300);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                } else {

                    // LocationActivity.locList.setVisibility(View.INVISIBLE);
                    try {
                        context.runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                notifyDataSetInvalidated();
                                //LocationActivity.noLoc.setVisibility(View.VISIBLE);
                                //LocationActivity.locaProgress.setVisibility(View.GONE);
                            }
                        });
                        Thread.sleep(300);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }
            }
        };
        return filter;
    }
}