package com.groundhog.vendor.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.groundhog.vendor.android.utils.Config;
import com.groundhog.vendor.android.utils.PlaythoraUtility;
import com.groundhog.vendor.android.utils.PreferenceManager;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

import static com.groundhog.vendor.android.utils.Config.RALEWAY_REGULAR;

/**
 * Created by Nakul on 9/15/2016.
 */
public class ChangePasswordActivity  extends AppCompatActivity{

    EditText newPassword,confirmPassword,oldPassword;
    TextInputLayout newPasswordInputLayout,confirmPasswordInputLayout,oldPasswordInputLayout;
    Button save;
    PreferenceManager mPref;
    Toolbar toolbar;
    TextView title;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.groundhog.vendor.android.R.layout.change_password_layout);

       /* getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(Html.fromHtml("<font color=\"white\"><small>" + "Change your password" + "</small></font>"));*/

        toolbar = (Toolbar) findViewById(com.groundhog.vendor.android.R.id.psw_toolbar);
        title = (TextView) findViewById(com.groundhog.vendor.android.R.id.toolbar_pwd);
        toolbar.setNavigationIcon(com.groundhog.vendor.android.R.drawable.ic_back_arrow);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        title.setText("Change your password");
        title.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        initializeAllComponents();

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(checkForAllValidation()){
                    resetPassword();
                }
            }
        });


        oldPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
               oldPasswordInputLayout.setError(null);
            }
        });


        newPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
              newPasswordInputLayout.setError(null);
            }
        });

        confirmPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
               confirmPasswordInputLayout.setError(null);
            }
        });
    }

    private void resetPassword() {
        if(PlaythoraUtility.checkInternetConnection(getApplicationContext())){
            PlaythoraUtility.showProgressDialog(ChangePasswordActivity.this);
            AsyncHttpClient mHttpClient = new AsyncHttpClient();
            mHttpClient.addHeader("accessToken",mPref.getAccessToken());

            RequestParams params = new RequestParams();
            params.add("id",mPref.getId());
            params.add("oldPassword",oldPassword.getText().toString());
            params.add("newPassword",confirmPassword.getText().toString());
            Log.e("RESET_PARAMS", params.toString());

            mHttpClient.post(Config.RESET_PASSWORD, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    PlaythoraUtility.hideProgressDialog();
                    String response = new String(responseBody);
                    Log.e("reset",response);
                    Toast.makeText(ChangePasswordActivity.this,"Password reset successfully",Toast.LENGTH_SHORT).show();
                    Intent save = new Intent(ChangePasswordActivity.this,HomeActivity.class);
                    save.putExtra("ActivityName","Settings");
                    startActivity(save);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    PlaythoraUtility.hideProgressDialog();
                    try {
                        String s = new String(responseBody);
                        Log.e("RESPO",s);
                        JSONObject object = new JSONObject(s);
                        if(object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400){
                            mPref.setAccessToken("");
                            Toast.makeText(getApplicationContext(),object.getString("message"),Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(ChangePasswordActivity.this,LoginActivity.class);
                            startActivity(intent);
                            finish();
                        }else Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                    }catch (JSONException e){
                        e.printStackTrace();
                        //Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    private boolean checkForAllValidation() {

        if(oldPassword.getText().toString().trim().isEmpty() || oldPassword.getText().toString().contains(" ")){
            oldPasswordInputLayout.setError("Old password is required");
            oldPasswordInputLayout.setErrorEnabled(true);
        }else {
            oldPasswordInputLayout.setErrorEnabled(false);
        }

        if(newPassword.getText().toString().trim().isEmpty() || newPassword.getText().toString().contains(" ")){
            newPasswordInputLayout.setError("New password is required");
            newPasswordInputLayout.setErrorEnabled(true);
        }else {
            newPasswordInputLayout.setErrorEnabled(false);
        }

        if(confirmPassword.getText().toString().trim().isEmpty() || confirmPassword.getText().toString().contains(" ")){
            confirmPasswordInputLayout.setError("Confirm password is required");
            confirmPasswordInputLayout.setErrorEnabled(true);
        }else {
            confirmPasswordInputLayout.setErrorEnabled(false);
        }

        if(!(newPassword.getText().toString().equals(confirmPassword.getText().toString()))){
            confirmPasswordInputLayout.setError("Password does not match");
            confirmPasswordInputLayout.setErrorEnabled(true);
        }else {
            confirmPasswordInputLayout.setErrorEnabled(false);
        }

        if(oldPasswordInputLayout.isErrorEnabled() || newPasswordInputLayout.isErrorEnabled() || confirmPasswordInputLayout.isErrorEnabled()){
            return false;
        }else {
            return true;
        }

    }

    private void initializeAllComponents() {
        mPref = PreferenceManager.instance(getApplicationContext());
        newPassword = (EditText) findViewById(com.groundhog.vendor.android.R.id.newPassword);
        confirmPassword = (EditText) findViewById(com.groundhog.vendor.android.R.id.confirmPassword);
        oldPassword = (EditText) findViewById(com.groundhog.vendor.android.R.id.oldPassword);
        save = (Button) findViewById(com.groundhog.vendor.android.R.id.btnChangePassword);
        newPasswordInputLayout = (TextInputLayout) findViewById(com.groundhog.vendor.android.R.id.input_layout_new_password);
        confirmPasswordInputLayout = (TextInputLayout) findViewById(com.groundhog.vendor.android.R.id.input_layout_new_confirm_password);
        oldPasswordInputLayout = (TextInputLayout) findViewById(com.groundhog.vendor.android.R.id.input_layout_old_password);
        changePasswordUI(findViewById(com.groundhog.vendor.android.R.id.changePasswordActivity));


        save.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        newPasswordInputLayout.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        confirmPasswordInputLayout.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        oldPasswordInputLayout.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));



    }

    private void changePasswordUI(View viewById) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(viewById instanceof EditText)) {
            viewById.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    PlaythoraUtility.hideSoftKeyboard(ChangePasswordActivity.this);
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (viewById instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) viewById).getChildCount(); i++) {
                View innerView = ((ViewGroup) viewById).getChildAt(i);
                changePasswordUI(innerView);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                break;
        }

        return false;
    }
}
