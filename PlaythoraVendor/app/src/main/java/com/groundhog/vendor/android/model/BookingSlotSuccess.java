package com.groundhog.vendor.android.model;

import java.util.List;

/**
 * Created by vaishakha on 25/1/17.
 */
public class BookingSlotSuccess {
    private int id;
    private String name;
    private int venue_id;
    private int sports_id;
    private int slot_id;
    private String slot_price;
    private List<FreeSlotsList> free_slots;
    private List<BookedSlotsList> booked_slots;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getVenue_id() {
        return venue_id;
    }

    public void setVenue_id(int venue_id) {
        this.venue_id = venue_id;
    }

    public int getSports_id() {
        return sports_id;
    }

    public void setSports_id(int sports_id) {
        this.sports_id = sports_id;
    }

    public int getSlot_id() {
        return slot_id;
    }

    public void setSlot_id(int slot_id) {
        this.slot_id = slot_id;
    }

    public String getSlot_price() {
        return slot_price;
    }

    public void setSlot_price(String slot_price) {
        this.slot_price = slot_price;
    }

    public List<FreeSlotsList> getFree_slots() {
        return free_slots;
    }

    public void setFree_slots(List<FreeSlotsList> free_slots) {
        this.free_slots = free_slots;
    }

    public List<BookedSlotsList> getBooked_slots() {
        return booked_slots;
    }

    public void setBooked_slots(List<BookedSlotsList> booked_slots) {
        this.booked_slots = booked_slots;
    }
}
