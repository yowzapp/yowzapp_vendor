package com.groundhog.vendor.android.activity;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.groundhog.vendor.android.adapter.CustomAdapter;
import com.groundhog.vendor.android.adapter.SportsAdapter;
import com.groundhog.vendor.android.model.SportsSuccessList;
import com.groundhog.vendor.android.utils.Config;
import com.groundhog.vendor.android.utils.PlaythoraUtility;
import com.groundhog.vendor.android.utils.PreferenceManager;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ViewHolder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

import cz.msebera.android.httpclient.Header;

import static com.groundhog.vendor.android.utils.Config.RALEWAY_REGULAR;

/**
 * Created by vaishakha on 20/12/16.
 */
public class EditVenueSlotsActivity extends AppCompatActivity {
    private TextView gName, groundVerified;
    RecyclerView recyclerSports;
    SportsAdapter sportsAdapter;
    EditText slotDuration, slotPrice, slotMember, GName;
    TextView startTime, endTime;
    TextInputLayout slotDurationInputLayout, slotPriceInputLayout, memberInputLayout, groundTextInputLayout;
    Button save;
    ArrayList<SportsSuccessList> sportsArrayList;
    Gson gson;
    PreferenceManager mPref;
    String sportName, sportId, games, mTime, getStartTime, getEndTime;
    int gID;
    int position, Id;
    String[] slotDur;
    Toolbar toolbar;
    TextView title, exampleText;
    TextView sportsText,text_sports,sports_name;
    Dialog dialog;
    Button selectSlot;
    RelativeLayout textSportsType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.groundhog.vendor.android.R.layout.venue_slots_layout);
        mPref = PreferenceManager.instance(this);

        toolbar = (Toolbar) findViewById(com.groundhog.vendor.android.R.id.venuSlotsToolbar);
        title = (TextView) findViewById(com.groundhog.vendor.android.R.id.toolbar_title);
        toolbar.setNavigationIcon(com.groundhog.vendor.android.R.drawable.ic_back_arrow);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        title.setText("Edit Ground");
        title.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));


        // getSupportActionBar().setTitle(Html.fromHtml("<font color=\"white\"><small>" + "Edit venue slots" + "</small></font>"));

        initializeAllComponents();
        showDialogs();
        recyclerSports.setHasFixedSize(true);
        recyclerSports.setLayoutManager(new GridLayoutManager(getApplicationContext(), 3));
        if (getIntent().hasExtra("POSITION")) {
            try {
                games = AddGroundsActivity.allsports;
                position = getIntent().getIntExtra("POSITION", 0);
                Log.e("DEATIL", String.valueOf(position));
                //String[] sTime = CustomAdapter.apiList.get(position).getStart_time().split(":");
               // String[] eTime = CustomAdapter.apiList.get(position).getEnd_time().split(":");
                getStartTime = CustomAdapter.apiList.get(position).getStart_time();
                getEndTime = CustomAdapter.apiList.get(position).getEnd_time();
               // startTime.setText(PlaythoraUtility.getTimeInaaFormat(Integer.parseInt(sTime[0]), Integer.parseInt(sTime[1])));
               // endTime.setText(PlaythoraUtility.getTimeInaaFormat(Integer.parseInt(eTime[0]), Integer.parseInt(eTime[1])));
                //slotDuration.setText(CustomAdapter.apiList.get(position).getSlot_duration() + " min");
               // slotMember.setText(CustomAdapter.apiList.get(position).getMax_member() + "");
                //slotPrice.setText(CustomAdapter.apiList.get(position).getSlot_price() + "");
                GName.setText(CustomAdapter.apiList.get(position).getName() + "");
                Id = CustomAdapter.apiList.get(position).getSports_id();
                gID = CustomAdapter.apiList.get(position).getId();
                sports_name.setText(CustomAdapter.apiList.get(position).getSports_name());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        sportsArrayList = new ArrayList<>();
        if (games != null) {
            gson = new Gson();
            JSONArray jsonArray = null;
            try {
                jsonArray = new JSONArray(games);
                for (int i = 0; i < jsonArray.length(); i++) {
                    sportsArrayList.add(gson.fromJson(jsonArray.get(i).toString(), SportsSuccessList.class));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        sportsAdapter = new SportsAdapter(EditVenueSlotsActivity.this, sportsArrayList, Id);
        recyclerSports.setAdapter(sportsAdapter);

        gName.setText(AddGroundsActivity.groundName);
        if (AddGroundsActivity.verified.equalsIgnoreCase("true")) {
            String colorText = "<font color=\"#40ae79\">" + "Verified!" + "</font>" + " Available on platform";
            groundVerified.setText(Html.fromHtml(colorText));
        } else groundVerified.setVisibility(View.GONE);

        endTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTime = "endTime";
                callTimePicker(mTime);
            }
        });

        startTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTime = "startTime";
                callTimePicker(mTime);
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // slotDur = slotDuration.getText().toString().split(" ");
               // String[] startMin = getStartTime.toString().split(":");
               // String[] endMin = getEndTime.split(":");
                if (checkForValidation()) {
                    if (SportsAdapter.sportName.isEmpty()) {
                        Toast.makeText(EditVenueSlotsActivity.this, "Select sports", Toast.LENGTH_SHORT).show();
                    }
                    //else if (!PlaythoraUtility.calculateMinute(startMin, endMin, slotDur[0])) {
                     //   Toast.makeText(EditVenueSlotsActivity.this, "Timing set is not proper please check", Toast.LENGTH_SHORT).show();
                   // }
                    else {
                        EditGround();
                    }
                }
            }
        });

        slotDuration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DurationDialog();
            }
        });

        textSportsType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.show();
            }
        });

    }

    private void showDialogs() {

        dialog = new Dialog(EditVenueSlotsActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(com.groundhog.vendor.android.R.layout.sports_type_dialog);

        recyclerSports = (RecyclerView) dialog.findViewById(com.groundhog.vendor.android.R.id.recycler_sports);
        sportsText = (TextView) dialog.findViewById(com.groundhog.vendor.android.R.id.sports_text);
        selectSlot = (Button) dialog.findViewById(com.groundhog.vendor.android.R.id.venue_slots_);
        sportsText.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));

        selectSlot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SportsAdapter.sportName.isEmpty()) {
                    Toast.makeText(EditVenueSlotsActivity.this, "Select sports", Toast.LENGTH_SHORT).show();
                }
                else{
                    text_sports.setText(Html.fromHtml("Selected Sports Type: <font color='#939393'>"+"</font>"));
                    sports_name.setText(SportsAdapter.sportName);
                    dialog.dismiss();
                }
            }
        });

    }

    private void EditGround() {
       // slotDur = slotDuration.getText().toString().split(" ");
        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            PlaythoraUtility.showProgressDialog(EditVenueSlotsActivity.this);
            AsyncHttpClient mHttpClient = new AsyncHttpClient();
            mHttpClient.addHeader("accessToken", mPref.getAccessToken());
            RequestParams params = new RequestParams();
           // params.add("start_time", getStartTime);
           // params.add("end_time", getEndTime);
           // params.add("slot_duration", slotDur[0]);
          //  params.add("max_member", slotMember.getText().toString().trim());
          //  params.add("slot_price", slotPrice.getText().toString().trim());
            params.add("sports_id", SportsAdapter.sportId);
            params.add("sports_name", SportsAdapter.sportName);
            params.add("name", GName.getText().toString().trim());
            params.add("ground_id", String.valueOf(gID));

            Log.e("PARAMS", String.valueOf(params));
            mHttpClient.post(Config.EDIT_GROUND, params,
                    new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                            PlaythoraUtility.hideProgressDialog();
                            try {
                                String s = new String(responseBody);
                                Log.e("RESPONSE", s);
                                JSONObject object = new JSONObject(s);
                                if (object.getString("status").equalsIgnoreCase("success")) {
                                    AddGroundsActivity.value = true;
                                    Intent i = new Intent();
                                    i.putExtra("Detail", object.getString("success").toString());
                                    i.putExtra("position", position);
                                    setResult(2, i);
                                    finish();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                            PlaythoraUtility.hideProgressDialog();
                            try {
                                String s = new String(responseBody);
                                Log.e("RESPONSE_FAIL", s);
                                JSONObject object = new JSONObject(s);
                                if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                                    mPref.setAccessToken("");
                                    Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(EditVenueSlotsActivity.this, LoginActivity.class);
                                    startActivity(intent);
                                    finish();
                                } else
                                    Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                            } catch (Exception e) {
                                e.printStackTrace();
                                //Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        } else {

            Toast.makeText(getApplicationContext(), "Not connected to internet", Toast.LENGTH_LONG).show();

        }
    }

    private void DurationDialog() {

        final DialogPlus dialogPlus = DialogPlus.newDialog(EditVenueSlotsActivity.this)
                .setContentHolder(new ViewHolder(com.groundhog.vendor.android.R.layout.duration_dialog))
                .setCancelable(true)
                .setExpanded(false)
                .create();

        final TextView thirtyMin = (TextView) dialogPlus.findViewById(com.groundhog.vendor.android.R.id.text_first);
        final TextView sixtyMin = (TextView) dialogPlus.findViewById(com.groundhog.vendor.android.R.id.text_sec);
        final TextView ninetyMin = (TextView) dialogPlus.findViewById(com.groundhog.vendor.android.R.id.text_third);

        thirtyMin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                slotDuration.setText(thirtyMin.getText().toString());
                dialogPlus.dismiss();
            }
        });
        sixtyMin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                slotDuration.setText(sixtyMin.getText().toString());
                dialogPlus.dismiss();
            }
        });
        ninetyMin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                slotDuration.setText(ninetyMin.getText().toString());
                dialogPlus.dismiss();
            }
        });

        dialogPlus.show();

    }

    private void callTimePicker(final String time) {
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;

        mTimePicker = new TimePickerDialog(EditVenueSlotsActivity.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                String t = PlaythoraUtility.getTimeInaaFormat(selectedHour, selectedMinute);
                //getTime = time.replace("AM", "am").replace("PM","pm");
                if (time.equalsIgnoreCase("startTime")) {
                    getStartTime = PlaythoraUtility.getTime(selectedHour, selectedMinute);
                    startTime.setText(t);
                } else {
                    getEndTime = PlaythoraUtility.getTime(selectedHour, selectedMinute);
                    endTime.setText(t);
                }
                Log.e("TTTTTTt", mTime);
            }
        }, hour, minute, false);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }


    private boolean checkForValidation() {
     /*   boolean value;

        if (startTime.getText().toString().trim().isEmpty() || endTime.getText().toString().isEmpty()) {
            Toast.makeText(EditVenueSlotsActivity.this, "Select time", Toast.LENGTH_SHORT).show();
            value = true;
        } else value = false;

        if (slotDuration.getText().toString().trim().isEmpty()) {
            slotDurationInputLayout.setError("Please select slot duration");
            slotDurationInputLayout.setErrorEnabled(true);
        } else slotDurationInputLayout.setErrorEnabled(false);

        if (slotPrice.getText().toString().trim().isEmpty()) {
            slotPriceInputLayout.setError("Please select slot price");
            slotPriceInputLayout.setErrorEnabled(true);
        } else slotPriceInputLayout.setErrorEnabled(false);

        if (slotMember.getText().toString().trim().isEmpty()) {
            memberInputLayout.setError("Please select slot members");
            memberInputLayout.setErrorEnabled(true);
        } else memberInputLayout.setErrorEnabled(false);*/

        if (GName.getText().toString().trim().isEmpty()) {
            groundTextInputLayout.setError("Please enter ground name");
            groundTextInputLayout.setErrorEnabled(true);
        }/*else {
            if(GName.getText().toString().matches("^([a-zA-Z ]+[0-9]+[ *\\ ×xX][0-9]+[a-zA-Z]+)$")){
                groundTextInputLayout.setErrorEnabled(false);
            }*/ else {
            // groundTextInputLayout.setError("(Ex, Ground Name 90×90m)");
            groundTextInputLayout.setErrorEnabled(false);
        }


        if (groundTextInputLayout.isErrorEnabled())
            return false;
        else return true;
    }

    private void initializeAllComponents() {
        gName = (TextView) findViewById(com.groundhog.vendor.android.R.id.venue_slots_gname);
        groundVerified = (TextView) findViewById(com.groundhog.vendor.android.R.id.venue_slots_verified);
        slotDurationInputLayout = (TextInputLayout) findViewById(com.groundhog.vendor.android.R.id.input_layout_slot_duration);
        slotPriceInputLayout = (TextInputLayout) findViewById(com.groundhog.vendor.android.R.id.input_layout_slot_price);
        memberInputLayout = (TextInputLayout) findViewById(com.groundhog.vendor.android.R.id.input_layout_num_mem);
        startTime = (TextView) findViewById(com.groundhog.vendor.android.R.id.start_text);
        endTime = (TextView) findViewById(com.groundhog.vendor.android.R.id.end_text);
        slotDuration = (EditText) findViewById(com.groundhog.vendor.android.R.id.slot_text);
        slotPrice = (EditText) findViewById(com.groundhog.vendor.android.R.id.slot_price);
        slotMember = (EditText) findViewById(com.groundhog.vendor.android.R.id.number_mem);
        save = (Button) findViewById(com.groundhog.vendor.android.R.id.venue_slots_save);
        GName = (EditText) findViewById(com.groundhog.vendor.android.R.id.text_one);
        groundTextInputLayout = (TextInputLayout) findViewById(com.groundhog.vendor.android.R.id.text_input_ground);
        signupUI(findViewById(com.groundhog.vendor.android.R.id.venue_slot_relative));
        signupUI(findViewById(com.groundhog.vendor.android.R.id.recycler_sports));
        exampleText = (TextView) findViewById(com.groundhog.vendor.android.R.id.exampleText);
        textSportsType = (RelativeLayout) findViewById(com.groundhog.vendor.android.R.id.text_sports_type);
        text_sports = (TextView) findViewById(com.groundhog.vendor.android.R.id.text_sports);
        sports_name = (TextView) findViewById(com.groundhog.vendor.android.R.id.sports_name);

        text_sports.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        gName.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        groundVerified.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        slotDurationInputLayout.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        slotPriceInputLayout.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        memberInputLayout.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        // startTime.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        // endTime.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        save.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        GName.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        groundTextInputLayout.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        exampleText.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                //Toast.makeText(getApplicationContext(),"Back button clicked", Toast.LENGTH_SHORT).show();
                onBackPressed();
                return true;
            default:
                break;

        }
        return false;
    }

    private void signupUI(View viewById) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(viewById instanceof EditText)) {
            viewById.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    PlaythoraUtility.hideSoftKeyboard(EditVenueSlotsActivity.this);
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (viewById instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) viewById).getChildCount(); i++) {
                View innerView = ((ViewGroup) viewById).getChildAt(i);
                signupUI(innerView);
            }
        }

    }

}
