package com.groundhog.vendor.android.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.appcompat.BuildConfig;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.groundhog.vendor.android.adapter.CustomAdapter;
import com.groundhog.vendor.android.model.VenueSportsList;
import com.groundhog.vendor.android.utils.Config;
import com.groundhog.vendor.android.utils.ExpandableHeightListView;
import com.groundhog.vendor.android.utils.PlaythoraUtility;
import com.groundhog.vendor.android.utils.PreferenceManager;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import cz.msebera.android.httpclient.Header;

import static com.groundhog.vendor.android.utils.Config.RALEWAY_REGULAR;

/**
 * Created by vaishakha on 10/9/16.
 */
public class AddVenueActivity extends AppCompatActivity {

    ArrayList<VenueSportsList> newlist;
    Toolbar toolbar;
    ImageView uploadImage;
    TextView groundText;
    RelativeLayout uploadImageLayout,addCameraLayout;
    TextInputLayout inputLayoutGname,inputLayoutGdisp;
    ExpandableHeightListView groundList;
    Button save;
    EditText mGroundName,mDescription;
    CustomAdapter customAdapter;
    ArrayList<String> arrayList,listId;
    String selectedImagePath,groundName,groundImage="";
    private int PERMISSION_REQUEST_CODE = 200;
    private static final int PICK_FROM_CAM = 1;
    private int PICK_IMAGE_REQUEST = 2;
    File output=null;
    int serverResponseCode = 0 ;
    String[] permissions= new String[]{
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA,
            Manifest.permission.READ_EXTERNAL_STORAGE,
    };
    PreferenceManager mPref;
    ProgressBar progressBar;
    boolean uploading = true;
    TextView title;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.groundhog.vendor.android.R.layout.add_new_venue_layout);
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        initializeAllComponents();

        mPref = PreferenceManager.instance(this);

        groundText.setVisibility(View.GONE);
        groundList.setVisibility(View.GONE);


        save.setText("Create");
        groundList.setExpanded(true);


        toolbar = (Toolbar) findViewById(com.groundhog.vendor.android.R.id.add_venue_toolbar);
        title = (TextView) findViewById(com.groundhog.vendor.android.R.id.toolbar_title);
        toolbar.setNavigationIcon(com.groundhog.vendor.android.R.drawable.ic_back_arrow);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        title.setText("Add new venue");
        title.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));

        newlist = new ArrayList<>();
        /*customAdapter = new CustomAdapter(AddVenueActivity.this,newlist,"AddVenueActivity");
        groundList.setAdapter(customAdapter);*/

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        uploadImageLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("XXXXXXXXXXX", String.valueOf(checkPermission()));
                if (!checkPermission()) {
                }else UploadImage();
                }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(CheckForValidation()){
                    /*if(newlist.size()<=1 )
                        Toast.makeText(AddVenueActivity.this,"Add ground",Toast.LENGTH_SHORT).show();
                    else */
                    if(!uploading)
                        Toast.makeText(AddVenueActivity.this,"Image loading",Toast.LENGTH_SHORT).show();
                    else if(groundImage.isEmpty()){
                        Toast.makeText(AddVenueActivity.this,"Add ground Image",Toast.LENGTH_SHORT).show();
                    }else PostGround();
                }
            }
        });

        mGroundName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                inputLayoutGname.setError(null);
            }
        });

        mGroundName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if (mGroundName.getText().toString().trim().isEmpty())
                        inputLayoutGname.setError("Please provide ground name");
                    else
                        inputLayoutGname.setErrorEnabled(false);
                }
            }
        });
    }

    private void signupUI(View viewById) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(viewById instanceof EditText)) {
            viewById.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    PlaythoraUtility.hideSoftKeyboard(AddVenueActivity.this);
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (viewById instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) viewById).getChildCount(); i++) {
                View innerView = ((ViewGroup) viewById).getChildAt(i);
                signupUI(innerView);
            }
        }

    }

    private void PostGround() {

        /*String s = "{\n" +
                "\t\"cover_pic\":\"https://s3-ap-southeast-1.amazonaws.com/playthoraapp/vendors/venues/Hemant1480918732/20161202_120634.jpg\",\n" +
                "\t\"name\":\"testing venue names\",\n" +
                "\t\"description\":\"testing description\",\n" +
                "}";
        JSONObject onj = null;
        try {
            onj = new JSONObject(s);
        } catch (JSONException e) {
            e.printStackTrace();
        }*/
        JSONObject obj = makeJsonObject();
        Log.e("JSONOBJ",obj.toString());
        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            try {
                PlaythoraUtility.showProgressDialog(AddVenueActivity.this);
                AsyncHttpClient mHttpClient = new AsyncHttpClient();
                mHttpClient.addHeader("accessToken", mPref.getAccessToken());
                RequestParams params = new RequestParams();
                params.add("cover_pic", groundImage);
                params.add("name", mGroundName.getText().toString().trim());
                params.add("description", mDescription.getText().toString().trim());

                /*StringEntity se = new StringEntity(obj.toString());
                Log.e("JSONOBJ",se.toString());*/
                mHttpClient.post(Config.CREATE_VENUE, params,
                        new AsyncHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                PlaythoraUtility.hideProgressDialog();
                                try {
                                    String s = new String(responseBody);
                                    Log.e("RESPONSE", s);
                                    JSONObject object = new JSONObject(s);
                                    if (object.getString("status").equalsIgnoreCase("success")) {
                                        Intent i = new Intent(AddVenueActivity.this, VenueDetailsActivity.class);
                                        i.putExtra("IDs",object.getJSONObject("success").getInt("id"));
                                        startActivity(i);
                                        finish();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                }

                            }

                            @Override
                            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                                PlaythoraUtility.hideProgressDialog();
                                try {
                                    String s = new String(bytes);
                                    Log.d("RESPONSE_FAIL", s);
                                    JSONObject object = new JSONObject(s);
                                    if(object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400){
                                        mPref.setAccessToken("");
                                        Toast.makeText(getApplicationContext(),object.getString("message"),Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(AddVenueActivity.this,LoginActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }else Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    try {
                                        String s = new String(bytes);
                                        Log.d("RESPONSE_FAIL", s);
                                        JSONObject object = new JSONObject(s);
                                        if(object.getString("status").equalsIgnoreCase("error"))
                                            Toast.makeText(getApplicationContext(),object.getString("message"),Toast.LENGTH_SHORT).show();
                                        else Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                    } catch (JSONException e1) {
                                        e1.printStackTrace();
                                        Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                    }
                                }

                            }

                        });
            }catch (Exception e){}
        }else{

            Toast.makeText(getApplicationContext(), "Not connected to internet", Toast.LENGTH_LONG).show();

        }

    }

    private JSONObject makeJsonObject() {

        JSONObject jsonObject= null;
        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < newlist.size(); i++) {
            jsonObject = new JSONObject();
            try {
                jsonObject.put("sport_id", newlist.get(i).getId());
                jsonObject.put("sports_name", newlist.get(i).getSports_name());
                jsonObject.put("name", newlist.get(i).getName());
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            jsonArray.put(jsonObject);
        }
        JSONObject finalobject = new JSONObject();
        try {
            //finalobject.put("grounds", jsonArray);
            finalobject.put("cover_pic", groundImage);
            finalobject.put("name", mGroundName.getText().toString().trim());
            finalobject.put("description", mDescription.getText().toString().trim());
            //finalobject.put("id", mPref.getId());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("Grounds",jsonArray.toString());
        return finalobject;
    }

    private boolean CheckForValidation() {

        if(mGroundName.getText().toString().trim().isEmpty()){
            inputLayoutGname.setError("Please provide ground name");
            return false;
        }else{
            inputLayoutGname.setErrorEnabled(false);
            return true;
        }

    }

    private  boolean checkPermission() {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p:permissions) {
            result = ContextCompat.checkSelfPermission(AddVenueActivity.this,p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(AddVenueActivity.this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),PERMISSION_REQUEST_CODE);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions, int[] grantResults) {


        if(permsRequestCode==200){
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.e("startperm","request");
                UploadImage();
            } else {
                Toast.makeText(AddVenueActivity.this,  "Permission denied, You need to give permission to use this feature", Toast.LENGTH_SHORT).show();

            }

        }

    }


    private void UploadImage() {
        final String[] items = new String[] { "From Camera", "From Gallery" };
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(AddVenueActivity.this, android.R.layout.select_dialog_item, items);
        AlertDialog.Builder builder = new AlertDialog.Builder(AddVenueActivity.this);

        builder.setTitle("Select Image");
        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File dir =
                            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
                    output = new File(dir, "DesignString" + String.valueOf(System.currentTimeMillis()) + ".jpeg");
                    if (output != null) {
                        Uri photoURI=null;
                        if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
                            photoURI = Uri.fromFile(output);
                        }else if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            photoURI = FileProvider.getUriForFile(AddVenueActivity.this,
                                     getPackageName()+".provider",
                                    output);
                        }
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                        try {
                            startActivityForResult(intent, PICK_FROM_CAM);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    dialog.cancel();
                } else {
                    showImagePicker();
                }
            }
        });

        final AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void showImagePicker() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, PICK_IMAGE_REQUEST);
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.e("RRRRRRRRRRR", String.valueOf(resultCode) + " " + String.valueOf(requestCode));

        /*try {
            if (requestCode == 5) {
                //arrayList = data.getStringArrayListExtra("ArrayListSports");
                groundName = data.getStringExtra("GroundName");
                //listId = data.getStringArrayListExtra("ArrayListID");
                for (int i = 0; i < AddGroundPopUp.listSports.size(); i++) {
                    newlist.add(i, new VenueSportsList(AddGroundPopUp.listSports.get(i),AddGroundPopUp.listID.get(i), groundName));
                    Log.e("CCCCCCCCCC", newlist.get(i).getSports_name());
                }
                customAdapter.refresh(newlist,"AddVenueActivity");
                *//*customAdapter = new CustomAdapter(AddVenueActivity.this, newlist,"AddVenueActivity");
                groundList.setAdapter(customAdapter);*//*
            }
        }catch (Exception e){}
*/

        if(resultCode == RESULT_OK) {
            if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {


                Uri selectedImageUri = data.getData();
                try{
                    selectedImagePath = getPath(selectedImageUri);
                    Log.e("DDDDDDDDDD",selectedImagePath);
                }
                catch (NullPointerException e){
                    Toast.makeText(getApplicationContext(), "Couldn't fetch the image try again.", Toast.LENGTH_SHORT).show();
                }

                try {

                    ExifInterface exif = new ExifInterface(selectedImagePath);
                    int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
                    int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                    Log.e("ROTATE", orientation + "");
                    Bitmap bm;
                    int rotation=0;
                    if(rotation==0){
                        if(orientation==6){
                            rotation=90;
                        }
                        if(orientation==3){
                            rotation=180;
                        }
                        if(orientation==8){
                            rotation=270;
                        }
                        if(orientation==4){
                            rotation=180;
                        }

                    }
                    Log.e("ooooooo", String.valueOf(currentapiVersion));
                    Matrix matrix = new Matrix();
                    matrix.postRotate(rotation);
                    bm = Bitmap.createScaledBitmap(BitmapFactory.decodeFile(selectedImagePath), 100, 100, true);
                    bm = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), matrix, true);
                    //uploadImage.setImageBitmap(bm);
                    uploadImage.setVisibility(View.VISIBLE);
                    addCameraLayout.setVisibility(View.GONE);
                    progressBar.setVisibility(View.VISIBLE);
                    uploading = false;
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                uploadFile(selectedImagePath);
                            }catch (Exception e){
                                e.printStackTrace();
                                runOnUiThread(new Runnable() {
                                    public void run() {
                                        uploading = true;
                                        progressBar.setVisibility(View.GONE);
                                        addCameraLayout.setVisibility(View.VISIBLE);
                                        uploadImage.setVisibility(View.GONE);
                                        Toast.makeText(AddVenueActivity.this, com.groundhog.vendor.android.R.string.image_error,Toast.LENGTH_SHORT).show();
                                    }});
                            }
                        }
                    }).start();


                } catch (IOException e) {
                    e.printStackTrace();
                    uploading = true;
                    progressBar.setVisibility(View.GONE);
                    addCameraLayout.setVisibility(View.VISIBLE);
                    uploadImage.setVisibility(View.GONE);
                    Toast.makeText(AddVenueActivity.this, com.groundhog.vendor.android.R.string.image_error,Toast.LENGTH_SHORT).show();
                }catch (OutOfMemoryError e){
                    e.printStackTrace();
                    uploading = true;
                    progressBar.setVisibility(View.GONE);
                    addCameraLayout.setVisibility(View.VISIBLE);
                    uploadImage.setVisibility(View.GONE);
                    Toast.makeText(AddVenueActivity.this, com.groundhog.vendor.android.R.string.image_error,Toast.LENGTH_SHORT).show();
                } catch (NullPointerException e){
                    uploading = true;
                    progressBar.setVisibility(View.GONE);
                    addCameraLayout.setVisibility(View.VISIBLE);
                    uploadImage.setVisibility(View.GONE);
                    Toast.makeText(AddVenueActivity.this, com.groundhog.vendor.android.R.string.image_error,Toast.LENGTH_SHORT).show();
                    return;
                }catch (IllegalArgumentException e){
                    uploading = true;
                    progressBar.setVisibility(View.GONE);
                    addCameraLayout.setVisibility(View.VISIBLE);
                    uploadImage.setVisibility(View.GONE);
                    Toast.makeText(AddVenueActivity.this, com.groundhog.vendor.android.R.string.image_error,Toast.LENGTH_SHORT).show();
                    return;
                }

            } else {
               /* Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
                File destination = new File(Environment.getExternalStorageDirectory(),
                        "Daily" + String.valueOf(System.currentTimeMillis()) + ".jpg");
                final Uri mImageCaptureUri = Uri.fromFile(destination);

                System.out.println("Image Path : " + mImageCaptureUri);
                FileOutputStream fo;
                try {
                    destination.createNewFile();
                    fo = new FileOutputStream(destination);
                    fo.write(bytes.toByteArray());
                    fo.close();

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }*/
                selectedImagePath = getRealPathFromURI(Uri.fromFile(output).toString(), AddVenueActivity.this);
                Log.e("DDDDDDDDDDD",selectedImagePath);

                try {
                    ExifInterface exif = new ExifInterface(selectedImagePath);
                    int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
                    int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                    Log.e("ROTATE",orientation+"");
                    Bitmap bm;
                    int rotation=0;
                    if(rotation==0){
                        if(orientation==6){
                            rotation=90;
                        }
                        if(orientation==3){
                            rotation=180;
                        }
                        if(orientation==8){
                            rotation=270;
                        }
                        if(orientation==4){
                            rotation=180;
                        }

                    }
                    Log.e("ooooooo", String.valueOf(currentapiVersion));
                    Matrix matrix = new Matrix();
                    matrix.postRotate(rotation);
                    bm = Bitmap.createScaledBitmap(BitmapFactory.decodeFile(selectedImagePath), 100, 100, true);
                    bm = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), matrix, true);
                    //uploadImage.setImageBitmap(bm);
                    uploadImage.setVisibility(View.VISIBLE);
                    addCameraLayout.setVisibility(View.GONE);
                    progressBar.setVisibility(View.VISIBLE);
                    uploading = false;
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                uploadFile(selectedImagePath);
                            }catch (Exception e){
                                e.printStackTrace();
                                runOnUiThread(new Runnable() {
                                    public void run() {
                                        uploading = true;
                                        progressBar.setVisibility(View.GONE);
                                        addCameraLayout.setVisibility(View.VISIBLE);
                                        uploadImage.setVisibility(View.GONE);
                                        Toast.makeText(AddVenueActivity.this, com.groundhog.vendor.android.R.string.image_error,Toast.LENGTH_SHORT).show();
                                    }});
                            }
                        }
                    }).start();

                } catch (IOException e) {
                    e.printStackTrace();
                    uploading = true;
                    progressBar.setVisibility(View.GONE);
                    addCameraLayout.setVisibility(View.VISIBLE);
                    uploadImage.setVisibility(View.GONE);
                    Toast.makeText(AddVenueActivity.this, com.groundhog.vendor.android.R.string.image_error,Toast.LENGTH_SHORT).show();
                }catch (OutOfMemoryError e){
                    e.printStackTrace();
                    uploading = true;
                    progressBar.setVisibility(View.GONE);
                    addCameraLayout.setVisibility(View.VISIBLE);
                    uploadImage.setVisibility(View.GONE);
                    Toast.makeText(AddVenueActivity.this, com.groundhog.vendor.android.R.string.image_error,Toast.LENGTH_SHORT).show();
                }

            }
        }

    }


    public String getPath(Uri uri) {
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor =  getContentResolver().query(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private String getRealPathFromURI(String contentURI,Context context) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor =context.getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }


    private void uploadFile(final String sourceFileUri) {

        String fileName = sourceFileUri;

        HttpURLConnection conn = null;
        DataOutputStream dos = null;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;
        //File sourceFile = new File(sourceFileUri);
        File  sourceFile = new File(PlaythoraUtility.compressImage(sourceFileUri,AddVenueActivity.this));
        if (!sourceFile.isFile()) {
            Log.e("uploadFile", "Source File not exist :"+sourceFileUri);

            runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(getApplicationContext(),"Source File not exist :"+ sourceFileUri,Toast.LENGTH_SHORT).show();
                    uploading = true;
                    progressBar.setVisibility(View.GONE);
                    addCameraLayout.setVisibility(View.VISIBLE);
                    uploadImage.setVisibility(View.GONE);
                }
            });

            return ;

        }else {

            try {
                //Toast.makeText(EditProfileActivity.this, "FileInputStream", Toast.LENGTH_SHORT).show();
                FileInputStream fileInputStream = new FileInputStream(sourceFile);
                URL url = new URL(Config.UPLOAD_GROUND_IMAGE);
                trustAllHosts();
                // Open a HTTP  connection to  the URL
                conn = (HttpURLConnection) url.openConnection();
                conn.setDoInput(true); // Allow Inputs
                conn.setDoOutput(true); // Allow Outputs
                conn.setUseCaches(false); // Don't use a Cached Copy
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                conn.setRequestProperty("accessToken", mPref.getAccessToken());

                dos = new DataOutputStream(conn.getOutputStream());

                dos.writeBytes(twoHyphens + boundary + lineEnd);
                dos.writeBytes("Content-Disposition: form-data; name=\"image\";filename=\""
                        + fileName + "\"" + lineEnd); //profile_pic is a parameter

                dos.writeBytes(lineEnd);

                // create a buffer of  maximum size
                bytesAvailable = fileInputStream.available();

                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                buffer = new byte[bufferSize];

                // read file and write it into form...
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                while (bytesRead > 0) {
                    dos.write(buffer, 0, bufferSize);
                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                }

                // send multipart form data necesssary after file data...
                dos.writeBytes(lineEnd);
                dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                // Responses from the server (code and message)
                serverResponseCode = conn.getResponseCode();
                String serverResponseMessage = conn.getResponseMessage();
                Log.e("FILENAMESS","Content-Disposition: form-data; name=\"image\";filename=\""+ fileName + "\"" + lineEnd);
                Log.e("uploadFile", "HTTP Response is : "
                        + serverResponseMessage + ": " + serverResponseCode);
                final String serverError = serverResponseMessage;

                if(serverResponseCode == 200){
                    BufferedReader r = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    final StringBuilder total = new StringBuilder();
                    String line;
                    while ((line = r.readLine()) != null) {
                        total.append(line).append('\n');
                    }
                    Log.v("Output", String.valueOf(total));
                    runOnUiThread(new Runnable() {
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                            uploading = true;
                            JSONObject response = null;
                            try {
                                response = new JSONObject(total.toString());
                                Log.e("RESPONSE",response.toString());
                                groundImage = response.getString("success");
                                Picasso.with(AddVenueActivity.this).load(groundImage).fit().centerCrop().into(uploadImage);
                                //Toast.makeText(AddVenueActivity.this, "Upload Complete.", Toast.LENGTH_SHORT).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }else if(serverResponseCode == 400 ||serverResponseCode == 401){
                    BufferedReader r = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    final StringBuilder total = new StringBuilder();
                    String line;
                    while ((line = r.readLine()) != null) {
                        total.append(line).append('\n');
                    }
                    Log.v("Output", String.valueOf(total));

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            uploading = true;
                            progressBar.setVisibility(View.GONE);
                            addCameraLayout.setVisibility(View.VISIBLE);
                            uploadImage.setVisibility(View.GONE);
                            JSONObject object = null;
                            mPref.setAccessToken("");
                            Intent intent = new Intent(AddVenueActivity.this, LoginActivity.class);
                            startActivity(intent);
                            finishAffinity();
                        }
                    });


                } else {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            uploading = true;
                            progressBar.setVisibility(View.GONE);
                            addCameraLayout.setVisibility(View.VISIBLE);
                            uploadImage.setVisibility(View.GONE);
//                            Toast.makeText(getApplicationContext(), com.groundhog.vendor.android.R.string.error_msg, Toast.LENGTH_SHORT).show();
                            Toast.makeText(getApplicationContext(), serverError + ": " + serverResponseCode, Toast.LENGTH_LONG).show();
                        }
                    });
                }

                //close the streams //
                fileInputStream.close();
                dos.flush();
                dos.close();

            } catch (FileNotFoundException e) {
                e.printStackTrace();
                runOnUiThread(new Runnable() {
                    public void run() {
                        uploading = true;
                        progressBar.setVisibility(View.GONE);
                        addCameraLayout.setVisibility(View.VISIBLE);
                        uploadImage.setVisibility(View.GONE);
                        Toast.makeText(getApplicationContext(), com.groundhog.vendor.android.R.string.error_msg, Toast.LENGTH_SHORT).show();
                    }
                });
            } catch (MalformedURLException e) {
                e.printStackTrace();
                runOnUiThread(new Runnable() {
                    public void run() {
                        uploading = true;
                        progressBar.setVisibility(View.GONE);
                        addCameraLayout.setVisibility(View.VISIBLE);
                        uploadImage.setVisibility(View.GONE);
                        Toast.makeText(getApplicationContext(), com.groundhog.vendor.android.R.string.error_msg, Toast.LENGTH_SHORT).show();
                    }
                });
            } catch (ProtocolException e) {
                e.printStackTrace();
                runOnUiThread(new Runnable() {
                    public void run() {
                        uploading = true;
                        progressBar.setVisibility(View.GONE);
                        addCameraLayout.setVisibility(View.VISIBLE);
                        uploadImage.setVisibility(View.GONE);
                        Toast.makeText(getApplicationContext(), com.groundhog.vendor.android.R.string.error_msg, Toast.LENGTH_SHORT).show();
                    }
                });
            } catch (IOException e) {
                e.printStackTrace();
                runOnUiThread(new Runnable() {
                    public void run() {
                        uploading = true;
                        progressBar.setVisibility(View.GONE);
                        addCameraLayout.setVisibility(View.VISIBLE);
                        uploadImage.setVisibility(View.GONE);
                        Toast.makeText(getApplicationContext(), com.groundhog.vendor.android.R.string.error_msg, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }

    }

    private void trustAllHosts() {

        // Create a trust manager that does not validate certificate chains

        TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {

            public java.security.cert.X509Certificate[] getAcceptedIssuers() {

                return new java.security.cert.X509Certificate[]{};

            }

            public void checkClientTrusted(X509Certificate[] chain,

                                           String authType) throws CertificateException {

            }

            public void checkServerTrusted(X509Certificate[] chain,

                                           String authType) throws CertificateException {

            }

        }};

        // Install the all-trusting trust manager

        try {

            SSLContext sc = SSLContext.getInstance("TLS");

            sc.init(null, trustAllCerts, new java.security.SecureRandom());

            HttpsURLConnection

                    .setDefaultSSLSocketFactory(sc.getSocketFactory());

        } catch (Exception e) {

            e.printStackTrace();

        }
    }


    private void initializeAllComponents() {
        groundText = (TextView) findViewById(com.groundhog.vendor.android.R.id.ground_text);
        //toolbar = (Toolbar) findViewById(R.id.add_venue_toolbar);
        uploadImage = (ImageView) findViewById(com.groundhog.vendor.android.R.id.upload_image);
        groundList = (ExpandableHeightListView) findViewById(com.groundhog.vendor.android.R.id.ground_list);
        uploadImageLayout = (RelativeLayout) findViewById(com.groundhog.vendor.android.R.id.upload_image_layout);
        uploadImageLayout.setVisibility(View.VISIBLE);
        addCameraLayout = (RelativeLayout) findViewById(com.groundhog.vendor.android.R.id.camera_layout);
        save = (Button) findViewById(com.groundhog.vendor.android.R.id.relative_create);
        inputLayoutGname = (TextInputLayout) findViewById(com.groundhog.vendor.android.R.id.input_layout_gname);
        inputLayoutGdisp = (TextInputLayout) findViewById(com.groundhog.vendor.android.R.id.input_layout_gdisp);
        mGroundName = (EditText) findViewById(com.groundhog.vendor.android.R.id.name_text);
        mDescription = (EditText) findViewById(com.groundhog.vendor.android.R.id.desp_text);
        progressBar = (ProgressBar) findViewById(com.groundhog.vendor.android.R.id.add_venue_progressbar);
        signupUI(findViewById(com.groundhog.vendor.android.R.id.venue_new_relative));

        save.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        inputLayoutGname.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        inputLayoutGdisp.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        mGroundName.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        mDescription.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
    }

}
