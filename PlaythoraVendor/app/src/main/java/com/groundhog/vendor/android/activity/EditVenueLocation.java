package com.groundhog.vendor.android.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.Selection;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.groundhog.vendor.android.adapter.GooglePlacesAutocompleteAdapter;
import com.groundhog.vendor.android.utils.Config;
import com.groundhog.vendor.android.utils.PlaythoraUtility;
import com.groundhog.vendor.android.utils.PreferenceManager;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;

import static com.groundhog.vendor.android.utils.Config.RALEWAY_REGULAR;

/**
 /**
 * Created by vaishakha on 16/9/16.
 */
public class EditVenueLocation extends AppCompatActivity implements LocationListener, OnMapReadyCallback {
    private GoogleMap map;
    private double latitudevalue;
    private double longitudevalue;
    private String gName, verifiedText;
    private boolean isGPSEnabled;
    private LocationManager locationManager;
    private TextView groundName, groundVerified;
    private EditText searchCity;
    private Button save;
    private LatLng center;
    private Geocoder geocoder;
    private List<Address> addresses;
    private ListView locList;
    private String address = "", subLocality = "", city = "", country = "",
            state = "", LocationName = "", inilialvalue = "false",locationsDetail;
    private GooglePlacesAutocompleteAdapter dataAdapter;
    private RelativeLayout searchLocation;
    private ImageView mapLocator,clear;
    private MapFragment mapFragment;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    int venueID;
    PreferenceManager mPref;
    public static boolean value;
    int ID;
    boolean locationsStatus;
    JSONObject jsonObject;
    Toolbar toolbar;
    TextView title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.groundhog.vendor.android.R.layout.map_layout);

        toolbar = (Toolbar) findViewById(com.groundhog.vendor.android.R.id.map_toolbar);
        title = (TextView) findViewById(com.groundhog.vendor.android.R.id.toolbar_title);
        toolbar.setNavigationIcon(com.groundhog.vendor.android.R.drawable.ic_back_arrow);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //getSupportActionBar().setTitle(Html.fromHtml("<font color=\"white\"><small>" + "Edit venue location" + "</small></font>"));
        title.setText("Edit venue location");
        title.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));

        initializeAllComponent();
        mPref = PreferenceManager.instance(this);
        value = false;

        Intent intent = getIntent();
        verifiedText = intent.getStringExtra("Verified");
        gName = intent.getStringExtra("GroundName");
        venueID = intent.getIntExtra("VenueId", 0);
        locationsStatus = intent.getBooleanExtra("locationsStatus", false);
        locationsDetail = intent.getStringExtra("locationsDetail");

        groundName.setText(gName);
        if (verifiedText.equalsIgnoreCase("true")) {
            String colorText = "<font color=\"#40ae79\">" + "Verified!" + "</font>" + " Available on platform";
            groundVerified.setText(Html.fromHtml(colorText));
        } else groundVerified.setVisibility(View.GONE);

        //To setup location manager
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);


        if (locationsStatus) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            locationManager.removeUpdates(this);

            try {
                jsonObject = new JSONObject(locationsDetail);
                latitudevalue = Double.parseDouble(jsonObject.getString("lat"));
                longitudevalue = Double.parseDouble(jsonObject.getString("lng"));
                searchCity.setText(jsonObject.getString("location_name"));

                int position = searchCity.length();
                Editable etext = searchCity.getText();
                Selection.setSelection(etext, position);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            //latitudevalue = locationsDetail.split(",");


        }

        clear = (ImageView) findViewById(com.groundhog.vendor.android.R.id.clear);
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchCity.setText("");
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!searchCity.getText().toString().trim().isEmpty())
                editLocation();
                else Toast.makeText(getApplicationContext(),"Please set location name",Toast.LENGTH_SHORT).show();
            }
        });

        dataAdapter = new GooglePlacesAutocompleteAdapter(EditVenueLocation.this, com.groundhog.vendor.android.R.layout.text_view);


        locList.setAdapter(dataAdapter);

        //enables filtering for the contents of the given ListView
        locList.setTextFilterEnabled(true);

        searchCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                locList.setVisibility(View.VISIBLE);
                searchCity.addTextChangedListener(new TextWatcher() {
                    public void afterTextChanged(final Editable s) {
                        dataAdapter.getFilter().filter(s.toString());

                    }

                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }


                    public void onTextChanged(final CharSequence s, int start, int before, int count) {
                        if (s.toString().length() > 0) {
                            //getCurrent.setVisibility(View.GONE);
                        } else {

                        }


                    }
                });
            }
        });

        locList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                TextView tv = (TextView) view.findViewById(com.groundhog.vendor.android.R.id.text_loc);
                Log.e("AAAAAAAAAA", tv.getText().toString());
                LocationName = tv.getText().toString();
                dataAdapter.placeID.get(position);
                searchCity.setText(LocationName);
                Log.e("AAAAAAAAAA", dataAdapter.placeID.get(position));
                new Thread() {
                    public void run() {
                        try {
                            //LatLong(dataAdapter.placeID.get(position));

                            // Thread.sleep(300);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }.start();

        /*Intent intent=new Intent();
        intent.putExtra("LocName", listOfCities.get(position).getName());
        intent.putExtra("lat", listOfCities.get(position).getLoc().getLat() + "");
        intent.putExtra("lng", listOfCities.get(position).getLoc().getLng() + "");
        setResult(2,intent);
        finish();*/

                try {
                    LatLng p1 = getLocationFromAddress(EditVenueLocation.this, LocationName);
                    latitudevalue = p1.latitude;
                    longitudevalue = p1.longitude;
                    Log.e("AAAAAAAAAAAA", LocationName);
                    Log.e("AAAAAAAAAAAA", String.valueOf(p1));
                    MarkerOptions markerOptions = new MarkerOptions();
                    markerOptions.position(p1);
                    markerOptions.icon(null);
                    markerOptions.visible(false);
                    map.addMarker(markerOptions);
                    map.animateCamera(CameraUpdateFactory.newLatLngZoom(p1, 17.0f));
                    inilialvalue = "true";
                    locList.setVisibility(View.GONE);
                } catch (Exception e) {

                }

            }
        });

    }

    private void editLocation() {
        JSONObject obj = makeJsonObject();

        Log.e("JSONOBJ",obj.toString());

        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            try {
                PlaythoraUtility.showProgressDialog(EditVenueLocation.this);
                AsyncHttpClient mHttpClient = new AsyncHttpClient();
                mHttpClient.addHeader("accessToken", mPref.getAccessToken());
                StringEntity se = new StringEntity(obj.toString());
                Log.e("JSONOBJ",se.toString());
                mHttpClient.post(getApplicationContext(), Config.EDIT_VENUE, se, "application/json",
                        new AsyncHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                PlaythoraUtility.hideProgressDialog();
                                String s = new String(responseBody);
                                Log.e("RESPONSE", s);
                                try {
                                    JSONObject object = new JSONObject(s);
                                    if (object.getString("status").equalsIgnoreCase("success")) {
                                        value = true;
                                        ID = object.getJSONObject("success").getInt("id");
                                        Intent i = new Intent();
                                        setResult(1,i);
                                        finish();
                                    }
                                } catch (JSONException e) {e.printStackTrace();
                                    value = false;
                                    Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                }

                            }

                            @Override
                            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                                PlaythoraUtility.hideProgressDialog();
                                String s = new String(bytes);
                                Log.d("RESPONSE_FAIL", s);
                                try {
                                    value = false;
                                    JSONObject object = new JSONObject(s);
                                    if(object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400){
                                        mPref.setAccessToken("");
                                        Toast.makeText(getApplicationContext(),object.getString("message"),Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(EditVenueLocation.this,LoginActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }else Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                   // Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                }

                            }

                        });
            }catch (Exception e){}
        }else{

            Toast.makeText(getApplicationContext(), "Not connected to internet", Toast.LENGTH_LONG).show();

        }


    }

    private JSONObject makeJsonObject() {
        JSONObject jsonObject = null;
        JSONObject finalobject = new JSONObject();
        try {
            finalobject.put("lat", latitudevalue);
            finalobject.put("lng", longitudevalue);
            finalobject.put("venue_id", venueID);
            finalobject.put("location_name", searchCity.getText().toString().trim());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("Grounds", finalobject.toString());
        return finalobject;
    }

    public static boolean isLocationEnabled(Context context) {
        int locationMode = 0;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);

            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
            }

            return locationMode != Settings.Secure.LOCATION_MODE_OFF;

        } else {
            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }


    }

    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                //  TODO: Prompt with explanation!

                //Prompt the user once explanation has been shown
                /*ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);*/

            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay!
                    if (ActivityCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        map.setMyLocationEnabled(true);
                    }
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }

        }
    }


    public LatLng getLocationFromAddress(Context context, String strAddress) {

        Geocoder coder = new Geocoder(context);
        List<Address> address;
        LatLng p1 = null;

        try {
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null) {
                return null;
            }
            Address location = address.get(0);
            location.getLatitude();
            location.getLongitude();

            p1 = new LatLng(location.getLatitude(), location.getLongitude());

        } catch (Exception ex) {

            ex.printStackTrace();
        }

        return p1;
    }

    private void initializeMap() {
        if (PlaythoraUtility.checkInternetConnection(EditVenueLocation.this)) {

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1, 1, this);
       /* map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng point) {

                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(point);
                //markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_locator));
                markerOptions.icon(null);
                map.clear();
               *//* CameraPosition cameraPosition = new CameraPosition.Builder().target(
                        point).zoom(10).build();
                map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));*//*

                map.animateCamera(CameraUpdateFactory.newLatLng(point));
                //Log.e("LLLLLLLLLLLLL",point.toString());
                //Toast.makeText(getApplicationContext(),point.toString(), Toast.LENGTH_LONG).show();
                latitudevalue = point.latitude;
                longitudevalue= point.longitude;
                map.addMarker(markerOptions);

            }
        });*/
            map.getUiSettings().setMyLocationButtonEnabled(true);

            geocoder = new Geocoder(this, Locale.ENGLISH);
            try {
                List addressList = geocoder.getFromLocationName(LocationName, 1);
                if (addressList != null && addressList.size() > 0) {
                    Address address = (Address) addressList.get(0);
                    StringBuilder sb = new StringBuilder();
                    sb.append(address.getLatitude()).append("\n");
                    sb.append(address.getLongitude()).append("\n");
                    String result = sb.toString();
                    Log.e("adresssssss", result);
                    Log.e("adresssssss", LocationName);
                }
            } catch (IOException e) {
                Log.e("adresssssss", String.valueOf(e));
            }

                /*map.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
                    @Override
                    public void onMyLocationChange(Location location) {

                        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                        MarkerOptions markerOptions = new MarkerOptions();
                        markerOptions.position(latLng);
                        //markerOptions.title("My Location");
                        markerOptions.icon(null);
                        markerOptions.visible(false);
                        //adding marker to the map
                        map.addMarker(markerOptions);
                        //opening position with some zoom level in the map
                        map.animateCamera(CameraUpdateFactory.newLatLng(latLng));
                        locationManager.removeUpdates(this);
                    }
                });*/

            map.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
                @Override
                public void onCameraChange(CameraPosition arg0) {
                    // TODO Auto-generated method stub
                    center = map.getCameraPosition().target;

                    map.clear();

                    Log.e("latitude", String.valueOf(center.latitude));
                    Log.e("latitude", String.valueOf(center.longitude));

                    latitudevalue = center.latitude;
                    longitudevalue = center.longitude;
                    try {

                        if (inilialvalue.equalsIgnoreCase("false")) {
                            addresses = geocoder.getFromLocation(center.latitude, center.longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

                            if (addresses != null) {

                                if (!(addresses.get(0).getAddressLine(0) == null))
                                    address = addresses.get(0).getAddressLine(0) + ", "; // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                                else address = "";

                                if (!(addresses.get(0).getLocality() == null))
                                    city = addresses.get(0).getLocality() + ", ";
                                else city = "";

                                if (!(addresses.get(0).getAdminArea() == null))
                                    state = addresses.get(0).getAdminArea() + ", ";
                                else state = "";

                                if (!(addresses.get(0).getCountryName() == null))
                                    country = addresses.get(0).getCountryName();
                                else country = "";

                                if (!(addresses.get(0).getSubLocality() == null))
                                    subLocality = addresses.get(0).getSubLocality() + ", ";
                                else subLocality = "";

                                searchCity.setText(address + subLocality + city + state);

                                int position = searchCity.length();
                                Editable etext = searchCity.getText();
                                Selection.setSelection(etext, position);

                                String postalCode = addresses.get(0).getPostalCode();
                                String knownName = addresses.get(0).getFeatureName();
                                Log.e("Adressss", "address:" + address + " city:" + city + "state: " + state + " country:" + country +
                                        " postalCode:" + postalCode + " knownName:" + knownName);
                                Log.e("Adressss", addresses.get(0).getSubLocality() + " " + addresses.get(0).getSubAdminArea());
                            } else searchCity.setText("");
                        }
                        inilialvalue = "false";
                    } catch (Exception e) {
                        searchCity.setText("");
                        e.printStackTrace();
                    }
                }

            });
        } else
            Toast.makeText(EditVenueLocation.this, "Please check the internet connection", Toast.LENGTH_SHORT).show();
    }

    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(EditVenueLocation.this);
        // Setting Dialog Title
        alertDialog.setTitle("Location Services Disabled");
        // Setting Dialog Message
        alertDialog.setMessage("Please enable location services");
        // On pressing Settings button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        });
        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getApplicationContext(), "Your location has not been updated!", Toast.LENGTH_SHORT).show();
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }

    @Override
    public void onLocationChanged(Location location) {
        //To clear map data
        map.clear();
        //To hold location

        if(!locationsStatus) {
           // LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
            latitudevalue = location.getLatitude();
            longitudevalue = location.getLongitude();
        }else{
            try {
                latitudevalue = Double.parseDouble(jsonObject.getString("lat"));
                longitudevalue = Double.parseDouble(jsonObject.getString("lng"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        LatLng latLng = new LatLng(latitudevalue, longitudevalue);
        //To create marker in map
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        //markerOptions.title("My Location");
        markerOptions.icon(null);
        markerOptions.visible(false);
        //adding marker to the map
        map.addMarker(markerOptions);
        //opening position with some zoom level in the map
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17.0f));

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.removeUpdates(this);

        /*CameraPosition cameraPosition = new CameraPosition.Builder().target(
                latLng).zoom(10).build();
        map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));*/

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        map = googleMap;
        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            //User has previously accepted this permission
            if (ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                map.setMyLocationEnabled(true);
                initializeMap();
            }
        } else {
            //Not in api-23, no need to prompt
            map.setMyLocationEnabled(true);
            initializeMap();
        }

    }

    private void initializeAllComponent() {
        groundName = (TextView) findViewById(com.groundhog.vendor.android.R.id.map_ground_name) ;
        groundVerified = (TextView) findViewById(com.groundhog.vendor.android.R.id.map_verified) ;
        save = (Button) findViewById(com.groundhog.vendor.android.R.id.map_save);
        searchCity = (EditText) findViewById(com.groundhog.vendor.android.R.id.location_text);
        locList = (ListView) findViewById(com.groundhog.vendor.android.R.id.loc_list) ;
        searchLocation = (RelativeLayout) findViewById(com.groundhog.vendor.android.R.id.search_location);
        mapLocator = (ImageView) findViewById(com.groundhog.vendor.android.R.id.map_locator);
        mapFragment = (MapFragment) getFragmentManager().findFragmentById(com.groundhog.vendor.android.R.id.map);
        mapFragment.getMapAsync(this);

        groundName.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        groundVerified.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        save.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        searchCity.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                break;

        }
        return false;
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(!isLocationEnabled(getApplicationContext())) {
            showSettingsAlert();
        }else{
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                checkLocationPermission();
            }
        }

    }
}

