package com.groundhog.vendor.android.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.groundhog.vendor.android.R;
import com.groundhog.vendor.android.model.TimeModel;
import com.groundhog.vendor.android.utils.PlaythoraUtility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.groundhog.vendor.android.utils.Config.RALEWAY_BOLD;
import static com.groundhog.vendor.android.utils.Config.RALEWAY_REGULAR;

/**
 * Created by maheshprajapat on 3/23/18.
 */

public class SlotsAdapter extends RecyclerView.Adapter<SlotsAdapter.SlotsHolder> {
    private Activity context;
    private JSONObject responseObject;
    private JSONArray detailsArray;
    String tempEndTime = "";
    int tempMaxMember = -1;
    int priceValue;
    String startTimeVal, endTimeVal;
    String newTime = "";
    private JSONArray rangeArray;
    String deletedSlots = "";

    String timeListResponse = "[{\n" +
            "\t\"display_time\": \"12:00 AM - 12:30 AM\",\n" +
            "\t\"value1\": \"12:00 AM\",\n" +
            "\t\"value2\": \"12:30 AM\",\n" +
            "\t\"index\": 0\n" +
            "}, {\n" +
            "\t\"display_time\": \"12:30 AM - 1:00 AM\",\n" +
            "\t\"value1\": \"12:30 AM\",\n" +
            "\t\"value2\": \"1:00 AM\",\n" +
            "\t\"index\": 1\n" +
            "}, {\n" +
            "\t\"display_time\": \"1:00 AM - 1:30 AM\",\n" +
            "\t\"value1\": \"1:00 AM\",\n" +
            "\t\"value2\": \"1:30 AM\",\n" +
            "\t\"index\": 2\n" +
            "}, {\n" +
            "\t\"display_time\": \"1:30 AM - 2:00 AM\",\n" +
            "\t\"value1\": \"1:30 AM\",\n" +
            "\t\"value2\": \"2:00 AM\",\n" +
            "\t\"index\": 3\n" +
            "}, {\n" +
            "\t\"display_time\": \"2:00 AM - 2:30 AM\",\n" +
            "\t\"value1\": \"2:00 AM\",\n" +
            "\t\"value2\": \"2:30 AM\",\n" +
            "\t\"index\": 4\n" +
            "}, {\n" +
            "\t\"display_time\": \"2:30 AM - 3:00 AM\",\n" +
            "\t\"value1\": \"2:30 AM\",\n" +
            "\t\"value2\": \"3:00 AM\",\n" +
            "\t\"index\": 5\n" +
            "}, {\n" +
            "\t\"display_time\": \"3:00 AM - 3:30 AM\",\n" +
            "\t\"value1\": \"3:00 AM\",\n" +
            "\t\"value2\": \"3:30 AM\",\n" +
            "\t\"index\": 6\n" +
            "}, {\n" +
            "\t\"display_time\": \"3:30 AM - 4:00 AM\",\n" +
            "\t\"value1\": \"3:30 AM\",\n" +
            "\t\"value2\": \"4:00 AM\",\n" +
            "\t\"index\": 7\n" +
            "}, {\n" +
            "\t\"display_time\": \"4:00 AM - 4:30 AM\",\n" +
            "\t\"value1\": \"4:00 AM\",\n" +
            "\t\"value2\": \"4:30 AM\",\n" +
            "\t\"index\": 8\n" +
            "}, {\n" +
            "\t\"display_time\": \"4:30 AM - 5:00 AM\",\n" +
            "\t\"value1\": \"4:30 AM\",\n" +
            "\t\"value2\": \"5:00 AM\",\n" +
            "\t\"index\": 9\n" +
            "}, {\n" +
            "\t\"display_time\": \"5:00 AM - 5:30 AM\",\n" +
            "\t\"value1\": \"5:00 AM\",\n" +
            "\t\"value2\": \"5:30 AM\",\n" +
            "\t\"index\": 10\n" +
            "}, {\n" +
            "\t\"display_time\": \"5:30 AM - 6:00 AM\",\n" +
            "\t\"value1\": \"5:30 AM\",\n" +
            "\t\"value2\": \"6:00 AM\",\n" +
            "\t\"index\": 11\n" +
            "}, {\n" +
            "\t\"display_time\": \"6:00 AM - 6:30 AM\",\n" +
            "\t\"value1\": \"6:00 AM\",\n" +
            "\t\"value2\": \"6:30 AM\",\n" +
            "\t\"index\": 12\n" +
            "}, {\n" +
            "\t\"display_time\": \"6:30 AM - 7:00 AM\",\n" +
            "\t\"value1\": \"6:30 AM\",\n" +
            "\t\"value2\": \"7:00 AM\",\n" +
            "\t\"index\": 13\n" +
            "}, {\n" +
            "\t\"display_time\": \"7:00 AM - 7:30 AM\",\n" +
            "\t\"value1\": \"7:00 AM\",\n" +
            "\t\"value2\": \"7:30 AM\",\n" +
            "\t\"index\": 14\n" +
            "}, {\n" +
            "\t\"display_time\": \"7:30 AM - 8:00 AM\",\n" +
            "\t\"value1\": \"7:30 AM\",\n" +
            "\t\"value2\": \"8:00 AM\",\n" +
            "\t\"index\": 15\n" +
            "}, {\n" +
            "\t\"display_time\": \"8:00 AM - 8:30 AM\",\n" +
            "\t\"value1\": \"8:00 AM\",\n" +
            "\t\"value2\": \"8:30 AM\",\n" +
            "\t\"index\": 16\n" +
            "}, {\n" +
            "\t\"display_time\": \"8:30 AM - 9:00 AM\",\n" +
            "\t\"value1\": \"8:30 AM\",\n" +
            "\t\"value2\": \"9:00 AM\",\n" +
            "\t\"index\": 17\n" +
            "}, {\n" +
            "\t\"display_time\": \"9:00 AM - 9:30 AM\",\n" +
            "\t\"value1\": \"9:00 AM\",\n" +
            "\t\"value2\": \"9:30 AM\",\n" +
            "\t\"index\": 18\n" +
            "}, {\n" +
            "\t\"display_time\": \"9:30 AM - 10:00 AM\",\n" +
            "\t\"value1\": \"9:30 AM\",\n" +
            "\t\"value2\": \"10:00 AM\",\n" +
            "\t\"index\": 19\n" +
            "}, {\n" +
            "\t\"display_time\": \"10:00 AM - 10:30 AM\",\n" +
            "\t\"value1\": \"10:00 AM\",\n" +
            "\t\"value2\": \"10:30 AM\",\n" +
            "\t\"index\": 20\n" +
            "}, {\n" +
            "\t\"display_time\": \"10:30 AM - 11:00 AM\",\n" +
            "\t\"value1\": \"10:30 AM\",\n" +
            "\t\"value2\": \"11:00 AM\",\n" +
            "\t\"index\": 21\n" +
            "}, {\n" +
            "\t\"display_time\": \"11:00 AM - 11:30 AM\",\n" +
            "\t\"value1\": \"11:00 AM\",\n" +
            "\t\"value2\": \"11:30 AM\",\n" +
            "\t\"index\": 22\n" +
            "}, {\n" +
            "\t\"display_time\": \"11:30 AM - 12:00 PM\",\n" +
            "\t\"value1\": \"11:30 AM\",\n" +
            "\t\"value2\": \"12:00 PM\",\n" +
            "\t\"index\": 23\n" +
            "},{\n" +
            "\t\"display_time\": \"12:00 PM - 12:30 PM\",\n" +
            "\t\"value1\": \"12:00 PM\",\n" +
            "\t\"value2\": \"12:30 PM\",\n" +
            "\t\"index\": 24\n" +
            "}, {\n" +
            "\t\"display_time\": \"12:30 PM - 1:00 PM\",\n" +
            "\t\"value1\": \"12:30 PM\",\n" +
            "\t\"value2\": \"1:00 PM\",\n" +
            "\t\"index\": 25\n" +
            "}, {\n" +
            "\t\"display_time\": \"1:00 PM - 1:30 PM\",\n" +
            "\t\"value1\": \"1:00 PM\",\n" +
            "\t\"value2\": \"1:30 PM\",\n" +
            "\t\"index\": 26\n" +
            "}, {\n" +
            "\t\"display_time\": \"1:30 PM - 2:00 PM\",\n" +
            "\t\"value1\": \"1:30 PM\",\n" +
            "\t\"value2\": \"2:00 PM\",\n" +
            "\t\"index\": 27\n" +
            "}, {\n" +
            "\t\"display_time\": \"2:00 PM - 2:30 PM\",\n" +
            "\t\"value1\": \"2:00 PM\",\n" +
            "\t\"value2\": \"2:30 PM\",\n" +
            "\t\"index\": 28\n" +
            "}, {\n" +
            "\t\"display_time\": \"2:30 PM - 3:00 PM\",\n" +
            "\t\"value1\": \"2:30 PM\",\n" +
            "\t\"value2\": \"3:00 PM\",\n" +
            "\t\"index\": 29\n" +
            "}, {\n" +
            "\t\"display_time\": \"3:00 PM - 3:30 PM\",\n" +
            "\t\"value1\": \"3:00 PM\",\n" +
            "\t\"value2\": \"3:30 PM\",\n" +
            "\t\"index\": 30\n" +
            "}, {\n" +
            "\t\"display_time\": \"3:30 PM - 4:00 PM\",\n" +
            "\t\"value1\": \"3:30 PM\",\n" +
            "\t\"value2\": \"4:00 PM\",\n" +
            "\t\"index\": 31\n" +
            "}, {\n" +
            "\t\"display_time\": \"4:00 PM - 4:30 PM\",\n" +
            "\t\"value1\": \"4:00 PM\",\n" +
            "\t\"value2\": \"4:30 PM\",\n" +
            "\t\"index\": 32\n" +
            "}, {\n" +
            "\t\"display_time\": \"4:30 PM - 5:00 PM\",\n" +
            "\t\"value1\": \"4:30 PM\",\n" +
            "\t\"value2\": \"5:00 PM\",\n" +
            "\t\"index\": 33\n" +
            "}, {\n" +
            "\t\"display_time\": \"5:00 PM - 5:30 PM\",\n" +
            "\t\"value1\": \"5:00 PM\",\n" +
            "\t\"value2\": \"5:30 PM\",\n" +
            "\t\"index\": 34\n" +
            "}, {\n" +
            "\t\"display_time\": \"5:30 PM - 6:00 PM\",\n" +
            "\t\"value1\": \"5:30 PM\",\n" +
            "\t\"value2\": \"6:00 PM\",\n" +
            "\t\"index\": 35\n" +
            "}, {\n" +
            "\t\"display_time\": \"6:00 PM - 6:30 PM\",\n" +
            "\t\"value1\": \"6:00 PM\",\n" +
            "\t\"value2\": \"6:30 PM\",\n" +
            "\t\"index\": 36\n" +
            "}, {\n" +
            "\t\"display_time\": \"6:30 PM - 7:00 PM\",\n" +
            "\t\"value1\": \"6:30 PM\",\n" +
            "\t\"value2\": \"7:00 PM\",\n" +
            "\t\"index\": 37\n" +
            "}, {\n" +
            "\t\"display_time\": \"7:00 PM - 7:30 PM\",\n" +
            "\t\"value1\": \"7:00 PM\",\n" +
            "\t\"value2\": \"7:30 PM\",\n" +
            "\t\"index\": 38\n" +
            "}, {\n" +
            "\t\"display_time\": \"7:30 PM - 8:00 PM\",\n" +
            "\t\"value1\": \"7:30 PM\",\n" +
            "\t\"value2\": \"8:00 PM\",\n" +
            "\t\"index\": 39\n" +
            "}, {\n" +
            "\t\"display_time\": \"8:00 PM - 8:30 PM\",\n" +
            "\t\"value1\": \"8:00 PM\",\n" +
            "\t\"value2\": \"8:30 PM\",\n" +
            "\t\"index\": 40\n" +
            "}, {\n" +
            "\t\"display_time\": \"8:30 PM - 9:00 PM\",\n" +
            "\t\"value1\": \"8:30 PM\",\n" +
            "\t\"value2\": \"9:00 PM\",\n" +
            "\t\"index\": 41\n" +
            "}, {\n" +
            "\t\"display_time\": \"9:00 PM - 9:30 PM\",\n" +
            "\t\"value1\": \"9:00 PM\",\n" +
            "\t\"value2\": \"9:30 PM\",\n" +
            "\t\"index\": 42\n" +
            "}, {\n" +
            "\t\"display_time\": \"9:30 PM - 10:00 PM\",\n" +
            "\t\"value1\": \"9:30 PM\",\n" +
            "\t\"value2\": \"10:00 PM\",\n" +
            "\t\"index\": 43\n" +
            "}, {\n" +
            "\t\"display_time\": \"10:00 PM - 10:30 PM\",\n" +
            "\t\"value1\": \"10:00 PM\",\n" +
            "\t\"value2\": \"10:30 PM\",\n" +
            "\t\"index\": 44\n" +
            "}, {\n" +
            "\t\"display_time\": \"10:30 PM - 11:00 PM\",\n" +
            "\t\"value1\": \"10:30 PM\",\n" +
            "\t\"value2\": \"11:00 PM\",\n" +
            "\t\"index\": 45\n" +
            "}, {\n" +
            "\t\"display_time\": \"11:00 PM - 11:30 PM\",\n" +
            "\t\"value1\": \"11:00 PM\",\n" +
            "\t\"value2\": \"11:30 PM\",\n" +
            "\t\"index\": 46\n" +
            "}, {\n" +
            "\t\"display_time\": \"11:30 PM - 12:00 AM\",\n" +
            "\t\"value1\": \"11:30 PM\",\n" +
            "\t\"value2\": \"12:00 AM\",\n" +
            "\t\"index\": 47\n" +
            "}]";


    String startTimeIndex = "{\n" +
            "\t\"12:00 AM\": \"0\",\n" +
            "\t\"12:30 AM\": \"1\",\n" +
            "\t\"1:00 AM\": \"2\",\n" +
            "\t\"1:30 AM\": \"3\",\n" +
            "\t\"2:00 AM\": \"4\",\n" +
            "\t\"2:30 AM\": \"5\",\n" +
            "\t\"3:00 AM\": \"6\",\n" +
            "\t\"3:30 AM\": \"7\",\n" +
            "\t\"4:00 AM\": \"8\",\n" +
            "\t\"4:30 AM\": \"9\",\n" +
            "\t\"5:00 AM\": \"10\",\n" +
            "\t\"5:30 AM\": \"11\",\n" +
            "\t\"6:00 AM\": \"12\",\n" +
            "\t\"6:30 AM\": \"13\",\n" +
            "\t\"7:00 AM\": \"14\",\n" +
            "\t\"7:30 AM\": \"15\",\n" +
            "\t\"8:00 AM\": \"16\",\n" +
            "\t\"8:30 AM\": \"17\",\n" +
            "\t\"9:00 AM\": \"18\",\n" +
            "\t\"9:30 AM\": \"19\",\n" +
            "\t\"10:00 AM\": \"20\",\n" +
            "\t\"10:30 AM\": \"21\",\n" +
            "\t\"11:00 AM\": \"22\",\n" +
            "\t\"11:30 AM\": \"23\",\n" +
            "\t\"12:00 PM\": \"24\",\n" +
            "\t\"12:30 PM\": \"25\",\n" +
            "\t\"1:00 PM\": \"26\",\n" +
            "\t\"1:30 PM\": \"27\",\n" +
            "\t\"2:00 PM\": \"28\",\n" +
            "\t\"2:30 PM\": \"29\",\n" +
            "\t\"3:00 PM\": \"30\",\n" +
            "\t\"3:30 PM\": \"31\",\n" +
            "\t\"4:00 PM\": \"32\",\n" +
            "\t\"4:30 PM\": \"33\",\n" +
            "\t\"5:00 PM\": \"34\",\n" +
            "\t\"5:30 PM\": \"35\",\n" +
            "\t\"6:00 PM\": \"36\",\n" +
            "\t\"6:30 PM\": \"37\",\n" +
            "\t\"7:00 PM\": \"38\",\n" +
            "\t\"7:30 PM\": \"39\",\n" +
            "\t\"8:00 PM\": \"40\",\n" +
            "\t\"8:30 PM\": \"41\",\n" +
            "\t\"9:00 PM\": \"42\",\n" +
            "\t\"9:30 PM\": \"43\",\n" +
            "\t\"10:00 PM\": \"44\",\n" +
            "\t\"10:30 PM\": \"45\",\n" +
            "\t\"11:00 PM\": \"46\",\n" +
            "\t\"11:30 PM\": \"47\"\n" +
            "}";

    String endTimeIndex = "{   \"12:30 AM\": \"0\",\n" +
            "    \"1:00 AM\": \"1\",\n" +
            "    \"1:30 AM\": \"2\",\n" +
            "    \"2:00 AM\": \"3\",\n" +
            "    \"2:30 AM\": \"4\",\n" +
            "    \"3:00 AM\": \"5\",\n" +
            "    \"3:30 AM\": \"6\",\n" +
            "    \"4:00 AM\": \"7\",\n" +
            "    \"4:30 AM\": \"8\",\n" +
            "    \"5:00 AM\": \"9\",\n" +
            "    \"5:30 AM\": \"10\",\n" +
            "    \"6:00 AM\": \"11\",\n" +
            "    \"6:30 AM\": \"12\",\n" +
            "    \"7:00 AM\": \"13\",\n" +
            "    \"7:30 AM\": \"14\",\n" +
            "    \"8:00 AM\": \"15\",\n" +
            "    \"8:30 AM\": \"16\",\n" +
            "    \"9:00 AM\": \"17\",\n" +
            "    \"9:30 AM\": \"18\",\n" +
            "    \"10:00 AM\": \"19\",\n" +
            "    \"10:30 AM\": \"20\",\n" +
            "    \"11:00 AM\": \"21\",\n" +
            "    \"11:30 AM\": \"22\",\n" +
            "    \"12:00 PM\": \"23\",\n" +
            "    \"12:30 PM\": \"24\",\n" +
            "    \"1:00 PM\": \"25\",\n" +
            "    \"1:30 PM\": \"26\",\n" +
            "    \"2:00 PM\": \"27\",\n" +
            "    \"2:30 PM\": \"28\",\n" +
            "    \"3:00 PM\": \"29\",\n" +
            "    \"3:30 PM\": \"30\",\n" +
            "    \"4:00 PM\": \"31\",\n" +
            "    \"4:30 PM\": \"32\",\n" +
            "    \"5:00 PM\": \"33\",\n" +
            "    \"5:30 PM\": \"34\",\n" +
            "    \"6:00 PM\": \"35\",\n" +
            "    \"6:30 PM\": \"36\",\n" +
            "    \"7:00 PM\": \"37\",\n" +
            "    \"7:30 PM\": \"38\",\n" +
            "    \"8:00 PM\": \"39\",\n" +
            "    \"8:30 PM\": \"40\",\n" +
            "    \"9:00 PM\": \"41\",\n" +
            "    \"9:30 PM\": \"42\",\n" +
            "    \"10:00 PM\": \"43\",\n" +
            "    \"10:30 PM\": \"44\",\n" +
            "    \"11:00 PM\": \"45\",\n" +
            "    \"11:30 PM\": \"46\",\n" +
            "    \"12:00 AM\": \"47\"\n" +
            "}";

    String index = "{\n" +
            "\t\"0\": {\n" +
            "\t\t\"value1\": \"12:00 AM\",\n" +
            "\t\t\"value2\": \"12:30 AM\"\n" +
            "\t},\n" +
            "\t\"1\": {\n" +
            "\t\t\"value1\": \"12:30 AM\",\n" +
            "\t\t\"value2\": \"1:00 AM\"\n" +
            "\t},\n" +
            "\t\"2\": {\n" +
            "\t\t\"value1\": \"1:00 AM\",\n" +
            "\t\t\"value2\": \"1:30 AM\"\n" +
            "\t},\n" +
            "\t\"3\": {\n" +
            "\t\t\"value1\": \"1:30 AM\",\n" +
            "\t\t\"value2\": \"2:00 AM\"\n" +
            "\t},\n" +
            "\t\"4\": {\n" +
            "\t\t\"value1\": \"2:00 AM\",\n" +
            "\t\t\"value2\": \"2:30 AM\"\n" +
            "\t},\n" +
            "\t\"5\": {\n" +
            "\t\t\"value1\": \"2:30 AM\",\n" +
            "\t\t\"value2\": \"3:00 AM\"\n" +
            "\t},\n" +
            "\t\"6\": {\n" +
            "\t\t\"value1\": \"3:00 AM\",\n" +
            "\t\t\"value2\": \"3:30 AM\"\n" +
            "\t},\n" +
            "\t\"7\": {\n" +
            "\t\t\"value1\": \"3:30 AM\",\n" +
            "\t\t\"value2\": \"4:00 AM\"\n" +
            "\t},\n" +
            "\t\"8\": {\n" +
            "\t\t\"value1\": \"4:00 AM\",\n" +
            "\t\t\"value2\": \"4:30 AM\"\n" +
            "\t},\n" +
            "\t\"9\": {\n" +
            "\t\t\"value1\": \"4:30 AM\",\n" +
            "\t\t\"value2\": \"5:00 AM\"\n" +
            "\t},\n" +
            "\t\"10\": {\n" +
            "\t\t\"value1\": \"5:00 AM\",\n" +
            "\t\t\"value2\": \"5:30 AM\"\n" +
            "\t},\n" +
            "\t\"11\": {\n" +
            "\t\t\"value1\": \"5:30 AM\",\n" +
            "\t\t\"value2\": \"6:00 AM\"\n" +
            "\t},\n" +
            "\t\"12\": {\n" +
            "\t\t\"value1\": \"6:00 AM\",\n" +
            "\t\t\"value2\": \"6:30 AM\"\n" +
            "\t},\n" +
            "\t\"13\": {\n" +
            "\t\t\"value1\": \"6:30 AM\",\n" +
            "\t\t\"value2\": \"7:00 AM\"\n" +
            "\t},\n" +
            "\t\"14\": {\n" +
            "\t\t\"value1\": \"7:00 AM\",\n" +
            "\t\t\"value2\": \"7:30 AM\"\n" +
            "\t},\n" +
            "\t\"15\": {\n" +
            "\t\t\"value1\": \"7:30 AM\",\n" +
            "\t\t\"value2\": \"8:00 AM\"\n" +
            "\t},\n" +
            "\t\"16\": {\n" +
            "\t\t\"value1\": \"8:00 AM\",\n" +
            "\t\t\"value2\": \"8:30 AM\"\n" +
            "\t},\n" +
            "\t\"17\": {\n" +
            "\t\t\"value1\": \"8:30 AM\",\n" +
            "\t\t\"value2\": \"9:00 AM\"\n" +
            "\t},\n" +
            "\t\"18\": {\n" +
            "\t\t\"value1\": \"9:00 AM\",\n" +
            "\t\t\"value2\": \"9:30 AM\"\n" +
            "\t},\n" +
            "\t\"19\": {\n" +
            "\t\t\"value1\": \"9:30 AM\",\n" +
            "\t\t\"value2\": \"10:00 AM\"\n" +
            "\t},\n" +
            "\t\"20\": {\n" +
            "\t\t\"value1\": \"10:00 AM\",\n" +
            "\t\t\"value2\": \"10:30 AM\"\n" +
            "\t},\n" +
            "\t\"21\": {\n" +
            "\t\t\"value1\": \"10:30 AM\",\n" +
            "\t\t\"value2\": \"11:00 AM\"\n" +
            "\t},\n" +
            "\t\"22\": {\n" +
            "\t\t\"value1\": \"11:00 AM\",\n" +
            "\t\t\"value2\": \"11:30 AM\"\n" +
            "\t},\n" +
            "\t\"23\": {\n" +
            "\t\t\"value1\": \"11:30 AM\",\n" +
            "\t\t\"value2\": \"12:00 PM\"\n" +
            "\t},\n" +
            "\t\"24\": {\n" +
            "\t\t\"value1\": \"12:00 PM\",\n" +
            "\t\t\"value2\": \"12:30 PM\"\n" +
            "\t},\n" +
            "\t\"25\": {\n" +
            "\t\t\"value1\": \"12:30 PM\",\n" +
            "\t\t\"value2\": \"1:00 PM\"\n" +
            "\t},\n" +
            "\t\"26\": {\n" +
            "\t\t\"value1\": \"1:00 PM\",\n" +
            "\t\t\"value2\": \"1:30 PM\"\n" +
            "\t},\n" +
            "\t\"27\": {\n" +
            "\t\t\"value1\": \"1:30 PM\",\n" +
            "\t\t\"value2\": \"2:00 PM\"\n" +
            "\t},\n" +
            "\t\"28\": {\n" +
            "\t\t\"value1\": \"2:00 PM\",\n" +
            "\t\t\"value2\": \"2:30 PM\"\n" +
            "\t},\n" +
            "\t\"29\": {\n" +
            "\t\t\"value1\": \"2:30 PM\",\n" +
            "\t\t\"value2\": \"3:00 PM\"\n" +
            "\t},\n" +
            "\t\"30\": {\n" +
            "\t\t\"value1\": \"3:00 PM\",\n" +
            "\t\t\"value2\": \"3:30 PM\"\n" +
            "\t},\n" +
            "\t\"31\": {\n" +
            "\t\t\"value1\": \"3:30 PM\",\n" +
            "\t\t\"value2\": \"4:00 PM\"\n" +
            "\t},\n" +
            "\t\"32\": {\n" +
            "\t\t\"value1\": \"4:00 PM\",\n" +
            "\t\t\"value2\": \"4:30 PM\"\n" +
            "\t},\n" +
            "\t\"33\": {\n" +
            "\t\t\"value1\": \"4:30 PM\",\n" +
            "\t\t\"value2\": \"5:00 PM\"\n" +
            "\t},\n" +
            "\t\"34\": {\n" +
            "\t\t\"value1\": \"5:00 PM\",\n" +
            "\t\t\"value2\": \"5:30 PM\"\n" +
            "\t},\n" +
            "\t\"35\": {\n" +
            "\t\t\"value1\": \"5:30 PM\",\n" +
            "\t\t\"value2\": \"6:00 PM\"\n" +
            "\t},\n" +
            "\t\"36\": {\n" +
            "\t\t\"value1\": \"6:00 PM\",\n" +
            "\t\t\"value2\": \"6:30 PM\"\n" +
            "\t},\n" +
            "\t\"37\": {\n" +
            "\t\t\"value1\": \"6:30 PM\",\n" +
            "\t\t\"value2\": \"7:00 PM\"\n" +
            "\t},\n" +
            "\t\"38\": {\n" +
            "\t\t\"value1\": \"7:00 PM\",\n" +
            "\t\t\"value2\": \"7:30 PM\"\n" +
            "\t},\n" +
            "\t\"39\": {\n" +
            "\t\t\"value1\": \"7:30 PM\",\n" +
            "\t\t\"value2\": \"8:00 PM\"\n" +
            "\t},\n" +
            "\t\"40\": {\n" +
            "\t\t\"value1\": \"8:00 PM\",\n" +
            "\t\t\"value2\": \"8:30 PM\"\n" +
            "\t},\n" +
            "\t\"41\": {\n" +
            "\t\t\"value1\": \"8:30 PM\",\n" +
            "\t\t\"value2\": \"9:00 PM\"\n" +
            "\t},\n" +
            "\t\"42\": {\n" +
            "\t\t\"value1\": \"9:00 PM\",\n" +
            "\t\t\"value2\": \"9:30 PM\"\n" +
            "\t},\n" +
            "\t\"43\": {\n" +
            "\t\t\"value1\": \"9:30 PM\",\n" +
            "\t\t\"value2\": \"10:00 PM\"\n" +
            "\t},\n" +
            "\t\"44\": {\n" +
            "\t\t\"value1\": \"10:00 PM\",\n" +
            "\t\t\"value2\": \"10:30 PM\"\n" +
            "\t},\n" +
            "\t\"45\": {\n" +
            "\t\t\"value1\": \"10:30 PM\",\n" +
            "\t\t\"value2\": \"11:00 PM\"\n" +
            "\t},\n" +
            "\t\"46\": {\n" +
            "\t\t\"value1\": \"11:00 PM\",\n" +
            "\t\t\"value2\": \"11:30 PM\"\n" +
            "\t},\n" +
            "\t\"47\": {\n" +
            "\t\t\"value1\": \"11:30 PM\",\n" +
            "\t\t\"value2\": \"12:00 AM\"\n" +
            "\t}\n" +
            "}";

    ArrayList<String> mainArray = new ArrayList<>();
    ArrayList<String> arrayList = new ArrayList<>();
    ArrayList<String> intialList = new ArrayList<>();
    ArrayList<TimeModel> timeList;

    int[] myIntArray = new int[]{0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47};
    TextView save;

    public SlotsAdapter(Activity context, String response, TextView save) {
        this.context = context;
        this.save = save;
        try {
            responseObject = new JSONObject(response);
            detailsArray = responseObject.getJSONObject("success").getJSONArray("details");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public SlotsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View contentView = LayoutInflater.from(parent.getContext()).inflate(R.layout.edit_slot_details, null);
        return new SlotsHolder(contentView);
    }

    @Override
    public void onBindViewHolder(final SlotsHolder holder, final int position) {
        if (!responseObject.isNull("success")) {
            prefill(holder, position);
        }

        holder.deleteSlot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteSlot(holder, position);
            }
        });

        holder.timeRange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                try {
                    tempEndTime = detailsArray.getJSONObject(position).getString("start_time");
                    newTime = detailsArray.getJSONObject(position).getString("end_time");

                    Log.e("START", tempEndTime + "");
                    Log.e("NEWTIME", newTime + "");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (arrayList.size() > 0) {
                    arrayList.clear();
                }
                openTimeList(holder.timeRange, tempEndTime, newTime, detailsArray, position, holder);

            }
        });

    /*    holder.startTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(position==detailsArray.length()-1) {
                    startTimeVal = holder.startTime.getText().toString();
                    callTimePicker(holder, position, "Start");

                }
            }
        });
        holder.endTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(position==detailsArray.length()-1) {
                    endTimeVal = holder.startTime.getText().toString();
                    //callTimePicker(holder, position, "End");
                }
            }
        });*/


//        holder.startTime.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if(position==detailsArray.length()-1) {
//                    startTimeVal = holder.startTime.getText().toString();
//                    callTimePicker(holder, position, "Start");
//                }
//            }
//        });
//        holder.endTime.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if(position==detailsArray.length()-1) {
//                    endTimeVal = holder.startTime.getText().toString();
//                    //callTimePicker(holder, position, "End");
//                }
//            }
//        });


        holder.slot30.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    String start =   detailsArray.getJSONObject(position).getString("start_time");
                    String end  =   detailsArray.getJSONObject(position).getString("end_time");
                    ArrayList<String> rangeArray = getRangeArray(start,end);
                    if (rangeArray.size() >= 1) {
                        selectedSlot(30, holder, position);
                    }
                }catch (JSONException e ){
                    e.printStackTrace();
                }
            }
        });
        holder.slot60.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    String start =   detailsArray.getJSONObject(position).getString("start_time");
                    String end  =   detailsArray.getJSONObject(position).getString("end_time");
                    ArrayList<String> rangeArray = getRangeArray(start,end);
                    if (rangeArray.size() >= 2) {
                        selectedSlot(60, holder, position);
                    }
                }catch (JSONException e ){
                    e.printStackTrace();
                }
            }
        });
        holder.slot90.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    String start =   detailsArray.getJSONObject(position).getString("start_time");
                    String end  =   detailsArray.getJSONObject(position).getString("end_time");
                    ArrayList<String> rangeArray = getRangeArray(start,end);
                    if (rangeArray.size() > 2) {
                        selectedSlot(90, holder, position);
                    }
                }catch (JSONException e ){
                    e.printStackTrace();
                }

            }
        });


        holder.pricePerSlot.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (!holder.pricePerSlot.getText().toString().equalsIgnoreCase("")) {
                    try {
                        detailsArray.getJSONObject(position).put("price", Integer.parseInt(holder.pricePerSlot.getText().toString()));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    priceValue = Integer.parseInt(holder.pricePerSlot.getText().toString());
                } else {
                    priceValue = 0;
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

           /* holder.pricePerSlot.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (!hasFocus) {
                        if (!holder.pricePerSlot.getText().toString().equalsIgnoreCase("")) {
                            try {
                                detailsArray.getJSONObject(position).put("price", Integer.parseInt(holder.pricePerSlot.getText().toString()));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            });*/


        holder.maxMembers.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (!holder.maxMembers.getText().toString().equalsIgnoreCase("")) {
                    try {
                        detailsArray.getJSONObject(position).put("total_players", Integer.parseInt(holder.maxMembers.getText().toString()));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    tempMaxMember = Integer.parseInt(holder.maxMembers.getText().toString());
                } else {
                    tempMaxMember = 0;
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
          /*  holder.maxMembers.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (!hasFocus) {
                        if (!holder.maxMembers.getText().toString().equalsIgnoreCase("")) {
                            try {
                                detailsArray.getJSONObject(position).put("total_players", Integer.parseInt(holder.maxMembers.getText().toString()));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            tempMaxMember = Integer.parseInt(holder.maxMembers.getText().toString());
                        }
                    }
                }
            });*/

        holder.checkboxApplyAll.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                try {
                    if (isChecked) {
                        detailsArray.getJSONObject(position).put("apply_for_rest_days", 1);
                    } else {
                        detailsArray.getJSONObject(position).put("apply_for_rest_days", 0);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void openTimeList(final TextView timeRange, String start,
                              String end, final JSONArray detailsArray, final int adapterPosition, final SlotsHolder holder) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.time_list_dialog_layout);
        // TextView textView = (TextView) dialog.findViewById(R.id.day_text);
        ListView listView = (ListView) dialog.findViewById(R.id.time_list_view);
        ImageView close = (ImageView) dialog.findViewById(R.id.close);
        ImageView done = (ImageView) dialog.findViewById(R.id.check);
        JSONArray detailsArray1 = detailsArray;

        Gson gson = new Gson();
        JSONArray jsonArray = null;
        timeList = new ArrayList<>();

        try {
            jsonArray = new JSONArray(timeListResponse);
            for (int i = 0; i < jsonArray.length(); i++) {
                timeList.add(gson.fromJson(jsonArray.get(i).toString(), TimeModel.class));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("START_1", start + "");
        Log.e("NEWTIME_1", end + "");

        if (!start.isEmpty() && !end.isEmpty()) {

            arrayList = getRangeArray(start, end);
            this.intialList = arrayList;
            for (String value : arrayList) {
                if (mainArray.contains(value)) {
                    mainArray.remove(value);
                }
            }


        }

        Log.e("START_2", start + "");
        Log.e("NEWTIME_2", end + "");
        final TimeAdapter timeAdapter = new TimeAdapter(context, timeList, start, end, detailsArray1);
        listView.setAdapter(timeAdapter);


        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("time_list_size", timeAdapter.getStrings().size() + "");
                int size = timeAdapter.getStrings().size();
                String firstIndex = null, lastIndex = null;
               /* for(int i=0;i<timeAdapter.getStrings().size();i++){
                   firstIndex = timeAdapter.getStrings().get(0);
                   Log.e("1st_index",firstIndex+"");
                    lastIndex = timeAdapter.getStrings().get(size-1);
                    Log.e("last_index",lastIndex+"");
                }*/

                firstIndex = timeAdapter.getSatartTime();
                lastIndex = timeAdapter.getEndTime();

                Log.e("1st_index", firstIndex + "");
                Log.e("last_index", lastIndex + "");

                if (!firstIndex.isEmpty() && !lastIndex.isEmpty()) {
                    timeRange.setText("From: " + firstIndex + " - To: " + lastIndex);
                    tempEndTime = firstIndex;
                    newTime = lastIndex;
                } else {
                    timeRange.setText("Select range");
                }


                try {
                    detailsArray.getJSONObject(adapterPosition).put("start_time", tempEndTime);
                    detailsArray.getJSONObject(adapterPosition).put("end_time", newTime);
                    detailsArray.getJSONObject(adapterPosition).put("range", arrayList);

                    Log.e("range array", arrayList + "");
                    for (String value : arrayList) {
                        if (!mainArray.contains(value)) {
                            mainArray.add(value);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.e("arrayListSize", arrayList.size() + " size");

                if (arrayList.size() <= 1) {
                    selectedSlot(30, holder, adapterPosition);
                    holder.slot60.setBackgroundResource(R.drawable.slot_disable_background);
                    holder.slot90.setBackgroundResource(R.drawable.slot_disable_background);
                } else if (arrayList.size() > 1 && arrayList.size() <= 2) {
                    selectedSlot(60, holder, adapterPosition);
                    holder.slot90.setBackgroundResource(R.drawable.slot_disable_background);
                } else if (arrayList.size() > 2) {
                    selectedSlot(90, holder, adapterPosition);
                } else {
                    selectedSlot(0, holder, adapterPosition);
                }


                dialog.dismiss();
            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (String value : intialList) {
                    if (!mainArray.contains(value)) {
                        mainArray.add(value);
                    }
                }
                dialog.dismiss();
            }
        });

        dialog.show();

    }

    private ArrayList<String> getRangeArray(String start, String end) {
        ArrayList<String> range = new ArrayList<>();
        if (start.isEmpty() || end.isEmpty()){
            return range;
        }
        try {
            JSONObject jsonObject = new JSONObject(startTimeIndex);
            int startTimVal = Integer.parseInt(jsonObject.getString(start));

            JSONObject jsonObject1 = new JSONObject(endTimeIndex);
            int endTimVal = Integer.parseInt(jsonObject1.getString(end));


            if (startTimVal < endTimVal) {

                for (int i = 0; i < 48; i++) {
                    if ((i >= startTimVal) && (i <= endTimVal)) {
                        if (!range.contains(i + "")) {
                            range.add(i + "");
                        }
                    }
                }
            } else if (startTimVal > endTimVal) {

                for (int i = 0; i < 48; i++) {
                    if ((i >= endTimVal) && (i <= startTimVal)) {
                        if (!range.contains(i + "")) {
                            range.add(i + "");
                        }
                    }
                }

            } else if (startTimVal == endTimVal) {
                range.add(startTimVal + "");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        for (String value : range) {
            if (!mainArray.contains(value)) {
                mainArray.add(value);
            }
        }
        Log.e("RANGE", range + "");
        return range;
    }

    public class TimeAdapter extends BaseAdapter {
        Context context;
        List<TimeModel> list;
        private boolean[] mCheckedState;
        String selectedColor = "#FFBE7C";
        String disabledColor = "#D3D3D3";
        String startTime, endTime;
        JSONArray jsonArray;

        public TimeAdapter(Context context, List<TimeModel> list, String start, String end, JSONArray detailsArray1) {
            this.context = context;
            this.list = list;
            this.startTime = start;
            this.endTime = end;
            Log.e("start_time_1", startTime + "");
            Log.e("end_time_1", endTime + "");
            mCheckedState = new boolean[list.size()];

            jsonArray = detailsArray1;

        }

        public ArrayList<String> getStrings() {
            return arrayList;
        }

        public String getSatartTime() {
            return startTime;
        }

        public String getEndTime() {
            return endTime;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            convertView = inflater.inflate(R.layout.time_list_layout, parent, false);

            final CallHolder holder = new CallHolder();

            holder.time = (TextView) convertView.findViewById(R.id.time);
            holder.check = (ImageView) convertView.findViewById(R.id.check);
            holder.itemLayout = (LinearLayout) convertView.findViewById(R.id.item_view);
            holder.timeStatus = (TextView) convertView.findViewById(R.id.time_status);

            holder.time.setText(list.get(position).getDisplay_time());
            boolean aBoolean = false;
            if (arrayList.contains(list.get(position).getIndex() + "")) {
                holder.itemLayout.setBackgroundColor(Color.parseColor(selectedColor));

                try {

                    JSONObject jsonObject = new JSONObject(startTimeIndex);
                    JSONObject jsonObject1 = new JSONObject(endTimeIndex);


                    if (position == Integer.parseInt(jsonObject.getString(startTime))) {
                        String startTimeTemp = "From: " + startTime;
                        Log.e("SSSS", jsonObject.getString(startTime));
                        holder.timeStatus.setText(startTimeTemp);
                    }

                    if (position == Integer.parseInt(jsonObject1.getString(endTime))) {
                        Log.e("EEE", jsonObject1.getString(endTime));
                        String endTimeTemp = "To: " + endTime;
                        holder.timeStatus.setText(endTimeTemp);
                    }
                    if (arrayList.size() == 1) {
                        holder.timeStatus.setText("");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else if (mainArray.contains(list.get(position).getIndex() + "")) {
                holder.itemLayout.setBackgroundColor(Color.parseColor(disabledColor));

            }


            holder.itemLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    int selectedIndex = list.get(position).getIndex();
                    if (mainArray.contains(selectedIndex + "")) {
                        PlaythoraUtility.showToast(context,"Selected time range is not available, Please select different time range");
                        return;
                    }
                    int start = 0;
                    int end = 0;
                    try {
                        JSONObject jsonObject = new JSONObject(startTimeIndex);
                        JSONObject jsonObject1 = new JSONObject(endTimeIndex);

                        if (!startTime.isEmpty() && !endTime.isEmpty()) {
                            start = Integer.parseInt(jsonObject.getString(startTime));
                            end = Integer.parseInt(jsonObject1.getString(endTime));
                        }
                        Log.v("AAA", start + "" + end + " " + selectedIndex + " ");
                        ArrayList<String> rangeArray = new ArrayList<String>();
                        if (startTime.isEmpty() && endTime.isEmpty()) {
                            rangeArray.add(list.get(position).getIndex() + "");
                            List<String> currentSelectedValues = new ArrayList<String>(mainArray);
                            currentSelectedValues.retainAll(rangeArray);
                            if (currentSelectedValues.size() > 0) {
                                return;
                            }
                            arrayList = rangeArray;
                            startTime = list.get(position).getValue1();
                            endTime = list.get(position).getValue2();
                            notifyDataSetChanged();
                        } else if (selectedIndex == start || selectedIndex == end) {
                            Log.e("array List", arrayList + "");
                            if (selectedIndex == start) {
                                if (arrayList.contains(start + "")) {
                                    arrayList.remove(start + "");
                                }
                            } else if (selectedIndex == end) {
                                if (arrayList.contains(end + "")) {
                                    arrayList.remove(end + "");
                                }
                            }

                            if (arrayList.size() > 0) {
                                JSONObject indexJSON = new JSONObject(index);
                                if (selectedIndex == start) {
                                    int indexValue = selectedIndex + 1;
                                    if (indexValue > 47) {
                                        indexValue = 0;
                                    }
                                    Log.e("array indexValue", indexValue + "");
                                    indexJSON = indexJSON.getJSONObject(indexValue + "");
                                    startTime = indexJSON.getString("value1");
                                }
                                if (selectedIndex == end) {
                                    int indexValue = selectedIndex - 1;
                                    if (indexValue < 0) {
                                        indexValue = 47;
                                    }
                                    Log.e("array indexValue", indexValue + "");
                                    indexJSON = indexJSON.getJSONObject(indexValue + "");
                                    endTime = indexJSON.getString("value2");
                                }
                            } else {
                                startTime = "";
                                endTime = "";
                            }

                            notifyDataSetChanged();
                        } else if (selectedIndex > start) {
                            for (int i = 0; i < 48; i++) {
                                if ((i >= start) && (i <= selectedIndex)) {
                                    if (!rangeArray.contains(i + "")) {
                                        rangeArray.add(i + "");
                                    }

                                }
                            }
                            List<String> currentSelectedValues = new ArrayList<String>(mainArray);
                            currentSelectedValues.retainAll(rangeArray);
                            if (currentSelectedValues.size() > 0) {
                                return;
                            }
                            arrayList = rangeArray;
                            endTime = list.get(position).getValue2();
                            notifyDataSetChanged();

                        } else if (selectedIndex < start) {
                            for (int i = 0; i < 48; i++) {
                                Log.e("size", i + "");
                                if ((i >= selectedIndex) && (i <= end)) {
                                    rangeArray.add(i + "");
                                }
                            }
                            List<String> currentSelectedValues = new ArrayList<String>(mainArray);
                            currentSelectedValues.retainAll(rangeArray);
                            if (currentSelectedValues.size() > 0) {
                                return;
                            }
                            startTime = list.get(position).getValue1();
                            arrayList = rangeArray;
                            notifyDataSetChanged();

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });

            return convertView;
        }


        public class CallHolder {
            TextView time, timeStatus;
            ImageView check;
            LinearLayout itemLayout;

        }
    }

    private void deleteSlot(SlotsHolder holder, int position) {
        int detailsArrayLength = detailsArray.length();
        Log.e("DETAILS_ARRAY_LENGTH", detailsArray.length() + "");


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                JSONObject detailsSlot = detailsArray.getJSONObject(position);
                if (detailsSlot.has("id")) {
                    deletedSlots = deletedSlots + ", " + detailsSlot.get("id");
                    Log.e("DELETED_SLOT", deletedSlots);
                }
                JSONArray rangeArray = detailsSlot.getJSONArray("range");
                for (int i = 0; i < rangeArray.length(); i++) {
                    String rangeValue = rangeArray.getString(i);
                    if (mainArray.contains(rangeValue)) {
                        mainArray.remove(rangeValue);
                    }

                    detailsArray.remove(position);
                    holder.selectedSlot = 0;
                    notifyItemRemoved(position);
                    notifyDataSetChanged();

                    if(detailsArray.length()==0){
                        save.setVisibility(View.GONE);
                    }else {
                        save.setVisibility(View.VISIBLE);
                    }

                    Log.e("DETAILS_ARRAY", detailsArray.toString());

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            detailsArray.remove(position);
            holder.selectedSlot = 0;
            notifyItemRemoved(position);
            notifyDataSetChanged();

            Log.e("DETAILS_ARRAY", detailsArray.toString());
        }
    }


//    private void callTimePicker(final SlotsHolder holder, final int position, final String time) {
//
//        String input;
//        if (time.equalsIgnoreCase("Start")) {
//            input = holder.startTime.getText().toString();
//        } else {
//            input = holder.endTime.getText().toString();
//        }
//        //Format of the date defined in the input String
//        DateFormat df = new SimpleDateFormat("hh:mm");
//        //Desired format: 24 hour format: Change the pattern as per the need
//        DateFormat outputformat = new SimpleDateFormat("HH:mm");
//        Date date = null;
//        String output = null;
//        try{
//            //Converting the input String to Date
//            date= df.parse(input);
//            //Changing the format of date and storing it in String
//            output = outputformat.format(date);
//        }catch(ParseException pe){
//            pe.printStackTrace();
//        }
//
//        String[] timeString = output.split(":");
//        int hour = Integer.parseInt(timeString[0]);
//        int minute = Integer.parseInt(timeString[1]);
//        TimePickerDialog mTimePicker;
//
//        mTimePicker = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
//            @Override
//            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
//
//                String t = PlaythoraUtility.getTimeInaaFormat(selectedHour, selectedMinute);
//                //getTime = time.replace("AM", "am").replace("PM","pm");
//                if (time.equalsIgnoreCase("Start")) {
//                    String getStartTime = PlaythoraUtility.getTime(selectedHour, selectedMinute);
//                    holder.startTime.setText(t);
//                    try {
//                        detailsArray.getJSONObject(position).put("start_time", holder.startTime.getText().toString());
//                        startTimeVal = holder.startTime.getText().toString();
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                } else {
//                    String getEndTime = PlaythoraUtility.getTime(selectedHour, selectedMinute);
//                    holder.endTime.setText(t);
//                    try {
//                        detailsArray.getJSONObject(position).put("end_time", holder.endTime.getText().toString());
//                        endTimeVal = holder.endTime.getText().toString();
//                        newTime = holder.endTime.getText().toString();
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                    tempEndTime = t;
//                }
//            }
//        }, hour, minute, false);//Yes 24 hour time
//        mTimePicker.setTitle("Select " + time + " time");
//        mTimePicker.show();
//    }

    private void selectedSlot(int i, SlotsHolder holder, int position) {
        if (i == 30) {
            holder.slot30.setBackground(ContextCompat.getDrawable(context, R.drawable.slot_duration_selected));
            holder.slot60.setBackground(ContextCompat.getDrawable(context, R.drawable.background_create_ground_slot));
            holder.slot90.setBackground(ContextCompat.getDrawable(context, R.drawable.background_create_ground_slot));
            holder.slot30.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryDark));
            holder.slot60.setTextColor(ContextCompat.getColor(context, android.R.color.tab_indicator_text));
            holder.slot90.setTextColor(ContextCompat.getColor(context, android.R.color.tab_indicator_text));
            holder.selectedSlot = 30;
        } else if (i == 60) {
            holder.slot30.setBackground(ContextCompat.getDrawable(context, R.drawable.background_create_ground_slot));
            holder.slot60.setBackground(ContextCompat.getDrawable(context, R.drawable.slot_duration_selected));
            holder.slot90.setBackground(ContextCompat.getDrawable(context, R.drawable.background_create_ground_slot));
            holder.slot30.setTextColor(ContextCompat.getColor(context, android.R.color.tab_indicator_text));
            holder.slot60.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryDark));
            holder.slot90.setTextColor(ContextCompat.getColor(context, android.R.color.tab_indicator_text));
            holder.selectedSlot = 60;
        } else if (i == 90) {
            holder.slot30.setBackground(ContextCompat.getDrawable(context, R.drawable.background_create_ground_slot));
            holder.slot60.setBackground(ContextCompat.getDrawable(context, R.drawable.background_create_ground_slot));
            holder.slot90.setBackground(ContextCompat.getDrawable(context, R.drawable.slot_duration_selected));
            holder.slot30.setTextColor(ContextCompat.getColor(context, android.R.color.tab_indicator_text));
            holder.slot60.setTextColor(ContextCompat.getColor(context, android.R.color.tab_indicator_text));
            holder.slot90.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryDark));
            holder.selectedSlot = 90;
        } else {
            holder.slot30.setBackground(ContextCompat.getDrawable(context, R.drawable.background_create_ground_slot));
            holder.slot60.setBackground(ContextCompat.getDrawable(context, R.drawable.background_create_ground_slot));
            holder.slot90.setBackground(ContextCompat.getDrawable(context, R.drawable.background_create_ground_slot));
            holder.slot30.setTextColor(ContextCompat.getColor(context, android.R.color.tab_indicator_text));
            holder.slot60.setTextColor(ContextCompat.getColor(context, android.R.color.tab_indicator_text));
            holder.slot90.setTextColor(ContextCompat.getColor(context, android.R.color.tab_indicator_text));
            holder.selectedSlot = 0;
        }


        try {
            String start =   detailsArray.getJSONObject(position).getString("start_time");
            String end  =   detailsArray.getJSONObject(position).getString("end_time");
            ArrayList<String> rangeArray = getRangeArray(start,end);
            if (rangeArray.size() == 1) {
                holder.slot60.setBackgroundResource(R.drawable.slot_disable_background);
                holder.slot90.setBackgroundResource(R.drawable.slot_disable_background);
            } else if (rangeArray.size() > 1 && rangeArray.size() <= 2) {
                holder.slot90.setBackgroundResource(R.drawable.slot_disable_background);
            } else if (rangeArray.size() < 1) {
                holder.slot30.setBackgroundResource(R.drawable.background_create_ground_slot);
                holder.slot60.setBackgroundResource(R.drawable.background_create_ground_slot);
                holder.slot90.setBackgroundResource(R.drawable.background_create_ground_slot);
            }
            detailsArray.getJSONObject(position).put("duration", holder.selectedSlot);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void prefill(SlotsHolder holder, int position) {
        try {

            tempEndTime =   detailsArray.getJSONObject(position).getString("start_time");
            newTime  =   detailsArray.getJSONObject(position).getString("end_time");
            Log.e("START_PREFIL",tempEndTime+"");
            Log.e("END_PREFIL",newTime+"");

          if(tempEndTime.equalsIgnoreCase("00:30 AM")){
              tempEndTime = "12:30 AM";
          }

            if(newTime.equalsIgnoreCase("00:30 AM")){
                newTime = "12:30 AM";
            }

            if(tempEndTime.equalsIgnoreCase("00:00 AM")){
                tempEndTime = "12:00 AM";
            }

            if(newTime.equalsIgnoreCase("00:00 AM")){
                newTime = "12:00 AM";
            }
            detailsArray.getJSONObject(position).put("start_time",tempEndTime);
            detailsArray.getJSONObject(position).put("end_time",newTime);

            if(!tempEndTime.isEmpty() && !newTime.isEmpty()) {
                holder.timeRange.setText("From: " + tempEndTime + " - To: " + newTime);
            }else {
                holder.timeRange.setText("Select range");
            }
            ArrayList<String> rangeArray = getRangeArray(tempEndTime,newTime);

           if(!tempEndTime.isEmpty() && !newTime.isEmpty()) {
               holder.timeRange.setText("From: " + tempEndTime + " - To: " + newTime);
           }else {
               holder.timeRange.setText("Select range");
           }

            if (detailsArray.getJSONObject(position).getInt("duration") == 30) {
                selectedSlot(30, holder, position);
            } else if (detailsArray.getJSONObject(position).getInt("duration") == 60) {
                selectedSlot(60, holder, position);
            } else if (detailsArray.getJSONObject(position).getInt("duration") == 90) {
                selectedSlot(90, holder, position);
            } else {
                selectedSlot(0, holder, position);
            }
            holder.pricePerSlot.setText(detailsArray.getJSONObject(position).getInt("price")+"");
            holder.maxMembers.setText(detailsArray.getJSONObject(position).getInt("total_players")+"");
            detailsArray.getJSONObject(position).put("range",rangeArray);
            priceValue = detailsArray.getJSONObject(position).getInt("price");
            tempMaxMember = detailsArray.getJSONObject(position).getInt("total_players");

            holder.pricePerSlot.setSelection(holder.pricePerSlot.getText().length());
            holder.maxMembers.setSelection(holder.maxMembers.getText().length());

            if (detailsArray.getJSONObject(position).getInt("apply_for_rest_days") == 1)
                holder.checkboxApplyAll.setChecked(true);
            else
                holder.checkboxApplyAll.setChecked(false);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    @Override
    public int getItemCount() {
        return detailsArray.length();
    }

    class SlotsHolder extends RecyclerView.ViewHolder {
        TextView textTimeRange, textFrom, textTo, textPickSlotDuration, textPerSlot, textMaxMember, slot30, slot60, slot90;
        //        TextView startTime, endTime;
        TextView timeRange;
        EditText pricePerSlot, maxMembers;
        int selectedSlot;
        CheckBox checkboxApplyAll;
        ImageView deleteSlot;
        RelativeLayout itemRelative;

        SlotsHolder(View itemView) {
            super(itemView);
            slot30 = (TextView) itemView.findViewById(R.id.slot_30);
            slot60 = (TextView) itemView.findViewById(R.id.slot_60);
            slot90 = (TextView) itemView.findViewById(R.id.slot_90);
            textTimeRange = (TextView) itemView.findViewById(R.id.text_time_range);
            textFrom = (TextView) itemView.findViewById(R.id.text_from);
            textTo = (TextView) itemView.findViewById(R.id.text_to);
            textPickSlotDuration = (TextView) itemView.findViewById(R.id.text_pick_slot_duration);
            textPerSlot = (TextView) itemView.findViewById(R.id.text_per_slot);
            textMaxMember = (TextView) itemView.findViewById(R.id.text_max_member);
            deleteSlot = (ImageView) itemView.findViewById(R.id.delete_slot);
//            startTime = (TextView) itemView.findViewById(R.id.start_time);
//            endTime = (TextView) itemView.findViewById(R.id.end_time);
            pricePerSlot = (EditText) itemView.findViewById(R.id.price_per_slot);
            maxMembers = (EditText) itemView.findViewById(R.id.max_member);
            timeRange = (TextView) itemView.findViewById(R.id.time_range);
            itemRelative = (RelativeLayout) itemView.findViewById(R.id.item_relative_layout);

            checkboxApplyAll = (CheckBox) itemView.findViewById(R.id.checkbox_apply_all);

            slot30.setTypeface(PlaythoraUtility.getFont(context, RALEWAY_BOLD));
            slot60.setTypeface(PlaythoraUtility.getFont(context, RALEWAY_BOLD));
            slot90.setTypeface(PlaythoraUtility.getFont(context, RALEWAY_BOLD));
            textTimeRange.setTypeface(PlaythoraUtility.getFont(context, RALEWAY_REGULAR));
            textFrom.setTypeface(PlaythoraUtility.getFont(context, RALEWAY_REGULAR));
            textTo.setTypeface(PlaythoraUtility.getFont(context, RALEWAY_REGULAR));
            textPickSlotDuration.setTypeface(PlaythoraUtility.getFont(context, RALEWAY_REGULAR));
            textPerSlot.setTypeface(PlaythoraUtility.getFont(context, RALEWAY_REGULAR));
            textMaxMember.setTypeface(PlaythoraUtility.getFont(context, RALEWAY_REGULAR));
//            startTime.setTypeface(PlaythoraUtility.getFont(context, RALEWAY_REGULAR));
//            endTime.setTypeface(PlaythoraUtility.getFont(context, RALEWAY_REGULAR));
            pricePerSlot.setTypeface(PlaythoraUtility.getFont(context, RALEWAY_REGULAR));
            maxMembers.setTypeface(PlaythoraUtility.getFont(context, RALEWAY_REGULAR));
            checkboxApplyAll.setTypeface(PlaythoraUtility.getFont(context, RALEWAY_REGULAR));

        }
    }

    public void addNewTimeRange(TextView buttonSave) {
        Log.e("ARRAY",detailsArray.length()+"");
        Log.e("start",tempEndTime+"");
        Log.e("end",newTime+"");
        Log.e("tempMaxMember",tempMaxMember+"");
        Log.e("priceValue",priceValue+"");

        this.save = buttonSave;
        this.save.setVisibility(View.VISIBLE);

        if ((tempMaxMember!=0 && priceValue!=0 ) || detailsArray.length()==0) {

            if (mainArray.size() > 47) {
                PlaythoraUtility.showToast(context, "slots not available");
                return;
            }

            try {
                for (int i = 0; i < detailsArray.length(); i++) {
                    if (detailsArray.getJSONObject(i).getString("start_time").isEmpty() && detailsArray.getJSONObject(i).getString("end_time").isEmpty()) {
                        PlaythoraUtility.showToast(context, "Please select range");
                        return;
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
         /* if(mainArray.size()==0){
              Toast.makeText(context,"Please select slots",Toast.LENGTH_SHORT).show();
              return;
          }*/


            Log.e("AAA", tempEndTime + "");
            Log.e("BBB", newTime + "");


            arrayList = new ArrayList<>();
            String start = "", end = "";

            try {
                if (!tempEndTime.isEmpty()) {
                    JSONObject jsonObject = new JSONObject(startTimeIndex);
                    start = jsonObject.getString(tempEndTime);
                    Log.e("start_index", start);
                    // arrayList.add(start);
                }
                if (!newTime.isEmpty()) {
                    JSONObject jsonObject1 = new JSONObject(endTimeIndex);
                    end = jsonObject1.getString(newTime);
                    Log.e("end_index", end);
                    // arrayList.add(end);
                }

                if (!start.isEmpty() && !end.isEmpty()) {
                    for (int i = 0; i < myIntArray.length; i++) {
                        int ints = myIntArray[i];

                        if (Integer.parseInt(start) <= Integer.parseInt(end)) {
                            Log.e("integer", ints + "");
                            if (ints >= Integer.parseInt(start) && ints <= Integer.parseInt(end)) {
                                arrayList.add(String.valueOf(ints));
                            }
                        } else if (Integer.parseInt(start) > Integer.parseInt(end)) {
                            if (!(ints > Integer.parseInt(start)) && !(ints < Integer.parseInt(end))) {
                                arrayList.add(String.valueOf(ints));
                            }
                        }
                    }

                }


                Log.e("AAA", arrayList + "");


            } catch (JSONException e) {
                e.printStackTrace();
            }

            tempEndTime = "";
            newTime = "";

            try {
                JSONObject newItem = new JSONObject();
                newItem.put("apply_for_rest_days", 0);
                newItem.put("start_time", tempEndTime);
                newItem.put("end_time", newTime);
                newItem.put("duration", 0);
                newItem.put("price", 0);
                newItem.put("total_players", 0);
                newItem.put("range",arrayList);

                detailsArray.put(newItem);
                tempMaxMember = 0;
                priceValue = 0;

                Log.e("ADD_DETAILS_ARRAY", detailsArray.toString());
                notifyItemInserted(detailsArray.length());

            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else {
            PlaythoraUtility.showToast(context, "Please fill the previous range first");
        }
    }

    private boolean checktimings(String time, String endtime) {

        String pattern = "HH:mm";
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);

        try {
            Date date1 = sdf.parse(time);
            Date date2 = sdf.parse(endtime);

            Log.e("date1", date1 + "");
            Log.e("date2", date2 + "");

            if (date1.before(date2) || !date1.equals(date2)) {
                return true;
            } else {

                return false;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }

    public JSONArray getDetailsArray() {
        return detailsArray;
    }

    public String getDeletedSlots() {
        return deletedSlots;
    }
}