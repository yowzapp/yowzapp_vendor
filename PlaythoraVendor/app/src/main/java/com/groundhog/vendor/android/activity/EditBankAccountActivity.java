package com.groundhog.vendor.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.groundhog.vendor.android.R;
import com.groundhog.vendor.android.utils.Config;
import com.groundhog.vendor.android.utils.PlaythoraUtility;
import com.groundhog.vendor.android.utils.PreferenceManager;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

import static com.groundhog.vendor.android.utils.Config.RALEWAY_REGULAR;

/**
 * Created by Nakul on 9/14/2016.
 */
public class EditBankAccountActivity  extends AppCompatActivity {

    EditText accName,accNum,branchName,bankName,ifsc,confirmAccNum;
    TextInputLayout accNameInputLayout,accNumInputLayout,branchNameInputLayout,bankNameInputLayout,ifscInputLayout,confirmAccNumInputLayout;
    String name,num,bName,ifscCode,banName,position;
    int acctype,accountId;
    Button save;
    CheckBox checkbox;
    String bankAccount;
    int posi;
    String edtAccName,edtAccNum,edtBranchName,edtBankName,edtIfsc;
    int pos;
    PreferenceManager mPref;
    boolean isPrimary = false;
    Toolbar toolbar;
    TextView title;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_bank_account);

        toolbar = (Toolbar) findViewById(R.id.edit_account_toolbar);
        title = (TextView) findViewById(R.id.toolbar_title);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        title.setText("Edit bank account");
        title.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));


       // getSupportActionBar().setTitle(Html.fromHtml("<font color=\"white\"><small>" + "Edit bank account" + "</small></font>"));

        initializeAllComponents();
        Intent edit = getIntent();
        name = edit.getStringExtra("AccName");
        num =  edit.getStringExtra("AccNumber");
        bName = edit.getStringExtra("BranchName");
        ifscCode = edit.getStringExtra("IFSC");
        banName = edit.getStringExtra("BankName");
        acctype = edit.getIntExtra("isCheck",0);
        position = edit.getStringExtra("position");
        accountId = edit.getIntExtra("account_id",0);
        posi = Integer.parseInt(position);
        Log.e("&&&&&&&&&&&", String.valueOf(posi));
        Log.e("is_primary", String.valueOf(acctype));
        Log.e("account_id", ""+accountId);


        accName.setText(name);
        branchName.setText(bName);
        bankName.setText(banName);
        ifsc.setText(ifscCode);
        accNum.setText(num);
        confirmAccNum.setText(num);
        if(acctype==0){
            checkbox.setChecked(false);
            checkbox.setText("Primary account");
        }else {
            checkbox.setChecked(true);
            checkbox.setText("Primary account");

        }

        checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    acctype=1;
                    checkbox.setText("Primary account");
                }else {
                    acctype=0;
                    checkbox.setText("Primary account");
                }
            }
        });

        pos = accName.length();
        Editable editable = accName.getText();
        Selection.setSelection(editable,pos);

        pos = bankName.length();
        Editable editable1 = bankName.getText();
        Selection.setSelection(editable1,pos);

        pos = branchName.length();
        Editable editable2 = branchName.getText();
        Selection.setSelection(editable2,pos);

        pos = ifsc.length();
        Editable editable3 = ifsc.getText();
        Selection.setSelection(editable3,pos);

        pos = accNum.length();
        Editable editable4 = accNum.getText();
        Selection.setSelection(editable4,pos);

        pos = confirmAccNum.length();
        Editable editable5 = confirmAccNum.getText();
        Selection.setSelection(editable5,pos);



        Log.e("name", String.valueOf(accName));



        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(checkForValidation()){
                    editBankAccount();
                }

            }
        });


        accName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                accNameInputLayout.setError(null);
            }
        });

        bankName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                bankNameInputLayout.setError(null);
            }
        });

        branchName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                branchNameInputLayout.setError(null);
            }
        });

        ifsc.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                ifscInputLayout.setError(null);
            }
        });

        accNum.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                accNumInputLayout.setError(null);
            }
        });

        confirmAccNum.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                confirmAccNumInputLayout.setError(null);
            }
        });



    }

    private void editBankAccount() {
        if(PlaythoraUtility.checkInternetConnection(getApplicationContext())){
            PlaythoraUtility.showProgressDialog(EditBankAccountActivity.this);
            AsyncHttpClient httpClient = new AsyncHttpClient();
            httpClient.addHeader("accessToken",mPref.getAccessToken());

            RequestParams params = new RequestParams();
            params.add("account_name",accName.getText().toString());
            params.add("id",mPref.getId());
            params.add("bank_name",bankName.getText().toString());
            params.add("branch_name", branchName.getText().toString());
            params.add("ifsc_code",ifsc.getText().toString());
            params.add("is_primary", String.valueOf(acctype));
            params.add("account_id", String.valueOf(accountId));
            params.add("account_number",accNum.getText().toString());

            httpClient.post(Config.EDIT_BANK_ACCOUNT, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    PlaythoraUtility.hideProgressDialog();
                    String string = new String(responseBody);
                    Log.e("editBankAccount",string);

                    try {
                        JSONObject object = new JSONObject(string);
                        isPrimary = object.getJSONObject("success").getBoolean("is_primary");
                        mPref.setAccountType(isPrimary);

                        if(mPref.setAccountType(true)){
                            acctype=1;
                            isPrimary=true;
                        }else {
                            acctype=0;
                            isPrimary=false;
                        }
                        Log.e("isPrimary", String.valueOf(isPrimary));

                        Toast.makeText(EditBankAccountActivity.this,"Bank account updated successfully",Toast.LENGTH_SHORT).show();
                        Intent home = new Intent(EditBankAccountActivity.this,HomeActivity.class);
                        home.putExtra("ActivityName","bankAccount");
                        startActivity(home);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    PlaythoraUtility.hideProgressDialog();
                    try {
                        String failur = new String(responseBody);
                        Log.e("editBankAccountFailure",failur);
                        JSONObject object = new JSONObject(failur);
                        if(object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400){
                            mPref.setAccessToken("");
                            Toast.makeText(getApplicationContext(),object.getString("message"),Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(EditBankAccountActivity.this,LoginActivity.class);
                            startActivity(intent);
                            finish();
                        }else Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();

                    }catch (Exception e){
                        e.printStackTrace();
                        //Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                    }

                }
            });

        }else {
            Toast.makeText(EditBankAccountActivity.this,"check your internet connection!",Toast.LENGTH_SHORT).show();
        }
    }

    private boolean checkForValidation() {

        edtAccName = accName.getText().toString();
        edtAccNum = accNum.getText().toString();
        edtBankName = bankName.getText().toString();
        edtBranchName = branchName.getText().toString();
        edtIfsc = ifsc.getText().toString();


        if(accName.getText().toString().isEmpty()){
            accNameInputLayout.setError("Account name is required");
            accNameInputLayout.setErrorEnabled(true);
        }else {
            accNameInputLayout.setErrorEnabled(false);
        }

        if(bankName.getText().toString().isEmpty()){
            bankNameInputLayout.setError("Bank name is required");
            bankNameInputLayout.setErrorEnabled(true);
        }else {
            bankNameInputLayout.setErrorEnabled(false);
        }

        if(branchName.getText().toString().isEmpty()){
            branchNameInputLayout.setError("Branch name is required");
            branchNameInputLayout.setErrorEnabled(true);
        }else {
            branchNameInputLayout.setErrorEnabled(false);
        }

        if(ifsc.getText().toString().isEmpty()){
            ifscInputLayout.setError("ifsc code is required");
            ifscInputLayout.setErrorEnabled(true);
        }else {
            ifscInputLayout.setErrorEnabled(false);
        }

        if(accNum.getText().toString().isEmpty()){
            accNumInputLayout.setError("Account number is required");
            accNumInputLayout.setErrorEnabled(true);
        }else {
            accNumInputLayout.setErrorEnabled(false);
        }

        if(!(accNum.getText().toString().equals(confirmAccNum.getText().toString()))){
            confirmAccNumInputLayout.setError("Account number does not match");
            confirmAccNumInputLayout.setErrorEnabled(true);
        }else {
            confirmAccNumInputLayout.setErrorEnabled(false);
        }

        if(accNameInputLayout.isErrorEnabled() || bankNameInputLayout.isErrorEnabled() || branchNameInputLayout.isErrorEnabled() || ifscInputLayout.isErrorEnabled() || accNumInputLayout.isErrorEnabled() || confirmAccNumInputLayout.isErrorEnabled()){
            return false;
        }else {
            return true;
        }
    }

    private void initializeAllComponents() {
        mPref = PreferenceManager.instance(getApplicationContext());
        accName = (EditText) findViewById(R.id.accountName);
        bankName = (EditText) findViewById(R.id.bankName);
        branchName= (EditText) findViewById(R.id.branchName);
        accNum = (EditText) findViewById(R.id.accountNumber);
        ifsc = (EditText) findViewById(R.id.ifsc);
        confirmAccNum = (EditText) findViewById(R.id.confirmAccountNumber);
        checkbox = (CheckBox) findViewById(R.id.checkbox);
        save = (Button) findViewById(R.id.saveAccount);
        accNameInputLayout = (TextInputLayout) findViewById(R.id.input_layout_name);
        bankNameInputLayout = (TextInputLayout) findViewById(R.id.input_layout_bank_name);
        branchNameInputLayout = (TextInputLayout) findViewById(R.id.input_layout_branch_name);
        ifscInputLayout = (TextInputLayout) findViewById(R.id.input_layout_ifsc);
        accNumInputLayout = (TextInputLayout) findViewById(R.id.input_layout_account_number);
        confirmAccNumInputLayout = (TextInputLayout) findViewById(R.id.input_layout_confirm_account_number);
        editBankAccountUI(findViewById(R.id.editBankAccountActivity));

        accName.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        bankName.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        branchName.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        ifsc.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        checkbox.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        save.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        accNameInputLayout.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        bankNameInputLayout.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        branchNameInputLayout.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        ifscInputLayout.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        accNumInputLayout.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        confirmAccNumInputLayout.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));

    }

    private void editBankAccountUI(View viewById) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(viewById instanceof EditText)) {
            viewById.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    PlaythoraUtility.hideSoftKeyboard(EditBankAccountActivity.this);
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (viewById instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) viewById).getChildCount(); i++) {
                View innerView = ((ViewGroup) viewById).getChildAt(i);
                editBankAccountUI(innerView);
            }
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:

                onBackPressed();
                return true;
            default:
                break;

        }
        return false;
    }
}
