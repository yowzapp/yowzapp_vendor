package com.groundhog.vendor.android.utils;

import android.content.Context;
import android.content.SharedPreferences;


public class PreferenceManager {

    private static final String USER_PREF_FILE_NAME = "UserDataPrefs";
    private static PreferenceManager sInstance;
    private SharedPreferences mPreferences;
    private SharedPreferences.Editor mEditor;



    public static PreferenceManager instance(Context ctx) {
        if (sInstance == null)
            sInstance = new PreferenceManager(ctx);
        return sInstance;
    }

    private PreferenceManager(Context ctx) {
        mPreferences = ctx.getSharedPreferences(USER_PREF_FILE_NAME, Context.MODE_PRIVATE);
        mEditor = mPreferences.edit();
    }


    public void setName(String UserName) {
        mEditor.putString("Name", UserName).commit();
    }

    public String getName() {
        return mPreferences.getString("Name", "");
    }

    public void setId(String id){
        mEditor.putString("id", id).commit();
    }

    public String getId(){
        return mPreferences.getString("id", "");
    }

    public void setAccessToken(String accessToken) {
        mEditor.putString("accesstoken", accessToken).commit();
    }

    public String getAccessToken() {
        return mPreferences.getString("accesstoken", "");
    }

    public void setMobilVerified(String mobileVerified) {
        mEditor.putString("mobile_verified",mobileVerified).commit();

    }

    public String getMobilVerified() {
        return mPreferences.getString("mobile_verified","");
    }

    public void setAndroid_id(String AndroidId){
        mEditor.putString("AndroidId", AndroidId).commit();
    }

    public String getAndroid_id(){
        return  mPreferences.getString("AndroidId","");
    }


    public void setUser_id(String UserId){
        mEditor.putString("UserId", UserId).commit();
    }

    public String getUser_id(){
        return  mPreferences.getString("UserId","");
    }

    public void setGcmToken(String gcmToken){
        mEditor.putString("gcmToken", gcmToken).commit();
    }

    public String getGcmToken(){
        return  mPreferences.getString("gcmToken","");
    }

    public void setProfilePic(String profile_pic){
        mEditor.putString("profile_pic", profile_pic).commit();
    }

    public String getProfilePic(){
        return mPreferences.getString("profile_pic", "");
    }


    public void setAccountId(String id){
        mEditor.putString("account_id", id).commit();
    }

    public String getAccountId(){
        return mPreferences.getString("account_id", "");
    }


    public void setAccountName(String accountName){
        mEditor.putString("account_name", accountName).commit();
    }

    public String getAccountName(){
        return mPreferences.getString("account_name", "");
    }


    public void setBankName(String bankName){
        mEditor.putString("bank_name", bankName).commit();
    }

    public String getBankName(){
        return mPreferences.getString("bank_name", "");
    }

    public void setBranchName(String branchName){
        mEditor.putString("branch_name", branchName).commit();
    }

    public String getBranchName(){
        return mPreferences.getString("branch_name", "");
    }

    public void setIfscCode(String ifscCode){
        mEditor.putString("ifsc_code", ifscCode).commit();
    }

    public String getIfscCode(){
        return mPreferences.getString("ifsc_code", "");
    }

    public void setAccountNumber(String accountNumber){
        mEditor.putString("account_number", accountNumber).commit();
    }

    public String getAccountNumber(){
        return mPreferences.getString("account_number", "");
    }
    public boolean setAccountType(boolean accountType) {
        mEditor.putBoolean("is_primary", accountType).commit();
        return accountType;
    }

    public boolean getAccountType() {
        return mPreferences.getBoolean("is_primary",false);
    }

}
