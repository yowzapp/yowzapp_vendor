package com.groundhog.vendor.android.model;

import java.util.List;

/**
 * Created by vaishakha on 25/1/17.
 */
public class VenueListing {
    private String name;
    private int id;
    private List<VenueGroundListing> grounds;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<VenueGroundListing> getGrounds() {
        return grounds;
    }

    public void setGrounds(List<VenueGroundListing> grounds) {
        this.grounds = grounds;
    }
}
