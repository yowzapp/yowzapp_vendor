package com.groundhog.vendor.android.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.groundhog.vendor.android.R;
import com.groundhog.vendor.android.activity.ChangePasswordActivity;
import com.groundhog.vendor.android.activity.EditProfileActivity;
import com.groundhog.vendor.android.activity.ManageUsersActivity;
import com.groundhog.vendor.android.utils.PlaythoraUtility;

import static com.groundhog.vendor.android.utils.Config.RALEWAY_REGULAR;

/**
 * Created by Nakul on 9/15/2016.
 */
public class SettingsFragment extends Fragment {

     RelativeLayout editProfile,changePassword,manageUsers;
     TextView editProifleText, changePasswordText;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(com.groundhog.vendor.android.R.layout.settings_layout,container,false);

        initializeAllComponents(view);



        editProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent edit =new Intent(getActivity(), EditProfileActivity.class);
                startActivity(edit);
            }
        });

        changePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent change =new Intent(getActivity(), ChangePasswordActivity.class);
                startActivity(change);
            }
        });

        manageUsers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent users =new Intent(getActivity(), ManageUsersActivity.class);
                startActivity(users);
            }
        });

        return view;

    }

    private void initializeAllComponents(View view) {

        editProfile = (RelativeLayout) view.findViewById(com.groundhog.vendor.android.R.id.edtProfileLayout);
        changePassword= (RelativeLayout) view.findViewById(com.groundhog.vendor.android.R.id.changePasswordLayout);
        manageUsers = (RelativeLayout) view.findViewById(com.groundhog.vendor.android.R.id.manageUsersLayout);
        editProifleText = (TextView) view.findViewById(R.id.edit_profile_text);
        changePasswordText = (TextView) view.findViewById(R.id.change_password_text);

        editProifleText.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_REGULAR));
        changePasswordText.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_REGULAR));
    }
}
