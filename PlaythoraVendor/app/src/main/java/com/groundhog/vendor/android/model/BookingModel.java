package com.groundhog.vendor.android.model;

import java.util.List;

/**
 * Created by vaishakha on 25/1/17.
 */
public class BookingModel {
    private String status;
    private String date;
    private String raw_date;
    private List<VenueListing> success;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<VenueListing> getSuccess() {
        return success;
    }

    public void setSuccess(List<VenueListing> success) {
        this.success = success;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getRaw_date() {
        return raw_date;
    }

    public void setRaw_date(String raw_date) {
        this.raw_date = raw_date;
    }
}
