package com.groundhog.vendor.android.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.groundhog.vendor.android.R;
import com.groundhog.vendor.android.model.BookingSlotsModel;
import com.groundhog.vendor.android.utils.Config;
import com.groundhog.vendor.android.utils.PlaythoraUtility;
import com.groundhog.vendor.android.utils.PreferenceManager;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

import static com.groundhog.vendor.android.utils.Config.RALEWAY_REGULAR;

/**
 * Created by hemanth on 28/9/16.
 */
public class BookingSlotsActivity extends AppCompatActivity {

    RecyclerView bookingSlotsRecyclerView;
    public static ArrayList<BookingSlotsModel> bookingSlotsArrayList;
    Gson gson;
    BookingSlotsAdapter bookingSlotsAdapter;
    PreferenceManager mPref;
    String rawDate, date;
    int groundID;
    TextView emptyText;
    ArrayList<String> startArrayList = new ArrayList<>();
    ArrayList<String> endArrayList = new ArrayList<>();
    ArrayList<Integer> slotIdArrayList = new ArrayList<>();
    private boolean[] mCheckedState;
    Toolbar toolbar;
    TextView title;
    Toast toast;
    boolean localCheck;
    int slotsSize;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.booking_slots_layout);

        toolbar = (Toolbar) findViewById(R.id.main_toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        //  getSupportActionBar().setTitle(Html.fromHtml("<font color=\"white\"><small>" + "Booking slots" + "</small></font>"));
        title = (TextView) findViewById(R.id.toolbar_title);
        title.setText("Booking slots");

        title.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        mPref = PreferenceManager.instance(this);

        emptyText = (TextView) findViewById(R.id.emptyText);
        bookingSlotsRecyclerView = (RecyclerView) findViewById(R.id.booking_slots_recycler_view);
        bookingSlotsRecyclerView.setLayoutManager(new LinearLayoutManager(BookingSlotsActivity.this, LinearLayoutManager.VERTICAL, false));
        emptyText.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        rawDate = getIntent().getStringExtra("RAWDATE");
        groundID = getIntent().getIntExtra("GROUNDID", 0);
        date = getIntent().getStringExtra("DATE");
        if (getIntent().hasExtra("SLOTSTATUS")) {
            mCheckedState = getIntent().getBooleanArrayExtra("SLOTSTATUS");
            Log.e("mCheckedState", mCheckedState.toString()+"");
            localCheck = true;
        }

        Log.e("RAWDATE", rawDate);
        Log.e("DATE", date);
        Log.e("GROUNDID", groundID + "");

        loadSlots();

    }

    private void loadSlots() {

        if (PlaythoraUtility.checkInternetConnection(BookingSlotsActivity.this)) {
            PlaythoraUtility.showProgressDialog(BookingSlotsActivity.this);
            AsyncHttpClient mHttpClient = new AsyncHttpClient();
            mHttpClient.addHeader("accessToken", mPref.getAccessToken());
            Log.e("ACCESSTOKEN", mPref.getAccessToken());
            RequestParams params = new RequestParams();
            params.add("ground_id", groundID + "");
//            params.add("date",rawDate);
            Log.e("PARAMS", params + "");
            mHttpClient.post(Config.VENUE_GROUND_DETAIL + "?date=" + rawDate, params,
                    new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                            PlaythoraUtility.hideProgressDialog();
                            try {
                                String s = new String(responseBody);
                                Log.e("RESPONSESLOTS", s);
                                JSONObject object = new JSONObject(s);
                                if (object.getString("status").equalsIgnoreCase("success")) {
                                    try {
                                        JSONArray jsonArray = object.getJSONObject("success").getJSONArray("slots");
                                        slotsSize = jsonArray.length();
                                        bookingSlotsArrayList = new ArrayList<>();
                                        gson = new Gson();
                                        for (int i1 = 0; i1 < slotsSize; i1++) {
                                            bookingSlotsArrayList.add(gson.fromJson(jsonArray.get(i1).toString(), BookingSlotsModel.class));
                                        }
                                        if (bookingSlotsArrayList.isEmpty()) {
                                            emptyText.setText("No slots");
                                        } else {
                                            //maxMem = object.getJSONObject("success").getInt("max_member");
//                                            for(int i=0; i<slotsSize; i++) {
//                                                slotIdArrayList.add(bookingSlotsArrayList.get(i).getId());
//                                                Log.e("slotIdArrayList", i + bookingSlotsArrayList.get(i).getId() + "");
//                                            }
//                                            price = object.getJSONObject(s"success").getString("slot_price");

                                            emptyText.setVisibility(View.GONE);
                                            bookingSlotsAdapter = new BookingSlotsAdapter(BookingSlotsActivity.this, bookingSlotsRecyclerView, bookingSlotsArrayList);
                                            bookingSlotsRecyclerView.setAdapter(bookingSlotsAdapter);
                                        }

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                            PlaythoraUtility.hideProgressDialog();
                            try {
                                String s = new String(bytes);
                                Log.e("RESPONSE_FAIL", s);
                                JSONObject object = new JSONObject(s);
                                if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                                    Toast.makeText(BookingSlotsActivity.this, object.getString("message"), Toast.LENGTH_SHORT).show();
                                    mPref.setAccessToken("");
                                    Intent intent = new Intent(BookingSlotsActivity.this, LoginActivity.class);
                                    startActivity(intent);
                                    finish();
                                } else
                                    Toast.makeText(BookingSlotsActivity.this, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                            } catch (Exception e) {
                                e.printStackTrace();
                                // Toast.makeText(getActivity(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                            }

                        }

                    });
        } else {
            Toast.makeText(BookingSlotsActivity.this, "Not connected to internet", Toast.LENGTH_LONG).show();
        }

    }

    private class BookingSlotsAdapter extends RecyclerView.Adapter<BookingSlotsAdapter.MyViewHolder> {
        private Context mContext;
        ArrayList<BookingSlotsModel> bookingSlotsArrayList;
        RecyclerView recyclerView;
        String slotVal;
        boolean testFlag;

        public BookingSlotsAdapter(Context mContext, RecyclerView recyclerView, ArrayList<BookingSlotsModel> bookingSlotsArrayList) {
            this.mContext = mContext;
            this.bookingSlotsArrayList = bookingSlotsArrayList;
            this.recyclerView = recyclerView;
            if (!localCheck) {
                mCheckedState = new boolean[bookingSlotsArrayList.size()];
                int bookingArrayListLength = bookingSlotsArrayList.size();
                for (int i = 0; i < bookingArrayListLength; i++) {
                    mCheckedState[i] = false;
                }
            }
        }

        @Override
        public BookingSlotsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.booking_slots_item_layout, parent, false);
            return new BookingSlotsAdapter.MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final BookingSlotsAdapter.MyViewHolder holder, final int position) {

            holder.bSlots.setText(bookingSlotsArrayList.get(position).getStart_time() + "-" + bookingSlotsArrayList.get(position).getEnd_time());


            holder.bSlots.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (toast != null) {
                        toast.cancel();
                    }
                    if (bookingSlotsArrayList.get(position).is_booked()) {
                        holder.bSlots.setChecked(true);
                        toast = Toast.makeText(mContext, "This slot has been booked already, try other slot!", Toast.LENGTH_SHORT);
                        toast.show();
                    }
                    if(holder.bSlots.isChecked()){
                        mCheckedState[position] = true;
                    } else {
                        mCheckedState[position] = false;
                    }
                    /*Intent intent=new Intent();
                    intent.putExtra("Slots",bookingSlotsArrayList.get(position).getSlot());
                    setResult(1,intent);
                    finish();*/
                }
            });

            if (bookingSlotsArrayList.get(position).is_booked()) {
                holder.bSlots.setChecked(true);
                holder.bSlots.setClickable(false);
                holder.rSlot.setBackgroundDrawable(getResources().getDrawable(R.drawable.game_slots_date_selected));
            } else {
                holder.bSlots.setChecked(false);
                holder.bSlots.setClickable(true);
                holder.rSlot.setBackgroundDrawable(getResources().getDrawable(R.color.white));
            }
            Log.e("localCheck _out", localCheck+"  "+mCheckedState[position]);
            if (localCheck) {
                Log.e("localCheck _in", localCheck+"  "+mCheckedState[position]);
                if (mCheckedState[position]) {
                    holder.bSlots.setChecked(true);
//                    holder.bSlots.setClickable(false);
//                    holder.rSlot.setBackgroundDrawable(getResources().getDrawable(R.drawable.game_slots_date_selected));
                }
            }

//            holder.bSlots.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//                @Override
//                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                    Log.e("localCheck onclick", position+ "  "+"  "+mCheckedState[position]+"  "+isChecked);
//                    mCheckedState[position] = isChecked;
////                    testFlag = isChecked;
//                }
//            });

//            holder.bSlots.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    mCheckedState[position] = testFlag;
//                }
//            });

        }

        @Override
        public int getItemCount() {
            return (null != bookingSlotsArrayList ? bookingSlotsArrayList.size() : 0);
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            CheckBox bSlots;
            RelativeLayout rSlot;

            public MyViewHolder(View itemView) {
                super(itemView);
                bSlots = (CheckBox) itemView.findViewById(R.id.slotsText);
                rSlot = (RelativeLayout) itemView.findViewById(R.id.relative_slot);
            }
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_done, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.ic_slot_done:
                saveSlots();
                return true;
            default:
                break;

        }
        return false;
    }

    private void saveSlots() {
        int bookingSlotsArrayListSize = bookingSlotsArrayList.size();
        for (int i = 0; i < bookingSlotsArrayListSize; i++) {
            Log.v("localCheck _ChState ", mCheckedState[i]+"  "+i);
            if (mCheckedState[i]) {
                startArrayList.add(bookingSlotsArrayList.get(i).getStart_time());
                endArrayList.add(bookingSlotsArrayList.get(i).getEnd_time());
                slotIdArrayList.add(bookingSlotsArrayList.get(i).getId());
            }
        }
        try {
            JSONArray jsonArray = new JSONArray();
            for (int i = 0; i < startArrayList.size(); i++) {
                JSONObject object = new JSONObject();
                object.put("date", date);
                object.put("start_time", startArrayList.get(i));
                object.put("end_time", endArrayList.get(i));
                object.put("slot_id", slotIdArrayList.get(i));
                jsonArray.put(object);
            }
            Log.e("SLOTS", jsonArray.toString());
            //int totalPrice = Integer.parseInt(price)*jsonArray.length();
            Log.e("SLOTSDETAIL", " SlotsCount " + jsonArray.length()
//                            + " SlotsIdF" + slotId
            );
            if (jsonArray.length() != 0) {
                Intent intent = new Intent();
                intent.putExtra("SlotsCount", jsonArray.length());
//                intent.putExtra("SlotsId", slotId);
//                intent.putExtra("SlotsMaxMem", maxMem);
//                intent.putExtra("SlotsPrice", totalPrice);
                intent.putStringArrayListExtra("startArrayList", startArrayList);
                intent.putStringArrayListExtra("endArrayList", endArrayList);
                intent.putIntegerArrayListExtra("slotIdArrayList", slotIdArrayList);
                intent.putExtra("slotStatus", mCheckedState);
                Log.e("SlotsCount", jsonArray.length()+"");
                Log.e("startArrayList", startArrayList.toString());
                Log.e("endArrayList", endArrayList.toString());
                Log.e("slotIdArrayList", slotIdArrayList.toString());
                Log.e("slotStatus", mCheckedState.toString());
                setResult(1, intent);
                finish();
            } else
                Toast.makeText(getApplicationContext(), "Select atleast one slot", Toast.LENGTH_SHORT).show();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
