package com.groundhog.vendor.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;
import com.groundhog.vendor.android.adapter.CustomAdapter;
import com.groundhog.vendor.android.model.VenueGroundList;
import com.groundhog.vendor.android.utils.ExpandableHeightListView;
import com.groundhog.vendor.android.utils.PlaythoraUtility;
import com.groundhog.vendor.android.utils.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.groundhog.vendor.android.utils.Config.RALEWAY_REGULAR;


public class AddGroundsActivity extends AppCompatActivity {

    FloatingActionButton addGround;
    ExpandableHeightListView groundList;
    CustomAdapter customAdapter;
    PreferenceManager mPref;
    public static String groundName,groundDetail,venueId,deatailList;
    public static String allsports;
    public static String verified;
    public ArrayList<VenueGroundList> newList;
    Gson gson;
    TextView venueGroundName,verifiedText,emptyText,mGrounds;
    public static boolean value;
    Toolbar toolbar;
    TextView title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.groundhog.vendor.android.R.layout.activity_add_grounds);

        toolbar = (Toolbar) findViewById(com.groundhog.vendor.android.R.id.addGroundToolbar);
        title = (TextView) findViewById(com.groundhog.vendor.android.R.id.toolbar_title);
        toolbar.setNavigationIcon(com.groundhog.vendor.android.R.drawable.ic_back_arrow);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
      //  getSupportActionBar().setTitle(Html.fromHtml("<font color=\"white\"><small>" + "Manage grounds" + "</small></font>"));

        title.setText("Manage grounds");
        title.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));

        addGround = (FloatingActionButton) findViewById(com.groundhog.vendor.android.R.id.add_grounds);
        groundList = (ExpandableHeightListView) findViewById(com.groundhog.vendor.android.R.id.ground_list);
        venueGroundName = (TextView) findViewById(com.groundhog.vendor.android.R.id.ground_slot_name);
        verifiedText = (TextView) findViewById(com.groundhog.vendor.android.R.id.ground_verified);
        emptyText = (TextView) findViewById(com.groundhog.vendor.android.R.id.empty_text);
        mGrounds = (TextView) findViewById(com.groundhog.vendor.android.R.id.ground_text);

        venueGroundName.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        verifiedText.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        emptyText.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        mGrounds.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));

        mPref = PreferenceManager.instance(this);
        try {
            Intent intent = getIntent();
            verified=intent.getStringExtra("Verified");
            groundName=intent.getStringExtra("GroundName");
            groundDetail=intent.getStringExtra("GROUNDDETAIL");
            allsports=intent.getStringExtra("AllSports");
            venueId=intent.getStringExtra("VenueId");
        }catch (Exception e){
            e.printStackTrace();
        }

        addGround.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent addGround = new Intent(AddGroundsActivity.this, CreateVenueSlotsActivity.class);
                addGround.putExtra("Verified",verified);
                addGround.putExtra("GroundName",groundName);
                addGround.putExtra("AllSports",allsports);
                addGround.putExtra("VenueId",venueId);
                startActivityForResult(addGround,6);
            }
        });

        venueGroundName.setText(groundName);
        if(verified.equalsIgnoreCase("true")){
            String colorText= "<font color=\"#345560\">" + "Verified!" + "</font>" + " Available on platform";
            verifiedText.setText(Html.fromHtml(colorText));
//            verifiedText.setVisibility(View.VISIBLE);
        }else {
            verifiedText.setVisibility(View.GONE);
        }

        /*verified = "true";
        ground = "HSR";
        groundImage = "https://s3-ap-southeast-1.amazonaws.com/playthoraapp/vendors/venues/Hemant1480918732/20161202_120634.jpg";
        venueID = 19;*/

        newList = new ArrayList<VenueGroundList>();
        Log.e("DETAIL",groundDetail);
        if(groundDetail!=null){
            if(groundDetail.equalsIgnoreCase("[]")) {
                emptyText.setVisibility(View.VISIBLE);
                venueGroundName.setVisibility(View.GONE);
                //mGrounds.setVisibility(View.GONE);
            }else {
                emptyText.setVisibility(View.GONE);
                venueGroundName.setVisibility(View.VISIBLE);
                //mGrounds.setVisibility(View.GONE);
                gson = new Gson();
                JSONArray jsonArray = null;
                try {
                    jsonArray = new JSONArray(groundDetail);
                    for(int i=0;i<jsonArray.length();i++){
                        newList.add(gson.fromJson(jsonArray.get(i).toString(),VenueGroundList.class));
                  Log.e("response",groundDetail+"");
                    }
                /*newList.add(jsonArray.length(),new VenueSportsList("",0));
                for(int i=0;i<jsonArray.length();i++){
                    apiList.add(gson.fromJson(jsonArray.get(i).toString(),VenueSportsList.class));
                }*/
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }else {
            emptyText.setVisibility(View.VISIBLE);
            venueGroundName.setVisibility(View.GONE);
            //mGrounds.setVisibility(View.GONE);
        }
        customAdapter = new CustomAdapter(AddGroundsActivity.this, newList,"EditVenueDetailsActivity");
        groundList.setAdapter(customAdapter);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        /*if (requestCode == 5) {
            groundName = data.getStringExtra("GroundName");
            for (int i = 0; i < AddGroundPopUp.listSports.size(); i++) {
                newList.add(i, new VenueGroundList(AddGroundPopUp.listSports.get(i),AddGroundPopUp.listID.get(i), groundName));
            }
            customAdapter.refresh(newList,"EditVenueDetailsActivity");

        }*/

        try {
            if (requestCode == 6) {
                deatailList = data.getStringExtra("Detail");
                if (deatailList != null) {
                    if(deatailList.equalsIgnoreCase("[]")) {
                        emptyText.setVisibility(View.VISIBLE);
                        venueGroundName.setVisibility(View.GONE);
                        //mGrounds.setVisibility(View.GONE);
                    }else {
                        emptyText.setVisibility(View.GONE);
                        venueGroundName.setVisibility(View.VISIBLE);
                        //mGrounds.setVisibility(View.GONE);
                        gson = new Gson();
                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(deatailList);
                            newList.add(gson.fromJson(jsonObject.toString(), VenueGroundList.class));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
                customAdapter.Refresh(newList);
            }

            if(requestCode == 7) {
                deatailList = data.getStringExtra("Detail");
                int position = data.getIntExtra("position",0);
                if (deatailList != null) {
                    if(deatailList.equalsIgnoreCase("[]")) {
                        emptyText.setVisibility(View.VISIBLE);
                        venueGroundName.setVisibility(View.GONE);
                        //mGrounds.setVisibility(View.GONE);
                    }else {
                        emptyText.setVisibility(View.GONE);
                        venueGroundName.setVisibility(View.VISIBLE);
                        //mGrounds.setVisibility(View.VISIBLE);
                        Log.e("VVVVVVVVV",deatailList.toString());
                        gson = new Gson();
                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(deatailList);
                            newList.set(position, gson.fromJson(jsonObject.toString(), VenueGroundList.class));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
                customAdapter.Refresh(newList);
                customAdapter.notifyDataSetChanged();
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                //Toast.makeText(getApplicationContext(),"Back button clicked", Toast.LENGTH_SHORT).show();
                onBackPressed();
                return true;
            default:
                break;

        }
        return false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        customAdapter.notifyDataSetChanged();
        if(newList.size()==0){
            emptyText.setVisibility(View.VISIBLE);
            venueGroundName.setVisibility(View.GONE);
        } else {
            emptyText.setVisibility(View.GONE);
            venueGroundName.setVisibility(View.VISIBLE);
        }
    }

    public ArrayList<VenueGroundList> getNewList() {
        return newList;
    }
}