package com.groundhog.vendor.android.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.groundhog.vendor.android.R;
import com.groundhog.vendor.android.model.SportsListModel;
import com.groundhog.vendor.android.model.SportsSuccessList;
import com.groundhog.vendor.android.utils.Config;
import com.groundhog.vendor.android.utils.PlaythoraUtility;
import com.groundhog.vendor.android.utils.PreferenceManager;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

/**
 * Created by vaishakha on 10/9/16.
 */
public class AddGroundPopUp extends AppCompatActivity {
    ImageView cancel;
    TextView title;
    ListView sportsListView;
    Gson gson;
    Button create;
    EditText groundName;
    TextInputLayout textInputGround;
    public static ArrayList<String> listSports;
    public static ArrayList<Integer> listID;
    //SportsTypeAdapter sportsTypeAdpter;
    SportsListModel sportsListModel;
    static List<SportsSuccessList> arrayList;
    static List<SportsSuccessList> sportsList;
    String sName,gName;
    int sID,position;
    boolean value = false;
    PreferenceManager mPref;
    Gson mGson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_ground_pop_up);
        cancel = (ImageView) findViewById(R.id.image_cancel);
        sportsListView = (ListView) findViewById(R.id.sports_type);
        create = (Button) findViewById(R.id.create_btn);
        groundName = (EditText) findViewById(R.id.text_one);
        textInputGround = (TextInputLayout) findViewById(R.id.text_input_ground);
        title = (TextView) findViewById(R.id.textView);
        listSports = new ArrayList<String>();
        listID = new ArrayList<Integer>();
        mPref = PreferenceManager.instance(this);

        Intent intent = getIntent();
        if(intent.hasExtra("SportsID")){
            sName = intent.getStringExtra("SportsName");
            gName = intent.getStringExtra("GroundName");
            sID = intent.getIntExtra("SportsID",0);
            Log.e("SPORTSID", String.valueOf(sID));
            groundName.setText(gName);
            create.setText("save");
            title.setText("Edit ground");
            value = true;
            position = intent.getIntExtra("POSITION",0);
        }

        groundName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                textInputGround.setError(null);
            }
        });

        groundName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if (groundName.getText().toString().trim().isEmpty())
                        textInputGround.setError("Please provide ground name");
                    else
                        textInputGround.setErrorEnabled(false);
                }
            }
        });

        getSports();

        sportsListView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);


        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (CheckForValidation()) {
                        Log.e("Arraylist", String.valueOf(sportsList.size()));
                        String s = "";
                        for (int i = 0; i < sportsList.size(); i++) {
                            /*if(value){
                                if (sportsList.get(i).getSelected()) {
                                    listSports.set(position,sportsList.get(i).getName());
                                    listID.set(position,sportsList.get(i).getId());
                                    Log.e("Arraylist", sportsList.get(i).getName());
                                }
                            }else {*/
                                if (sportsList.get(i).getSelected()) {
                                    listSports.add(sportsList.get(i).getName());
                                    listID.add(sportsList.get(i).getId());
                                    Log.e("Arraylist", String.valueOf(sportsList.get(i).getId()));
                                }
                            //}
                        }
                    if (listSports.size() > 0) {
                        if(value){

                            Log.e("Arraylist", "LIST");
                            Intent intent = new Intent();
                            //intent.putStringArrayListExtra("ArrayListSports", listSports);
                            //intent.putIntegerArrayListExtra("ArrayListID", listID);
                            intent.putExtra("GroundName", groundName.getText().toString().trim());
                            intent.putExtra("POSITION", position);
                            setResult(2, intent);
                            finish();

                        }else {
                            //to add ground
                            Log.e("Arraylist", "LIST");
                            Intent intent = new Intent();
                            //intent.putStringArrayListExtra("ArrayListSports", listSports);
                            //intent.putIntegerArrayListExtra("ArrayListID", listID);
                            intent.putExtra("GroundName", groundName.getText().toString().trim());
                            setResult(2, intent);
                            finish();
                        }
                    }else Toast.makeText(AddGroundPopUp.this, "Please select sports type", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void getSports() {

        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            PlaythoraUtility.showProgressDialog(AddGroundPopUp.this);
            AsyncHttpClient mHttpClient = new AsyncHttpClient();
            mHttpClient.addHeader("accessToken",mPref.getAccessToken());
            mHttpClient.get(Config.SPORTS_LIST,
                    new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                            PlaythoraUtility.hideProgressDialog();
                            String s = new String(responseBody);
                            Log.e("RESPONSE", s);

                            sportsListModel = new SportsListModel();
                            mGson = new Gson();
                            sportsListModel = mGson.fromJson(s,SportsListModel.class);
                            Log.e("RESPONSE", String.valueOf(sportsListModel.getSuccess().size()));
                            if(sportsListModel.getSuccess().size()!=0){
                                Log.e("RESPONSE", String.valueOf(sportsListModel.getSuccess().size()));
                                if(sportsListView.getAdapter()==null) {
                                    Log.e("SUCCEss", String.valueOf(sportsListModel.getSuccess()));
                                    //sportsTypeAdpter = new SportsTypeAdapter(AddGroundPopUp.this, sportsListModel.getSuccess(), sID);

                                    //sportsListView.setAdapter(sportsTypeAdpter);
                                    sportsListView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

                                }
                            }

                        }
                        @Override
                        public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                            PlaythoraUtility.hideProgressDialog();

                            try {
                                String s = new String(bytes);
                                Log.e("RESPONSE_FAIL", s);
                                JSONObject object = new JSONObject(s);
                                if(object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400){
                                    mPref.setAccessToken("");
                                    Toast.makeText(getApplicationContext(),object.getString("message"),Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(AddGroundPopUp.this,LoginActivity.class);
                                    startActivity(intent);
                                    finish();
                                }else Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                            } catch (Exception e) {
                                e.printStackTrace();
                                //Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                            }

                        }

                    });
        }else{

            Toast.makeText(getApplicationContext(), "Not connected to internet", Toast.LENGTH_LONG).show();

        }


    }

    private boolean CheckForValidation() {
        if(groundName.getText().toString().trim().isEmpty()){
            textInputGround.setError("Please provide ground name");
            return false;
        }else{
            textInputGround.setErrorEnabled(false);
            return true;
        }
    }


    public static class SportsTypeAdapter extends BaseAdapter{
        Context context;
        //private boolean[] mCheckedState;
        int sID;


        public SportsTypeAdapter(Context applicationContext, List<SportsSuccessList> sportList, int sID) {
            context = applicationContext;
            sportsList = sportList;
            //mCheckedState = new boolean[sportsList.size()];
            this.sID = sID;

            for(int i=0 ; i<sportsList.size();i++){
                if(sportsList.get(i).getId() == sID) {
                    sportsList.get(i).setSelected(true);
                }else sportsList.get(i).setSelected(false);
            }

            for(int i=0 ; i<sportsList.size();i++){
                if(sportsList.get(i).getId() == sID){
                    //mCheckedState[i]=true;
                }
                //else mCheckedState[i]=false;
            }
        }

        @Override
        public int getCount() {
            return sportsList.size();
        }

        @Override
        public Object getItem(int i) {
            return sportsList.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(final int position, View view, final ViewGroup viewGroup) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            view = inflater.inflate(R.layout.sports_type_item,viewGroup,false);

            final CallHolder holder = new CallHolder();
           /* holder.sportsType = (CheckBox) view.findViewById(R.id.check_box);
            holder.sportsType.setText(sportsList.get(position).getName());
            holder.sportsType.setTag(position);

            Log.e("NAME",sportsList.get(position).getName());
            Log.e("ID", String.valueOf(sportsList.get(position).getId()));
            view.setTag(holder);
            if(sportsList.get(position).getSelected()){
                Log.e("NAME1",sportsList.get(position).getName());
                holder.sportsType.setChecked(true);
                holder.sportsType.setTextColor(Color.parseColor("#000000"));
                //mCheckedState[position]=true;
            }else{
                Log.e("NAME2",sportsList.get(position).getName());
                holder.sportsType.setChecked(false);
                holder.sportsType.setTextColor(Color.parseColor("#BBBBBB"));
               // mCheckedState[position]=false;
            }*/



            holder.sportsType.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });

            holder.sportsType.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                    Log.e("ITEMS","ITEMS");
                    if(b){
                        holder.sportsType.setTextColor(Color.parseColor("#000000"));
                        //mCheckedState[position]=true;

                    }
                    else {
                        holder.sportsType.setTextColor(Color.parseColor("#BBBBBB"));
                        //mCheckedState[position]=false;
                    }

                    for (int i = 0 ; i <sportsList.size();i++){
                        sportsList.get(i).setSelected(false);
                    }
                    sportsList.get(position).setSelected(true);
                    notifyDataSetChanged();
                    Log.e("NAME3",sportsList.get(position).getName());
                    for (int i = 0; i <sportsList.size();i++){
                        Log.e("LISTITEM",sportsList.get(i).getName());
                    }

                }
            });
            return view;
        }

        static class CallHolder{
            RelativeLayout relativePopUp;
            CheckBox sportsType;


        }
    }
}
