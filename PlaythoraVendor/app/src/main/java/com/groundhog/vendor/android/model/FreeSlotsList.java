package com.groundhog.vendor.android.model;

/**
 * Created by vaishakha on 25/1/17.
 */
public class FreeSlotsList {
    private String start_time;
    private String end_time;
    private String slot_price;
    private int slot_id;

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public String getSlot_price() {
        return slot_price;
    }

    public void setSlot_price(String slot_price) {
        this.slot_price = slot_price;
    }

    public int getSlot_id() {
        return slot_id;
    }

    public void setSlot_id(int slot_id) {
        this.slot_id = slot_id;
    }
}
