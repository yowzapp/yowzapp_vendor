package com.groundhog.vendor.android.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.groundhog.vendor.android.activity.AddBankAccountActivity;
import com.groundhog.vendor.android.activity.LoginActivity;
import com.groundhog.vendor.android.activity.ViewBankAccountActivity;
import com.groundhog.vendor.android.model.BankAccountModel;
import com.groundhog.vendor.android.model.BankAccountSuccess;
import com.groundhog.vendor.android.utils.Config;
import com.groundhog.vendor.android.utils.PlaythoraUtility;
import com.groundhog.vendor.android.utils.PreferenceManager;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import cz.msebera.android.httpclient.Header;

import static com.groundhog.vendor.android.utils.Config.RALEWAY_REGULAR;

/**
 * Created by Nakul on 9/14/2016.
 */
public class BankAccountsFragment extends Fragment {

    RecyclerView bankAccountsRecyclerView;
    FloatingActionButton addBankAccount;
    Gson gson;
    public    BankAccountAdapter bankAccountAdapter;
    public static  List<BankAccountSuccess> bankAccountArrayList;
    public static boolean IsChanged = false;
    String response;
    PreferenceManager mPref;
    BankAccountModel bankAccountModel;
    TextView noList;
    SwipeRefreshLayout swipeRefreshLayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(com.groundhog.vendor.android.R.layout.bank_accounts_layout,container,false);
        mPref = PreferenceManager.instance(getActivity());

        bankAccountsRecyclerView = (RecyclerView) view.findViewById(com.groundhog.vendor.android.R.id.bank_accounts_recycler_view);
        noList = (TextView) view.findViewById(com.groundhog.vendor.android.R.id.emptyBankList);
        noList.setTypeface(PlaythoraUtility.getFont(getActivity(),RALEWAY_REGULAR));
        bankAccountsRecyclerView.setHasFixedSize(true);
        bankAccountsRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        addBankAccount = (FloatingActionButton) view.findViewById(com.groundhog.vendor.android.R.id.add_account);

      //  bankAccountRecycler();

        addBankAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent addAccount = new Intent(getActivity(),AddBankAccountActivity.class);
                startActivity(addAccount);

            }
        });

        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(com.groundhog.vendor.android.R.id.swipeContainer_bank_account);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                bankAccountRecycler();
            }
        });

        return view;
    }

    private void bankAccountRecycler() {

        if(PlaythoraUtility.checkInternetConnection(getActivity())){
               PlaythoraUtility.showProgressDialog(getActivity());
                AsyncHttpClient httpClient = new AsyncHttpClient();
                httpClient.addHeader("accessToken",mPref.getAccessToken());

                httpClient.get(Config.BANK_ACCOUNT_LIST, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        try {
                       PlaythoraUtility.hideProgressDialog();
                        swipeRefreshLayout.setRefreshing(false);
                            String s = new String(responseBody);
                            Log.e("bankList",s);

                            bankAccountModel = new BankAccountModel();
                            gson = new Gson();
                            bankAccountModel = gson.fromJson(s,BankAccountModel.class);
                            if(bankAccountModel.getSuccess().size()!=0){
                                if(bankAccountsRecyclerView.getAdapter()==null){
                                    bankAccountAdapter = new BankAccountAdapter(getActivity(),bankAccountModel.getSuccess());
                                    bankAccountsRecyclerView.setAdapter(bankAccountAdapter);
                                    bankAccountAdapter.notifyDataSetChanged();

                                }
                            }else {
                              //  Toast.makeText(getActivity(),"Add Bank Account",Toast.LENGTH_SHORT).show();
                                noList.setVisibility(View.VISIBLE);
                            }

                        }catch (Exception e){
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        PlaythoraUtility.hideProgressDialog();
                        swipeRefreshLayout.setRefreshing(false);
                        try {
                            String s = new String(responseBody);
                            Log.e("RESPO",s);
                            JSONObject object = new JSONObject(s);
                            if(object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400){
                                mPref.setAccessToken("");
                                Toast.makeText(getActivity(),object.getString("message"),Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getActivity(),LoginActivity.class);
                                startActivity(intent);
                                getActivity().finish();
                            }else Toast.makeText(getActivity(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                        }catch (JSONException e){
                            e.printStackTrace();
                            //Toast.makeText(getActivity(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                        }

                    }
                });
            }else {

            Toast.makeText(getContext(),"check your internet connection!",Toast.LENGTH_SHORT).show();

        }

    }


    private class BankAccountAdapter extends RecyclerView.Adapter<BankAccountAdapter.MyViewHolder>{
        private Context mContext;
        List<BankAccountSuccess> bankAccountArrayList;
        RecyclerView recyclerView;

        public BankAccountAdapter(Context mContext, List<BankAccountSuccess> bankAccountArrayList) {
            this.mContext = mContext;
            this.bankAccountArrayList = bankAccountArrayList;
            this.recyclerView = recyclerView;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View itemView = LayoutInflater.from(parent.getContext()).inflate(com.groundhog.vendor.android.R.layout.bank_account_item_layout,parent,false);


            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, final int position) {
            holder.bankName.setText(bankAccountArrayList.get(position).getBank_name());
            holder.bankAccountNumber.setText(bankAccountArrayList.get(position).getAccount_number());

            if(bankAccountArrayList.get(position).is_primary()==1){
               holder.accountType.setText("Primary account");
            }else {
                holder.accountType.setText("Secondary account");
            }

            final View itemView = holder.itemView;

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent viewAccount = new Intent(getActivity(), ViewBankAccountActivity.class);
                    viewAccount.putExtra("position",String.valueOf(position));
                    viewAccount.putExtra("AccName",bankAccountArrayList.get(position).getAccount_name());
                    viewAccount.putExtra("AccNumber",bankAccountArrayList.get(position).getAccount_number());
                    viewAccount.putExtra("BankName",bankAccountArrayList.get(position).getBank_name());
                    viewAccount.putExtra("BranchName",bankAccountArrayList.get(position).getBranch_name());
                    viewAccount.putExtra("IFSC",bankAccountArrayList.get(position).getIfsc_code());
                    viewAccount.putExtra("isCheck",bankAccountArrayList.get(position).is_primary());
                    viewAccount.putExtra("id",bankAccountArrayList.get(position).getId());
                    Log.e("id", String.valueOf(bankAccountArrayList.get(position).getId()));

                    startActivity(viewAccount);

                }
            });

        }

        @Override
        public int getItemCount() {
            return (null != bankAccountArrayList ? bankAccountArrayList.size() : 0);
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView bankName,bankAccountNumber,accountType;
            public MyViewHolder(View itemView) {
                super(itemView);
                bankName = (TextView) itemView.findViewById(com.groundhog.vendor.android.R.id.bank_name);
                bankAccountNumber = (TextView) itemView.findViewById(com.groundhog.vendor.android.R.id.bank_account_number);
                accountType = (TextView) itemView.findViewById(com.groundhog.vendor.android.R.id.accountType);

                bankName.setTypeface(PlaythoraUtility.getFont(getActivity(),RALEWAY_REGULAR));
                accountType.setTypeface(PlaythoraUtility.getFont(getActivity(),RALEWAY_REGULAR));

            }
        }


        public void refresh(List<BankAccountSuccess> bankAccountArrayList){
            this.bankAccountArrayList=bankAccountArrayList;
            notifyDataSetChanged();

        }
    }


    @Override
    public void onResume() {
        super.onResume();
     //   PlaythoraUtility.hideProgressDialog();
        bankAccountRecycler();

        /*if(bankAccountsRecyclerView.getAdapter()==null){
          bankAccountRecycler();
        }else {
            bankAccountAdapter.refresh(bankAccountArrayList);
        }*/


    }
}
