package com.groundhog.vendor.android.model;

import java.util.List;

/**
 * Created by vaishakha on 1/2/17.
 */
public class ReportList {
    private int venue_id;
    private String venue_name;
    private List<GraphList> graph;
    private String date;
    private List<GraphList.Grounds> pie;


    public int getVenue_id() {
        return venue_id;
    }

    public void setVenue_id(int venue_id) {
        this.venue_id = venue_id;
    }

    public String getVenue_name() {
        return venue_name;
    }

    public void setVenue_name(String venue_name) {
        this.venue_name = venue_name;
    }

    public List<GraphList> getGraph() {
        return graph;
    }

    public void setGraph(List<GraphList> graph) {
        this.graph = graph;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<GraphList.Grounds> getPie() {
        return pie;
    }

    public void setPie(List<GraphList.Grounds> pie) {
        this.pie = pie;
    }
}
