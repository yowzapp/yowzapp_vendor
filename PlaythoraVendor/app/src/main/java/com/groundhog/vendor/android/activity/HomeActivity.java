package com.groundhog.vendor.android.activity;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.groundhog.vendor.android.R;
import com.groundhog.vendor.android.adapter.NavigationDrawerAdapter;
import com.groundhog.vendor.android.fragment.BankAccountsFragment;
import com.groundhog.vendor.android.fragment.BookingFragment;
import com.groundhog.vendor.android.fragment.NavigationDrawerFragment;
import com.groundhog.vendor.android.fragment.ReportFragment;
import com.groundhog.vendor.android.fragment.SettingsFragment;
import com.groundhog.vendor.android.fragment.VenueFragment;
import com.groundhog.vendor.android.utils.Config;
import com.groundhog.vendor.android.utils.PlaythoraUtility;
import com.groundhog.vendor.android.utils.PreferenceManager;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

import static com.groundhog.vendor.android.utils.Config.RALEWAY_REGULAR;

/**
 * Created by vaishakha on 8/9/16.
 */
public class HomeActivity extends AppCompatActivity implements NavigationDrawerFragment.FragmentDrawerListener {
    DrawerLayout mDrawerLayout;
    NavigationDrawerFragment navigationDrawerFragment;
    public static Toolbar mToolbar;
    View view;
    String title;
    int notification_count;
    String activityName = "home";
    PreferenceManager mPref;
    SweetAlertDialog dialog;
    TextView notificationCount = null;
    View menu_notification;
    Context mContext;
    boolean exit = false;
    static int previousPos=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_layout);
        mPref = PreferenceManager.instance(getApplicationContext());

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        view = (View) findViewById(R.id.view_toolbar);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);


        if (!mPref.getMobilVerified().equalsIgnoreCase("true")) {
            Intent i = new Intent(HomeActivity.this, VerificationActivity.class);
            i.putExtra("activity", "LoginActivity");
            startActivity(i);
            finish();
        }

        if (android.os.Build.VERSION.SDK_INT >= 21) {
            mToolbar.setElevation(10);
            view.setVisibility(View.GONE);

        } else {
            view.setVisibility(View.VISIBLE);
        }


        navigationDrawerFragment = (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        navigationDrawerFragment.setUp(R.id.fragment_navigation_drawer, mDrawerLayout, mToolbar);
        navigationDrawerFragment.setDrawerListener(this);


        try {
            Intent i = getIntent();
            activityName = i.getStringExtra("ActivityName");
            Log.e("bankAccount", activityName + "");
            if (null != activityName) {
                if (activityName.equals("bankAccount")) {
                    displayView(3);
                    NavigationDrawerAdapter.selected_item = 3;
                    NavigationDrawerFragment.CallRefersh();
                    Log.e("@@@@@@@@", activityName);
                } else if (activityName.equals("Settings")) {
                    displayView(4);
                    NavigationDrawerAdapter.selected_item = 4;
                    NavigationDrawerFragment.CallRefersh();
                    Log.e("######", activityName);
                } else if (activityName.equals("newBooking")) {
                    displayView(0);
                    NavigationDrawerAdapter.selected_item = 0;
                    NavigationDrawerFragment.CallRefersh();
                } else if (activityName.equals("CriticalActionsActivity")) {
                    displayView(1);
                    NavigationDrawerAdapter.selected_item = 1;
                    NavigationDrawerFragment.CallRefersh();
                } else {
                    displayView(0);
                    NavigationDrawerAdapter.selected_item = 0;
                    NavigationDrawerFragment.CallRefersh();
                }
            } else {
                displayView(0);
                NavigationDrawerAdapter.selected_item = 0;
                NavigationDrawerFragment.CallRefersh();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


        if (mToolbar != null) {
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    // if (appPreferences.getUserLoggedStatus()) {
                    if ((mDrawerLayout.isDrawerOpen(GravityCompat.START))) {
                        mDrawerLayout.closeDrawer(GravityCompat.START);
                    } else {
                        mDrawerLayout.openDrawer(GravityCompat.START);
                    }

                }
            });

        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        getUserDetail();
    }

    private void getUserDetail() {
        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            AsyncHttpClient httpClient = new AsyncHttpClient();
            httpClient.addHeader("accessToken", mPref.getAccessToken());
            Log.e("accessToken", mPref.getAccessToken());
            httpClient.get(Config.GET_USER, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    try {
                        String s = new String(responseBody);
                        Log.e("userDetail", s);

                        JSONObject object = new JSONObject(s);
                        if (object.getInt("statusCode") == 200) {

                            int id = object.getJSONObject("success").getInt("id");
                            String nameValue = object.getJSONObject("success").getString("name");
                            String profile_pic = object.getJSONObject("success").getString("profile_pic");
                            String email = object.getJSONObject("success").getString("email");
                            String mobile = object.getJSONObject("success").getString("mobile");
                            String mobileVerified = object.getJSONObject("success").getBoolean("mobile_verified") + "";
                            String pending_notification = object.getJSONObject("success").getInt("pending_notification") + "";
                            notification_count = object.getJSONObject("success").getInt("notification_count");

                            Log.e("id", id + "");
                            Log.e("name", nameValue);
                            Log.e("profile_pic", profile_pic);
                            Log.e("email", email);
                            Log.e("mobile", mobile);
                            Log.e("mobile_verified", mobileVerified);
                            Log.e("pending_notification", pending_notification);
                            Log.e("notification_count", notification_count + "");

                            if (notification_count > 0) {
                                notificationCount.setVisibility(View.VISIBLE);
                                notificationCount.setText(notification_count + "");
                            } else {
                                notificationCount.setVisibility(View.GONE);
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();

                    }

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

                    try {
                        String s = new String(responseBody);
                        Log.e("failure", s);
                        JSONObject object = new JSONObject(s);

//                        if(object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400){
                        mPref.setAccessToken("");
                        Toast.makeText(HomeActivity.this, object.getString("message"), Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
                        startActivity(intent);
                        finishAffinity();

//                        }else {
//                            Toast.makeText(HomeActivity.this, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
//                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        try {
                            String s = new String(responseBody);
                            Log.e("failure", s);
                            JSONObject object = new JSONObject(s);
                            Toast.makeText(HomeActivity.this, object.getString("message"), Toast.LENGTH_SHORT).show();
                        } catch (Exception e1) {
                            e1.printStackTrace();
                            Toast.makeText(HomeActivity.this, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                        }

                    }

                }
            });


        } else {
            Toast.makeText(HomeActivity.this, "Check your internet connections!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onDrawerItemSelected(View view, int position) {
        displayView(position);
    }


    private void displayView(int position) {
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = new BookingFragment();
                title = getString(R.string.nav_item_booking);
                previousPos=position;
                break;
            case 1:
                fragment = new VenueFragment();
                title = getString(R.string.nav_item_venue);
                previousPos=position;
                break;
            case 2:
                fragment = new ReportFragment();
                title = getString(R.string.nav_item_report);
                previousPos=position;
                break;
            case 3:
                fragment = new BankAccountsFragment();
                title = getString(R.string.nav_item_bankaccount);
                previousPos=position;
                break;
            case 4:
                fragment = new SettingsFragment();
                title = getString(R.string.nav_item_settings);
                previousPos=position;
                break;
            case 5:
               /* fragment = new LogoutFragment();
                title = getString(R.string.nav_item_logout);*/
                showDialog();
                break;
            case 6:
                break;

            case 7:
                Intent intent = new Intent(Intent.ACTION_SENDTO);
               /* gmailIntent.setAction(Intent.ACTION_SEND);
                gmailIntent.setClassName(InviteFriendsActivity.this,"com.google.android.gm");
                gmailIntent.putExtra(Intent.EXTRA_EMAIL, "playthora.com");
                gmailIntent.setType("text/plain");*/

                intent.setType("text/html");
                intent.setData(Uri.parse("mailto:feedback@DesignString.co")); // only email apps should handle this
                intent.putExtra(Intent.EXTRA_EMAIL, "feedback@DesignString.co");
                intent.putExtra(Intent.EXTRA_SUBJECT, "DesignString feedback");
                intent.putExtra(Intent.EXTRA_TEXT, "Hi there");

                // startActivity(Intent.createChooser(intent, "Send Email"));

                try {
                    startActivity(intent);
                    //finish();
                } catch (ActivityNotFoundException ex) {
                    ex.printStackTrace();
                }

                /*fragment = new SendFeedBackFragment();
                title = getString(R.string.nav_item_send_feedback);*/
                break;

            default:
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.commit();
            if (getSupportActionBar() != null) {
                getSupportActionBar().setTitle(title);
            }

        }
    }

    private void showDialog() {

        final Dialog dialog = new Dialog(HomeActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.logout_dialog);

        Button yes = (Button) dialog.findViewById(R.id.yes_btn_logout);
        Button no = (Button) dialog.findViewById(R.id.no_btn_logout);
        TextView logoutText = (TextView) dialog.findViewById(R.id.logout_text);

        yes.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        no.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        logoutText.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
//                NavigationDrawerAdapter.selected_item = previousPos;
//                NavigationDrawerFragment.CallRefersh();
//                displayView(previousPos);
            }
        });
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
                dialog.dismiss();
            }
        });

        dialog.show();

    }

    private void logout() {

        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            PlaythoraUtility.showProgressDialog(HomeActivity.this);
            AsyncHttpClient httpClient = new AsyncHttpClient();
            httpClient.addHeader("accessToken", mPref.getAccessToken());

            RequestParams params = new RequestParams();
            params.add("id", mPref.getId());
            params.add("device_id", mPref.getAndroid_id());

            httpClient.post(Config.LOGOUT, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                    PlaythoraUtility.hideProgressDialog();
                    try {
                        String s = new String(responseBody);
                        Log.e("RESPO", s);
                        mPref.setAccessToken("");
                        mPref.setId("");
                        Intent logout = new Intent(HomeActivity.this, LoginActivity.class);
                        startActivity(logout);
                        finish();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

                    PlaythoraUtility.hideProgressDialog();

                    try {
                        String s = new String(responseBody);
                        Log.e("logoutFail", s);
                        JSONObject object = new JSONObject(s);
                        if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                            mPref.setAccessToken("");
                            Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
                            startActivity(intent);
                            finish();
                        } else
                            Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                        //Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } else {
            Toast.makeText(HomeActivity.this, "check your internet connection", Toast.LENGTH_LONG).show();
        }
    }

//    @Override
//    public void onBackPressed() {
//        if(title.equals(getString(R.string.nav_item_booking))){
//            super.onBackPressed();
//        }else{
//            NavigationDrawerAdapter.selected_item=0;
//            NavigationDrawerFragment.CallRefersh();
//
//            displayView(0);
//
//        }
//    }

    int backButtonCount;

    @Override
    public void onBackPressed() {
        if (navigationDrawerFragment.getmDrawerLayout().isDrawerOpen(navigationDrawerFragment.getContainerView()))
            navigationDrawerFragment.getmDrawerLayout().closeDrawer(navigationDrawerFragment.getContainerView());
        else {
            if (title.equals(getString(R.string.nav_item_booking))) {
                if (exit) {
                    finish(); // finish activity
                } else {
                    Toast.makeText(this, "Press back button again to exit", Toast.LENGTH_SHORT).show();
                    exit = true;
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            exit = false;
                        }
                    }, 3000);// 3 sec
                }
            } else {
                NavigationDrawerAdapter.selected_item = 0;
                NavigationDrawerFragment.CallRefersh();
                displayView(0);
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);


        View menu_notification = menu.findItem(R.id.menu_notification).getActionView();
        notificationCount = (TextView) menu_notification.findViewById(R.id.txt_notification_count);

       /* if(notification_count>0){
            notificationCount.setVisibility(View.VISIBLE);
            notificationCount.setText(notification_count+"");
        }else {
            notificationCount.setVisibility(View.GONE);


        }*/

        menu_notification.findViewById(R.id.icNotifications).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                notificationCount.setText("");
                notificationCount.setVisibility(View.GONE);
                startActivity(new Intent(HomeActivity.this, Notification.class));
            }
        });


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.home:
                onBackPressed();
                return true;


          /*  case R.id.menu_notification:
                Intent notification = new Intent(HomeActivity.this,Notification.class);
                startActivity(notification);
                return true;*/

            default:
                break;
        }


        return false;


    }
}
