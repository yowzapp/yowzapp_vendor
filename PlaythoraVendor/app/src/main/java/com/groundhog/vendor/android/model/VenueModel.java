package com.groundhog.vendor.android.model;

/**
 * Created by Nakul on 9/21/2016.
 */
public class VenueModel {

    private String id;
    private String groundName;

    public VenueModel(String id, String groundName) {
        this.id = id;
        this.groundName = groundName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGroundName() {
        return groundName;
    }

    public void setGroundName(String groundName) {
        this.groundName = groundName;
    }
}
