package com.groundhog.vendor.android.model;

/**
 * Created by vaishakha on 15/9/16.
 */
public class ImageModel {
    private int id;
    private String image;
    private String created_at;
    private String updated_at;
    private int venue_id;

    public ImageModel(String image) {
        this.image = image;
    }

    public ImageModel(String image, String updated_at, String created_at, int id) {
        this.image = image;
        this.updated_at = updated_at;
        this.created_at = created_at;
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public int getVenue_id() {
        return venue_id;
    }

    public void setVenue_id(int venue_id) {
        this.venue_id = venue_id;
    }
}
