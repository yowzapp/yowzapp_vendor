package com.groundhog.vendor.android.utils;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by vaishakha on 8/12/16.
 */
public class HackyProblematicViewGroup extends ViewPager {

    public HackyProblematicViewGroup(Context context) {
        super(context);
    }

    public HackyProblematicViewGroup(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public HackyProblematicViewGroup(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context) {
        //do stuff that was in your original constructor...
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        try {
            return super.onInterceptTouchEvent(ev);
        } catch (IllegalArgumentException e) {
            //uncomment if you really want to see these errors
            e.printStackTrace();
            return false;
        }
    }
}
