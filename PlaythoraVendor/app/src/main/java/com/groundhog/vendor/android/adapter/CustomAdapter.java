package com.groundhog.vendor.android.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.groundhog.vendor.android.R;
import com.groundhog.vendor.android.activity.AddGroundsActivity;
import com.groundhog.vendor.android.activity.EditVenueSlotsActivity;
import com.groundhog.vendor.android.activity.ManageGroundActivity;
import com.groundhog.vendor.android.model.VenueGroundList;
import com.groundhog.vendor.android.utils.Config;
import com.groundhog.vendor.android.utils.PlaythoraUtility;
import com.groundhog.vendor.android.utils.PreferenceManager;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

import static com.groundhog.vendor.android.utils.Config.RALEWAY_REGULAR;
import static com.groundhog.vendor.android.utils.Config.RALEWAY_SEMIBOLD;

/**
 * Created by vaishakha on 10/9/16.
 */
public class CustomAdapter extends BaseAdapter {
    private static LayoutInflater inflater = null;
    private Context context;
    public static ArrayList<VenueGroundList> apiList, newlist;

    String activityname;
    PreferenceManager mPref;
    Gson gson;
    int groundNameLine = 1;

    public CustomAdapter(Context context, ArrayList<VenueGroundList> newlist, String activityname) {
        mPref = PreferenceManager.instance(context);
        this.context = context;
        Log.e("api", newlist + "");
        this.newlist = new ArrayList<>();
        this.apiList = new ArrayList<>();
        // this.apiList = newlist;
        this.activityname = activityname;
        for (int i = 0; i < newlist.size(); i++) {
            this.newlist.add(i, newlist.get(i));
            this.apiList.add(i, newlist.get(i));
        }
    }

    public void Refresh(ArrayList<VenueGroundList> newlist) {
        this.newlist = newlist;
        this.apiList = newlist;
        notifyDataSetChanged();
        Log.e("RESULT", "RESULT3");
        Log.e("RESULT", String.valueOf(this.apiList));
    }

    @Override
    public int getCount() {
        return apiList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    /*public void refresh(ArrayList<VenueGroundList> newlist, String activityname) {
        //this.newlist = newlist;
        this.newlist = new ArrayList<>();
        this.apiList = new ArrayList<>();
        for (int i = 0;i<newlist.size();i++){
            this.newlist.add(i,newlist.get(i));
            this.apiList.add(i,newlist.get(i));
        }
        this.activityname = activityname;
        notifyDataSetChanged();
    }*/


    public class Holder {
        RelativeLayout addGroundRelative, listItemRelative;
        TextView groundName, sportsName, starttime, member, price, sports, timings, mem, priceText;
        ImageView disabled, edit, sports_icon;
        LinearLayout linearLayout;
    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        View rowView;

        final Holder holder = new Holder();
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        rowView = inflater.inflate(com.groundhog.vendor.android.R.layout.list_item_layout, null);

        holder.addGroundRelative = (RelativeLayout) rowView.findViewById(com.groundhog.vendor.android.R.id.add_ground_relative);
        holder.groundName = (TextView) rowView.findViewById(com.groundhog.vendor.android.R.id.ground);
        holder.sportsName = (TextView) rowView.findViewById(com.groundhog.vendor.android.R.id.sports_name);
        holder.sports_icon = (ImageView) rowView.findViewById(com.groundhog.vendor.android.R.id.sports_icon);
        holder.disabled = (ImageView) rowView.findViewById(com.groundhog.vendor.android.R.id.clear_item);
        holder.listItemRelative = (RelativeLayout) rowView.findViewById(com.groundhog.vendor.android.R.id.list_item_relative);
        holder.edit = (ImageView) rowView.findViewById(com.groundhog.vendor.android.R.id.edit);
        holder.starttime = (TextView) rowView.findViewById(com.groundhog.vendor.android.R.id.sports_starttime);
        holder.member = (TextView) rowView.findViewById(com.groundhog.vendor.android.R.id.sports_member);
        holder.price = (TextView) rowView.findViewById(com.groundhog.vendor.android.R.id.sports_price);
        holder.linearLayout = (LinearLayout) rowView.findViewById(com.groundhog.vendor.android.R.id.ground_linear);

        holder.sports = (TextView) rowView.findViewById(com.groundhog.vendor.android.R.id.sports);
        holder.timings = (TextView) rowView.findViewById(com.groundhog.vendor.android.R.id.timings);
        holder.mem = (TextView) rowView.findViewById(com.groundhog.vendor.android.R.id.memberText);
        holder.priceText = (TextView) rowView.findViewById(com.groundhog.vendor.android.R.id.price);

        holder.groundName.setSingleLine(false);
        holder.groundName.setEllipsize(TextUtils.TruncateAt.END);
        holder.groundName.setLines(groundNameLine);
        holder.groundName.setTypeface(PlaythoraUtility.getFont(context, RALEWAY_SEMIBOLD));
        holder.sportsName.setTypeface(PlaythoraUtility.getFont(context, RALEWAY_REGULAR));
        holder.starttime.setTypeface(PlaythoraUtility.getFont(context, RALEWAY_REGULAR));
        holder.member.setTypeface(PlaythoraUtility.getFont(context, RALEWAY_REGULAR));
        holder.price.setTypeface(PlaythoraUtility.getFont(context, RALEWAY_REGULAR));
        holder.sports.setTypeface(PlaythoraUtility.getFont(context, RALEWAY_REGULAR));
        holder.timings.setTypeface(PlaythoraUtility.getFont(context, RALEWAY_REGULAR));
        holder.mem.setTypeface(PlaythoraUtility.getFont(context, RALEWAY_REGULAR));
        holder.priceText.setTypeface(PlaythoraUtility.getFont(context, RALEWAY_REGULAR));


        try {
//            String[] sTime = apiList.get(position).getStart_time().split(":");
//            String[] eTime = apiList.get(position).getEnd_time().split(":");
            holder.sportsName.setText(apiList.get(position).getSports_name());
            Log.e("sportsname", apiList.get(position).getSports_name() + "");
            holder.groundName.setText(apiList.get(position).getName());
            Picasso.with(this.context).load(apiList.get(position).getSports_image()).into(holder.sports_icon);
            Log.e("img", apiList.get(position).getSports_image() + "");
            /*holder.starttime.setText(PlaythoraUtility.getTimeInaaFormat(Integer.parseInt(sTime[0]),Integer.parseInt(sTime[1]))
                    + " - " +PlaythoraUtility.getTimeInaaFormat(Integer.parseInt(eTime[0]),Integer.parseInt(eTime[1])));*/
            //startTime.setText(PlaythoraUtility.getTimeInaaFormat(Integer.parseInt(sTime[0]),Integer.parseInt(sTime[1])));
            //holder.member.setText(""+newlist.get(position).getMax_member());
            // holder.price.setText(" AED " + newlist.get(position).getSlot_price());
            if (apiList.get(position).is_disabled()) {
                holder.disabled.setBackgroundResource(com.groundhog.vendor.android.R.drawable.ic_toggle_white);
                holder.linearLayout.setBackgroundColor(Color.parseColor("#e9e9e9"));
            } else {
                holder.disabled.setBackgroundResource(R.drawable.ic_toggle_orange);
                holder.linearLayout.setBackgroundColor(Color.parseColor("#ffffff"));
            }
            holder.listItemRelative.setVisibility(View.VISIBLE);
            holder.addGroundRelative.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }

        holder.disabled.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (apiList.get(position).is_disabled()) {
                    showDialog(position, false);
                } else {
                    showDialog(position, true);
                }
            }
        });

        /*holder.clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               newlist.remove(position);
                apiList.remove(position);
                notifyDataSetChanged();
                *//* AlertDialog.Builder adb=new AlertDialog.Builder(context);
                adb.setTitle("Delete?");
                adb.setMessage("Are you sure you want to delete " );
                final int positionToRemove = position;
                adb.setNegativeButton("Cancel", null);
                adb.setPositiveButton("Ok", new AlertDialog.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        newlist.remove(positionToRemove);
                        notifyDataSetChanged();
                    }});
                adb.show();*//*
            }
        });
*/
       /* holder.addGroundRelative.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, AddGroundPopUp.class);
                    ((Activity) context).startActivityForResult(intent,5);
                }
            });
        */
        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, EditVenueSlotsActivity.class);
                intent.putExtra("POSITION", position);
                ((Activity) context).startActivityForResult(intent, 7);
            }
        });


            holder.listItemRelative.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!apiList.get(position).is_disabled()) {
                        Intent intent = new Intent(context, ManageGroundActivity.class);
                        intent.putExtra("ground_id", newlist.get(position).getId());
                        intent.putExtra("ground_name", newlist.get(position).getName());
                        intent.putExtra("sports_icon", newlist.get(position).getSports_image());
                        intent.putExtra("sports_type", newlist.get(position).getSports_name());
                        intent.putExtra("POSITION", position);
                        Log.e("sports_icon", newlist.get(position).getSports_image());
                        Log.e("ground_name", newlist.get(position).getName());
                        Log.e("ground_id", newlist.get(position).getId() + "");

                        context.startActivity(intent);
                    }
                }
            });

        return rowView;
    }


    private void showDialog(final int position, final boolean b) {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(com.groundhog.vendor.android.R.layout.disable_ground);

        TextView text = (TextView) dialog.findViewById(com.groundhog.vendor.android.R.id.confrm_text_two);
        Button yes = (Button) dialog.findViewById(com.groundhog.vendor.android.R.id.yes_btn_two);
        Button no = (Button) dialog.findViewById(com.groundhog.vendor.android.R.id.no_btn_two);

        text.setTypeface(PlaythoraUtility.getFont(context, RALEWAY_REGULAR));
        yes.setTypeface(PlaythoraUtility.getFont(context, RALEWAY_REGULAR));
        no.setTypeface(PlaythoraUtility.getFont(context, RALEWAY_REGULAR));

        if (b) {
            text.setText("Are you sure you want to disable the\nground?");
        } else text.setText("Are you sure you want to enable the\nground?");
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editSlot(position, b);
                dialog.dismiss();
            }
        });

        dialog.show();

    }

    private void editSlot(final int position, boolean b) {

        if (PlaythoraUtility.checkInternetConnection(context)) {
            PlaythoraUtility.showProgressDialog(context);
            AsyncHttpClient mHttpClient = new AsyncHttpClient();
            mHttpClient.addHeader("accessToken", mPref.getAccessToken());
            RequestParams params = new RequestParams();
            params.add("ground_id", String.valueOf(apiList.get(position).getId()));
            params.add("is_disabled", String.valueOf(b));
            Log.e("PARAMS", String.valueOf(params));
            mHttpClient.post(Config.EDIT_GROUND, params,
                    new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                            PlaythoraUtility.hideProgressDialog();
                            try {
                                String s = new String(responseBody);
                                Log.e("RESPONSE", s);
                                JSONObject object = new JSONObject(s);
                                if (object.getString("status").equalsIgnoreCase("success")) {
                                    AddGroundsActivity.value = true;
                                    String status = object.getJSONObject("success").toString();
                                    if (status != null) {
                                        gson = new Gson();
                                        JSONObject jsonObject = null;
                                        try {
                                            jsonObject = new JSONObject(status);
                                            apiList.get(position).setIs_disabled(object.getJSONObject("success").getBoolean("is_disabled"));
                                            //newlist.set(position,gson.fromJson(jsonObject.toString(), VenueGroundList.class));
                                            notifyDataSetChanged();
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                            PlaythoraUtility.hideProgressDialog();
                            try {
                                String s = new String(responseBody);
                                Log.e("RESPONSE_FAIL", s);
                                JSONObject object = new JSONObject(s);

                            } catch (Exception e) {
                                e.printStackTrace();
                                Toast.makeText(context, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        } else {

            Toast.makeText(context, "Not connected to internet", Toast.LENGTH_LONG).show();

        }

    }

}
