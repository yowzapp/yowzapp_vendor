package com.groundhog.vendor.android.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.groundhog.vendor.android.R;
import com.groundhog.vendor.android.activity.LoginActivity;
import com.groundhog.vendor.android.model.GraphList;
import com.groundhog.vendor.android.model.ReportModel;
import com.groundhog.vendor.android.model.VenueListItems;
import com.groundhog.vendor.android.utils.Config;
import com.groundhog.vendor.android.utils.PlaythoraUtility;
import com.groundhog.vendor.android.utils.PreferenceManager;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

import static com.groundhog.vendor.android.utils.Config.RALEWAY_REGULAR;
import static com.groundhog.vendor.android.utils.Config.RALEWAY_SEMIBOLD;

/**
 * Created by vaishakha on 10/9/16.
 */
public class ReportFragment extends Fragment {
    ProgressBar progressBar;
    PreferenceManager mPref;
    ReportModel model;
    Gson gson;
    BarChart barChart;
    PieChart pieChartOne;
    ArrayList<Entry> entriestwo;
    ArrayList<BarDataSet> dataSets;
    RecyclerView barRecyclerView,pieList;
    public GridLayoutManager gridLayoutManager;
    ArrayList<String> xValues;
    ArrayList<BarDataSet> compareDataSets;
    ReportAdapter adapter;
    TextView date,filterDate,filterMonth,trend,summaryText,noVenue;
    String i = "monthly";
    String day = "monthly";
    PieAdapter pieAdapter;
    Dialog dialog;
    ListView dialogListView;
    ProgressBar dialogProgressBar;
    ArrayList<VenueListItems> venueListItems;
    VenueAdapter venueAdapter;
    int venue;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(com.groundhog.vendor.android.R.layout.report_layout, container,
                false);

        mPref = PreferenceManager.instance(getActivity());
        barChart = (BarChart) view.findViewById(com.groundhog.vendor.android.R.id.chart);
        pieChartOne = (PieChart) view.findViewById(com.groundhog.vendor.android.R.id.piechart_one);
        barRecyclerView = (RecyclerView) view.findViewById(com.groundhog.vendor.android.R.id.bar_recycler);
        pieList = (RecyclerView) view.findViewById(com.groundhog.vendor.android.R.id.pie_list);
        date = (TextView) view.findViewById(com.groundhog.vendor.android.R.id.datetext);
        filterDate = (TextView) view.findViewById(com.groundhog.vendor.android.R.id.date_drop_down) ;
        filterMonth = (TextView) view.findViewById(com.groundhog.vendor.android.R.id.venue_drop_down) ;
        trend = (TextView) view.findViewById(com.groundhog.vendor.android.R.id.trend_text) ;
        summaryText = (TextView) view.findViewById(com.groundhog.vendor.android.R.id.summary_text) ;

        barRecyclerView.setNestedScrollingEnabled(false);

        date.setTypeface(PlaythoraUtility.getFont(getActivity(),RALEWAY_REGULAR));
        filterDate.setTypeface(PlaythoraUtility.getFont(getActivity(),RALEWAY_REGULAR));
        filterMonth.setTypeface(PlaythoraUtility.getFont(getActivity(),RALEWAY_REGULAR));
        trend.setTypeface(PlaythoraUtility.getFont(getActivity(),RALEWAY_REGULAR));
        summaryText.setTypeface(PlaythoraUtility.getFont(getActivity(),RALEWAY_REGULAR));

        gridLayoutManager = new GridLayoutManager(getActivity(),2);
        barRecyclerView.setLayoutManager(gridLayoutManager);
        barRecyclerView.setHasFixedSize(true);
        barRecyclerView.setFocusable(false);

        pieList.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        pieList.setHasFixedSize(true);
        pieList.setNestedScrollingEnabled(false);


        XAxis xAxis = barChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);//labels at bottom
        xAxis.setLabelsToSkip(0);
        xAxis.setLabelRotationAngle(30);
        YAxis yAxis = barChart.getAxisRight();
        yAxis.setEnabled(false);

        loadReport(day,venue);

        filterDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showGroundList();
            }
        });

        filterMonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showVenueList();
            }
        });

        //barChart.setDescription("My Chart");
/*barChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
    @Override
    public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
        Log.e("CLICK",dataSets.get(dataSetIndex).getLabel()+"");
        Toast.makeText(getActivity(),dataSets.get(dataSetIndex).getLabel()+"",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNothingSelected() {

    }
});*/

        /*entriestwo = new ArrayList<>();
        float pief1 = 1300f;
        entriestwo.add(new Entry(pief6, 5));

       *//* PieDataSet datasettwo = new PieDataSet(entriestwo, "");
        datasettwo.setDrawValues(false);
        datasettwo.setSliceSpace(0);*//*


        ArrayList<Integer> color = new ArrayList<Integer>();
        for (int c : new int[]{Color.parseColor("#09a0ef")})
            color.add(c);

        */

        return view;
    }

    private void showVenueList() {
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(com.groundhog.vendor.android.R.layout.venue_list_dialog);

        dialogListView = (ListView) dialog.findViewById(com.groundhog.vendor.android.R.id.venue_list);
        dialogProgressBar = (ProgressBar) dialog.findViewById(com.groundhog.vendor.android.R.id.dialog_progress);
        noVenue = (TextView) dialog.findViewById(R.id.no_venue);

        TextView textView = (TextView) dialog.findViewById(com.groundhog.vendor.android.R.id.venue_text);
        textView.setText("Select Venue");
        textView.setTypeface(PlaythoraUtility.getFont(getActivity(),RALEWAY_SEMIBOLD));
        noVenue.setTypeface(PlaythoraUtility.getFont(getActivity(),RALEWAY_SEMIBOLD));


        loadVenueList();

        dialogListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                try {
                    venue = venueListItems.get(position).getId();
                    filterMonth.setText(venueListItems.get(position).getName());
                    Log.e("venue_id",venue+" ID");
                    loadReport(i,venue);
                }catch (Exception e){
                    e.printStackTrace();
                }
                dialog.dismiss();
            }
        });

        dialog.show();
    }


    private void showGroundList() {


        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(com.groundhog.vendor.android.R.layout.report_daily_layout);

        TextView daily = (TextView) dialog.findViewById(com.groundhog.vendor.android.R.id.daily_item);
        TextView title = (TextView) dialog.findViewById(com.groundhog.vendor.android.R.id.report_time_title);
        TextView monthly = (TextView) dialog.findViewById(com.groundhog.vendor.android.R.id.monthly_item);
        RelativeLayout d = (RelativeLayout) dialog.findViewById(com.groundhog.vendor.android.R.id.relaytive_daily);
        RelativeLayout m = (RelativeLayout) dialog.findViewById(com.groundhog.vendor.android.R.id.relative_monthly);
        final ImageView ds = (ImageView) dialog.findViewById(com.groundhog.vendor.android.R.id.selected_image_daily);
        final ImageView ms = (ImageView) dialog.findViewById(com.groundhog.vendor.android.R.id.selected_image_monthly);

        daily.setTypeface(PlaythoraUtility.getFont(getActivity(),RALEWAY_REGULAR));
        monthly.setTypeface(PlaythoraUtility.getFont(getActivity(),RALEWAY_REGULAR));
        title.setTypeface(PlaythoraUtility.getFont(getActivity(),RALEWAY_SEMIBOLD));

        if(i.equalsIgnoreCase("daily")){
            ds.setVisibility(View.VISIBLE);
            ms.setVisibility(View.INVISIBLE);
            i="daily";
        }else {
            ds.setVisibility(View.INVISIBLE);
            ms.setVisibility(View.VISIBLE);
            i="monthly";
        }

        d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ds.setVisibility(View.VISIBLE);
                ms.setVisibility(View.INVISIBLE);
                filterDate.setText("Daily");
                i="daily";
                loadReport("daily",venue);
                dialog.dismiss();
            }
        });

        m.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ds.setVisibility(View.INVISIBLE);
                ms.setVisibility(View.VISIBLE);
                filterDate.setText("Monthly");
                i="monthly";
                loadReport("monthly",venue);
                dialog.dismiss();
            }
        });

        dialog.show();


    }

    private void loadVenueList() {
        if (PlaythoraUtility.checkInternetConnection(getActivity())) {
            try {
                dialogProgressBar.setVisibility(View.VISIBLE);
                AsyncHttpClient mHttpClient = new AsyncHttpClient();
                mHttpClient.addHeader("accessToken", mPref.getAccessToken());
                Log.e("ACCESSTOKEN", mPref.getAccessToken());
                mHttpClient.get(Config.NEW_BOOK,
                        new AsyncHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                dialogProgressBar.setVisibility(View.GONE);
                                try {
                                    String s = new String(responseBody);
                                    Log.e("RESPONSE", s);
                                    JSONObject object = new JSONObject(s);
                                    if (object.getString("status").equalsIgnoreCase("success")) {
                                        if (object.getJSONArray("venueList").length() != 0) {
                                            if (object.get("venueList") instanceof JSONArray) {
                                                JSONArray jsonArray = object.getJSONArray("venueList");
                                                venueListItems = new ArrayList<>();
                                                gson = new Gson();
                                                for (int i = 0; i < jsonArray.length(); i++) {
                                                    venueListItems.add(gson.fromJson(jsonArray.get(i).toString(), VenueListItems.class));
                                                }
                                                if (!venueListItems.isEmpty()) {
                                                    venueAdapter = new VenueAdapter(getActivity(), venueListItems, venue);
                                                    dialogListView.setAdapter(venueAdapter);
                                                }else {
                                                    dialogListView.setAdapter(null);
                                                    noVenue.setVisibility(View.VISIBLE);
                                                }
                                            }
                                        }else {
                                            dialogListView.setAdapter(null);
                                            noVenue.setVisibility(View.VISIBLE);
                                        }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                                dialogProgressBar.setVisibility(View.GONE);
                                try {
                                    String s = new String(bytes);
                                    Log.e("RESPONSE_FAIL", s);
                                    JSONObject object = new JSONObject(s);
                                    if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                                        Toast.makeText(getActivity(), object.getString("message"), Toast.LENGTH_SHORT).show();
                                        mPref.setAccessToken("");
                                        Intent intent = new Intent(getActivity(), LoginActivity.class);
                                        startActivity(intent);
                                        getActivity().finish();
                                    } else
                                        Toast.makeText(getActivity(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    // Toast.makeText(getActivity(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
            }catch (Exception e){
                e.printStackTrace();
                dialogProgressBar.setVisibility(View.GONE);
            }
        }else{
            Toast.makeText(getActivity(), "Not connected to internet", Toast.LENGTH_LONG).show();
        }
    }

    public static class VenueAdapter extends BaseAdapter {
        Context context;
        List<VenueListItems> list;
        private boolean[] mCheckedState;

        public VenueAdapter(Context context, List<VenueListItems> list, int venueId) {
            this.context = context;
            this.list = list;
            mCheckedState = new boolean[list.size()];

            for(int i=0 ; i<list.size();i++){
                mCheckedState[i]=false;
            }

            for(int i=0 ; i<list.size();i++){
                if(venueId == list.get(i).getId())
                    mCheckedState[i]=true;
            }
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            convertView = inflater.inflate(com.groundhog.vendor.android.R.layout.venue_list_item,parent,false);

            CallHolder holder = new CallHolder();
            holder.venueText = (TextView) convertView.findViewById(com.groundhog.vendor.android.R.id.venue_list_item);
            holder.image = (ImageView) convertView.findViewById(com.groundhog.vendor.android.R.id.selected_image);
            holder.relativeLayout = (RelativeLayout) convertView.findViewById(com.groundhog.vendor.android.R.id.item_view);

            holder.venueText.setText(list.get(position).getName());

            holder.venueText.setTypeface(PlaythoraUtility.getFont(context,RALEWAY_REGULAR));

            if(mCheckedState[position])
                holder.image.setVisibility(View.VISIBLE);
            else holder.image.setVisibility(View.GONE);

            holder.image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    for(int i=0 ; i<list.size();i++){
                        mCheckedState[i]=false;
                    }
                    mCheckedState[position]=true;
                    notifyDataSetChanged();
                }
            });


            return convertView;
        }


        static class CallHolder{
            TextView venueText;
            ImageView image;
            RelativeLayout relativeLayout;
        }
    }

    private void loadReport(String day, int venues) {
        Log.e("VENUE_ID",venues+" id");
        String v = (venues==0)?"":venues+"";
        if (PlaythoraUtility.checkInternetConnection(getActivity())) {
            PlaythoraUtility.showProgressDialog(getActivity());
            AsyncHttpClient mHttpClient = new AsyncHttpClient();
            mHttpClient.addHeader("accessToken",mPref.getAccessToken());
            Log.e("ACCESSTOKEN",mPref.getAccessToken());
            Log.e("REPORT_URL",Config.REPORTS + "?type=" + day + "&venue_id=" + v);
            mHttpClient.get(Config.REPORTS + "?type=" + day + "&venue_id=" + v,
                    new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                            try {

                                PlaythoraUtility.hideProgressDialog();
                                String s = new String(responseBody);
                                Log.e("RESPONSE", s);

                                model = new ReportModel();
                                gson = new Gson();
                                model = gson.fromJson(s, ReportModel.class);

                                xValues = new ArrayList<>();
                                compareDataSets = new ArrayList<>();

                                if (!model.getSuccess().getVenue_name().isEmpty()) {
                                    venue = model.getSuccess().getVenue_id();
                                    filterMonth.setText(model.getSuccess().getVenue_name());
                                }

                                if (model.getSuccess().getGraph().size() != 0) {
                                    List<GraphList> graph = model.getSuccess().getGraph();
                                    for (int i = 0; i < graph.size(); i++) {
                                        xValues.add(graph.get(i).getDate());

                                        for (int j = 0; j < graph.get(i).getGrounds().size(); j++) {
                                            ArrayList<BarEntry> valueSet1 = new ArrayList<>();
                                            float f = Float.parseFloat(graph.get(i).getGrounds().get(j).getAmount());
                                            BarEntry v1e1 = new BarEntry(f, j); // Jan
                                            valueSet1.add(new BarEntry(f, j));
                                            BarDataSet barDataSet1 = new BarDataSet(valueSet1, "");
                                            barDataSet1.setColors(new int[]{Color.parseColor(graph.get(i).getGrounds().get(j).getHex())}); //bar color
                                            Log.e("Color", graph.get(i).getGrounds().get(j).getHex());
                                            compareDataSets.add(barDataSet1);

                                        /*float f1 = Float.parseFloat(value1);
                                        compareDataMe.add(new BarEntry(f1, pos));
                                        BarDataSet barDataSet1 = new BarDataSet(compareDataMe, "Me");
                                        int[] color = new int[]{getResources().getColor(R.color.colorPrimary)};
                                        barDataSet1.setColors(color);
                                        barDataSet1.setValueTextSize(5);*/
                                        }
                                    }

                                    BarData data = new BarData(xValues, compareDataSets);
                                    barChart.setData(data);
                                    data.setDrawValues(false);
                                    barChart.animateXY(2000, 2000);
                                    barChart.invalidate();
                                    barChart.getLegend().setEnabled(false);
                                    barChart.setDescription("");
                                    barChart.setPinchZoom(false);
                                    barChart.setDoubleTapToZoomEnabled(false);
                                    barChart.setScaleEnabled(false);
                                    barChart.setDrawGridBackground(false);//background color
                                    barChart.getAxisLeft().setDrawGridLines(false);// x axis lines
                                    barChart.getXAxis().setDrawGridLines(false);// y axis lines
                                    barChart.getAxisRight().setDrawGridLines(false);
                                    barChart.setNoDataText("");
                                    barChart.getXAxis().setDrawLabels(true);
                                    //data.setValueFormatter(new MyValueFormatter());
                                    barChart.getAxisRight().setDrawLabels(true);
                                    barChart.setTouchEnabled(false);
                                    barChart.setDrawValueAboveBar(false);
                                    barChart.setNoDataText("");
                                    barChart.setNoDataTextDescription("");
                                    barChart.setDrawHighlightArrow(false);

                                }

                                date.setText(model.getSuccess().getDate());
                                if (model.getSuccess().getPie().size() != 0) {
                                    adapter = new ReportAdapter(getActivity(), model.getSuccess().getPie());
                                    barRecyclerView.setAdapter(adapter);

                                    pieAdapter = new PieAdapter(getActivity(), model.getSuccess().getPie());
                                    pieList.setAdapter(pieAdapter);

                                    ArrayList<Integer> color = new ArrayList<Integer>();
                                    ArrayList<String> labelstwo = new ArrayList<String>();
                                    entriestwo = new ArrayList<>();

                                    for (int i = 0; i < model.getSuccess().getPie().size(); i++) {
                                        float f = Float.parseFloat(model.getSuccess().getPie().get(i).getAmount());
                                        entriestwo.add(new Entry(f, i));
                                        for (int c : new int[]{Color.parseColor(model.getSuccess().getPie().get(i).getHex())})
                                            color.add(c);
                                        labelstwo.add("i");
                                    }
                                    PieDataSet datasettwo = new PieDataSet(entriestwo, "");
                                    datasettwo.setDrawValues(false);
                                    datasettwo.setSliceSpace(0);

                                    datasettwo.setColors(color);

                                    PieData piedatatwo = new PieData(labelstwo, datasettwo);
                                    pieChartOne.setData(piedatatwo);

                                    pieChartOne.setDescription("");

                                    pieChartOne.animateY(0);
                                    pieChartOne.setDrawHoleEnabled(true);
                                    pieChartOne.setHoleColorTransparent(true);
                                    pieChartOne.setHoleRadius(50);
                                    pieChartOne.setTransparentCircleRadius(0);
                                    pieChartOne.setCenterText("");
                                    pieChartOne.setCenterTextSize(16);
                                    pieChartOne.setDrawSliceText(false);
                                    pieChartOne.getLegend().setEnabled(false);
                                    pieChartOne.setClickable(false);
                                    pieChartOne.setRotationEnabled(false);
                                    pieChartOne.setTouchEnabled(false);
                                    pieChartOne.setNoDataText("");
                                    pieChartOne.invalidate();

                                }

                            }catch (Exception e){
                                e.printStackTrace();
                            }

                        }
                        @Override
                        public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                            PlaythoraUtility.hideProgressDialog();

                            try {
                                String s = new String(bytes);
                                Log.e("RESPONSE_FAIL", s);
                                JSONObject object = new JSONObject(s);
                                if(object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400){
                                    Toast.makeText(getActivity(),object.getString("message"),Toast.LENGTH_SHORT).show();
                                    mPref.setAccessToken("");
                                    Intent intent = new Intent(getActivity(),LoginActivity.class);
                                    startActivity(intent);
                                    getActivity().finish();
                                }else Toast.makeText(getActivity(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                            } catch (Exception e) {
                                e.printStackTrace();
                                // Toast.makeText(getActivity(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                            }

                        }

                    });
        }else{

            Toast.makeText(getActivity(), "Not connected to internet", Toast.LENGTH_LONG).show();

        }

    }

    private class ReportAdapter extends RecyclerView.Adapter<ReportAdapter.MyViewHolder>{
        private Context mContext;
        List<GraphList.Grounds> model;

        public ReportAdapter(Context mContext, List<GraphList.Grounds> model) {
            this.mContext = mContext;
            this.model = model;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(com.groundhog.vendor.android.R.layout.report_timeline,parent,false);


            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, final int position) {
            try {

                holder.mText.setText(model.get(position).getName());
                holder.view.setBackgroundColor(Color.parseColor(model.get(position).getHex()));
                holder.mText.setTypeface(PlaythoraUtility.getFont(getActivity(),RALEWAY_REGULAR));

            }catch (Exception e){
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return model.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView mText;
            View view;

            public MyViewHolder(View itemView) {
                super(itemView);
                mText = (TextView) itemView.findViewById(com.groundhog.vendor.android.R.id.text_two);
                view = (View) itemView.findViewById(com.groundhog.vendor.android.R.id.view_report);

            }
        }
    }

    private class PieAdapter extends RecyclerView.Adapter<PieAdapter.MyViewHolder>{
        private Context mContext;
        List<GraphList.Grounds> models;

        public PieAdapter(Context mContext, List<GraphList.Grounds> models) {
            this.mContext = mContext;
            this.models = models;
            Log.e("SIZE",models.size()+"");
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(com.groundhog.vendor.android.R.layout.report_item,parent,false);


            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, final int position) {
            try {


                holder.mText.setText(": AED " + models.get(position).getAmount());
                holder.name.setText(models.get(position).getName());
                holder.name.setTextColor(Color.parseColor(models.get(position).getHex()));
                //holder.view.setBackgroundColor(Color.parseColor(model.get(position).getHex()));
                holder.name.setTypeface(PlaythoraUtility.getFont(getActivity(),RALEWAY_REGULAR));

            }catch (Exception e){
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return models.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView mText,name;

            public MyViewHolder(View itemView) {
                super(itemView);
                mText = (TextView) itemView.findViewById(com.groundhog.vendor.android.R.id.amount_text);
                name = (TextView) itemView.findViewById(com.groundhog.vendor.android.R.id.name_text);

            }
        }
    }


}
