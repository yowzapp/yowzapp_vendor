package com.groundhog.vendor.android.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.groundhog.vendor.android.R;
import com.groundhog.vendor.android.model.VenueGroundList;
import com.groundhog.vendor.android.model.VenueListItems;
import com.groundhog.vendor.android.utils.Config;
import com.groundhog.vendor.android.utils.PlaythoraUtility;
import com.groundhog.vendor.android.utils.PreferenceManager;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;

import static com.groundhog.vendor.android.utils.Config.RALEWAY_REGULAR;

/**
 * Created by hemanth on 28/9/16.
 */

public class NewBookingActivity extends AppCompatActivity implements View.OnClickListener {

    TextInputLayout bookingPersonNameInputLayout, bookingPhoneNumberInputLayout;
    EditText bPersonName, bPhoneNumber;
    CheckBox bCheckBox;
    TextView bText, daily, weekly, custom, sunday, monday, tuesday, wednesday, thursday, bVenue, bGround, friday, saturday, bDate, bSlots;
    Button save;
    RelativeLayout checkBoxLayout;
    LinearLayout daysLayout, customDaysLayout;
    boolean isCheck, isSunday = false, isMonday = false, isTuesday = false, isWednesday = false, isThursday = false, isFriday = false, isSaturday = false, isCustom = false;
    int year, month, day;
    Calendar calendar;
    String toDate, date, rawDate;
    int slotCount;
    ListView dialogListView, groundListview;
    ProgressBar dialogProgressBar, groundProgressBar;
    Gson gson;
    PreferenceManager mPref;
    VenueAdapter adapter;
    int venueId, groundId;
    Dialog dialog;
    ArrayList<VenueListItems> venueListItems;
    ArrayList<VenueGroundList> venueGroundItems;
    VenueGroundAdapter groundAdapter;
    int slotId;
    ArrayList<String> startArrayList = new ArrayList<>();
    ArrayList<String> endArrayList = new ArrayList<>();
    ArrayList<Integer> slotIdArrayList = new ArrayList<>();
    String repeatSlot = "";
    int[] customList;
    Toolbar toolbar;
    TextView title;
    Toast toast;
    String venueName, gName, dateValue;
    boolean[] slotStatus;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.groundhog.vendor.android.R.layout.new_booking_layout);

        toolbar = (Toolbar) findViewById(com.groundhog.vendor.android.R.id.main_toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(com.groundhog.vendor.android.R.drawable.ic_back_arrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        // getSupportActionBar().setTitle(Html.fromHtml("<font color=\"#345560\"><small>" + "New booking" + "</small></font>"));

        title = (TextView) findViewById(com.groundhog.vendor.android.R.id.toolbar_title);
        title.setText("New booking");

        title.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));


        repeatSlot = "";
        venueListItems = new ArrayList<>();
        venueGroundItems = new ArrayList<>();
        mPref = PreferenceManager.instance(NewBookingActivity.this);
        initializeAllComponents();
        setupListeners();

        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);

        if (getIntent().hasExtra("VENUENAME")) {
            venueName = getIntent().getStringExtra("VENUENAME");
            venueId = getIntent().getIntExtra("VENUEID", 0);
            gName = getIntent().getStringExtra("GROUNDNAME");
            groundId = getIntent().getIntExtra("GROUNDID", 0);
            dateValue = getIntent().getStringExtra("DATE");
            slotCount = getIntent().getIntExtra("SlotsCount", 0);
//            slotId = getIntent().getIntExtra("SlotsId", 0);
            startArrayList = getIntent().getStringArrayListExtra("startArrayList");
            endArrayList = getIntent().getStringArrayListExtra("endArrayList");
            slotIdArrayList = getIntent().getIntegerArrayListExtra("slotIdArrayList");

            Log.e("VENUENAME", venueName);
            Log.e("VENUEID", venueId + "");
            Log.e("GROUNDNAME", gName);
            Log.e("GROUNDID", groundId + "");
            Log.e("DATE", dateValue);
            Log.e("SlotsCount", slotCount + "");
            Log.e("SlotsId", slotId + "");
            Log.e("startArrayList", String.valueOf(startArrayList));
            Log.e("endArrayList", String.valueOf(endArrayList));
            Log.e("slotIdArrayList", String.valueOf(slotIdArrayList));

            bVenue.setText(venueName);
            bGround.setText(gName);
            bDate.setText(dateValue);
            bSlots.setText(slotCount + " slots selected");

            String[] strings = dateValue.split("/");
            rawDate = strings[0] + "-" + strings[1] + "-" + strings[2];
            date = strings[2] + "-" + strings[1] + "-" + strings[0];

            Log.e("date", date);
            Log.e("rawDate", rawDate);

        }

        loadVenueList();

        customList = new int[7];
        for (int i = 0; i < 7; i++) {
            customList[i] = 0;
        }

        bVenue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showVenueList(venueId);
            }
        });

        bGround.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            if (!bVenue.getText().toString().isEmpty())
                showGroundDialog(groundId);
            else
                Toast.makeText(getApplicationContext(), "Booking Venue is required", Toast.LENGTH_SHORT).show();
            }
        });

        bDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!bGround.getText().toString().isEmpty()) {
                    showDatePicker();
                } else
                    Toast.makeText(getApplicationContext(), "Booking ground required", Toast.LENGTH_SHORT).show();
            }
        });


        bSlots.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!bDate.getText().toString().isEmpty()) {
                    goToSlotsSelection();
                } else
                    Toast.makeText(getApplicationContext(), "Booking date is required", Toast.LENGTH_SHORT).show();
            }
        });


        checkBoxLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!isCheck) {
                    bCheckBox.setChecked(true);
                    daysLayout.setVisibility(View.VISIBLE);
                    isCheck = true;
                    repeatSlot = "";
                } else {
                    bCheckBox.setChecked(false);
                    daysLayout.setVisibility(View.GONE);
                    customDaysLayout.setVisibility(View.GONE);
                    isCheck = false;
                    repeatSlot = "";
                }

            }
        });

        bCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    bCheckBox.setChecked(true);
                    daysLayout.setVisibility(View.VISIBLE);
                    bCheckBox.setTextColor(getResources().getColor(com.groundhog.vendor.android.R.color.black));
                    repeatSlot = "";
                } else {
                    bCheckBox.setChecked(false);
                    daysLayout.setVisibility(View.GONE);
                    customDaysLayout.setVisibility(View.GONE);
                    bCheckBox.setTextColor(getResources().getColor(com.groundhog.vendor.android.R.color.hint_color));
                    repeatSlot = "";
                }
            }
        });

        bPersonName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                bookingPersonNameInputLayout.setError(null);

            }
        });

        bPhoneNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                bookingPhoneNumberInputLayout.setError(null);
            }
        });


        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkForValidation()) {
                    /*Intent save = new Intent(NewBookingActivity.this,HomeActivity.class);
                    save.putExtra("ActivityName","newBooking");
                    startActivity(save);*/
                    JSONObject mainObj = new JSONObject();
                    try {
                        mainObj.put("ground_id", groundId + "");
                        mainObj.put("slot_id", slotId + "");
                        // mainObj.put("total_price",TotalPrice+"");
                        //mainObj.put("no_players",maxMem);
                        mainObj.put("venue_id", venueId);
                        mainObj.put("date", date);
                        mainObj.put("mobile", bPhoneNumber.getText().toString());
                        mainObj.put("player_name", bPersonName.getText().toString());

                        if (!repeatSlot.isEmpty()) {
                            if (repeatSlot.equalsIgnoreCase("repeat_custom")) {
                                JSONArray j = new JSONArray();
                                for (int i = 0; i < customList.length; i++) {
                                    j.put(customList[i]);
                                }
                                mainObj.put(repeatSlot, j);
                            } else mainObj.put(repeatSlot, true);
                        }


                        JSONArray jsonArray = new JSONArray();
                        for (int i = 0; i < startArrayList.size(); i++) {
                            JSONObject object = new JSONObject();
                            object.put("slot_id", slotIdArrayList.get(i));
                            object.put("date", date);
                            object.put("start_time", startArrayList.get(i));
                            object.put("end_time", endArrayList.get(i));
                            jsonArray.put(object);
                        }
                        Log.e("SLOTS", jsonArray.toString());

                        mainObj.put("slots", jsonArray);
                        BookGround(mainObj);
                        Log.e("JSON", mainObj.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }
        });

    }

    private void goToSlotsSelection() {
        Intent slots = new Intent(NewBookingActivity.this, BookingSlotsActivity.class);
        slots.putExtra("GROUNDID", groundId);
        slots.putExtra("RAWDATE", date);
        slots.putExtra("DATE", rawDate);
        if (slotStatus != null)
            slots.putExtra("SLOTSTATUS", slotStatus);
        startActivityForResult(slots, 1);
    }

    private void showDatePicker() {

        final Calendar myCalendar = Calendar.getInstance();
        DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                if ((monthOfYear + 1) < 10) {
                    toDate = dayOfMonth + "/" + "0" + (monthOfYear + 1) + "/" + year;
                    if (dayOfMonth < 10) {
                        date = year + "-" + "0" + (monthOfYear + 1) + "-" + "0" + dayOfMonth;
                        rawDate = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                    } else {
                        date = year + "-" + "0" + (monthOfYear + 1) + "-" + dayOfMonth;
                        rawDate = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                    }
                } else {
                    toDate = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
                    if (dayOfMonth < 10) {
                        date = year + "-" + (monthOfYear + 1) + "-" + "0" + dayOfMonth;
                        rawDate = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                    } else {
                        date = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                        rawDate = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                    }
                }
                bDate.setText(toDate);
                bSlots.setText("");
                startArrayList.clear();
                endArrayList.clear();
                slotIdArrayList.clear();
                goToSlotsSelection();
            }
        };

        DatePickerDialog dpDialog = new DatePickerDialog(NewBookingActivity.this, AlertDialog.THEME_HOLO_LIGHT, listener, year, month, day);

        Calendar calendar1 = Calendar.getInstance();

        int mYear = calendar1.get(Calendar.YEAR);
        int mMonth = calendar1.get(Calendar.MONTH);
        int mDay = calendar1.get(Calendar.DAY_OF_MONTH);

              /*  dpDialog.getDatePicker().setMaxDate(calendar1.getTimeInMillis());

                dpDialog.getDatePicker().updateDate(mYear, mMonth, mDay);*/
        dpDialog.show();


    }

    private void BookGround(JSONObject object) {

        if (PlaythoraUtility.checkInternetConnection(NewBookingActivity.this)) {
            try {
                PlaythoraUtility.showProgressDialog(NewBookingActivity.this);
                AsyncHttpClient mHttpClient = new AsyncHttpClient();
                mHttpClient.addHeader("accessToken", mPref.getAccessToken());
                Log.e("ACCESSTOKEN", mPref.getAccessToken());

                StringEntity entity = new StringEntity(object.toString());

                mHttpClient.post(getApplicationContext(), Config.GROUND_BOOK, entity, "application/json",
                        new AsyncHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                PlaythoraUtility.hideProgressDialog();
                                try {
                                    String s = new String(responseBody);
                                    Log.e("RESPONSE", s);
                                    JSONObject object = new JSONObject(s);
                                    if (object.getString("status").equalsIgnoreCase("success")) {
                                        Toast.makeText(getApplicationContext(), object.getString("success").toString(), Toast.LENGTH_SHORT).show();

                                        Intent intent = new Intent(NewBookingActivity.this, HomeActivity.class);
                                        startActivity(intent);
                                        finishAffinity();
                                    }

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }

                            @Override
                            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                                PlaythoraUtility.hideProgressDialog();
                                try {
                                    String s = new String(bytes);
                                    Log.e("RESPONSE_FAIL", s);
                                    JSONObject object = new JSONObject(s);
                                    if(object.has("message")){
                                        Toast.makeText(NewBookingActivity.this, object.getString("message"), Toast.LENGTH_SHORT).show();
                                    } else if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                                        Toast.makeText(NewBookingActivity.this, object.getString("message"), Toast.LENGTH_SHORT).show();
                                        mPref.setAccessToken("");
                                        Intent intent = new Intent(NewBookingActivity.this, LoginActivity.class);
                                        startActivity(intent);
                                        finish();
                                    } else {
                                        Toast.makeText(NewBookingActivity.this, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                     Toast.makeText(NewBookingActivity.this, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                }

                            }

                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(NewBookingActivity.this, "Not connected to internet", Toast.LENGTH_LONG).show();
        }


    }

    private void loadVenueList() {
        if (PlaythoraUtility.checkInternetConnection(NewBookingActivity.this)) {
            PlaythoraUtility.showProgressDialog(NewBookingActivity.this);
            AsyncHttpClient mHttpClient = new AsyncHttpClient();
            mHttpClient.addHeader("accessToken", mPref.getAccessToken());
            Log.e("ACCESSTOKEN", mPref.getAccessToken());
            mHttpClient.get(Config.NEW_BOOK,
                    new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                            PlaythoraUtility.hideProgressDialog();
                            try {
                                String s = new String(responseBody);
                                Log.e("RESPONSE", s);
                                JSONObject object = new JSONObject(s);
                                if (object.getString("status").equalsIgnoreCase("success")) {
                                    if (object.getJSONArray("venueList").length() != 0) {
                                        if (object.get("venueList") instanceof JSONArray) {
                                            JSONArray jsonArray = object.getJSONArray("venueList");
                                            venueListItems = new ArrayList<>();
                                            gson = new Gson();
                                            for (int i = 0; i < jsonArray.length(); i++) {
                                                venueListItems.add(gson.fromJson(jsonArray.get(i).toString(), VenueListItems.class));
                                            }
                                            //adapter = new VenueAdapter(NewBookingActivity.this,venueListItems);
                                            ;
                                        }
                                        if (object.get("venueDetails") instanceof JSONArray) {

                                        }

                                        if (object.get("groundDetails") instanceof JSONArray) {

                                        }
                                    }

                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                            PlaythoraUtility.hideProgressDialog();
                            try {
                                String s = new String(bytes);
                                Log.e("RESPONSE_FAIL", s);
                                JSONObject object = new JSONObject(s);
                                if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                                    Toast.makeText(NewBookingActivity.this, object.getString("message"), Toast.LENGTH_SHORT).show();
                                    mPref.setAccessToken("");
                                    Intent intent = new Intent(NewBookingActivity.this, LoginActivity.class);
                                    startActivity(intent);
                                    finish();
                                } else
                                    Toast.makeText(NewBookingActivity.this, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                            } catch (Exception e) {
                                e.printStackTrace();
                                // Toast.makeText(getActivity(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                            }

                        }

                    });
        } else {
            Toast.makeText(NewBookingActivity.this, "Not connected to internet", Toast.LENGTH_LONG).show();
        }

    }

    private void showVenueList(int venueid) {
        dialog = new Dialog(NewBookingActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(com.groundhog.vendor.android.R.layout.venue_list_dialog);

        dialogListView = (ListView) dialog.findViewById(com.groundhog.vendor.android.R.id.venue_list);
        dialogProgressBar = (ProgressBar) dialog.findViewById(com.groundhog.vendor.android.R.id.dialog_progress);
        TextView textView = (TextView) dialog.findViewById(com.groundhog.vendor.android.R.id.venue_text);
        TextView noVenue = (TextView) dialog.findViewById(R.id.no_venue);
        textView.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        noVenue.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        try {
            if (!venueListItems.isEmpty()) {
                adapter = new VenueAdapter(NewBookingActivity.this, venueListItems, venueid);
                dialogListView.setAdapter(adapter);
                noVenue.setVisibility(View.GONE);
            } else {
                dialogListView.setAdapter(null);
                noVenue.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        dialogListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                bVenue.setText(venueListItems.get(position).getName());
                NewBookingActivity.this.venueId = venueListItems.get(position).getId();
                bGround.setText("");
                bDate.setText("");
                bSlots.setText("");
                startArrayList.clear();
                endArrayList.clear();
                slotIdArrayList.clear();
                showGroundDialog(groundId);
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void showGroundDialog(int groundId) {

        final Dialog dialog = new Dialog(NewBookingActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(com.groundhog.vendor.android.R.layout.venue_list_dialog);

        TextView title = (TextView) dialog.findViewById(com.groundhog.vendor.android.R.id.venue_text);
        title.setText("Select ground");
        title.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        groundListview = (ListView) dialog.findViewById(com.groundhog.vendor.android.R.id.venue_list);
        groundProgressBar = (ProgressBar) dialog.findViewById(com.groundhog.vendor.android.R.id.dialog_progress);

        LoadGround(groundId);

        groundListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                bGround.setText(venueGroundItems.get(position).getName());
                NewBookingActivity.this.groundId = venueGroundItems.get(position).getId();
                bDate.setText("");
                bSlots.setText("");
                startArrayList.clear();
                endArrayList.clear();
                slotIdArrayList.clear();
                showDatePicker();
                dialog.dismiss();
            }
        });

        dialog.show();

    }

    private void LoadGround(final int ground_id) {

        if (PlaythoraUtility.checkInternetConnection(NewBookingActivity.this)) {
            groundProgressBar.setVisibility(View.VISIBLE);
            AsyncHttpClient mHttpClient = new AsyncHttpClient();
            mHttpClient.addHeader("accessToken", mPref.getAccessToken());
            Log.e("ACCESSTOKEN", mPref.getAccessToken());
            RequestParams params = new RequestParams();
            params.add("venue_id", String.valueOf(venueId));
            mHttpClient.post(Config.VENUE_GROUND, params,
                    new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                            groundProgressBar.setVisibility(View.GONE);
                            try {
                                String s = new String(responseBody);
                                Log.e("RESPONSE", s);
                                JSONObject object = new JSONObject(s);
                                if (object.getString("status").equalsIgnoreCase("success")) {
                                    if (object.getJSONArray("success").length() != 0) {
                                        if (object.get("success") instanceof JSONArray) {
                                            JSONArray jsonArray = object.getJSONArray("success");
                                            venueGroundItems = new ArrayList<>();
                                            gson = new Gson();
                                            for (int i = 0; i < jsonArray.length(); i++) {
                                                venueGroundItems.add(gson.fromJson(jsonArray.get(i).toString(), VenueGroundList.class));
                                            }
                                            groundAdapter = new VenueGroundAdapter(NewBookingActivity.this, venueGroundItems, ground_id);
                                            groundListview.setAdapter(groundAdapter);
                                        }
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                            groundProgressBar.setVisibility(View.GONE);
                            try {
                                String s = new String(bytes);
                                Log.e("RESPONSE_FAIL", s);
                                JSONObject object = new JSONObject(s);
                                if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                                    Toast.makeText(NewBookingActivity.this, object.getString("message"), Toast.LENGTH_SHORT).show();
                                    mPref.setAccessToken("");
                                    Intent intent = new Intent(NewBookingActivity.this, LoginActivity.class);
                                    startActivity(intent);
                                    finish();
                                } else
                                    Toast.makeText(NewBookingActivity.this, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                            } catch (Exception e) {
                                e.printStackTrace();
                                // Toast.makeText(getActivity(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        } else {
            Toast.makeText(NewBookingActivity.this, "Not connected to internet", Toast.LENGTH_LONG).show();
        }

    }

    public static class VenueAdapter extends BaseAdapter {
        Context context;
        List<VenueListItems> list;
        private boolean[] mCheckedState;

        public VenueAdapter(Context context, List<VenueListItems> list, int venueId) {
            this.context = context;
            this.list = list;
            mCheckedState = new boolean[list.size()];

            for (int i = 0; i < list.size(); i++) {
                mCheckedState[i] = false;
            }

            for (int i = 0; i < list.size(); i++) {
                if (venueId == list.get(i).getId())
                    mCheckedState[i] = true;
            }
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            convertView = inflater.inflate(com.groundhog.vendor.android.R.layout.venue_list_item, parent, false);

            CallHolder holder = new CallHolder();
            holder.venueText = (TextView) convertView.findViewById(com.groundhog.vendor.android.R.id.venue_list_item);
            holder.image = (ImageView) convertView.findViewById(com.groundhog.vendor.android.R.id.selected_image);
            holder.relativeLayout = (RelativeLayout) convertView.findViewById(com.groundhog.vendor.android.R.id.item_view);

            holder.venueText.setText(list.get(position).getName());

            holder.venueText.setTypeface(PlaythoraUtility.getFont(context, RALEWAY_REGULAR));

            if (mCheckedState[position])
                holder.image.setVisibility(View.VISIBLE);
            else holder.image.setVisibility(View.INVISIBLE);

            holder.image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    for (int i = 0; i < list.size(); i++) {
                        mCheckedState[i] = false;
                    }
                    mCheckedState[position] = true;
                    notifyDataSetChanged();
                }
            });


            return convertView;
        }


        static class CallHolder {
            TextView venueText;
            ImageView image;
            RelativeLayout relativeLayout;
        }
    }


    public static class VenueGroundAdapter extends BaseAdapter {
        Context context;
        List<VenueGroundList> list;
        private boolean[] mCheckedState;

        public VenueGroundAdapter(Context context, List<VenueGroundList> list, int groundId) {
            this.context = context;
            this.list = list;
            mCheckedState = new boolean[list.size()];

            for (int i = 0; i < list.size(); i++) {
                mCheckedState[i] = false;
            }

            for (int i = 0; i < list.size(); i++) {
                if (groundId == list.get(i).getId())
                    mCheckedState[i] = true;
            }
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            convertView = inflater.inflate(com.groundhog.vendor.android.R.layout.venue_list_item, parent, false);

            CallHolder holder = new CallHolder();
            holder.venueText = (TextView) convertView.findViewById(com.groundhog.vendor.android.R.id.venue_list_item);
            holder.image = (ImageView) convertView.findViewById(com.groundhog.vendor.android.R.id.selected_image);
            holder.relativeLayout = (RelativeLayout) convertView.findViewById(com.groundhog.vendor.android.R.id.item_view);

            holder.venueText.setText(list.get(position).getName());
            holder.venueText.setTypeface(PlaythoraUtility.getFont(context, RALEWAY_REGULAR));

            if (mCheckedState[position])
                holder.image.setVisibility(View.VISIBLE);
            else holder.image.setVisibility(View.INVISIBLE);

            holder.image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    for (int i = 0; i < list.size(); i++) {
                        mCheckedState[i] = false;
                    }
                    mCheckedState[position] = true;
                    notifyDataSetChanged();
                }
            });

            return convertView;
        }


        static class CallHolder {
            TextView venueText;
            RelativeLayout relativeLayout;
            ImageView image;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null) {
            if (requestCode == 1) {
                slotCount = data.getIntExtra("SlotsCount", 0);
                slotId = data.getIntExtra("SlotsId", 0);
                startArrayList = data.getStringArrayListExtra("startArrayList");
                endArrayList = data.getStringArrayListExtra("endArrayList");
                slotIdArrayList = data.getIntegerArrayListExtra("slotIdArrayList");
                slotStatus = data.getBooleanArrayExtra("slotStatus");
                bSlots.setText(slotCount + " slot(s) selected: ");
                Log.e("startArrayList", startArrayList.toString());
                Log.e("endArrayList", endArrayList.toString());
                Log.e("slotIdArrayList", slotIdArrayList.toString());
                for (int i = 0; i < startArrayList.size(); i++) {
                    bSlots.setText(bSlots.getText().toString().concat("[" + startArrayList.get(i)) + "-" + endArrayList.get(i) + "]");
                }
            }
        }

    }

    private void setupListeners() {
        daily.setOnClickListener(this);
        weekly.setOnClickListener(this);
        custom.setOnClickListener(this);
        sunday.setOnClickListener(this);
        monday.setOnClickListener(this);
        tuesday.setOnClickListener(this);
        wednesday.setOnClickListener(this);
        thursday.setOnClickListener(this);
        friday.setOnClickListener(this);
        saturday.setOnClickListener(this);


    }

    private boolean checkForValidation() {

        if (toast != null) {
            toast.cancel();
        }

        if (bVenue.getText().toString().isEmpty()) {
            toast = Toast.makeText(NewBookingActivity.this, "Please select booking venue", Toast.LENGTH_SHORT);
            toast.show();
            return false;
        }

        if (bGround.getText().toString().isEmpty()) {
            toast = Toast.makeText(NewBookingActivity.this, "Please select booking ground", Toast.LENGTH_SHORT);
            toast.show();
            return false;
        }
        if (bDate.getText().toString().isEmpty()) {
            toast = Toast.makeText(NewBookingActivity.this, "Please select booking date", Toast.LENGTH_SHORT);
            toast.show();
            return false;
        }

        if (slotCount == 0) {
            toast = Toast.makeText(NewBookingActivity.this, "Please select booking slot", Toast.LENGTH_SHORT);
            toast.show();
            return false;

        }

        if (bPersonName.getText().toString().isEmpty()) {
            bookingPersonNameInputLayout.setError("Person name is required");
            bookingPersonNameInputLayout.setErrorEnabled(true);
        } else {
            bookingPersonNameInputLayout.setErrorEnabled(false);
        }

        if (bPhoneNumber.getText().toString().isEmpty()) {
            bookingPhoneNumberInputLayout.setError("Phone number is required");
            bookingPhoneNumberInputLayout.setErrorEnabled(true);
        } else {
            bookingPhoneNumberInputLayout.setErrorEnabled(false);
        }

        if (bPhoneNumber.getText().toString().length() != 10) {
            bookingPhoneNumberInputLayout.setError("Enter valid number");
            bookingPhoneNumberInputLayout.setErrorEnabled(true);
        } else
            bookingPhoneNumberInputLayout.setErrorEnabled(false);

        if (bookingPersonNameInputLayout.isErrorEnabled() || bookingPhoneNumberInputLayout.isErrorEnabled()) {
            return false;
        } else {
            return true;
        }


    }

    private void initializeAllComponents() {

        // bookingDateInputLayout = (TextInputLayout) findViewById(R.id.input_layout_date);
        //bookingSlotsInputLayout = (TextInputLayout) findViewById(R.id.input_layout_slot);
        bookingPersonNameInputLayout = (TextInputLayout) findViewById(com.groundhog.vendor.android.R.id.input_layout_person_name);
        bookingPhoneNumberInputLayout = (TextInputLayout) findViewById(com.groundhog.vendor.android.R.id.input_layout_phone_num);
        bVenue = (TextView) findViewById(com.groundhog.vendor.android.R.id.bookingVenue);
        bGround = (TextView) findViewById(com.groundhog.vendor.android.R.id.bookingGround);
        bDate = (TextView) findViewById(com.groundhog.vendor.android.R.id.bookingDate);
        bSlots = (TextView) findViewById(com.groundhog.vendor.android.R.id.bookingSlot);
        bPersonName = (EditText) findViewById(com.groundhog.vendor.android.R.id.bookingPersonName);
        bPhoneNumber = (EditText) findViewById(com.groundhog.vendor.android.R.id.bookingPhoneNumber);
        bCheckBox = (CheckBox) findViewById(com.groundhog.vendor.android.R.id.bookingCheckBox);
        bText = (TextView) findViewById(com.groundhog.vendor.android.R.id.bookingRepeatText);
        bookingUI(findViewById(com.groundhog.vendor.android.R.id.bookingActivity));
        save = (Button) findViewById(com.groundhog.vendor.android.R.id.saveBooking);
        daily = (TextView) findViewById(com.groundhog.vendor.android.R.id.daily_text);
        weekly = (TextView) findViewById(com.groundhog.vendor.android.R.id.weekly_text);
        custom = (TextView) findViewById(com.groundhog.vendor.android.R.id.custom_text);
        checkBoxLayout = (RelativeLayout) findViewById(com.groundhog.vendor.android.R.id.checkBoxLayout);
        daysLayout = (LinearLayout) findViewById(com.groundhog.vendor.android.R.id.days_linear_layout);
        sunday = (TextView) findViewById(com.groundhog.vendor.android.R.id.custom_sunday_text);
        monday = (TextView) findViewById(com.groundhog.vendor.android.R.id.custom_monday_text);
        tuesday = (TextView) findViewById(com.groundhog.vendor.android.R.id.custom_tuesday_text);
        wednesday = (TextView) findViewById(com.groundhog.vendor.android.R.id.custom_wednesday_text);
        thursday = (TextView) findViewById(com.groundhog.vendor.android.R.id.custom_thursday_text);
        friday = (TextView) findViewById(com.groundhog.vendor.android.R.id.custom_friday_text);
        saturday = (TextView) findViewById(com.groundhog.vendor.android.R.id.custom_saturday_text);
        customDaysLayout = (LinearLayout) findViewById(com.groundhog.vendor.android.R.id.custom_days_Layout);


        bVenue.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        bGround.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        bDate.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        bPersonName.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        bSlots.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        bCheckBox.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        save.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        bookingPersonNameInputLayout.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        bookingPhoneNumberInputLayout.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));


    }

    private void bookingUI(View viewById) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(viewById instanceof EditText)) {
            viewById.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    PlaythoraUtility.hideSoftKeyboard(NewBookingActivity.this);
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (viewById instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) viewById).getChildCount(); i++) {
                View innerView = ((ViewGroup) viewById).getChildAt(i);
                bookingUI(innerView);
            }
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                break;

        }
        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startArrayList.clear();
        endArrayList.clear();
        slotIdArrayList.clear();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case com.groundhog.vendor.android.R.id.daily_text:
                customDaysLayout.setVisibility(View.GONE);
                daily.setBackgroundDrawable(getResources().getDrawable(com.groundhog.vendor.android.R.drawable.profile_background));
                daily.setTextColor(getResources().getColor(com.groundhog.vendor.android.R.color.white));
                weekly.setTextColor(getResources().getColor(com.groundhog.vendor.android.R.color.text_one));
                custom.setTextColor(getResources().getColor(com.groundhog.vendor.android.R.color.text_one));
                weekly.setBackgroundResource(0);
                custom.setBackgroundResource(0);
                repeatSlot = "repeat_daily";
                break;

            case com.groundhog.vendor.android.R.id.weekly_text:
                customDaysLayout.setVisibility(View.GONE);
                weekly.setBackgroundDrawable(getResources().getDrawable(com.groundhog.vendor.android.R.drawable.profile_background));
                weekly.setTextColor(getResources().getColor(com.groundhog.vendor.android.R.color.white));
                daily.setTextColor(getResources().getColor(com.groundhog.vendor.android.R.color.text_one));
                custom.setTextColor(getResources().getColor(com.groundhog.vendor.android.R.color.text_one));
                daily.setBackgroundResource(0);
                custom.setBackgroundResource(0);
                repeatSlot = "repeat_weekly";
                break;

            case com.groundhog.vendor.android.R.id.custom_text:

                customDaysLayout.setVisibility(View.VISIBLE);
                custom.setBackgroundDrawable(getResources().getDrawable(com.groundhog.vendor.android.R.drawable.profile_background));
                custom.setTextColor(getResources().getColor(com.groundhog.vendor.android.R.color.white));
                daily.setTextColor(getResources().getColor(com.groundhog.vendor.android.R.color.text_one));
                weekly.setTextColor(getResources().getColor(com.groundhog.vendor.android.R.color.text_one));
                daily.setBackgroundResource(0);
                weekly.setBackgroundResource(0);
                repeatSlot = "repeat_custom";
                break;

            case com.groundhog.vendor.android.R.id.custom_sunday_text:

                if (isSunday) {
                    sunday.setTextColor(ContextCompat.getColor(NewBookingActivity.this, R.color.sub_text));
                    sunday.setBackgroundDrawable(getResources().getDrawable(com.groundhog.vendor.android.R.drawable.day_back));
                    isSunday = false;
                    customList[0] = 0;
                } else {
                    // sunday.setBackgroundResource(R.drawable.oval_selected);
                    sunday.setTextColor(ContextCompat.getColor(NewBookingActivity.this, R.color.white));
                    sunday.setBackgroundDrawable(getResources().getDrawable(com.groundhog.vendor.android.R.drawable.day_full_back));
                    isSunday = true;
                    customList[0] = 1;
                }

                break;

            case com.groundhog.vendor.android.R.id.custom_monday_text:

                if (isMonday) {
                    // monday.setBackgroundResource(R.drawable.oval_deselected);
                    monday.setTextColor(ContextCompat.getColor(NewBookingActivity.this, R.color.sub_text));
                    monday.setBackgroundDrawable(getResources().getDrawable(com.groundhog.vendor.android.R.drawable.day_back_two));
                    isMonday = false;
                    customList[1] = 0;
                } else {

                    // monday.setBackgroundResource(R.drawable.oval_selected);
                    monday.setTextColor(ContextCompat.getColor(NewBookingActivity.this, R.color.white));
                    monday.setBackgroundDrawable(getResources().getDrawable(com.groundhog.vendor.android.R.drawable.day_full_back_two));
                    isMonday = true;
                    customList[1] = 1;
                }

                break;

            case com.groundhog.vendor.android.R.id.custom_tuesday_text:

                if (isTuesday) {
                    //  tuesday.setBackgroundResource(R.drawable.oval_deselected);
                    tuesday.setTextColor(ContextCompat.getColor(NewBookingActivity.this, R.color.sub_text));
                    tuesday.setBackgroundDrawable(getResources().getDrawable(com.groundhog.vendor.android.R.drawable.day_back_three));
                    isTuesday = false;
                    customList[2] = 0;
                } else {
                    // tuesday.setBackgroundResource(R.drawable.oval_selected);
                    tuesday.setTextColor(ContextCompat.getColor(NewBookingActivity.this, R.color.white));
                    tuesday.setBackgroundDrawable(getResources().getDrawable(com.groundhog.vendor.android.R.drawable.day_full_back_three));
                    isTuesday = true;
                    customList[2] = 1;
                }

                break;

            case com.groundhog.vendor.android.R.id.custom_wednesday_text:

                if (isWednesday) {
                    // wednesday.setBackgroundResource(R.drawable.oval_deselected);
                    wednesday.setTextColor(ContextCompat.getColor(NewBookingActivity.this, R.color.sub_text));
                    wednesday.setBackgroundDrawable(getResources().getDrawable(com.groundhog.vendor.android.R.drawable.day_back_four));
                    isWednesday = false;
                    customList[3] = 0;
                } else {

                    // wednesday.setBackgroundResource(R.drawable.oval_selected);
                    wednesday.setTextColor(ContextCompat.getColor(NewBookingActivity.this, R.color.white));
                    wednesday.setBackgroundDrawable(getResources().getDrawable(com.groundhog.vendor.android.R.drawable.day_full_back_four));
                    isWednesday = true;
                    customList[3] = 1;
                }


                break;

            case com.groundhog.vendor.android.R.id.custom_thursday_text:

                if (isThursday) {
                    //  thursday.setBackgroundResource(R.drawable.oval_deselected);
                    thursday.setTextColor(ContextCompat.getColor(NewBookingActivity.this, R.color.sub_text));
                    thursday.setBackgroundDrawable(getResources().getDrawable(com.groundhog.vendor.android.R.drawable.day_back_five));
                    isThursday = false;
                    customList[4] = 0;
                } else {
                    //  thursday.setBackgroundResource(R.drawable.oval_selected);
                    thursday.setTextColor(ContextCompat.getColor(NewBookingActivity.this, R.color.white));
                    thursday.setBackgroundDrawable(getResources().getDrawable(com.groundhog.vendor.android.R.drawable.day_full_back_five));
                    isThursday = true;
                    customList[4] = 1;
                }


                break;

            case com.groundhog.vendor.android.R.id.custom_friday_text:

                if (isFriday) {
                    // friday.setBackgroundResource(R.drawable.oval_deselected);
                    friday.setTextColor(ContextCompat.getColor(NewBookingActivity.this, R.color.sub_text));
                    friday.setBackgroundDrawable(getResources().getDrawable(com.groundhog.vendor.android.R.drawable.day_back_six));
                    isFriday = false;
                    customList[5] = 0;
                } else {

                    // friday.setBackgroundResource(R.drawable.oval_selected);
                    friday.setTextColor(ContextCompat.getColor(NewBookingActivity.this, R.color.white));
                    friday.setBackgroundDrawable(getResources().getDrawable(com.groundhog.vendor.android.R.drawable.day_full_back_six));
                    isFriday = true;
                    customList[5] = 1;
                }

                break;

            case com.groundhog.vendor.android.R.id.custom_saturday_text:


                if (isSaturday) {
                    // saturday.setBackgroundResource(R.drawable.oval_deselected);
                    saturday.setTextColor(ContextCompat.getColor(NewBookingActivity.this, R.color.sub_text));
                    saturday.setBackgroundDrawable(getResources().getDrawable(com.groundhog.vendor.android.R.drawable.day_back_seven));
                    isSaturday = false;
                    customList[6] = 0;
                } else {
                    // saturday.setBackgroundResource(R.drawable.oval_selected);
                    saturday.setTextColor(ContextCompat.getColor(NewBookingActivity.this, R.color.white));
                    saturday.setBackgroundDrawable(getResources().getDrawable(com.groundhog.vendor.android.R.drawable.day_full_back_seven));
                    isSaturday = true;
                    customList[6] = 1;
                }

                break;
        }
    }
}
