package com.groundhog.vendor.android.activity;

import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.sql.Time;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by vaishakha on 20/9/16.
 */
public class AddGroundSlotsActivity extends AppCompatActivity {
    RelativeLayout startTimeRelative,endTimeRelative;
    TextView startTime,endTime,groundName,verified;
    TextInputLayout priceTextInput,memberTextInput;
    EditText priceText,memberText;
    Button add;
    boolean select;
    String sTime="",eTime="",mPrice="",mMembers="",position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.groundhog.vendor.android.R.layout.add_slots_layout);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(Html.fromHtml("<font color=\"white\"><small>" + "Add ground slots" + "</small></font>"));
        initializeAllComponent();

        Intent intent = getIntent();
        if(intent.hasExtra("GroundName")){
            if(intent.getStringExtra("Verified").equalsIgnoreCase("true")) {
                String colorText = "<font color=\"#40ae79\">" + "Verified!" + "</font>" + " Available on platform";
                verified.setText(Html.fromHtml(colorText));
            }
            groundName.setText(intent.getStringExtra("GroundName"));
        }
        if(intent.hasExtra("StartTime")){
            sTime = intent.getStringExtra("StartTime");
            eTime = intent.getStringExtra("EndTime");
            mPrice = intent.getStringExtra("Price");
            mMembers = intent.getStringExtra("Member");
            position = intent.getStringExtra("Position");
            add.setText("SAVE");
            getSupportActionBar().setTitle(Html.fromHtml("<font color=\"white\"><small>" + "Edit ground slots" + "</small></font>"));
        }
        startTime.setText(sTime.replace("AM", "am").replace("PM","pm"));
        endTime.setText(eTime);
        priceText.setText(mPrice);
        memberText.setText(mMembers);


        startTimeRelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(AddGroundSlotsActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                        String time = getTime( selectedHour, selectedMinute);
                        String str = time.replace("AM", "am").replace("PM","pm");
                        startTime.setText(str);
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();

            }
        });

        endTimeRelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(AddGroundSlotsActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                        String time = getTime( selectedHour, selectedMinute);
                        String str = time.replace("AM", "am").replace("PM","pm");
                        endTime.setText(str);
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();

            }
        });

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(CheckForValidation()){
                    Log.e("slotTime",startTime.getText().toString() + " " + endTime.getText().toString() + " " + priceText.getText().toString() + " " + memberText.getText().toString());
                    Intent intent = new Intent();
                    intent.putExtra("StartTime",startTime.getText().toString().trim());
                    intent.putExtra("EndTime",endTime.getText().toString().trim());
                    intent.putExtra("Price",priceText.getText().toString().trim());
                    intent.putExtra("Member",memberText.getText().toString().trim());
                    setResult(2,intent);
                    finish();
                }
            }
        });


        priceText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                priceTextInput.setError(null);
            }
        });

        priceText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if (priceText.getText().toString().trim().isEmpty())
                        priceTextInput.setError("Please provide price");
                    else
                        priceTextInput.setErrorEnabled(false);
                }
            }
        });

        memberText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                memberTextInput.setError(null);
            }
        });

        memberText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if (memberText.getText().toString().trim().isEmpty())
                        memberTextInput.setError("Please provide number of members");
                    else
                        memberTextInput.setErrorEnabled(false);
                }
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                //Toast.makeText(getApplicationContext(),"Back button clicked", Toast.LENGTH_SHORT).show();
                onBackPressed();
                return true;
            default:
                break;

        }
        return false;
    }

    private boolean CheckForValidation() {

        if(priceText.getText().toString().trim().isEmpty()){
            priceTextInput.setError("Please provide price");
            priceTextInput.setErrorEnabled(true);
        }else{
            priceTextInput.setErrorEnabled(false);
        }

        if(memberText.getText().toString().trim().isEmpty()){
            memberTextInput.setError("Please provide number of members");
            memberTextInput.setErrorEnabled(true);
        }else{
            memberTextInput.setErrorEnabled(false);
        }

        if(startTime.getText().toString().trim().isEmpty() || endTime.getText().toString().trim().isEmpty()){
            Toast.makeText(AddGroundSlotsActivity.this,"Please select duration",Toast.LENGTH_SHORT).show();
            select = true;
        }else select = false;

        if(priceTextInput.isErrorEnabled()||memberTextInput.isErrorEnabled() || select)
            return false;
        else return true;
    }

    private void initializeAllComponent() {
        startTimeRelative = (RelativeLayout) findViewById(com.groundhog.vendor.android.R.id.relative_start_time);
        startTime = (TextView) findViewById(com.groundhog.vendor.android.R.id.start_time);
        endTimeRelative = (RelativeLayout) findViewById(com.groundhog.vendor.android.R.id.relative_end_time);
        endTime = (TextView) findViewById(com.groundhog.vendor.android.R.id.end_time);
        priceTextInput = (TextInputLayout) findViewById(com.groundhog.vendor.android.R.id.text_input_price);
        memberTextInput = (TextInputLayout) findViewById(com.groundhog.vendor.android.R.id.text_input_member);
        priceText = (EditText) findViewById(com.groundhog.vendor.android.R.id.price_text);
        memberText = (EditText) findViewById(com.groundhog.vendor.android.R.id.member_text);
        add = (Button) findViewById(com.groundhog.vendor.android.R.id.add_slots);
        groundName = (TextView) findViewById(com.groundhog.vendor.android.R.id.add_slot_ground);
        verified = (TextView) findViewById(com.groundhog.vendor.android.R.id.add_slot_ground_verified);
    }

    private String getTime(int hr, int min) {

        Time tme = new Time(hr,min,0);//seconds by default set to zero
        Format formatter;
        formatter = new SimpleDateFormat("h:mm a");
        return formatter.format(tme);
    }

}
