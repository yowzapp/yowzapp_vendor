package com.groundhog.vendor.android.model;

import java.util.List;

/**
 * Created by hemanth on 24/3/18.
 */

public class DaysDetailSuccess {

    private List<DaysDetail> details;

    public List<DaysDetail> getDetails() {
        return details;
    }

    public void setDetails(List<DaysDetail> details) {
        this.details = details;
    }
}
