package com.groundhog.vendor.android.model;

/**
 * Created by vaishakha on 20/9/16.
 */
public class GroundSlotsTimeModel {
    private String startTime,endTime,members,price;

    public GroundSlotsTimeModel(String startTime, String endTime, String members, String price) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.members = members;
        this.price =price;
     }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getMembers() {
        return members;
    }

    public void setMembers(String members) {
        this.members = members;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
