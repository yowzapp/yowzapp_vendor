package com.groundhog.vendor.android.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.groundhog.vendor.android.activity.BookingGroundSlotsActivity;
import com.groundhog.vendor.android.activity.LoginActivity;
import com.groundhog.vendor.android.activity.NewBookingActivity;
import com.groundhog.vendor.android.model.BookingModel;
import com.groundhog.vendor.android.model.VenueListing;
import com.groundhog.vendor.android.utils.Config;
import com.groundhog.vendor.android.utils.PlaythoraUtility;
import com.groundhog.vendor.android.utils.PreferenceManager;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.List;

import cz.msebera.android.httpclient.Header;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.groundhog.vendor.android.utils.Config.RALEWAY_BOLD;
import static com.groundhog.vendor.android.utils.Config.RALEWAY_REGULAR;
import static com.groundhog.vendor.android.utils.Config.RALEWAY_SEMIBOLD;

/**
 * Created by Nakul on 9/22/2016.
 */
public class BookingFragment extends Fragment {

    ExpandableListView expandableListView;
    FloatingActionButton newBooking, next;
    ExpnadableListWithAdapter adapter;
    PreferenceManager mPref;
    BookingModel bookingModel;
    Gson gson;
    TextView venueDate, emptyText;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(com.groundhog.vendor.android.R.layout.booking_layout, container, false);

        mPref = PreferenceManager.instance(getActivity());
        expandableListView = (ExpandableListView) view.findViewById(com.groundhog.vendor.android.R.id.list_top_grounds);
        newBooking = (FloatingActionButton) view.findViewById(com.groundhog.vendor.android.R.id.new_booking);
        venueDate = (TextView) view.findViewById(com.groundhog.vendor.android.R.id.venue_date);
        emptyText = (TextView) view.findViewById(com.groundhog.vendor.android.R.id.noBookings);
        next = (FloatingActionButton) view.findViewById(com.groundhog.vendor.android.R.id.next);

        venueDate.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_BOLD));
        emptyText.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_REGULAR));
        //next.setTypeface(PlaythoraUtility.getFont(getActivity(),RALEWAY_REGULAR));

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), BookingGroundSlotsActivity.class);
                startActivity(i);
            }
        });

        newBooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent newBooking = new Intent(getActivity(), NewBookingActivity.class);
                startActivity(newBooking);
            }
        });

        loadBookings();


        int currentPosi = expandableListView.getFirstVisiblePosition();
        expandableListView.setSelectionFromTop(currentPosi, 0);

        /*expandableListView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
            @Override
            public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY > oldScrollY) {
                    // Log.e("Scroll_DOWN","Scroll_DOWN");
                    newBooking.hide();
                }
                if (scrollY < oldScrollY) {
                    // Log.e("Scroll_UP","Scroll_UP");
                    newBooking.show();
                }
            }
        });
*/

        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                return true;
            }
        });
       /* expandableListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                final int currentFirstVisibleItem = expandableListView.getFirstVisiblePosition();
                if (currentFirstVisibleItem > firstVisibleItem) {
                    // Log.e("Scroll_DOWN","Scroll_DOWN");
                    newBooking.hide();
                } else if (currentFirstVisibleItem < firstVisibleItem) {
                    // Log.e("Scroll_UP","Scroll_UP");
                    newBooking.show();
                }
            }
        });*/

        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                Intent i = new Intent(getActivity(), BookingGroundSlotsActivity.class);
                i.putExtra("VENUENAME", bookingModel.getSuccess().get(groupPosition).getName());
                i.putExtra("VENUEID", bookingModel.getSuccess().get(groupPosition).getId());
                i.putExtra("GROUNDNAME", bookingModel.getSuccess().get(groupPosition).getGrounds().get(childPosition).getName());
                i.putExtra("GROUNDID", bookingModel.getSuccess().get(groupPosition).getGrounds().get(childPosition).getId());
                i.putExtra("DATE", bookingModel.getRaw_date());
                startActivity(i);
                return true;
            }
        });
        return view;
    }

    private void loadBookings() {
        if (PlaythoraUtility.checkInternetConnection(getActivity())) {
            try {
                PlaythoraUtility.showProgressDialog(getActivity());
                AsyncHttpClient mHttpClient = new AsyncHttpClient();
                mHttpClient.addHeader("accessToken", mPref.getAccessToken());
                Log.e("ACCESSTOKEN", mPref.getAccessToken());
                mHttpClient.get(Config.LATEST_BOOKING,
                        new AsyncHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                PlaythoraUtility.hideProgressDialog();
                                try {
                                    String s = new String(responseBody);
                                    Log.e("RESPONSE_BOOKING", s);
                                    JSONObject object = new JSONObject(s);
                                    if (object.getString("status").equalsIgnoreCase("success")) {
                                        bookingModel = new BookingModel();
                                        gson = new Gson();
                                        bookingModel = gson.fromJson(s, BookingModel.class);

                                        if (bookingModel.getSuccess().size() != 0) {
                                            //emptyVenue.setVisibility(View.GONE);
                                            venueDate.setText(bookingModel.getDate());
                                            adapter = new ExpnadableListWithAdapter(getActivity(), bookingModel.getSuccess());
                                            expandableListView.setAdapter(adapter);
                                            emptyText.setVisibility(View.GONE);
                                        } else {
                                            emptyText.setVisibility(View.VISIBLE);
                                        }
                                    }

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                                PlaythoraUtility.hideProgressDialog();
                                try {
                                    String s = new String(bytes);
                                    Log.e("RESPONSE_FAIL", s);
                                    JSONObject object = new JSONObject(s);
                                    if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                                        Toast.makeText(getActivity(), object.getString("message"), Toast.LENGTH_SHORT).show();
                                        mPref.setAccessToken("");
                                        Intent intent = new Intent(getActivity(), LoginActivity.class);
                                        startActivity(intent);
                                        getActivity().finish();
                                    } else
                                        Toast.makeText(getActivity(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    // Toast.makeText(getActivity(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                }
                            }

                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(getActivity(), "Not connected to internet", Toast.LENGTH_LONG).show();
        }


    }


    public class ExpnadableListWithAdapter extends BaseExpandableListAdapter {
        private Context context;
        List<VenueListing> parentList;
        List<String> childList;

        public ExpnadableListWithAdapter(Context context, List<VenueListing> success) {
            this.context = context;
            parentList = success;
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return parentList.get(groupPosition).getGrounds().size();
        }

        @Override
        public Object getChild(int groupPosition, int childPosition) {
            return parentList.get(groupPosition).getGrounds().get(childPosition);
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }


        @Override
        public int getChildType(int groupPosition, int childPosition) {
            return super.getChildType(groupPosition, childPosition);
        }


        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }

        @Override
        public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
            ChildViewHolder childViewHolder = new ChildViewHolder();

            if (convertView == null) {
                LayoutInflater infalInflater = (LayoutInflater) this.context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(com.groundhog.vendor.android.R.layout.list_items, null);
            }

            childViewHolder.ground_name = (TextView) convertView.findViewById(com.groundhog.vendor.android.R.id.venue_name);
            childViewHolder.sportImage = (CircleImageView) convertView.findViewById(com.groundhog.vendor.android.R.id.sport_image);
            childViewHolder.slotCount = (TextView) convertView.findViewById(com.groundhog.vendor.android.R.id.slots_count);
            childViewHolder.sportName = (TextView) convertView.findViewById(com.groundhog.vendor.android.R.id.ground_sport);
            childViewHolder.timings = (TextView) convertView.findViewById(com.groundhog.vendor.android.R.id.ground_timings);
            childViewHolder.sportsText = (TextView) convertView.findViewById(com.groundhog.vendor.android.R.id.sports_text);
            childViewHolder.timingsText = (TextView) convertView.findViewById(com.groundhog.vendor.android.R.id.ground_timings_text);
            childViewHolder.status = (TextView) convertView.findViewById(com.groundhog.vendor.android.R.id.status);

            childViewHolder.ground_name.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_SEMIBOLD));
            childViewHolder.sportName.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_REGULAR));
            childViewHolder.timings.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_REGULAR));
            childViewHolder.sportsText.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_REGULAR));
            childViewHolder.timingsText.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_REGULAR));
            childViewHolder.slotCount.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_REGULAR));
            childViewHolder.status.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_REGULAR));

            try {
                childViewHolder.ground_name.setText(parentList.get(groupPosition).getGrounds().get(childPosition).getName());
                childViewHolder.slotCount.setText(parentList.get(groupPosition).getGrounds().get(childPosition).getBooked_slots()
                        + "/" + parentList.get(groupPosition).getGrounds().get(childPosition).getFree_slots());
                childViewHolder.sportName.setText(parentList.get(groupPosition).getGrounds().get(childPosition).getSport_type());
               // childViewHolder.timings.setText(parentList.get(groupPosition).getGrounds().get(childPosition).getTiming());
                if (null != parentList.get(groupPosition).getGrounds().get(childPosition).getCover_pic())
                    if (!parentList.get(groupPosition).getGrounds().get(childPosition).getCover_pic().isEmpty())
                        Picasso.with(getActivity()).load(parentList.get(groupPosition).getGrounds().get(childPosition).getCover_pic()).fit().centerCrop().into(childViewHolder.sportImage);

            } catch (Exception e) {
                e.printStackTrace();
            }

            convertView.setTag(childViewHolder);
            return convertView;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public int getGroupCount() {
            Log.e("LENGTH", parentList.size() + "");
            return parentList.size();
        }

        @Override
        public Object getGroup(int groupPosition) {
            return null;
        }

        @Override
        public long getGroupId(int groupPosition) {
            return 0;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            final ParentViewHolder parentholder = new ParentViewHolder();
            if (convertView == null) {

                LayoutInflater infalInflater = (LayoutInflater) this.context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(com.groundhog.vendor.android.R.layout.parent_list, null);
            }
            parentholder.timeOfSlots = (TextView) convertView.findViewById(com.groundhog.vendor.android.R.id.time_slots);

            if(getChildrenCount(groupPosition)!=0) {
                parentholder.timeOfSlots.setText(parentList.get(groupPosition).getName());
                parentholder.timeOfSlots.setTypeface(PlaythoraUtility.getFont(getActivity(), RALEWAY_REGULAR));
                parentholder.timeOfSlots.setVisibility(View.VISIBLE);
                ((ExpandableListView) parent).expandGroup(groupPosition);
            }
            else{
                parentholder.timeOfSlots.setVisibility(View.GONE);
            }
            convertView.setTag(parentholder);
           /* ExpandableListView eLV = (ExpandableListView) parent;
            eLV.expandGroup(groupPosition);*/
            //((ExpandableListView) parent).expandGroup(groupPosition, true);
            return convertView;
        }


        protected class ParentViewHolder {
            protected TextView timeOfSlots;

        }

        protected class ChildViewHolder {
            protected TextView ground_name, slotCount, sportName, timings, sportsText, timingsText, status;
            CircleImageView sportImage;

        }


    }

   /* public class BookingAdapter extends RecyclerView.Adapter<BookingAdapter.MyViewHolder>{

        private Context mContext;
        ArrayList<BankAccountModel> bankAccountArrayList;
        RecyclerView recyclerView;


        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.booking_item_layout,parent,false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {

        }

        @Override
        public int getItemCount() {
            return 0;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView groundName,verified;
            GridView bookingGridView;
            public MyViewHolder(View itemView) {
                super(itemView);
                groundName = (TextView) itemView.findViewById(R.id.bookingGroundName);
                verified = (TextView) itemView.findViewById(R.id.bookingVerifiedText);
                bookingGridView = (GridView) itemView.findViewById(R.id.bookingGridView);
            }
        }
    }*/
}
