package com.groundhog.vendor.android.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.groundhog.vendor.android.model.NotificationList;
import com.groundhog.vendor.android.model.NotificationModel;
import com.groundhog.vendor.android.utils.Config;
import com.groundhog.vendor.android.utils.PlaythoraUtility;
import com.groundhog.vendor.android.utils.PreferenceManager;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import cz.msebera.android.httpclient.Header;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.groundhog.vendor.android.utils.Config.RALEWAY_REGULAR;

/**
 * Created by Nakul on 9/21/2016.
 */
public class Notification extends AppCompatActivity {

    RecyclerView notificationRecyclerView;
 
    Gson gson;
    public   NotificationAdapter notificationAdapter;
    Toolbar toolbar;
    TextView title,emptyText;
    PreferenceManager mPref;
    ProgressBar progressBar,paginateProgressBar;
    private int previousTotal = 0,pages = 1;
    private int visibleThreshold = 5;
    int firstVisibleItem, visibleItemCount, totalItemCount,lastVisibleItem;
    private LinearLayoutManager mLayoutManager;
    public NotificationModel notificationArrayList;
    List<NotificationList> tempList;
    private boolean loading = false;
    private Gson mGson;
    private SwipeRefreshLayout swipeContainer;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.groundhog.vendor.android.R.layout.notification_layout);
        mPref = PreferenceManager.instance(getApplicationContext());

        toolbar = (Toolbar) findViewById(com.groundhog.vendor.android.R.id.notification_toolbar);
        title = (TextView) findViewById(com.groundhog.vendor.android.R.id.toolbar_title);
        toolbar.setNavigationIcon(com.groundhog.vendor.android.R.drawable.ic_back_arrow);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        title.setText("Notifications");
        title.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));


       // getSupportActionBar().setTitle(Html.fromHtml("<font color=\"white\"><small>" + "Notifications" + "</small></font>"));

        notificationRecyclerView = (RecyclerView)findViewById(com.groundhog.vendor.android.R.id.notification_recycler_view);
        mLayoutManager = new LinearLayoutManager(Notification.this, LinearLayoutManager.VERTICAL, false);
        notificationRecyclerView.setLayoutManager(mLayoutManager);
        notificationRecyclerView.setHasFixedSize(true);
        paginateProgressBar = (ProgressBar) findViewById(com.groundhog.vendor.android.R.id.paginate_progress_bar);
        progressBar = (ProgressBar) findViewById(com.groundhog.vendor.android.R.id.progress_notification);
        emptyText = (TextView) findViewById(com.groundhog.vendor.android.R.id.empty_notification);

        emptyText.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));

        swipeContainer = (SwipeRefreshLayout) findViewById(com.groundhog.vendor.android.R.id.swipeContainer_notification);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                try {
                    populateNotification(1,false);
                    visibleThreshold = 10;
                    previousTotal = 0;
                    pages = 1;
                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        });
        swipeContainer.setColorSchemeResources(com.groundhog.vendor.android.R.color.colorPrimaryDark);


        notificationRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                visibleItemCount = recyclerView.getChildCount();
                totalItemCount = mLayoutManager.getItemCount();
                firstVisibleItem = mLayoutManager.findFirstVisibleItemPosition();
                lastVisibleItem = mLayoutManager.findLastVisibleItemPosition();
            //    Log.e("isHasMorePages", String.valueOf(notificationArrayList.isHasMorePages()));
                try {
                    if (notificationArrayList.isHasMorePages()) {
                        if (loading) {
                            if (totalItemCount > previousTotal) {
                                loading = false;
                                previousTotal = totalItemCount;
                                pages += 1;
                                try {
                                    paginateProgressBar.setVisibility(View.VISIBLE);
                                    populateNotification(pages, false);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    paginateProgressBar.setVisibility(View.GONE);
                                }
                                notificationAdapter.notifyDataSetChanged();

                            }
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
                if (!loading
                        && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    // End has been reached
                    Log.e("SIZE", "end called");
                    loading = true;
                    // Do something
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                Log.e("isHasMorePages", String.valueOf(notificationArrayList.isHasMorePages())+" onScrolled");
            }
        });

       // populateNotification(pages, false);
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            visibleThreshold = 10;
            previousTotal = 0;
            pages = 1;
            populateNotification(pages,true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void populateNotification(int pages, boolean b) {
        Log.e("PAGE",pages+"");


        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            try {
                if(b) {
                    progressBar.setVisibility(View.VISIBLE);
                } else {
                    paginateProgressBar.setVisibility(View.GONE);
                }
                AsyncHttpClient mHttpClient = new AsyncHttpClient();
                mHttpClient.addHeader("accessToken", mPref.getAccessToken());

                mHttpClient.get(Config.NOTIFICATION_LIST + "?page=" +pages,
                        new AsyncHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                progressBar.setVisibility(View.GONE);
                                paginateProgressBar.setVisibility(View.GONE);
                                swipeContainer.setRefreshing(false);
                                try {
                                    String s = new String(responseBody);
                                    Log.e("notification", s);
                                    mGson = new Gson();
                                    notificationArrayList = new NotificationModel();
                                    notificationArrayList = mGson.fromJson(s, NotificationModel.class);
                                    Log.e("RESPONSE", String.valueOf(notificationArrayList.isHasMorePages()));

                                    if(notificationArrayList.getSuccess().size()!=0){

                                        if(notificationArrayList.getCurrentPage()>1) {
                                            for(int i=0;i< notificationArrayList.getSuccess().size();i++){
                                                tempList.add(notificationArrayList.getSuccess().get(i));
                                            }

                                            Log.e("venueList", String.valueOf(notificationArrayList.getCurrentPage()));
                                            notificationAdapter.Refresh(tempList,2);
                                        } else {
                                            tempList = notificationArrayList.getSuccess();
                                            emptyText.setVisibility(View.GONE);
                                            notificationAdapter = new NotificationAdapter(Notification.this, notificationArrayList.getSuccess());
                                            notificationRecyclerView.setAdapter(notificationAdapter);
                                        }
                                    }else{
                                        notificationRecyclerView.setAdapter(null);
                                        emptyText.setVisibility(View.VISIBLE);
                                    }

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                }

                            }

                            @Override
                            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                                progressBar.setVisibility(View.GONE);
                                paginateProgressBar.setVisibility(View.GONE);
                                swipeContainer.setRefreshing(false);
                                String s = new String(bytes);
                                Log.d("RESPONSE_FAIL", s);
                                try {
                                    JSONObject object = new JSONObject(s);
                                    if(object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400){
                                        mPref.setAccessToken("");
                                        Toast.makeText(getApplicationContext(),object.getString("message"),Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(Notification.this,LoginActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }else Toast.makeText(Notification.this, "Some error occurred please try again later", Toast.LENGTH_SHORT).show();

                                } catch (JSONException e) {
                                    try {
                                        JSONObject object = new JSONObject(s);
                                        Toast.makeText(Notification.this, object.getString("message"), Toast.LENGTH_SHORT).show();
                                    }catch (Exception e1){
                                        Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                    }
                                }

                            }

                        });
            }catch (Exception e){}
        }else{
            swipeContainer.setRefreshing(false);
            paginateProgressBar.setVisibility(View.GONE);
            Toast.makeText(Notification.this, "Not connected to internet", Toast.LENGTH_LONG).show();

        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){

            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                break;
        }

        return false;
    }


    private class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.MyViewHolder>{
        public Context mContext;
        List<NotificationList> notificationArrayList;
        RecyclerView recyclerView;



        public NotificationAdapter(Context mContext, List<NotificationList> success) {
            this.mContext = mContext;
            this.notificationArrayList = success;

        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(com.groundhog.vendor.android.R.layout.notification_item_layout,parent,false);


            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, final int position) {

            holder.title.setText(notificationArrayList.get(position).getContent());
            holder.time.setText(notificationArrayList.get(position).getTime());

            final String type = notificationArrayList.get(position).getType();
            String status = notificationArrayList.get(position).getStatus()+"";
            String isRead = notificationArrayList.get(position).is_read()+"";
            if(notificationArrayList.get(position).getMore()!=null) {
                String more = notificationArrayList.get(position).getMore();
                Log.e("more",more);

                String[] strings = more.split(",");
                final String venueName = strings[0];
                final int venueId = Integer.parseInt(strings[1]);
                final String groundName = strings[2];
                final int groundId = Integer.parseInt(strings[3]);
                final String date = strings[4];

                Log.e("venueName",venueName);
                Log.e("venueId",venueId+"");
                Log.e("groundName",groundName);
                Log.e("groundId",groundId+"");
                Log.e("date",date);
            }
            final String id = notificationArrayList.get(position).getId()+"";

            Log.e("type",type);
            Log.e("status",status);
            Log.e("isRead",isRead);

            Log.e("id",id);

            if(isRead.equalsIgnoreCase("true")){
                holder.read.setVisibility(View.GONE);
            }else {
                holder.read.setVisibility(View.VISIBLE);
            }





            holder.title.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));



            if(notificationArrayList.get(position).getCover_pic()==null){

            }else {
                try{
                    Picasso.with(mContext).load(notificationArrayList.get(position).getCover_pic()).fit().config(Bitmap.Config.RGB_565).into(holder.userImage);
                }
                catch (IllegalArgumentException i){
                    i.printStackTrace();
                }

            }

            final View itemView = holder.itemView;


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String type = notificationArrayList.get(position).getType();
                    String id = notificationArrayList.get(position).getId()+"";
                    String more = notificationArrayList.get(position).getMore();

                    if(more!=null) {

                        String[] strings = more.split(",");
                        final String venueName = strings[0];
                        final int venueId = Integer.parseInt(strings[1]);
                        final String groundName = strings[2];
                        final int groundId = Integer.parseInt(strings[3]);
                        final String date = strings[4];

                        Log.e("venueName", venueName);
                        Log.e("venueId", venueId + "");
                        Log.e("groundName", groundName);
                        Log.e("groundId", groundId + "");
                        Log.e("date", date);

                        notifyRead(position, type, id, venueName, venueId, groundName, groundId, date);
                    }

                }
            });



        }

        private void notifyRead(final int position, final String type, String id, final String venueName, final int venueId, final String groundName, final int groundId, final String date) {

            if(PlaythoraUtility.checkInternetConnection(Notification.this)){
                try{
                    PlaythoraUtility.showProgressDialog(Notification.this);
                    AsyncHttpClient mHttpClient = new AsyncHttpClient(true, 80, 443);
                    mHttpClient.addHeader("accessToken", mPref.getAccessToken());
                    RequestParams params= new RequestParams();
                    params.add("notification_id",id);
                    Log.e("PARAMS", String.valueOf(params));

                    mHttpClient.post(Config.PLAYTHORA_VENDOR_BASEURL+"/player/notification/read",params,
                            new AsyncHttpResponseHandler() {
                                @Override
                                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                    PlaythoraUtility.hideProgressDialog();
                                    String s = new String(responseBody);
                                    Log.e("RESPONSE", s);
                                    try {
                                        JSONObject object = new JSONObject(s);
                                        if (object.getString("status").equalsIgnoreCase("success")) {

                                                Intent i = new Intent(Notification.this, BookingGroundSlotsActivity.class);
                                                i.putExtra("VENUENAME", venueName);
                                                i.putExtra("VENUEID", venueId);
                                                i.putExtra("GROUNDNAME", groundName);
                                                i.putExtra("GROUNDID", groundId);
                                                i.putExtra("DATE", date);
                                                startActivity(i);
                                        }
                                    } catch (JSONException e) {e.printStackTrace();
                                        Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                    }

                                }

                                @Override
                                public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                                    PlaythoraUtility.hideProgressDialog();
                                    try {
                                        String s = new String(bytes);
                                        Log.d("RESPONSE_FAIL", s);
                                        JSONObject object = new JSONObject(s);
                                        if(object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400){
                                            mPref.setAccessToken("");
                                            Toast.makeText(getApplicationContext(),object.getString("message"),Toast.LENGTH_SHORT).show();
                                            Intent intent = new Intent(Notification.this,LoginActivity.class);
                                            startActivity(intent);
                                            finishAffinity();

                                        }else Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        try{
                                            String s = new String(bytes);
                                            Log.d("RESPONSE_FAIL", s);
                                            JSONObject object = new JSONObject(s);
                                            Toast.makeText(getApplicationContext(),object.getString("message"),Toast.LENGTH_SHORT).show();
                                        }catch (Exception e1){
                                            e1.printStackTrace();
                                            Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                        }

                                    }

                                }

                            });
                }catch (Exception e){
                    e.printStackTrace();
                    PlaythoraUtility.hideProgressDialog();}

            }else Toast.makeText(getApplicationContext(), "Not connected to internet", Toast.LENGTH_LONG).show();

        }

        @Override
        public int getItemCount() {
            return (null != notificationArrayList ? notificationArrayList.size() : 0);
        }

        public void Refresh(List<NotificationList> tempList, int i) {
            this.notificationArrayList = tempList;
            notifyDataSetChanged();
        }


        public class MyViewHolder extends RecyclerView.ViewHolder {

            TextView title,time;
            CircleImageView userImage;
            View read;

            public MyViewHolder(View itemView) {
                super(itemView);
                title = (TextView) itemView.findViewById(com.groundhog.vendor.android.R.id.title);
                time = (TextView) itemView.findViewById(com.groundhog.vendor.android.R.id.notificationTime);
                read = (View) itemView.findViewById(com.groundhog.vendor.android.R.id.read_view);
                userImage = (CircleImageView) itemView.findViewById(com.groundhog.vendor.android.R.id.notification_image);


            }
        }
    }
}
