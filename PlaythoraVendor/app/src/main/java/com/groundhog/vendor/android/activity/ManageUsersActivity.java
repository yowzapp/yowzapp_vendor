package com.groundhog.vendor.android.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.groundhog.vendor.android.model.UsersModel;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Nakul on 9/15/2016.
 */
public class ManageUsersActivity extends AppCompatActivity{

    RecyclerView usersRecyclerView;
    FloatingActionButton addUsers;
    public static  ArrayList<UsersModel> usersArrayList;
    public  static  UsersAdapter usersAdapter;
    Gson gson;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.groundhog.vendor.android.R.layout.manage_users_layout);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(Html.fromHtml("<font color=\"white\"><small>" + "Users" + "</small></font>"));

        usersRecyclerView = (RecyclerView)findViewById(com.groundhog.vendor.android.R.id.users_recycler_view);
        usersRecyclerView.setLayoutManager(new LinearLayoutManager(ManageUsersActivity.this, LinearLayoutManager.VERTICAL, false));
        usersRecyclerView.setHasFixedSize(true);
        addUsers = (FloatingActionButton)findViewById(com.groundhog.vendor.android.R.id.add_users);
        addUsers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent addAccount = new Intent(ManageUsersActivity.this,AddUsersActivity.class);
                startActivity(addAccount);

            }
        });

    }

    private void UsersRecycler() {

        String response = "[{\n" +
                "    \"userName\": \"Nakul Kj\",\n" +
                "    \"id\": 1,\n" +
                "    \"role\": \"admin\",\n" +
                "    \"mobNumber\": \"9899978989\",\n" +
                "    \"email\": \"abc@gmail.com\",\n" +
                "    \"image\": \"http://images.bollywoodhungama.com/img/feature/16/mar/srkfan01.jpg \n" +
                "\n" +
                "\",\n" +
                "    \"venue\": \"Krs dam\"\n" +
                "}, {\n" +
                "    \"userName\": \"Rohith kj\",\n" +
                "    \"id\": 2,\n" +
                "    \"role\": \"manager\",\n" +
                "    \"mobNumber\": \"9898988989\",\n" +
                "    \"email\": \"xyz@gmail.com\",\n" +
                "    \"venue\": \"Krs dam\"\n" +
                "}]";


        try {

            JSONArray jsonArray = new JSONArray(response);
            usersArrayList=new ArrayList<>();
            gson = new Gson();
            for(int i=0;i<jsonArray.length();i++){
                usersArrayList.add(gson.fromJson(jsonArray.get(i).toString(),UsersModel.class));
            }
            if(usersRecyclerView.getAdapter()==null){
                usersAdapter = new UsersAdapter(ManageUsersActivity.this,usersRecyclerView,usersArrayList);
                usersRecyclerView.setAdapter(usersAdapter);
                usersAdapter.notifyDataSetChanged();

            }else {
                usersAdapter.refresh(usersArrayList);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    private class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.MyViewHolder>{
        private Context mContext;
        ArrayList<UsersModel> usersArrayList;
        RecyclerView recyclerView;

        public UsersAdapter(Context mContext,RecyclerView recyclerView, ArrayList<UsersModel> usersArrayList) {
            this.mContext = mContext;
            this.usersArrayList = usersArrayList;
            this.recyclerView = recyclerView;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(com.groundhog.vendor.android.R.layout.users_item_layout,parent,false);


            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, final int position) {
            holder.userName.setText(usersArrayList.get(position).getUserName());
            holder.userEmail.setText(usersArrayList.get(position).getEmail());
            holder.userRole.setText(usersArrayList.get(position).getRole());

            if(usersArrayList.get(position).getImage()==null){
                holder.userImage.setImageResource(com.groundhog.vendor.android.R.drawable.baby);
            }else {
                Picasso.with(mContext).load(usersArrayList.get(position).getImage()).fit().config(Bitmap.Config.RGB_565).into(holder.userImage);

            }

            holder.edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent edit = new Intent(ManageUsersActivity.this,EditUsersActivity.class);
                    edit.putExtra("UserImage",usersArrayList.get(position).getImage());
                    edit.putExtra("UserName",usersArrayList.get(position).getUserName());
                    edit.putExtra("UserEmail",usersArrayList.get(position).getEmail());
                    edit.putExtra("UserRole",usersArrayList.get(position).getRole());
                    edit.putExtra("UserMobileNumber",usersArrayList.get(position).getMobNumber());
                    edit.putExtra("UserVenue",usersArrayList.get(position).getVenue());
                    startActivity(edit);
                }
            });






            final View itemView = holder.itemView;



        }

        @Override
        public int getItemCount() {
            return (null != usersArrayList ? usersArrayList.size() : 0);
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView userName,userEmail,userRole;
            CircleImageView userImage;
            ImageView edit,delete;
            public MyViewHolder(View itemView) {
                super(itemView);
                userName = (TextView) itemView.findViewById(com.groundhog.vendor.android.R.id.userName);
                userEmail = (TextView) itemView.findViewById(com.groundhog.vendor.android.R.id.userEmailId);
                userRole = (TextView) itemView.findViewById(com.groundhog.vendor.android.R.id.userRole);
                userImage = (CircleImageView) itemView.findViewById(com.groundhog.vendor.android.R.id.users_image);
                edit = (ImageView) itemView.findViewById(com.groundhog.vendor.android.R.id.userEdit);
                delete = (ImageView) itemView.findViewById(com.groundhog.vendor.android.R.id.userDelete);


            }
        }

        public void refresh(ArrayList<UsersModel> usersArrayList){
            this.usersArrayList=usersArrayList;
            notifyDataSetChanged();

        }



    }


    @Override
    public void onResume() {
        super.onResume();
        if(usersRecyclerView.getAdapter()==null){
            UsersRecycler();
        }else {
            usersAdapter.refresh(usersArrayList);
        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                break;
        }

        return false;
    }
}
