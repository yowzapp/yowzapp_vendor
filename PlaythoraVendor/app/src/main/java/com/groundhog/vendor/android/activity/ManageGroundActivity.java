package com.groundhog.vendor.android.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.groundhog.vendor.android.R;
import com.groundhog.vendor.android.adapter.CustomAdapter;
import com.groundhog.vendor.android.adapter.SportsAdapter;
import com.groundhog.vendor.android.model.DaysListModel;
import com.groundhog.vendor.android.model.DaysListSuccess;
import com.groundhog.vendor.android.model.SportsSuccessList;
import com.groundhog.vendor.android.utils.Config;
import com.groundhog.vendor.android.utils.PlaythoraUtility;
import com.groundhog.vendor.android.utils.PreferenceManager;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

import static com.groundhog.vendor.android.adapter.CustomAdapter.newlist;
import static com.groundhog.vendor.android.utils.Config.RALEWAY_BOLD;
import static com.groundhog.vendor.android.utils.Config.RALEWAY_REGULAR;

public class ManageGroundActivity extends AppCompatActivity implements View.OnClickListener {
    TextView sunday, monday, tuesday, wednesday, thursday, friday, saturday;
    TextView toolbarTitle;
    TextView textGroundName, groundName, textSportsTypeName, sportsTypeName, changeGroundName, changeSportsTypeName;
    private Toolbar toolbar;
    PreferenceManager mPref;
    DaysListModel daysListModel;
    Gson gson;
    RecyclerView recyclerView;
    int groundID;
    String sportsIcon, groundNamee;

    RecyclerView recyclerSports;
    int position;
    private ArrayList<SportsSuccessList> sportsArrayList;
    private Dialog editGroundNameDialog;
    private Dialog editSportsTypeDialog;
    private Dialog deleteGroundDialog;
    private String games;
    private SportsAdapter sportsAdapter;
    private int sportsId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_ground);
        mPref = PreferenceManager.instance(ManageGroundActivity.this);
        initializeViews();
        setOnClickListeners();
        buildSportsDialog();


        Intent intent = getIntent();
        groundID = intent.getIntExtra("ground_id", 0);
        sportsIcon = intent.getStringExtra("sports_icon");
        groundNamee = intent.getStringExtra("ground_name");
        Log.e("groundID", groundID + " ground_id");

        Log.e("spoicon1", sportsIcon + "");
        Log.e("ground_name1", groundName + "");


        data();


        populateDaysList(groundID);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_delete, menu);
        return true;
    }

    private void data() {
        recyclerSports.setHasFixedSize(true);
        recyclerSports.setLayoutManager(new GridLayoutManager(getApplicationContext(), 3));
        if (getIntent().hasExtra("POSITION")) {
            try {
                games = AddGroundsActivity.allsports;
                position = getIntent().getIntExtra("POSITION", 0);
                Log.e("DEATIL", String.valueOf(position));
                groundName.setText(CustomAdapter.apiList.get(position).getName());
                sportsTypeName.setText(CustomAdapter.apiList.get(position).getSports_name());
                sportsId = CustomAdapter.apiList.get(position).getSports_id();
//                gID = CustomAdapter.apiList.get(position).getId();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        sportsArrayList = new ArrayList<>();
        if (games != null) {
            gson = new Gson();
            JSONArray jsonArray = null;
            try {
                jsonArray = new JSONArray(games);
                for (int i = 0; i < jsonArray.length(); i++) {
                    sportsArrayList.add(gson.fromJson(jsonArray.get(i).toString(), SportsSuccessList.class));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        sportsAdapter = new SportsAdapter(ManageGroundActivity.this, sportsArrayList, groundID);
        recyclerSports.setAdapter(sportsAdapter);

    }

    @Override
    protected void onResume() {
        super.onResume();
        populateDaysList(groundID);
    }

    private void populateDaysList(int id) {
        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {

           // PlaythoraUtility.showProgressDialog(ManageGroundActivity.this);

            AsyncHttpClient mHttpClient = new AsyncHttpClient();
            mHttpClient.addHeader("accessToken", mPref.getAccessToken());
            mHttpClient.get(Config.DAYS_RANGE + id, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                   // PlaythoraUtility.hideProgressDialog();
                    String response = new String(responseBody);
                    Log.e("DAYS_RANGE", response);
                    try {
                        JSONObject object = new JSONObject(response);
                        daysListModel = new DaysListModel();
                        gson = new Gson();
                        daysListModel = gson.fromJson(response, DaysListModel.class);
                        if (daysListModel.getStatusCode() == 200) {
                            if (daysListModel.getSuccess() != null && daysListModel.getSuccess().size() != 0) {
                                DaysAdapter daysAdapter = new DaysAdapter(ManageGroundActivity.this, daysListModel.getSuccess());
                                recyclerView.setAdapter(daysAdapter);
                            } else {
                                Toast.makeText(getApplicationContext(), "Ground Day Range List is Empty", Toast.LENGTH_SHORT).show();
                            }
                        }

                    } catch (JSONException e) {
                        Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                    } catch (IllegalArgumentException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                   // PlaythoraUtility.hideProgressDialog();

                    try {
                        String s = new String(bytes);
                        Log.e("DAYS_RANGE_FAIL", s);
                        JSONObject object = new JSONObject(s);
                        if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                            mPref.setAccessToken("");
                            Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(ManageGroundActivity.this, LoginActivity.class);
                            startActivity(intent);
                            finish();
                        } else
                            Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                        // Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                    }

                }

            });
        } else {
            Toast.makeText(getApplicationContext(), "Not connected to internet", Toast.LENGTH_LONG).show();

        }
    }

    private void setOnClickListeners() {
        changeGroundName.setOnClickListener(this);
        changeSportsTypeName.setOnClickListener(this);
    }

    private void initializeViews() {
        toolbar = (Toolbar) findViewById(R.id.venuSlotsToolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbarTitle = (TextView) findViewById(R.id.toolbar_title);
        textGroundName = (TextView) findViewById(R.id.text_ground_name);
        groundName = (TextView) findViewById(R.id.ground_name);
        textSportsTypeName = (TextView) findViewById(R.id.text_sports_type_name);
        sportsTypeName = (TextView) findViewById(R.id.sports_type_name);
        changeGroundName = (TextView) findViewById(R.id.change_ground_name);
        changeSportsTypeName = (TextView) findViewById(R.id.change_ground_sports_type);
        recyclerSports = (RecyclerView) findViewById(R.id.recycler_sports);
        recyclerView = (RecyclerView) findViewById(R.id.days_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(ManageGroundActivity.this, LinearLayoutManager.VERTICAL, false));


        toolbarTitle.setTypeface(PlaythoraUtility.getFont(this, RALEWAY_REGULAR));
        textGroundName.setTypeface(PlaythoraUtility.getFont(this, RALEWAY_REGULAR));
        groundName.setTypeface(PlaythoraUtility.getFont(this, RALEWAY_REGULAR));
        textSportsTypeName.setTypeface(PlaythoraUtility.getFont(this, RALEWAY_REGULAR));
        sportsTypeName.setTypeface(PlaythoraUtility.getFont(this, RALEWAY_REGULAR));
        changeGroundName.setTypeface(PlaythoraUtility.getFont(this, RALEWAY_REGULAR));
        changeSportsTypeName.setTypeface(PlaythoraUtility.getFont(this, RALEWAY_REGULAR));

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.change_ground_name:
                editGroundNameDialog();
                break;
            case R.id.change_ground_sports_type:
                editSportsTypeDialog.show();
                break;
            case R.id.ic_ground_delete:
                deleteGroundDialog();
                break;
            default:
                break;
        }
    }

    private void deleteGroundDialog() {
        deleteGroundDialog = new Dialog(ManageGroundActivity.this);
        deleteGroundDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        deleteGroundDialog.setCanceledOnTouchOutside(false);
        deleteGroundDialog.setContentView(com.groundhog.vendor.android.R.layout.delete_ground_dialog);

        TextView deleteText = (TextView) deleteGroundDialog.findViewById(R.id.text_delete_ground);
        TextView deleteConfirm = (TextView) deleteGroundDialog.findViewById(R.id.button_confirm_delete_ground);
        TextView deleteCancel = (TextView) deleteGroundDialog.findViewById(R.id.button_cancel_delete_ground);

        deleteText.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        deleteConfirm.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_BOLD));
        deleteCancel.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_BOLD));

        deleteConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callDeleteGroundAPI();
            }
        });

        deleteCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteGroundDialog.dismiss();
            }
        });

        deleteGroundDialog.show();
    }

    private void callDeleteGroundAPI() {
        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            PlaythoraUtility.showProgressDialog(ManageGroundActivity.this);
            AsyncHttpClient mHttpClient = new AsyncHttpClient();
            mHttpClient.addHeader("accessToken", mPref.getAccessToken());
            RequestParams params = new RequestParams();
            params.add("ground_id", String.valueOf(groundID));
            Log.e("PARAMS", String.valueOf(params));
            mHttpClient.post(Config.DELETE_GROUND, params,
                    new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                            PlaythoraUtility.hideProgressDialog();
                            try {
                                String s = new String(responseBody);
                                Log.e("RESPONSE", s);
                                JSONObject object = new JSONObject(s);
                                if(object.getInt("statusCode")==200){
                                    Toast.makeText(ManageGroundActivity.this, "Ground Deleted successfully", Toast.LENGTH_SHORT).show();
                                    deleteGroundDialog.dismiss();
                                    newlist.remove(position);
                                    onBackPressed();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        @Override
                        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                            PlaythoraUtility.hideProgressDialog();
                            try {
                                String s = new String(responseBody);
                                Log.e("RESPONSE_FAIL", s);
                                JSONObject object = new JSONObject(s);
                                if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                                    mPref.setAccessToken("");
                                    Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(ManageGroundActivity.this, LoginActivity.class);
                                    startActivity(intent);
                                    finish();
                                } else
                                    Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                            } catch (Exception e) {
                                e.printStackTrace();
                                //Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        } else {
            Toast.makeText(getApplicationContext(), "Not connected to internet", Toast.LENGTH_LONG).show();
        }
    }

    private void editGroundNameDialog() {
        editGroundNameDialog = new Dialog(ManageGroundActivity.this);
        editGroundNameDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        editGroundNameDialog.setCanceledOnTouchOutside(false);
        editGroundNameDialog.setContentView(com.groundhog.vendor.android.R.layout.edit_ground_name_dialog);

        TextView textEnterGroundName = (TextView) editGroundNameDialog.findViewById(R.id.text_enter_ground_name);
        final EditText enterGroundName = (EditText) editGroundNameDialog.findViewById(R.id.edit_ground_name);
        Button buttonChangeGroundName = (Button) editGroundNameDialog.findViewById(R.id.button_change_ground_name);

        textEnterGroundName.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        enterGroundName.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        buttonChangeGroundName.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));

        enterGroundName.setText(groundName.getText().toString());

        buttonChangeGroundName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (enterGroundName.getText().toString().trim().equalsIgnoreCase("")) {
                    editGroundNameDialog.dismiss();
                } else {
                    editGround(true, enterGroundName.getText().toString().trim());
                }
            }
        });
        editGroundNameDialog.show();
    }

    private void buildSportsDialog() {

        editSportsTypeDialog = new Dialog(ManageGroundActivity.this);
        editSportsTypeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        editSportsTypeDialog.setCanceledOnTouchOutside(false);
        editSportsTypeDialog.setContentView(com.groundhog.vendor.android.R.layout.sports_type_dialog);

        recyclerSports = (RecyclerView) editSportsTypeDialog.findViewById(com.groundhog.vendor.android.R.id.recycler_sports);
        TextView sportsText = (TextView) editSportsTypeDialog.findViewById(com.groundhog.vendor.android.R.id.sports_text);
        sportsText.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        Button selectSlot = (Button) editSportsTypeDialog.findViewById(com.groundhog.vendor.android.R.id.venue_slots_);
        selectSlot.setText("CHANGE");

        selectSlot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SportsAdapter.sportName.isEmpty()) {
                    Toast.makeText(ManageGroundActivity.this, "Select sport", Toast.LENGTH_SHORT).show();
                } else {
                    editGround(false, "");
                }
            }
        });

    }

    private void editGround(final boolean isName, final String name) {
        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            //PlaythoraUtility.showProgressDialog(ManageGroundActivity.this);
            AsyncHttpClient mHttpClient = new AsyncHttpClient();
            mHttpClient.addHeader("accessToken", mPref.getAccessToken());
            RequestParams params = new RequestParams();
//            params.add("start_time", getStartTime);
//            params.add("end_time", getEndTime);
//            params.add("slot_duration", slotDur[0]);
//            params.add("max_member", slotMember.getText().toString().trim());
//            params.add("slot_price", slotPrice.getText().toString().trim());
            params.add("ground_id", String.valueOf(groundID));
            if (isName) {
                params.add("name", name);
            } else {
                params.add("sports_id", SportsAdapter.sportId);
                params.add("sports_name", SportsAdapter.sportName);
            }
            Log.e("PARAMS", String.valueOf(params));
            mHttpClient.post(Config.EDIT_GROUND, params,
                    new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                            PlaythoraUtility.hideProgressDialog();
                            try {
                                String s = new String(responseBody);
                                Log.e("RESPONSE", s);
                                JSONObject object = new JSONObject(s);
                                if (object.getString("status").equalsIgnoreCase("success")) {
                                    if (isName) {
                                        groundName.setText(name);
                                        newlist.get(position).setName(name);
                                        editGroundNameDialog.dismiss();
                                    } else {
                                        AddGroundsActivity.value = true;
                                        sportsTypeName.setText(SportsAdapter.sportName);
                                        newlist.get(position).setSports_name(SportsAdapter.sportName);
                                        newlist.get(position).setSports_image(SportsAdapter.sportImage);
                                        editSportsTypeDialog.dismiss();
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                            PlaythoraUtility.hideProgressDialog();
                            try {
                                String s = new String(responseBody);
                                Log.e("RESPONSE_FAIL", s);
                                JSONObject object = new JSONObject(s);
                                if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                                    mPref.setAccessToken("");
                                    Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(ManageGroundActivity.this, LoginActivity.class);
                                    startActivity(intent);
                                    finish();
                                } else
                                    Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                            } catch (Exception e) {
                                e.printStackTrace();
                                //Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        } else {
            Toast.makeText(getApplicationContext(), "Not connected to internet", Toast.LENGTH_LONG).show();
        }
    }

    private void callEditGroundSlotDetailActivity(String dayString) {
        Intent editGroundSlotDetailActivity = new Intent(ManageGroundActivity.this, EditGroundSlotDetailsActivity.class);
        editGroundSlotDetailActivity.putExtra("dayString", dayString);
        startActivity(editGroundSlotDetailActivity);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.ic_ground_delete:
                deleteGroundDialog();
            default:
                break;
        }
        return false;
    }

    public class DaysAdapter extends RecyclerView.Adapter<DaysAdapter.DaysHolder> {

        Context context;
        List<DaysListSuccess> daysListSuccess;

        public DaysAdapter(ManageGroundActivity manageGroundActivity, List<DaysListSuccess> success) {
            this.context = manageGroundActivity;
            this.daysListSuccess = success;
        }

        @Override
        public DaysAdapter.DaysHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View contentView = LayoutInflater.from(parent.getContext()).inflate(R.layout.days_type_item, null);
            return new DaysHolder(contentView);
        }

        @Override
        public void onBindViewHolder(final DaysAdapter.DaysHolder holder, final int position) {

            if (daysListSuccess.get(position).getName() != null) {
                holder.dayText.setText(daysListSuccess.get(position).getName());
            }

            if (daysListSuccess.get(position).getData_available() == 0) {
                holder.availabilityStatus.setImageResource(R.drawable.ic_check_grey);
            } else {
                holder.availabilityStatus.setImageResource(R.drawable.ic_check_red);
            }

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ManageGroundActivity.this, EditGroundSlotDetailsActivity.class);
                    intent.putExtra("dayString", daysListSuccess.get(position).getName());
                    intent.putExtra("dayID", daysListSuccess.get(position).getId() + "");
                    intent.putExtra("groundID", groundID + "");
                    startActivityForResult(intent, 10);
                }
            });
        }

        @Override
        public int getItemCount() {
            return daysListSuccess.size();
        }

        public class DaysHolder extends RecyclerView.ViewHolder {
            TextView dayText;
            ImageView availabilityStatus;

            public DaysHolder(View itemView) {
                super(itemView);
                dayText = (TextView) itemView.findViewById(R.id.day_text);
                availabilityStatus = (ImageView) itemView.findViewById(R.id.check);
                dayText.setTypeface(PlaythoraUtility.getFont(ManageGroundActivity.this, RALEWAY_REGULAR));
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 10) {
            String groundId = "";
            groundId = data.getStringExtra("groundID");
            if (!groundId.isEmpty()) {
                populateDaysList(Integer.parseInt(groundId));
            }
        }
    }
}
