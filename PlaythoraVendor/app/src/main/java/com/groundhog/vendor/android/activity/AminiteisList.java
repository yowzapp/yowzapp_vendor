package com.groundhog.vendor.android.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.groundhog.vendor.android.model.AllAmenities;
import com.groundhog.vendor.android.utils.Config;
import com.groundhog.vendor.android.utils.PlaythoraUtility;
import com.groundhog.vendor.android.utils.PreferenceManager;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;

import static com.groundhog.vendor.android.utils.Config.RALEWAY_REGULAR;

/**
 * Created by vaishakha on 23/12/16.
 */
public class AminiteisList extends AppCompatActivity {
    ListView amenitiesList;
    AmenitiesAdapter adapter;
    String allAmenities = "", selectedAmenities = "";
    ArrayList<AllAmenities> allAmenitiesModel, selectedAmenitiesModel;
    Gson gson;
    Button save;
    private boolean[] mCheckedState;
    int venueId;
    PreferenceManager mPref;
    public static boolean value;
    Toolbar toolbar;
    TextView title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.groundhog.vendor.android.R.layout.amenities_list);
        toolbar = (Toolbar) findViewById(com.groundhog.vendor.android.R.id.amenities_toolbar);
        title = (TextView) findViewById(com.groundhog.vendor.android.R.id.toolbar_title);
        toolbar.setNavigationIcon(com.groundhog.vendor.android.R.drawable.ic_back_arrow);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        title.setText("Amenities");
        title.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));

        // getSupportActionBar().setTitle(Html.fromHtml("<font color=\"white\"><small>" + "Amenities" + "</small></font>"));

        amenitiesList = (ListView) findViewById(com.groundhog.vendor.android.R.id.amenities_list);
        save = (Button) findViewById(com.groundhog.vendor.android.R.id.save_amenities);
        mPref = PreferenceManager.instance(this);

        save.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));

        Intent intent = getIntent();
        allAmenities = intent.getStringExtra("ALLAMENITIES");
        selectedAmenities = intent.getStringExtra("SELECTEDAMENITIES");
        ;
        venueId = intent.getIntExtra("VenueId", 0);

        allAmenitiesModel = new ArrayList<>();
        if (!allAmenities.isEmpty()) {
            gson = new Gson();
            JSONArray jsonArray = null;
            try {
                jsonArray = new JSONArray(allAmenities);
                for (int i = 0; i < jsonArray.length(); i++) {
                    allAmenitiesModel.add(gson.fromJson(jsonArray.get(i).toString(), AllAmenities.class));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        selectedAmenitiesModel = new ArrayList<>();
        if (!selectedAmenities.isEmpty()) {
            gson = new Gson();
            JSONArray jsonArray = null;
            try {
                jsonArray = new JSONArray(selectedAmenities);
                for (int i = 0; i < jsonArray.length(); i++) {
                    selectedAmenitiesModel.add(gson.fromJson(jsonArray.get(i).toString(), AllAmenities.class));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject jsonObject = null;
                JSONArray jsonArray = new JSONArray();
                for (int i = 0; i < allAmenitiesModel.size(); i++) {
                    jsonObject = new JSONObject();
                    if (mCheckedState[i]) {
                        try {
                            jsonObject.put("id", allAmenitiesModel.get(i).getId());

                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        jsonArray.put(jsonObject);
                    }
                }

                JSONObject finalobject = new JSONObject();
                try {
                    finalobject.put("amenities", jsonArray);
                    finalobject.put("venue_id", venueId);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                postAmenities(finalobject);
            }
        });


        adapter = new AmenitiesAdapter(AminiteisList.this, allAmenitiesModel, selectedAmenitiesModel);
        amenitiesList.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        amenitiesList.setAdapter(adapter);

    }

    private void postAmenities(JSONObject finalobject) {

        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            try {
                PlaythoraUtility.showProgressDialog(AminiteisList.this);
                AsyncHttpClient mHttpClient = new AsyncHttpClient();
                mHttpClient.addHeader("accessToken", mPref.getAccessToken());
                StringEntity se = new StringEntity(finalobject.toString());
                Log.e("JSONOBJ", se.toString());
                mHttpClient.post(getApplicationContext(), Config.EDIT_VENUE, se, "application/json",
                        new AsyncHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                PlaythoraUtility.hideProgressDialog();
                                try {
                                    String s = new String(responseBody);
                                    Log.e("RESPONSE", s);
                                    JSONObject object = new JSONObject(s);
                                    if (object.getString("status").equalsIgnoreCase("success")) {
                                        value = true;
                                        Intent i = new Intent();
                                        setResult(1, i);
                                        finish();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                }

                            }

                            @Override
                            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                                PlaythoraUtility.hideProgressDialog();
                                try {
                                    String s = new String(bytes);
                                    Log.d("RESPONSE_FAIL", s);
                                    JSONObject object = new JSONObject(s);
                                    if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                                        mPref.setAccessToken("");
                                        Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(AminiteisList.this, LoginActivity.class);
                                        startActivity(intent);
                                        finish();
                                    } else
                                        Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    try {
                                        String s = new String(bytes);
                                        Log.d("RESPONSE_FAIL", s);
                                        JSONObject object = new JSONObject(s);
                                        Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_SHORT).show();
                                    } catch (Exception e1) {
                                        e1.printStackTrace();
                                        //Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                    }

                                }

                            }

                        });
            } catch (Exception e) {
            }
        } else {

            Toast.makeText(getApplicationContext(), "Not connected to internet", Toast.LENGTH_LONG).show();

        }

    }

    public class AmenitiesAdapter extends BaseAdapter {
        Context context;
        ArrayList<AllAmenities> allAmenitiesModel;
        ArrayList<AllAmenities> selectedAmenitiesModel;

        public AmenitiesAdapter(Context context, ArrayList<AllAmenities> allAmenitiesModel, ArrayList<AllAmenities> selectedAmenitiesModel) {
            this.context = context;
            this.allAmenitiesModel = allAmenitiesModel;
            this.selectedAmenitiesModel = selectedAmenitiesModel;
            mCheckedState = new boolean[this.allAmenitiesModel.size()];
            /*for(int i = 0 ;i<this.allAmenitiesModel.size();i++){
                for (int j = 0;j<this.selectedAmenitiesModel.size();j++){
                    if(this.selectedAmenitiesModel.get(j).getId()==this.allAmenitiesModel.get(i).getId()){
                        mCheckedState[i]=true;
                    }else mCheckedState[i]=false;
                }
            }*/

            for (int i = 0; i < allAmenitiesModel.size(); i++) {
                mCheckedState[i] = false;
            }
        }

        @Override
        public int getCount() {
            return allAmenitiesModel.size();
        }

        @Override
        public Object getItem(int position) {
            return allAmenitiesModel.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            convertView = inflater.inflate(com.groundhog.vendor.android.R.layout.amenities_item, parent, false);
            AmenitiesHolder holder = new AmenitiesHolder();
            holder.checkBox = (CheckBox) convertView.findViewById(com.groundhog.vendor.android.R.id.amenities_item);
            holder.amenitiesText = (TextView) convertView.findViewById(com.groundhog.vendor.android.R.id.amenities_name);
            holder.amenitiesText.setText(allAmenitiesModel.get(position).getName());

            holder.amenitiesText.setTypeface(PlaythoraUtility.getFont(context, RALEWAY_REGULAR));

            for (int i = 0; i < selectedAmenitiesModel.size(); i++) {
                if (allAmenitiesModel.get(position).getId() == selectedAmenitiesModel.get(i).getId()) {
                    holder.checkBox.setChecked(true);
                    mCheckedState[position] = true;
                }
            }
            /*if(mCheckedState[position]){
                holder.checkBox.setChecked(true);
            }else holder.checkBox.setChecked(false);*/

            holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        mCheckedState[position] = true;
                    } else mCheckedState[position] = false;
                }
            });

            return convertView;
        }

        class AmenitiesHolder {
            CheckBox checkBox;
            TextView amenitiesText;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                //Toast.makeText(getApplicationContext(),"Back button clicked", Toast.LENGTH_SHORT).show();
                onBackPressed();
                return true;
            default:
                break;

        }
        return false;
    }

}
