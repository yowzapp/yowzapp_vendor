package com.groundhog.vendor.android.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.appcompat.BuildConfig;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.groundhog.vendor.android.utils.Config;
import com.groundhog.vendor.android.utils.PlaythoraUtility;
import com.groundhog.vendor.android.utils.PreferenceManager;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import cz.msebera.android.httpclient.Header;

import static com.groundhog.vendor.android.utils.Config.RALEWAY_REGULAR;

/**
 * Created by Nakul on 9/15/2016.
 */
public class EditProfileActivity extends AppCompatActivity {

    EditText name;
    Button save;
    ImageView profileImage;
    RelativeLayout profImageLayout;
    private static final int PICK_FROM_CAM = 1;
    private int PICK_IMAGE_REQUEST = 2;
    String selectedImagePath = "",profile_pic,userName;
    TextInputLayout nameInputLayout;
    PreferenceManager mPref;
    int serverResponseCode = 0 ;
    Toolbar toolbar;
    TextView title;
    private File output;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.groundhog.vendor.android.R.layout.edit_profile_layout);
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        mPref = PreferenceManager.instance(this);
        getUserDetail();
       // getSupportActionBar().setDisplayHomeAsUpEnabled(true);
       // getSupportActionBar().setTitle(Html.fromHtml("<font color=\"white\"><small>" + "Edit your profile" + "</small></font>"));
        toolbar = (Toolbar) findViewById(com.groundhog.vendor.android.R.id.edit_toolbar);
        title = (TextView) findViewById(com.groundhog.vendor.android.R.id.toolbar_title_profile);
        toolbar.setNavigationIcon(com.groundhog.vendor.android.R.drawable.ic_back_arrow);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        title.setText("Edit your profile");
        title.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));

        initializeAllComponents();

        profImageLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (shouldAskPermission()) {
                    String[] perms = {"android.permission.WRITE_EXTERNAL_STORAGE", "android.permission.READ_EXTERNAL_STORAGE", "android.permission.CAMERA"};

                    int permsRequestCode = 200;

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(perms, permsRequestCode);
                    }
                } else {
                    UploadImage();
                }

            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(checkForValidation()){
                    EditUser();
                    //PlaythoraUtility.showProgressDialog(EditProfileActivity.this);
                    /*Intent save = new Intent(EditProfileActivity.this,HomeActivity.class);
                    save.putExtra("ActivityName","Settings");
                    startActivity(save);*/
                }

            }
        });

        name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
               nameInputLayout.setError(null);
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions, int[] grantResults) {


        if(permsRequestCode==200){
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.e("startperm","request");
                UploadImage();
            } else {
                Toast.makeText(EditProfileActivity.this,  "Permission denied, You need to give permission to use this feature", Toast.LENGTH_SHORT).show();

            }

        }

    }

    private void EditUser() {
        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            PlaythoraUtility.showProgressDialog(EditProfileActivity.this);
            AsyncHttpClient mHttpClient = new AsyncHttpClient();
            mHttpClient.addHeader("accessToken",mPref.getAccessToken());
            RequestParams params= new RequestParams();
            params.add("name", name.getText().toString().trim());
            mHttpClient.post(Config.EDIT_USER,params,
                    new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                            PlaythoraUtility.hideProgressDialog();
                            String s = new String(responseBody);
                            Log.e("RESPONSE", s);
                            try {
                                JSONObject object = new JSONObject(s);
                                if(object.getString("status").equalsIgnoreCase("success")) {
                                    userName = object.getJSONObject("success").getString("name");
                                    mPref.setName(userName);
                                    Toast.makeText(getApplicationContext(), object.getString("message").toString(), Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(EditProfileActivity.this,HomeActivity.class);
                                    intent.putExtra("ActivityName","Settings");
                                    startActivity(intent);
                                    finishAffinity();
                                }
                            } catch (JSONException e) {
                                Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                            }

                        }
                        @Override
                        public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                            PlaythoraUtility.hideProgressDialog();
                            String s = new String(bytes);
                            Log.e("RESPONSE_FAIL", s);
                            try {
                                JSONObject object = new JSONObject(s);
                                if(object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400){
                                    mPref.setAccessToken("");
                                    Toast.makeText(getApplicationContext(),object.getString("message"),Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(EditProfileActivity.this,LoginActivity.class);
                                    startActivity(intent);
                                    finish();
                                }else Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                                //Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                            }

                        }

                    });
        }else{

            Toast.makeText(getApplicationContext(), "Not connected to internet", Toast.LENGTH_LONG).show();

        }

    }

    private void getUserDetail() {

        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            PlaythoraUtility.showProgressDialog(EditProfileActivity.this);
            AsyncHttpClient mHttpClient = new AsyncHttpClient();
            mHttpClient.addHeader("accessToken",mPref.getAccessToken());
            mHttpClient.get(Config.USER_DETAIL,
                    new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                            PlaythoraUtility.hideProgressDialog();
                            String s = new String(responseBody);
                            Log.e("getUser", s);
                            try {
                                JSONObject object = new JSONObject(s);
                                if(object.getString("status").equalsIgnoreCase("success")) {
                                    userName = object.getJSONObject("success").getString("name");
                                    profile_pic = object.getJSONObject("success").getString("profile_pic");
                                    mPref.setName(userName);
                                    mPref.setProfilePic(profile_pic);
                                    name.setText(userName);

                                    int pos = name.length();
                                    Editable editable = name.getText();
                                    Selection.setSelection(editable,pos);

                                    if (!profile_pic.isEmpty())
                                    Picasso.with(EditProfileActivity.this).load(profile_pic).fit().centerCrop().into(profileImage);
                                }
                            } catch (JSONException e) {
                                Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                            } catch (IllegalArgumentException e){
                                e.printStackTrace();
                            }

                        }
                        @Override
                        public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                            PlaythoraUtility.hideProgressDialog();

                            try {
                                String s = new String(bytes);
                                Log.e("RESPONSE_FAIL", s);
                                JSONObject object = new JSONObject(s);
                                if(object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400){
                                    mPref.setAccessToken("");
                                    Toast.makeText(getApplicationContext(),object.getString("message"),Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(EditProfileActivity.this,LoginActivity.class);
                                    startActivity(intent);
                                    finish();
                                }else Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                               // Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                            }

                        }

                    });
        }else{

            Toast.makeText(getApplicationContext(), "Not connected to internet", Toast.LENGTH_LONG).show();

        }

    }

    private boolean checkForValidation() {

        if(name.getText().toString().isEmpty()){
           nameInputLayout.setError("Enter your name");
            nameInputLayout.setErrorEnabled(true);
        }else {
            nameInputLayout.setErrorEnabled(false);
        }

        if(nameInputLayout.isErrorEnabled()){
            return false;
        }else {
            return true;
        }
    }

    private void UploadImage() {

        final String[] items = new String[] { "From Camera", "From SD Card" };
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(EditProfileActivity.this, android.R.layout.select_dialog_item, items);
        AlertDialog.Builder builder = new AlertDialog.Builder(EditProfileActivity.this);

        builder.setTitle("Select Image");
        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File dir =
                            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
                    output = new File(dir, "DesignString" + String.valueOf(System.currentTimeMillis()) + ".jpeg");
                    if (output != null) {
                        Uri photoURI=null;
                        if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
                            photoURI = Uri.fromFile(output);
                        }else if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            photoURI = FileProvider.getUriForFile(EditProfileActivity.this,
                                    getPackageName()+".provider",
                                    output);
                        }
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                        try {
                            startActivityForResult(intent, PICK_FROM_CAM);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    dialog.cancel();
                } else {
                    showImagePicker();
                }
            }
        });

        final AlertDialog dialog = builder.create();
        dialog.show();

    }

    private void showImagePicker() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
// Start the Intent
        startActivityForResult(galleryIntent, PICK_IMAGE_REQUEST);
    }

    private boolean shouldAskPermission(){

        return(Build.VERSION.SDK_INT>Build.VERSION_CODES.LOLLIPOP_MR1);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(data==null){
            return;
        }

        if(resultCode == RESULT_OK) {
            if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {


                Uri selectedImageUri = data.getData();
                try{
                    selectedImagePath = getPath(selectedImageUri);
                }
                catch (NullPointerException e){
                    Toast.makeText(getApplicationContext(),"Couldn't fetch the image try again.", Toast.LENGTH_SHORT).show();
                }

                try {
                    ExifInterface exif = new ExifInterface(selectedImagePath);
                    int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
                    int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                    Log.e("ROTATE",orientation+"");
                    Bitmap bm;
                    int rotation=0;
                    if(rotation==0){
                        if(orientation==6){
                            rotation=90;
                        }
                        if(orientation==3){
                            rotation=180;
                        }
                        if(orientation==8){
                            rotation=270;
                        }
                        if(orientation==4){
                            rotation=180;
                        }

                    }
                    Log.e("ooooooo", String.valueOf(currentapiVersion));
                    Matrix matrix = new Matrix();
                    matrix.postRotate(rotation);
                    bm = Bitmap.createScaledBitmap(BitmapFactory.decodeFile(selectedImagePath), 100, 100, true);
                    bm = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), matrix, true);
                    //profileImage.setImageBitmap(bm);
                    PlaythoraUtility.showProgressDialog(EditProfileActivity.this);
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                uploadFile(selectedImagePath);
                            }catch (Exception e){}

                        }
                    }).start();


                } catch (IOException e) {
                    e.printStackTrace();
                }catch (OutOfMemoryError e){
                    e.printStackTrace();
                } catch (NullPointerException e){
                    Log.e("MMMMMMMMMMMMM", "Null");
                    Toast.makeText(getApplicationContext(),"Couldnt fetch the image try again.", Toast.LENGTH_SHORT).show();
                    return;
                }catch (IllegalArgumentException e){
                    Toast.makeText(getApplicationContext(),"Couldnt fetch the image try again.", Toast.LENGTH_SHORT).show();
                    return;
                }

            } else {
                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
                File destination = new File(Environment.getExternalStorageDirectory(),
                        "Daily" + String.valueOf(System.currentTimeMillis()) + ".jpg");
                final Uri mImageCaptureUri = Uri.fromFile(destination);

                System.out.println("Image Path : " + mImageCaptureUri);
                FileOutputStream fo;
                try {
                    destination.createNewFile();
                    fo = new FileOutputStream(destination);
                    fo.write(bytes.toByteArray());
                    fo.close();

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                selectedImagePath = getRealPathFromURI(mImageCaptureUri.toString(), EditProfileActivity.this);
                try {
                    ExifInterface exif = new ExifInterface(selectedImagePath);
                    int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
                    int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                    Log.e("ROTATE",orientation+"");
                    Bitmap bm;
                    int rotation=0;
                    if(rotation==0){
                        if(orientation==6){
                            rotation=90;
                        }
                        if(orientation==3){
                            rotation=180;
                        }
                        if(orientation==8){
                            rotation=270;
                        }
                        if(orientation==4){
                            rotation=180;
                        }

                    }
                    Log.e("ooooooo", String.valueOf(currentapiVersion));
                    Matrix matrix = new Matrix();
                    matrix.postRotate(rotation);
                    bm = Bitmap.createScaledBitmap(BitmapFactory.decodeFile(selectedImagePath), 100, 100, true);
                    bm = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), matrix, true);
                    //profileImage.setImageBitmap(bm);
                    PlaythoraUtility.showProgressDialog(EditProfileActivity.this);
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                uploadFile(selectedImagePath);
                            }catch (Exception e){}

                        }
                    }).start();


                } catch (IOException e) {
                    e.printStackTrace();
                }catch (OutOfMemoryError e){
                    e.printStackTrace();
                }

            }
        }

    }

    private String getRealPathFromURI(String contentURI,Context context) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor =context.getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }


    public String getPath(Uri uri) {
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor =  getContentResolver().query(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }


    private void initializeAllComponents() {
        profImageLayout = (RelativeLayout) findViewById(com.groundhog.vendor.android.R.id.profile_relative);
        name = (EditText) findViewById(com.groundhog.vendor.android.R.id.profile_name);
        profileImage = (ImageView) findViewById(com.groundhog.vendor.android.R.id.profile_image);
        save = (Button) findViewById(com.groundhog.vendor.android.R.id.save);
        nameInputLayout = (TextInputLayout) findViewById(com.groundhog.vendor.android.R.id.input_layout_name);
        editProfileUI(findViewById(com.groundhog.vendor.android.R.id.editProfileActivity));

        name.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        save.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        nameInputLayout.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));

    }

    private void editProfileUI(View viewById) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(viewById instanceof EditText)) {
            viewById.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    PlaythoraUtility.hideSoftKeyboard(EditProfileActivity.this);
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (viewById instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) viewById).getChildCount(); i++) {
                View innerView = ((ViewGroup) viewById).getChildAt(i);
                editProfileUI(innerView);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                break;
        }

        return false;
    }


    private void uploadFile(final String sourceFileUri) {

        String fileName = sourceFileUri;

        HttpURLConnection conn = null;
        DataOutputStream dos = null;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;
        //File sourceFile = new File(sourceFileUri);
        File  sourceFile = new File(PlaythoraUtility.compressImage(sourceFileUri,EditProfileActivity.this));
        if (!sourceFile.isFile()) {
            Log.e("uploadFile", "Source File not exist :"+sourceFileUri);

            runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(getApplicationContext(),"Source File not exist :"+ sourceFileUri,Toast.LENGTH_SHORT).show();
                }
            });

            return ;

        }else {

            try {
                //Toast.makeText(EditProfileActivity.this, "FileInputStream", Toast.LENGTH_SHORT).show();
                FileInputStream fileInputStream = new FileInputStream(sourceFile);
                URL url = new URL(Config.EDIT_USER);
                trustAllHosts();
                // Open a HTTP  connection to  the URL
                conn = (HttpURLConnection) url.openConnection();
                conn.setDoInput(true); // Allow Inputs
                conn.setDoOutput(true); // Allow Outputs
                conn.setUseCaches(false); // Don't use a Cached Copy
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                conn.setRequestProperty("accessToken", mPref.getAccessToken());

                dos = new DataOutputStream(conn.getOutputStream());

                dos.writeBytes(twoHyphens + boundary + lineEnd);
                dos.writeBytes("Content-Disposition: form-data; name=\"profile_pic\";filename=\""
                        + fileName + "\"" + lineEnd); //profile_pic is a parameter

                dos.writeBytes(lineEnd);

                // create a buffer of  maximum size
                bytesAvailable = fileInputStream.available();

                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                buffer = new byte[bufferSize];

                // read file and write it into form...
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                while (bytesRead > 0) {
                    dos.write(buffer, 0, bufferSize);
                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                }

                // send multipart form data necesssary after file data...
                dos.writeBytes(lineEnd);
                dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                // Responses from the server (code and message)
                serverResponseCode = conn.getResponseCode();
                String serverResponseMessage = conn.getResponseMessage();
                Log.e("FILENAMESS","Content-Disposition: form-data; name=\"profile_pic\";filename=\""+ fileName + "\"" + lineEnd);
                Log.e("uploadFile", "HTTP Response is : "
                        + serverResponseMessage + ": " + serverResponseCode);

                if(serverResponseCode == 200){
                    BufferedReader r = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    final StringBuilder total = new StringBuilder();
                    String line;
                    while ((line = r.readLine()) != null) {
                        total.append(line).append('\n');
                    }
                    Log.v("Output", String.valueOf(total));
                    runOnUiThread(new Runnable() {
                        public void run() {
                            PlaythoraUtility.hideProgressDialog();

                            JSONObject response = null;
                            try {
                                response = new JSONObject(total.toString());
                                profile_pic = response.getJSONObject("success").getString("profile_pic");
                                mPref.setProfilePic(profile_pic);
                                Picasso.with(EditProfileActivity.this).load(mPref.getProfilePic()).fit().centerCrop().into(profileImage);
                                Toast.makeText(EditProfileActivity.this, "Upload image Complete.", Toast.LENGTH_SHORT).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }else if(serverResponseCode == 400 ||serverResponseCode == 401){
                    BufferedReader r = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    final StringBuilder total = new StringBuilder();
                    String line;
                    while ((line = r.readLine()) != null) {
                        total.append(line).append('\n');
                    }
                    Log.v("Output", String.valueOf(total));

                    JSONObject object = null;
                    mPref.setAccessToken("");
                    Intent intent = new Intent(EditProfileActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();

                }else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            PlaythoraUtility.hideProgressDialog();
                            Picasso.with(EditProfileActivity.this).load(mPref.getProfilePic()).fit().centerCrop().into(profileImage);
                        }
                    });

                }

                //close the streams //
                fileInputStream.close();
                dos.flush();
                dos.close();

            } catch (FileNotFoundException e) {
                e.printStackTrace();
                PlaythoraUtility.hideProgressDialog();
            } catch (MalformedURLException e) {
                e.printStackTrace();
                PlaythoraUtility.hideProgressDialog();
            } catch (ProtocolException e) {
                e.printStackTrace();
                PlaythoraUtility.hideProgressDialog();
            } catch (IOException e) {
                e.printStackTrace();
                PlaythoraUtility.hideProgressDialog();
            }
        }

    }


    private void trustAllHosts() {

        // Create a trust manager that does not validate certificate chains

        TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {

            public java.security.cert.X509Certificate[] getAcceptedIssuers() {

                return new java.security.cert.X509Certificate[]{};

            }

            public void checkClientTrusted(X509Certificate[] chain,

                                           String authType) throws CertificateException {

            }

            public void checkServerTrusted(X509Certificate[] chain,

                                           String authType) throws CertificateException {

            }

        }};

        // Install the all-trusting trust manager

        try {

            SSLContext sc = SSLContext.getInstance("TLS");

            sc.init(null, trustAllCerts, new java.security.SecureRandom());

            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

        } catch (Exception e) {

            e.printStackTrace();

        }
    }
}
