package com.groundhog.vendor.android.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
import android.text.Selection;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.groundhog.vendor.android.adapter.VenueAdapter;
import com.groundhog.vendor.android.model.VenueModel;
import com.groundhog.vendor.android.utils.PlaythoraUtility;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by Nakul on 9/15/2016.
 */
public class EditUsersActivity extends AppCompatActivity {

    EditText name,emailAddress,mobileNumber;
    TextInputLayout nameInputLayout,emailInputLayout,mobileNumberInputLayout;
    TextView role,venue;
    Button save,popUpSave;
    String nameValue,emailValue,mobileNumberValue,roleValue,venueValue;
    TextView popUpAdmin,popUpManager;
    ImageView imgAdmin,imgManager;
    int position;
    ListView venueListView;
    public  static ArrayList<VenueModel> venueModelsArrayList;
    Gson gson ;
    public  static VenueAdapter venueAdapter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.groundhog.vendor.android.R.layout.edit_users_layout);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(Html.fromHtml("<font color=\"white\"><small>" + "Edit users details" + "</small></font>"));

        initializeAllComponents();
        Intent i = getIntent();
        nameValue = i.getStringExtra("UserName");
        emailValue = i.getStringExtra("UserEmail");
        roleValue =  i.getStringExtra("UserRole");
        mobileNumberValue = i.getStringExtra("UserMobileNumber");
        venueValue = i.getStringExtra("UserVenue");

        name.setText(nameValue);
        emailAddress.setText(emailValue);
        role.setText(roleValue);
        mobileNumber.setText(mobileNumberValue);
        venue.setText(venueValue);

        position = name.length();
        Editable editable = name.getText();
        Selection.setSelection(editable, position);

        position = emailAddress.length();
        Editable editable1 = emailAddress.getText();
        Selection.setSelection(editable1, position);

        position = mobileNumber.length();
        Editable editable2 = mobileNumber.getText();
        Selection.setSelection(editable2,position);

        role.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                roleValue = role.getText().toString();
                showDialog(roleValue);

            }
        });
        
        
        venue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                venueValue = venue.getText().toString();
                showVenueList(venueValue);
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(checkForAllValidation()){
                    Intent save = new Intent(EditUsersActivity.this,ManageUsersActivity.class);
                    startActivity(save);

                }

            }
        });

        name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                nameInputLayout.setError(null);
            }
        });

        emailAddress.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                emailInputLayout.setError(null);
            }
        });

        mobileNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
              mobileNumberInputLayout.setError(null);
            }
        });


    }

    private void showVenueList(String venueValue) {

        final Dialog dialog1 = new Dialog(EditUsersActivity.this);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(com.groundhog.vendor.android.R.layout.user_venue_dialog_layout);
        venueListView = (ListView) dialog1.findViewById(com.groundhog.vendor.android.R.id.venue_list);

        String response = "[{\n" +
                "    \"id\": \"1\",\n" +
                "    \"groundName\": \"nakul hockey\"\n" +
                "}, {\n" +
                "    \"id\": \"2\",\n" +
                "    \"groundName\": \"hemanth kho kho\"\n" +
                "}, {\n" +
                "    \"id\": \"3\",\n" +
                "    \"groundName\": \"pammi race\"\n" +
                "}]";

        Log.v("response",response);

        try {
            JSONArray jsonArray = new JSONArray(response);
            venueModelsArrayList = new ArrayList<VenueModel>();
            gson = new Gson();
            Log.v("SizeActivity", String.valueOf(jsonArray.length()));
            for (int i=0;i<jsonArray.length();i++){

                venueModelsArrayList.add(gson.fromJson(jsonArray.get(i).toString(),VenueModel.class));
            }
            Log.v("Venue", String.valueOf(venueModelsArrayList));

            venueAdapter = new VenueAdapter(EditUsersActivity.this,venueModelsArrayList,venueValue);
            venueListView.setAdapter(venueAdapter);

        } catch (JSONException e) {
            e.printStackTrace();
        }




        venueListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Log.v("GroundName",venueModelsArrayList.get(i).getGroundName());

                venue.setText(venueModelsArrayList.get(i).getGroundName());

                dialog1.dismiss();

            }
        });

        dialog1.show();



    }

    private void showDialog(String roleValue) {

        final Dialog dialog = new Dialog(EditUsersActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(com.groundhog.vendor.android.R.layout.user_role_dialog_layout);

        popUpAdmin = (TextView) dialog.findViewById(com.groundhog.vendor.android.R.id.admin);
        popUpManager = (TextView) dialog.findViewById(com.groundhog.vendor.android.R.id.manger);
        popUpSave = (Button) dialog.findViewById(com.groundhog.vendor.android.R.id.saveRole);
        imgAdmin = (ImageView) dialog.findViewById(com.groundhog.vendor.android.R.id.adminImage);
        imgManager = (ImageView) dialog.findViewById(com.groundhog.vendor.android.R.id.managerImage);

        if(popUpAdmin.getText().toString().equals(roleValue)){
            imgAdmin.setVisibility(View.VISIBLE);
            imgManager.setVisibility(View.GONE);
        }

        if(popUpManager.getText().toString().equals(roleValue)){
            imgManager.setVisibility(View.VISIBLE);
            imgAdmin.setVisibility(View.GONE);
        }

        popUpAdmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imgAdmin.setVisibility(View.VISIBLE);
                imgManager.setVisibility(View.GONE);
                role.setText("Admin");

            }
        });

        popUpManager.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imgManager.setVisibility(View.VISIBLE);
                imgAdmin.setVisibility(View.GONE);
                role.setText("Manager");

            }
        });

        popUpSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

            }
        });

        dialog.show();

    }

    private boolean checkForAllValidation() {

        if(name.getText().toString().isEmpty()){
            nameInputLayout.setError("User name is required");
            nameInputLayout.setErrorEnabled(true);
        }else {
            nameInputLayout.setErrorEnabled(false);
        }

        if(emailAddress.getText().toString().isEmpty()){
            emailInputLayout.setError("User email address is required");
            emailInputLayout.setErrorEnabled(true);
        }else if(!android.util.Patterns.EMAIL_ADDRESS.matcher(emailAddress.getText().toString()).matches()){
            emailInputLayout.setError("Please provide valid email address");
            emailInputLayout.setErrorEnabled(true);
        }else {
            emailInputLayout.setErrorEnabled(false);
        }


        if(mobileNumber.getText().toString().isEmpty()){
            mobileNumberInputLayout.setError("User mobile number is required");
            mobileNumberInputLayout.setErrorEnabled(true);
        }else if(mobileNumber.getText().toString().length()<10){
            mobileNumberInputLayout.setError("Enter 10 digit mobile number");
            mobileNumberInputLayout.setErrorEnabled(true);
        }else {
            mobileNumberInputLayout.setErrorEnabled(false);
        }

        if(role.getText().toString().isEmpty()){
            Toast.makeText(EditUsersActivity.this,"Select user role",Toast.LENGTH_SHORT).show();
            return false;
        }

        if(venue.getText().toString().isEmpty()){
            Toast.makeText(EditUsersActivity.this,"Select user venue",Toast.LENGTH_SHORT).show();
            return false;
        }

        if(nameInputLayout.isErrorEnabled() || emailInputLayout.isErrorEnabled() || mobileNumberInputLayout.isErrorEnabled()){
            return false;
        }else {
            return  true;
        }

    }

    private void initializeAllComponents() {

        name = (EditText) findViewById(com.groundhog.vendor.android.R.id.userName);
        emailAddress = (EditText) findViewById(com.groundhog.vendor.android.R.id.userEmailId);
        mobileNumber = (EditText) findViewById(com.groundhog.vendor.android.R.id.userMobileNumber);
        role = (TextView) findViewById(com.groundhog.vendor.android.R.id.userRole);
        venue = (TextView) findViewById(com.groundhog.vendor.android.R.id.venue);
        save = (Button) findViewById(com.groundhog.vendor.android.R.id.editUsersSaveBtn);
        nameInputLayout = (TextInputLayout) findViewById(com.groundhog.vendor.android.R.id.input_layout_user_name);
        emailInputLayout = (TextInputLayout) findViewById(com.groundhog.vendor.android.R.id.input_layout_user_email_address);
        mobileNumberInputLayout = (TextInputLayout) findViewById(com.groundhog.vendor.android.R.id.input_layout_user_mobile_number);
        editUserUI(findViewById(com.groundhog.vendor.android.R.id.editUsersActivity));
    }

    private void editUserUI(View viewById) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(viewById instanceof EditText)) {
            viewById.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    PlaythoraUtility.hideSoftKeyboard(EditUsersActivity.this);
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (viewById instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) viewById).getChildCount(); i++) {
                View innerView = ((ViewGroup) viewById).getChildAt(i);
                editUserUI(innerView);
            }
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                break;
        }

        return false;
    }
}


