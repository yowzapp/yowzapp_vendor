package com.groundhog.vendor.android.model;

import java.util.List;

/**
 * Created by vaishakha on 12/9/16.
 */
public class SportsListModel {
    private String status;
    private int statusCode;
    private String message;
    private List<SportsSuccessList> success;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<SportsSuccessList> getSuccess() {
        return success;
    }

    public void setSuccess(List<SportsSuccessList> success) {
        this.success = success;
    }
}
