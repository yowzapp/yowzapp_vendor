package com.groundhog.vendor.android.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.groundhog.vendor.android.BuildConfig;
import com.groundhog.vendor.android.model.ImageModel;
import com.groundhog.vendor.android.utils.Config;
import com.groundhog.vendor.android.utils.ExpandableHeightGridView;
import com.groundhog.vendor.android.utils.PlaythoraUtility;
import com.groundhog.vendor.android.utils.PreferenceManager;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import cz.msebera.android.httpclient.Header;

import static com.groundhog.vendor.android.utils.Config.RALEWAY_REGULAR;

/**
 * Created by vaishakha on 14/9/16.
 */
public class EditVenuePictures extends AppCompatActivity {
    ExpandableHeightGridView gridView;
    String image,groundName,verifiedText,VenueID;
    GridAdapter gridAdapter;
    public static ArrayList<ImageModel> imagesArray,imagess;
    TextView pictureGroundName,verified;
    private int PERMISSION_REQUEST_CODE = 200;
    private static final int PICK_FROM_CAM = 1;
    private int PICK_IMAGE_REQUEST = 2;
    File output=null;
    String selectedImagePath;
    Button save;
    String[] permissions= new String[]{
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA,
            Manifest.permission.READ_EXTERNAL_STORAGE,
    };
    Gson gson;
    int serverResponseCode = 0;
    PreferenceManager mPref;
    public static boolean value;
    Toolbar toolbar;
    TextView title;
    private Uri photoURI;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.groundhog.vendor.android.R.layout.edit_venue_picture_layout);

        toolbar = (Toolbar) findViewById(com.groundhog.vendor.android.R.id.toolbar);
        title = (TextView) findViewById(com.groundhog.vendor.android.R.id.toolbar_title);
        toolbar.setNavigationIcon(com.groundhog.vendor.android.R.drawable.ic_back_arrow);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        title.setText("Edit venue pictures");
        title.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));


        //getSupportActionBar().setTitle(Html.fromHtml("<font color=\"white\"><small>" + "Edit venue pictures" + "</small></font>"));

        initializeAllComponents();
        mPref = PreferenceManager.instance(this);
        value=false;

        final Intent intent = getIntent();
        image = intent.getStringExtra("Images");
        groundName = intent.getStringExtra("GroundName");
        verifiedText = intent.getStringExtra("VerifiedText");
        VenueID = intent.getStringExtra("VENUEID");

        pictureGroundName.setText(groundName);
        if(verifiedText.equalsIgnoreCase("true")){

        }else {

            verified.setVisibility(View.GONE);
        }

        Log.e("XXXXXXXXXXX",image);
        imagesArray = new ArrayList<ImageModel>();

        if(image!=null){
            gson = new Gson();
            JSONArray jsonArray = null;
            try {
                jsonArray = new JSONArray(image);
                for(int i=0;i<jsonArray.length();i++){
                    imagesArray.add(gson.fromJson(jsonArray.get(i).toString(),ImageModel.class));
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        gridAdapter = new GridAdapter(EditVenuePictures.this,imagesArray);
        gridView.setAdapter(gridAdapter);
        gridView.setExpanded(true);


        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Log.e("YYYYYYYYYY", String.valueOf(imagess.size()));
                if(imagess.size()-1==i){
                    if(!checkPermission()){

                        String[] perms = {"android.permission.WRITE_EXTERNAL_STORAGE", "android.permission.READ_EXTERNAL_STORAGE", "android.permission.CAMERA"};
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(perms, PERMISSION_REQUEST_CODE);
                        }
                    }else {
                        UploadImage();
                    }

                }else{
                    Intent intent1 = new Intent(EditVenuePictures.this,GalleryViewActivity.class);
                    intent1.putExtra("POSITION",i);
                    //intent1.putExtra("ImageCode",imagess.get(i).getCode());
                    startActivity(intent1);
                }


            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    private void initializeAllComponents() {
        gridView = (ExpandableHeightGridView) findViewById(com.groundhog.vendor.android.R.id.grid_venue_pictures);
        pictureGroundName = (TextView) findViewById(com.groundhog.vendor.android.R.id.picture_ground_name);
        verified = (TextView) findViewById(com.groundhog.vendor.android.R.id.edit_verified);
        save = (Button) findViewById(com.groundhog.vendor.android.R.id.relative_save);

        pictureGroundName.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        verified.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        save.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
    }

    private void UploadImage() {
        final String[] items = new String[] { "From Camera", "From Gallery" };
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(EditVenuePictures.this, android.R.layout.select_dialog_item, items);
        AlertDialog.Builder builder = new AlertDialog.Builder(EditVenuePictures.this);

        builder.setTitle("Select Image");
        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File dir =
                            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
                    output = new File(dir, "DesignString" + String.valueOf(System.currentTimeMillis()) + ".jpeg");
                    if (output != null) {
                        Uri photoURI=null;
                        if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
                            photoURI = Uri.fromFile(output);
                        }else if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            photoURI = FileProvider.getUriForFile(EditVenuePictures.this,
                                    getPackageName()+".provider",
                                    output);
                        }
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                        try {
                            startActivityForResult(intent, PICK_FROM_CAM);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    dialog.cancel();
                } else {
                    showImagePicker();
                }
            }
        });

        final AlertDialog dialog = builder.create();
        dialog.show();
    }


    private void showImagePicker() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, PICK_IMAGE_REQUEST);
    }


    private  boolean checkPermission() {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p:permissions) {
            result = ContextCompat.checkSelfPermission(EditVenuePictures.this,p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(EditVenuePictures.this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),PERMISSION_REQUEST_CODE);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions, int[] grantResults) {


        if(permsRequestCode==200){
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.e("startperm","request");
                UploadImage();
            } else {
                Toast.makeText(EditVenuePictures.this,  "Permission denied, You need to give permission to use this feature", Toast.LENGTH_SHORT).show();

            }

        }

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                //Toast.makeText(getApplicationContext(),"Back button clicked", Toast.LENGTH_SHORT).show();
                onBackPressed();
                return true;
            default:
                break;

        }
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK) {
            if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {


                Uri selectedImageUri = data.getData();
                Log.e("selectedImageUri",selectedImageUri+"");
                try{
                    selectedImagePath = getPath(selectedImageUri);
                    Log.e("selectedImagePath",selectedImagePath+"");
                    //imagesArray.add(new ImageModel(selectedImagePath,"2"));
                    PlaythoraUtility.showProgressDialog(EditVenuePictures.this);
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                uploadFile(selectedImagePath);
                                Log.e("RUNNING","RUNNING");
                            }catch (Exception e){
                                e.printStackTrace();
                            }

                        }
                    }).start();


                    //gridAdapter.mRefresh(imagesArray);
                    Log.e("imagesArraysize", String.valueOf(imagesArray.size()));
                   /* gridAdapter = new GridAdapter(EditVenuePictures.this,imagesArray);
                    gridView.setAdapter(gridAdapter);
*/
                }
                catch (NullPointerException e){
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),"Couldn't fetch the image try again.", Toast.LENGTH_SHORT).show();
                }

            } else {
                try {
                    if ((getRealPathFromURI(Uri.fromFile(output).toString(), EditVenuePictures.this) == null)) {
                        return;
                    } else {
                        selectedImagePath = getRealPathFromURI(Uri.fromFile(output).toString(), EditVenuePictures.this);
                        //imagesArray.add(new ImageModel(selectedImagePath, "2"));
                        PlaythoraUtility.showProgressDialog(EditVenuePictures.this);
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    uploadFile(selectedImagePath);
                                }catch (Exception e){}

                            }
                        }).start();

                        //gridAdapter.mRefresh(imagesArray);
                        Log.e("imagesArraysize", String.valueOf(imagesArray.size()));
                       /* gridAdapter = new GridAdapter(EditVenuePictures.this, imagesArray);
                        gridView.setAdapter(gridAdapter);*/
                        //gridAdapter.notifyDataSetChanged();
                    }
                }catch (Exception e){}
            }
        }

    }

    public String getPath(Uri uri) {
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor =  getContentResolver().query(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private String getRealPathFromURI(String contentURI,Context context) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor =context.getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }

    private void uploadFile(final String sourceFileUri) {
        Log.e("VenueID",VenueID);
        String fileName = sourceFileUri;

        HttpURLConnection conn = null;
        DataOutputStream dos = null;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;
        //File sourceFile = new File(sourceFileUri);
        File  sourceFile = new File(PlaythoraUtility.compressImage(sourceFileUri,EditVenuePictures.this));
        if (!sourceFile.isFile()) {
            Log.e("uploadFile", "Source File not exist :"+sourceFileUri);

            runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(getApplicationContext(),"Source File not exist :"+ sourceFileUri,Toast.LENGTH_SHORT).show();
                }
            });

            return ;

        }else {
            Log.e("RUNNING","RUNNING");
            try {

                FileInputStream fileInputStream = new FileInputStream(sourceFile);
                URL url = new URL(Config.ADD_VENUE_IMAGE);
                trustAllHosts();
                // Open a HTTP  connection to  the URL
                conn = (HttpURLConnection) url.openConnection();
                conn.setDoInput(true); // Allow Inputs
                conn.setDoOutput(true); // Allow Outputs
                conn.setUseCaches(false); // Don't use a Cached Copy
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                conn.setRequestProperty("accessToken",mPref.getAccessToken() );
                conn.setRequestProperty("venueId", VenueID);

                dos = new DataOutputStream(conn.getOutputStream());

                dos.writeBytes(twoHyphens + boundary + lineEnd);
                dos.writeBytes("Content-Disposition: form-data; name=\"image\";filename=\""
                        + fileName + "\"" + lineEnd);

                dos.writeBytes(lineEnd);

               /* dos.writeBytes("Content-Disposition: form-data; name=\"venue_id\"" + lineEnd);
                dos.writeBytes(lineEnd);
                dos.writeBytes(VenueID + lineEnd);
                dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);*/

                // create a buffer of  maximum size
                bytesAvailable = fileInputStream.available();

                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                buffer = new byte[bufferSize];

                // read file and write it into form...
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                while (bytesRead > 0) {
                    dos.write(buffer, 0, bufferSize);
                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                }

                // send multipart form data necesssary after file data...
                dos.writeBytes(lineEnd);
                dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                // Responses from the server (code and message)
                serverResponseCode = conn.getResponseCode();
                String serverResponseMessage = conn.getResponseMessage();
                Log.e("FILENAMESS","Content-Disposition: form-data; name=\"image\";filename=\""+ fileName + "\"" + lineEnd);
                Log.e("uploadFile", "HTTP Response is : "
                        + serverResponseMessage + ": " + serverResponseCode);

                if(serverResponseCode == 200){
                    BufferedReader r = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    final StringBuilder total = new StringBuilder();
                    String line;
                    while ((line = r.readLine()) != null) {
                        total.append(line).append('\n');
                    }
                    Log.v("Output", String.valueOf(total));
                    runOnUiThread(new Runnable() {
                        public void run() {
                            PlaythoraUtility.hideProgressDialog();

                            JSONObject response = null;
                            try {
                                response = new JSONObject(total.toString());
                                JSONObject success = response.getJSONObject("success");
                                imagesArray.add(new ImageModel(success.getString("image"),success.getString("updated_at"),success.getString("created_at"),success.getInt("id")));
                                gridAdapter.mRefresh(imagesArray);
                                gridAdapter.notifyDataSetChanged();
                                value = true;
                                Toast.makeText(EditVenuePictures.this, "Upload Complete.", Toast.LENGTH_SHORT).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }else if(serverResponseCode == 400 ||serverResponseCode == 401){
                    BufferedReader r = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    final StringBuilder total = new StringBuilder();
                    String line;
                    while ((line = r.readLine()) != null) {
                        total.append(line).append('\n');
                    }
                    Log.v("Output", String.valueOf(total));

                    JSONObject object = null;
                    mPref.setAccessToken("");
                    Intent intent = new Intent(EditVenuePictures.this, LoginActivity.class);
                    startActivity(intent);
                    finish();

                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            PlaythoraUtility.hideProgressDialog();
                            //Picasso.with(EditVenuePictures.this).load(mPref.getProfilePic()).fit().centerCrop().into(profileImage);
                        }
                    });

                }

                //close the streams //
                fileInputStream.close();
                dos.flush();
                dos.close();

            } catch (FileNotFoundException e) {
                PlaythoraUtility.hideProgressDialog();
            } catch (MalformedURLException e) {
                PlaythoraUtility.hideProgressDialog();
            } catch (ProtocolException e) {
                PlaythoraUtility.hideProgressDialog();
            } catch (IOException e) {
                PlaythoraUtility.hideProgressDialog();
            }
        }

    }


    private void trustAllHosts() {

        // Create a trust manager that does not validate certificate chains

        TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {

            public java.security.cert.X509Certificate[] getAcceptedIssuers() {

                return new java.security.cert.X509Certificate[]{};

            }

            public void checkClientTrusted(X509Certificate[] chain,

                                           String authType) throws CertificateException {

            }

            public void checkServerTrusted(X509Certificate[] chain,

                                           String authType) throws CertificateException {

            }

        }};

        // Install the all-trusting trust manager

        try {

            SSLContext sc = SSLContext.getInstance("TLS");

            sc.init(null, trustAllCerts, new java.security.SecureRandom());

            HttpsURLConnection

                    .setDefaultSSLSocketFactory(sc.getSocketFactory());

        } catch (Exception e) {

            e.printStackTrace();

        }
    }

    class GridAdapter extends BaseAdapter{
        private LayoutInflater inflater=null;
        private Context context;

        public GridAdapter(Context context,ArrayList<ImageModel> images) {
            this.context = context;
            imagess=new ArrayList<>();
            for(int i =0 ;i<images.size();i++){
                imagess.add(images.get(i));
                Log.e("imagesArray",images.get(i).getImage());

            }
            imagess.add(new ImageModel(""));
            Log.e("imagesArraysize", String.valueOf(imagess.size()));
        }

        @Override
        public int getCount() {
            return imagess.size();
        }

        @Override
        public Object getItem(int i) {
            return i;
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        public class Holder
        {
            RelativeLayout pictureLayout,clearRelative;
            ImageView imagePicture,imageClear;
        }

        public void mRefresh(ArrayList<ImageModel> images){
            imagess=new ArrayList<>();
            for(int i =0 ;i<images.size();i++){
                    imagess.add(images.get(i));
                    Log.e("imagesArray",images.get(i).getImage());

            }
            imagess.add(new ImageModel(""));
            notifyDataSetChanged();
        }

        @Override
        public View getView(final int i, View view, ViewGroup viewGroup) {
            View rowView;

            final Holder holder=new Holder();
            inflater = ( LayoutInflater )context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(com.groundhog.vendor.android.R.layout.picture_item_layout, null);
            holder.pictureLayout = (RelativeLayout) rowView.findViewById(com.groundhog.vendor.android.R.id.picture_layout);
            holder.imagePicture = (ImageView) rowView.findViewById(com.groundhog.vendor.android.R.id.image_picture);
            holder.imageClear = (ImageView) rowView.findViewById(com.groundhog.vendor.android.R.id.clear_image);
            holder.clearRelative = (RelativeLayout) rowView.findViewById(com.groundhog.vendor.android.R.id.rel_clear_image);

            if(imagess.get(i).getImage().isEmpty()){
                holder.imagePicture.setVisibility(View.GONE);
                holder.imageClear.setVisibility(View.GONE);
                holder.clearRelative.setVisibility(View.GONE);
                holder.pictureLayout.setVisibility(View.VISIBLE);
            }else{
                holder.imagePicture.setVisibility(View.VISIBLE);
                holder.imageClear.setVisibility(View.VISIBLE);
                holder.clearRelative.setVisibility(View.VISIBLE);
                    Picasso.with(getApplicationContext()).load(imagess.get(i).getImage()).fit().centerCrop().config(Bitmap.Config.RGB_565).into(holder.imagePicture);
                /*else if(imagess.get(i).getCode()=="2") {
                    try {
                        ExifInterface exif = new ExifInterface(imagess.get(i).getPath());
                        int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
                        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                        Log.e("ROTATE",orientation+"");
                        Bitmap bm;
                        int rotation=0;
                        if(rotation==0){
                            if(orientation==6){
                                rotation=90;
                            }
                            if(orientation==3){
                                rotation=180;
                            }
                            if(orientation==8){
                                rotation=270;
                            }
                            if(orientation==4){
                                rotation=180;
                            }

                        }
                        Log.e("ooooooo", String.valueOf(currentapiVersion));
                        Matrix matrix = new Matrix();
                        matrix.postRotate(rotation);
                        bm = Bitmap.createScaledBitmap(BitmapFactory.decodeFile(imagess.get(i).getPath()), 100, 100, true);
                        bm = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), matrix, true);
                        holder.imagePicture.setImageBitmap(bm);


                    } catch (IOException e) {
                        e.printStackTrace();
                    }catch (OutOfMemoryError e){
                        e.printStackTrace();
                    } catch (NullPointerException e){
                        Log.e("MMMMMMMMMMMMM", "Null");
                        Toast.makeText(getApplicationContext(),"Couldnt fetch the image try again.", Toast.LENGTH_SHORT).show();

                    }catch (IllegalArgumentException e){
                        Toast.makeText(getApplicationContext(),"Couldnt fetch the image try again.", Toast.LENGTH_SHORT).show();

                    }
                }*/

                holder.pictureLayout.setVisibility(View.GONE);
            }

            holder.imageClear.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    /*imagess.remove(i);
                    notifyDataSetChanged();
                    imagesArray.remove(i);
                    notifyDataSetChanged();*/
                    removeImage(imagess.get(i).getId(),i);

                }
            });
            return rowView;
        }

    }

    private void removeImage(int imgId, final int i) {


        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            try {
                PlaythoraUtility.showProgressDialog(EditVenuePictures.this);
                AsyncHttpClient mHttpClient = new AsyncHttpClient();
                mHttpClient.addHeader("accessToken", mPref.getAccessToken());
                RequestParams params = new RequestParams();
                params.add("venue_id", VenueID);
                params.add("gallery_id", String.valueOf(imgId));
                mHttpClient.post(Config.DELETE_VENUE_IMAGE, params,
                        new AsyncHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                PlaythoraUtility.hideProgressDialog();
                                try {
                                    String s = new String(responseBody);
                                    Log.e("RESPONSE", s);
                                    value = true;
                                    JSONObject object = new JSONObject(s);
                                    imagess.remove(i);
                                    imagesArray.remove(i);
                                    gridAdapter.notifyDataSetChanged();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                }

                            }

                            @Override
                            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                                PlaythoraUtility.hideProgressDialog();
                                try {
                                    String s = new String(bytes);
                                    Log.d("RESPONSE_FAIL", s);
                                    JSONObject object = new JSONObject(s);
                                    if(object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400){
                                        mPref.setAccessToken("");
                                        Toast.makeText(getApplicationContext(),object.getString("message"),Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(EditVenuePictures.this,LoginActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }else Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    //Toast.makeText(getApplicationContext(), "Some error occurred please try again later.", Toast.LENGTH_SHORT).show();
                                }

                            }

                        });
            }catch (Exception e){}
        }else{

            Toast.makeText(getApplicationContext(), "Not connected to internet", Toast.LENGTH_LONG).show();

        }




    }
}
