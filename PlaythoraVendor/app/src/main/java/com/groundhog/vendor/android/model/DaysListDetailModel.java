package com.groundhog.vendor.android.model;

/**
 * Created by hemanth on 24/3/18.
 */

public class DaysListDetailModel {
    private String status;
    private int statusCode;
    private String message;
    private DaysDetailSuccess success;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DaysDetailSuccess getSuccess() {
        return success;
    }

    public void setSuccess(DaysDetailSuccess success) {
        this.success = success;
    }
}
