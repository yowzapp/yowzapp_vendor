package com.groundhog.vendor.android.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.groundhog.vendor.android.R;
import com.groundhog.vendor.android.utils.Config;
import com.groundhog.vendor.android.utils.PlaythoraUtility;
import com.groundhog.vendor.android.utils.PreferenceManager;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

import static com.groundhog.vendor.android.utils.Config.RALEWAY_REGULAR;

/**
 * Created by Nakul on 9/8/2016.
 */
public class LoginActivity extends Activity {

    EditText emailBox,passwordBox;
    TextView forgotPassword,businessText,notYetRegister;
    Button login,signUp;
    TextInputLayout emailBoxInputLayout,passwordBoxInputLayout;
    ProgressDialog dialog;
    SweetAlertDialog sweetAlertDialog;

    String accessToken;
    int id;
    String android_id;
    String name;
    String profile_pic;

    public  static PreferenceManager mPref;
    String mobile_verified;
    String token,msg;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_layout);

        initializeAllComponents();


     /*   if(!mPref.getAccessToken().isEmpty() && mPref.getMobilVerified().equalsIgnoreCase("true")){
            Intent toMain = new Intent(LoginActivity.this,HomeActivity.class);
            startActivity(toMain);
            finish();
        }else if(mPref.getAccessToken().isEmpty() && mPref.getMobilVerified().equalsIgnoreCase("false")){
            Intent toMain = new Intent(LoginActivity.this,VerificationActivity.class);
            startActivity(toMain);
            finish();
        }*/


        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(checkForAllValidation()){
                    login();
                }
            }
        });

        token = FirebaseInstanceId.getInstance().getToken();

        msg = getString(R.string.msg_token_fmt, token);
       // Toast.makeText(LoginActivity.this, msg, Toast.LENGTH_SHORT).show();

        android_id = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);
        Log.e("ANDROID_ID",android_id);
        mPref.setAndroid_id(android_id);
        mPref.setGcmToken(token);

        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent signUp = new Intent(LoginActivity.this,SignUpActivity.class);
                startActivity(signUp);
            }
        });

        forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent signUp = new Intent(LoginActivity.this,ForgotPasswordActivity.class);
                startActivity(signUp);

            }
        });

        passwordBox.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId == EditorInfo.IME_ACTION_DONE){
                    if(checkForAllValidation()){
                        login();
                    }
                }
                return false;
            }
        });

        emailBox.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                emailBoxInputLayout.setError(null);
            }
        });

        passwordBox.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                passwordBoxInputLayout.setError(null);

            }
        });


    }

    private void login() {

        if(mPref.getGcmToken()==null){
            token = FirebaseInstanceId.getInstance().getToken();
            mPref.setGcmToken(token);
        }

        if(mPref.getAndroid_id()==null){
            android_id = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                    Settings.Secure.ANDROID_ID);
            Log.e("ANDROID_ID",android_id);
            mPref.setAndroid_id(android_id);
        }

        if(PlaythoraUtility.checkInternetConnection(getApplicationContext())){
            PlaythoraUtility.showProgressDialog(LoginActivity.this);
            AsyncHttpClient mHttpClient = new AsyncHttpClient();
            Log.v("GCmToke",mPref.getGcmToken());
            RequestParams params= new RequestParams();
            params.add("email",emailBox.getText().toString());
            params.add("password",passwordBox.getText().toString());
            params.add("device_type","android");
            params.add("gcm_token",mPref.getGcmToken());
            params.add("device_id",mPref.getAndroid_id());

            mHttpClient.post(Config.LOGIN, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                   /* if(dialog.isShowing()){
                        dialog.dismiss();
                    }*/
                   PlaythoraUtility.hideProgressDialog();
                    String response = new String(responseBody);
                    Log.e("response",response);
                    try {
                        JSONObject object = new JSONObject(response);
                        accessToken = object.getString("accessToken");
                        id = object.getJSONObject("success").getInt("id");
                        mobile_verified = String.valueOf(object.getJSONObject("success").getBoolean("mobile_verified"));

                        name = object.getJSONObject("success").getString("name");
                        profile_pic = object.getJSONObject("success").getString("profile_pic");
                        Log.e("ID", String.valueOf(id));

                        Log.e("access_token",accessToken);
                        Log.e("mobile_verified",mobile_verified);
                        Log.e("name",name);
                        Log.e("profile_pic",profile_pic);

                        mPref.setAccessToken(accessToken);
                        mPref.setId(String.valueOf(id));
                        mPref.setMobilVerified(mobile_verified);
                        mPref.setName(name);
                        mPref.setProfilePic(profile_pic);
                        Log.e("mobile_verified", String.valueOf(mPref.getMobilVerified()));


                        if(mPref.getMobilVerified().equalsIgnoreCase("true")){
                            Toast.makeText(LoginActivity.this, "User login successfully", Toast.LENGTH_SHORT).show();
                            Intent i = new Intent(LoginActivity.this, HomeActivity.class);
                            startActivity(i);
                            finish();

                        }else {
                            //Toast.makeText(getApplicationContext(), "Successfully logged in", Toast.LENGTH_SHORT).show();
                            Intent i = new Intent(LoginActivity.this, VerificationActivity.class);
                            i.putExtra("activity","LoginActivity");
                            startActivity(i);
                        }

                    } catch (JSONException e) {
                       PlaythoraUtility.hideProgressDialog();
                        e.printStackTrace();
                    }


                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    PlaythoraUtility.hideProgressDialog();

                    try {
                        String failure = new String(responseBody);
                        Log.e("Failure",failure);
                        JSONObject object = new JSONObject(failure);
                        object.getString("message");
                        Toast.makeText(LoginActivity.this, "Invalid user", Toast.LENGTH_SHORT).show();
                    }catch (Exception e) {
                        e.printStackTrace();

                    }

                }
            });
        }else {
            Toast.makeText(LoginActivity.this, "check your internet connection", Toast.LENGTH_LONG).show();
        }
    }

    private boolean checkForAllValidation() {

        if(emailBox.getText().toString().isEmpty()) {
            emailBoxInputLayout.setError("Please enter email address");
            emailBoxInputLayout.setErrorEnabled(true);
        }else if(!android.util.Patterns.EMAIL_ADDRESS.matcher(emailBox.getText().toString()).matches()){
                emailBoxInputLayout.setError("Please provide valid email address");
                emailBoxInputLayout.setErrorEnabled(true);
            }else {
                emailBoxInputLayout.setErrorEnabled(false);
            }


        if (passwordBox.getText().toString().length() == 0) {
            passwordBoxInputLayout.setError("Please provide password");
            passwordBoxInputLayout.setErrorEnabled(true);
        }else {
            passwordBoxInputLayout.setErrorEnabled(false);
        }

        if(passwordBoxInputLayout.isErrorEnabled() || emailBoxInputLayout.isErrorEnabled()){
            return false;
        }else {
            return true;
        }


    }

    private void initializeAllComponents() {
        mPref = PreferenceManager.instance(getApplicationContext());
        emailBox = (EditText) findViewById(R.id.loginEmailId);
        passwordBox = (EditText) findViewById(R.id.loginPassword);
        login = (Button) findViewById(R.id.login_btn);
        signUp = (Button) findViewById(R.id.btn_signup);
        forgotPassword = (TextView) findViewById(R.id.forgot_password);
        emailBoxInputLayout = (TextInputLayout) findViewById(R.id.input_layout_login_email);
        passwordBoxInputLayout = (TextInputLayout) findViewById(R.id.input_layout_login_password);
        loginUI(findViewById(R.id.login_activity));
        businessText = (TextView) findViewById(R.id.business_text);
        notYetRegister = (TextView) findViewById(R.id.notYetRegister);

        emailBox.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        passwordBox.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        login.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        signUp.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        forgotPassword.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        emailBoxInputLayout.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        passwordBoxInputLayout.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        notYetRegister.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        businessText.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));

    }

    private void loginUI(View viewById) {
        // Set up touch listener for non-text box views to hide keyboard.
        if (!(viewById instanceof EditText)) {
            viewById.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    PlaythoraUtility.hideSoftKeyboard(LoginActivity.this);
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (viewById instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) viewById).getChildCount(); i++) {
                View innerView = ((ViewGroup) viewById).getChildAt(i);
                loginUI(innerView);
            }
        }
    }
}
