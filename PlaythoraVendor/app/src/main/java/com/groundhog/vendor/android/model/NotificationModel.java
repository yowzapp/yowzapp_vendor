package com.groundhog.vendor.android.model;

import java.util.List;

/**
 * Created by Nakul on 9/21/2016.
 */
public class NotificationModel {

    private String status;
    private String statusCode;
    private String message;
    private List<NotificationList> success;
    private int currentPage;
    private boolean hasMorePages;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<NotificationList> getSuccess() {
        return success;
    }

    public void setSuccess(List<NotificationList> success) {
        this.success = success;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public boolean isHasMorePages() {
        return hasMorePages;
    }

    public void setHasMorePages(boolean hasMorePages) {
        this.hasMorePages = hasMorePages;
    }
}
