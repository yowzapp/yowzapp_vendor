package com.groundhog.vendor.android.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.groundhog.vendor.android.activity.AddVenueActivity;
import com.groundhog.vendor.android.activity.LoginActivity;
import com.groundhog.vendor.android.activity.VenueDetailsActivity;
import com.groundhog.vendor.android.model.VenueListItems;
import com.groundhog.vendor.android.model.VenueListingModel;
import com.groundhog.vendor.android.utils.Config;
import com.groundhog.vendor.android.utils.PlaythoraUtility;
import com.groundhog.vendor.android.utils.PreferenceManager;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.List;

import cz.msebera.android.httpclient.Header;
import eu.fiskur.chipcloud.ChipCloud;

import static com.groundhog.vendor.android.utils.Config.RALEWAY_REGULAR;
import static com.groundhog.vendor.android.utils.Config.RALEWAY_SEMIBOLD;

/**
 * Created by vaishakha on 8/9/16.
 */
public class VenueFragment extends Fragment {
    private RecyclerView recyclerView;
    private VenueAdapter venueAdapter;
    private FloatingActionButton floatingActionButton;
    Gson gson;
    PreferenceManager mPref;
    VenueListingModel venueListingModel;
    TextView emptyVenue;
    SwipeRefreshLayout swipeRefreshLayout;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(com.groundhog.vendor.android.R.layout.venue_layout, container, false);
        gson = new Gson();
        mPref = PreferenceManager.instance(getActivity());
        recyclerView = (RecyclerView) view.findViewById(com.groundhog.vendor.android.R.id.venue_recycler);
        floatingActionButton = (FloatingActionButton) view.findViewById(com.groundhog.vendor.android.R.id.fab);
        emptyVenue = (TextView) view.findViewById(com.groundhog.vendor.android.R.id.empty_venue);
        emptyVenue.setTypeface(PlaythoraUtility.getFont(getActivity(),RALEWAY_REGULAR));
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), AddVenueActivity.class);
                startActivity(i);
            }
        });

        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(com.groundhog.vendor.android.R.id.swipeContainer_venues);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                venuesList();
            }
        });


        return view;
    }

    private void venuesList() {
        if (PlaythoraUtility.checkInternetConnection(getActivity())) {
            PlaythoraUtility.showProgressDialog(getActivity());
            AsyncHttpClient mHttpClient = new AsyncHttpClient();
            mHttpClient.addHeader("accessToken",mPref.getAccessToken());
            Log.e("ACCESSTOKEN",mPref.getAccessToken());
            mHttpClient.get(Config.VENUE_LIST,
                    new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                            PlaythoraUtility.hideProgressDialog();
                            swipeRefreshLayout.setRefreshing(false);
                            String s = new String(responseBody);
                            Log.e("RESPONSE", s);

                            venueListingModel = new VenueListingModel();
                            gson = new Gson();
                            venueListingModel = gson.fromJson(s,VenueListingModel.class);
                            if(venueListingModel.getSuccess().size()!=0){
                                emptyVenue.setVisibility(View.GONE);
                                    venueAdapter = new VenueAdapter(getActivity(),venueListingModel.getSuccess());
                                    recyclerView.setAdapter(venueAdapter);
                            }else emptyVenue.setVisibility(View.VISIBLE);

                            /*bankAccountModel = new BankAccountModel();
                            gson = new Gson();
                            bankAccountModel = gson.fromJson(s,BankAccountModel.class);
                            if(bankAccountModel.getSuccess().size()!=0){
                                if(bankAccountsRecyclerView.getAdapter()==null){
                                    bankAccountAdapter = new BankAccountAdapter(getActivity(),bankAccountModel.getSuccess());
                                    bankAccountsRecyclerView.setAdapter(bankAccountAdapter);
                                    bankAccountAdapter.notifyDataSetChanged();

*/

                        }
                        @Override
                        public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                            PlaythoraUtility.hideProgressDialog();
                            swipeRefreshLayout.setRefreshing(false);

                            try {
                                String s = new String(bytes);
                                Log.e("RESPONSE_FAIL", s);
                                JSONObject object = new JSONObject(s);
                                if(object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400){
                                    Toast.makeText(getActivity(),object.getString("message"),Toast.LENGTH_SHORT).show();
                                    mPref.setAccessToken("");
                                    Intent intent = new Intent(getActivity(),LoginActivity.class);
                                    startActivity(intent);
                                    getActivity().finish();
                                }else Toast.makeText(getActivity(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                            } catch (Exception e) {
                                e.printStackTrace();
                               // Toast.makeText(getActivity(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                            }

                        }

                    });
        }else{

            Toast.makeText(getActivity(), "Not connected to internet", Toast.LENGTH_LONG).show();

        }
    }

    private class VenueAdapter extends RecyclerView.Adapter<VenueAdapter.MyViewHolder>{
        private Context mContext;
        List<VenueListItems> venueArrayList;
        RecyclerView recyclerView;

        public VenueAdapter(Context mContext, List<VenueListItems> venueArrayList) {
            this.mContext = mContext;
            this.venueArrayList = venueArrayList;
        }


        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(com.groundhog.vendor.android.R.layout.venue_item_layout,parent,false);


            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, final int position) {
            try {
            holder.groundName.setText(venueArrayList.get(position).getName());
            if(venueArrayList.get(position).getLocation_name().isEmpty()){
                holder.locationIcon.setVisibility(View.GONE);
                holder.groundLocation.setVisibility(View.GONE);
            }else {
                holder.locationIcon.setVisibility(View.VISIBLE);
                holder.groundLocation.setVisibility(View.VISIBLE);
                holder.groundLocation.setText(venueArrayList.get(position).getLocation_name());
            }
            if(venueArrayList.get(position).is_verified()){
                String colorText= "<font color=\"#40ae79\">" + "Verified!" + "</font>" + " Available on platform";
                holder.verifiedText.setText(Html.fromHtml(colorText));
            }else {
                holder.verifiedText.setVisibility(View.INVISIBLE);
            }

                holder.ratingBar.setRating(venueArrayList.get(position).getRating());
            Picasso.with(mContext).load(venueArrayList.get(position).getCover_pic()).fit().centerCrop().config(Bitmap.Config.RGB_565).into(holder.groundImage);
            //String[] s=venueArrayList.get(position).getGames().split(",");
            /*ArrayAdapter adapter = new ArrayAdapter<String>(getActivity(), R.layout.venue_grid_item_layout,s);
            holder.gridView.setAdapter(adapter);*/

            if(!venueArrayList.get(position).getSports_types().isEmpty()) {
                String[] sportsType = venueArrayList.get(position).getSports_types().split(",");
                new ChipCloud.Configure()
                        .chipCloud(holder.chipCloud)
                        .labels(sportsType)
                        .mode(ChipCloud.Mode.MULTI)
                        .build();
            }
            final View itemView = holder.itemView;

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(mContext, VenueDetailsActivity.class);
                    i.putExtra("IDs",venueArrayList.get(position).getId());
                    startActivity(i);
                }
            });
            }catch (Exception e){
                e.printStackTrace();
            }

        }

        @Override
        public int getItemCount() {
            return (null != venueArrayList ? venueArrayList.size() : 0);
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView groundName,groundLocation,verifiedText;
            ImageView groundImage,locationIcon;
            GridView gridView;
            ChipCloud chipCloud;
            RatingBar ratingBar;
            public MyViewHolder(View itemView) {
                super(itemView);
                groundName = (TextView) itemView.findViewById(com.groundhog.vendor.android.R.id.ground_name);
                groundLocation = (TextView) itemView.findViewById(com.groundhog.vendor.android.R.id.venue_location);
                verifiedText = (TextView) itemView.findViewById(com.groundhog.vendor.android.R.id.venue_verified_text);
                groundImage = (ImageView) itemView.findViewById(com.groundhog.vendor.android.R.id.venue_image);
                gridView = (GridView) itemView.findViewById(com.groundhog.vendor.android.R.id.grid_venue);
                chipCloud = (ChipCloud) itemView.findViewById(com.groundhog.vendor.android.R.id.chip_cloud);
                locationIcon = (ImageView) itemView.findViewById(com.groundhog.vendor.android.R.id.venue_location_image);
                ratingBar = (RatingBar) itemView.findViewById(com.groundhog.vendor.android.R.id.ratings);

                groundName.setTypeface(PlaythoraUtility.getFont(getActivity(),RALEWAY_SEMIBOLD));
                groundLocation.setTypeface(PlaythoraUtility.getFont(getActivity(),RALEWAY_REGULAR));
                verifiedText.setTypeface(PlaythoraUtility.getFont(getActivity(),RALEWAY_REGULAR));

            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        venuesList();
    }
}
