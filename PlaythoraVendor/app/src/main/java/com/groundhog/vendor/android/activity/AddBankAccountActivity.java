package com.groundhog.vendor.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.groundhog.vendor.android.utils.Config;
import com.groundhog.vendor.android.utils.PlaythoraUtility;
import com.groundhog.vendor.android.utils.PreferenceManager;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

import static com.groundhog.vendor.android.utils.Config.RALEWAY_REGULAR;

/**
 * Created by Nakul on 9/14/2016.
 */
public class AddBankAccountActivity extends AppCompatActivity {

    EditText accName, accNum, branchName, bankName, ifsc, confirmAccNum;
    TextInputLayout accNameInputLayout, accNumInputLayout, branchNameInputLayout, bankNameInputLayout, ifscInputLayout, confirmAccNumInputLayout;
    CheckBox checkbox;
    TextView type;
    Button save;
    boolean acctype = false, isIfsc;
    int j;
    String edtAccName, edtAccNum, edtBranchName, edtBankName, edtIfsc;
    int size, accountId, acctypeInt;
    PreferenceManager mPref;
    String accountName, banName, branName, ifscCode, accNumber;
    ProgressBar progressBar;
    Toolbar toolbar;
    TextView title;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.groundhog.vendor.android.R.layout.add_new_account_layout);

        toolbar = (Toolbar) findViewById(com.groundhog.vendor.android.R.id.bank_account_toolbar);
        title = (TextView) findViewById(com.groundhog.vendor.android.R.id.toolbar_title);
        toolbar.setNavigationIcon(com.groundhog.vendor.android.R.drawable.ic_back_arrow);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        title.setText("Add bank account");
        title.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));

        //  getSupportActionBar().setTitle(Html.fromHtml("<font color=\"white\"><small>" + "Add bank account" + "</small></font>"));

        initializeAllComponents();


        checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    acctype = true;
                    acctypeInt = 1;
                    Log.e("checkTrue", String.valueOf(acctypeInt));
                    checkbox.setChecked(true);
                    checkbox.setText("Primary account");
                    // Toast.makeText(AddBankAccountActivity.this," "+acctype,Toast.LENGTH_SHORT).show();
                } else {
                    acctype = false;
                    acctypeInt = 0;
                    Log.e("checkFalse", String.valueOf(acctypeInt));
                    checkbox.setChecked(false);
                    checkbox.setText("Primary account");
                    //  Toast.makeText(AddBankAccountActivity.this," "+acctype,Toast.LENGTH_SHORT).show();
                }
            }
        });


        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (checkForValidation()) {
                    addBankAccount();
                }


            }
        });

        accName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                accNameInputLayout.setError(null);
            }
        });

        bankName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                bankNameInputLayout.setError(null);
            }
        });

        branchName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                branchNameInputLayout.setError(null);
            }
        });


        ifsc.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Log.e("length", String.valueOf(charSequence.length()));

                // if(charSequence.length()==6 || charSequence.length()>6){

                //loadIfsc();

                //  }

            }

            @Override
            public void afterTextChanged(Editable editable) {
                ifscInputLayout.setError(null);
            }
        });

        accNum.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                accNumInputLayout.setError(null);
            }
        });

        confirmAccNum.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                confirmAccNumInputLayout.setError(null);
            }
        });

/*        ifsc.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    if(ifsc.getText().toString().isEmpty()){
                        ifscInputLayout.setError("ifsc code is required");
                        ifscInputLayout.setErrorEnabled(true);
                    }else {
                        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
                            progressBar.setVisibility(View.VISIBLE);
                            AsyncHttpClient httpClient = new AsyncHttpClient();

                            httpClient.get("https://ifsc.razorpay.com/"+ifsc.getText().toString(), new AsyncHttpResponseHandler() {
                                @Override
                                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                    progressBar.setVisibility(View.GONE);
                                    String ifsc = new String(responseBody);
                                    Log.e("ifsc", ifsc);
                                    isIfsc = true;
                                    ifscInputLayout.setErrorEnabled(false);
                                }

                                @Override
                                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                                    try {
                                        String s = new String(responseBody);
                                        progressBar.setVisibility(View.GONE);
                                        isIfsc = false;
                                        ifscInputLayout.setErrorEnabled(true);
                                        Log.e("s", s);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            });

                        } else {
                            Toast.makeText(AddBankAccountActivity.this, "check your internt connection!", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        });*/
    }

    private void loadIfsc() {
        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            progressBar.setVisibility(View.VISIBLE);
            AsyncHttpClient httpClient = new AsyncHttpClient(true, 80, 443);

            httpClient.get("https://ifsc.razorpay.com/" + "HDFC0001990", new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    progressBar.setVisibility(View.GONE);
                    String ifsc = new String(responseBody);
                    Log.e("ifsc", "bankDeatils" + ifsc);
                    isIfsc = true;
                    ifscInputLayout.setErrorEnabled(false);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    try {
                        String s = new String(responseBody);
                        progressBar.setVisibility(View.GONE);
                        isIfsc = false;
                        ifscInputLayout.setErrorEnabled(true);
                        Log.e("s", s);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } else {
            Toast.makeText(AddBankAccountActivity.this, "check your internt connection!", Toast.LENGTH_SHORT).show();
        }
    }

    private void addBankAccount() {

        if (PlaythoraUtility.checkInternetConnection(getApplicationContext())) {
            PlaythoraUtility.showProgressDialog(AddBankAccountActivity.this);
            AsyncHttpClient httpClient = new AsyncHttpClient();
            httpClient.addHeader("accessToken", mPref.getAccessToken());

            RequestParams params = new RequestParams();
            params.add("account_name", accName.getText().toString());
            params.add("id", mPref.getId());
            params.add("bank_name", bankName.getText().toString());
            params.add("branch_name", branchName.getText().toString());
            params.add("ifsc_code", ifsc.getText().toString());
            params.add("account_number", accNum.getText().toString());
            params.add("is_primary", String.valueOf(acctype));


            httpClient.post(Config.ADD_BANK_ACCOUNT, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    PlaythoraUtility.hideProgressDialog();
                    String add = new String(responseBody);
                    Log.e("AddBankAccount", add);
                    try {
                        JSONObject object = new JSONObject(add);
                        accountName = object.getJSONObject("success").getString("account_name");
                        banName = object.getJSONObject("success").getString("bank_name");
                        branName = object.getJSONObject("success").getString("branch_name");
                        ifscCode = object.getJSONObject("success").getString("ifsc_code");
                        accNumber = object.getJSONObject("success").getString("account_number");
                        accountId = object.getJSONObject("success").getInt("id");
                        acctype = object.getJSONObject("success").getBoolean("is_primary");
                        Log.e("boolean", String.valueOf(acctype));

                        mPref.setAccountName(accountName);
                        mPref.setBankName(banName);
                        mPref.setBranchName(branName);
                        mPref.setAccountType(acctype);
                        mPref.setIfscCode(ifscCode);
                        mPref.setAccountNumber(accNumber);
                        mPref.setAccountId(String.valueOf(accountId));

                        if (mPref.setAccountType(false)) {
                            acctypeInt = 0;
                            acctype = false;
                        } else {
                            acctypeInt = 1;
                            acctype = true;
                        }
                        Toast.makeText(AddBankAccountActivity.this, "Bank account created successfully", Toast.LENGTH_SHORT).show();
                        Intent addBank = new Intent(AddBankAccountActivity.this, HomeActivity.class);
                        addBank.putExtra("ActivityName", "bankAccount");
                        startActivity(addBank);
                        finishAffinity();


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    PlaythoraUtility.hideProgressDialog();
                    try {
                        String failure = new String(responseBody);
                        Log.e("failure", failure);
                        JSONObject object = new JSONObject(failure);
                        if (object.getInt("statusCode") == 401 || object.getInt("statusCode") == 400) {
                            mPref.setAccessToken("");
                            Toast.makeText(getApplicationContext(), object.getString("message"), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(AddBankAccountActivity.this, LoginActivity.class);
                            startActivity(intent);
                            finish();
                        } else
                            Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                        //Toast.makeText(getApplicationContext(), "Some error occurred please try again later", Toast.LENGTH_SHORT).show();
                    }

                }
            });
        } else {
            Toast.makeText(AddBankAccountActivity.this, "Check your internet connections", Toast.LENGTH_SHORT).show();
        }
    }

    private boolean checkForValidation() {

        edtAccName = accName.getText().toString();
        edtAccNum = accNum.getText().toString();
        edtBankName = bankName.getText().toString();
        edtBranchName = branchName.getText().toString();
        edtIfsc = ifsc.getText().toString();


        if (accName.getText().toString().isEmpty()) {
            accNameInputLayout.setError("Account name is required");
            accNameInputLayout.setErrorEnabled(true);
        } else {
            accNameInputLayout.setErrorEnabled(false);
        }

        if (bankName.getText().toString().isEmpty()) {
            bankNameInputLayout.setError("Bank name is required");
            bankNameInputLayout.setErrorEnabled(true);
        } else {
            bankNameInputLayout.setErrorEnabled(false);
        }

        if (branchName.getText().toString().isEmpty()) {
            branchNameInputLayout.setError("Branch name is required");
            branchNameInputLayout.setErrorEnabled(true);
        } else {
            branchNameInputLayout.setErrorEnabled(false);
        }

        if (ifsc.getText().toString().trim().isEmpty() || ifsc.getText().toString().contains(" ")) {
            ifscInputLayout.setError("ifsc code is required");
            ifscInputLayout.setErrorEnabled(true);
        } else {
            ifscInputLayout.setErrorEnabled(false);

        }

        if (accNum.getText().toString().trim().isEmpty() || accNum.getText().toString().contains(" ")) {
            accNumInputLayout.setError("Account number is required");
            accNumInputLayout.setErrorEnabled(true);
        } else {
            accNumInputLayout.setErrorEnabled(false);
        }

        if (!(accNum.getText().toString().equals(confirmAccNum.getText().toString()))) {
            confirmAccNumInputLayout.setError("Account number does not match");
            confirmAccNumInputLayout.setErrorEnabled(true);
        } else {
            confirmAccNumInputLayout.setErrorEnabled(false);
        }


        if (accNameInputLayout.isErrorEnabled() || bankNameInputLayout.isErrorEnabled() || branchNameInputLayout.isErrorEnabled() || ifscInputLayout.isErrorEnabled() || accNumInputLayout.isErrorEnabled() || confirmAccNumInputLayout.isErrorEnabled()) {
            return false;
        } else {
            return true;
        }


    }

    private void initializeAllComponents() {

        mPref = PreferenceManager.instance(getApplicationContext());
        accName = (EditText) findViewById(com.groundhog.vendor.android.R.id.accountName);
        bankName = (EditText) findViewById(com.groundhog.vendor.android.R.id.bankName);
        branchName = (EditText) findViewById(com.groundhog.vendor.android.R.id.branchName);
        accNum = (EditText) findViewById(com.groundhog.vendor.android.R.id.accountNumber);
        ifsc = (EditText) findViewById(com.groundhog.vendor.android.R.id.ifsc);
        confirmAccNum = (EditText) findViewById(com.groundhog.vendor.android.R.id.confirmAccountNumber);
        checkbox = (CheckBox) findViewById(com.groundhog.vendor.android.R.id.checkbox);
        type = (TextView) findViewById(com.groundhog.vendor.android.R.id.accountType);
        save = (Button) findViewById(com.groundhog.vendor.android.R.id.saveAccount);
        accNameInputLayout = (TextInputLayout) findViewById(com.groundhog.vendor.android.R.id.input_layout_name);
        bankNameInputLayout = (TextInputLayout) findViewById(com.groundhog.vendor.android.R.id.input_layout_bank_name);
        branchNameInputLayout = (TextInputLayout) findViewById(com.groundhog.vendor.android.R.id.input_layout_branch_name);
        ifscInputLayout = (TextInputLayout) findViewById(com.groundhog.vendor.android.R.id.input_layout_ifsc);
        accNumInputLayout = (TextInputLayout) findViewById(com.groundhog.vendor.android.R.id.input_layout_account_number);
        confirmAccNumInputLayout = (TextInputLayout) findViewById(com.groundhog.vendor.android.R.id.input_layout_confirm_account_number);
        addBankAccountUI(findViewById(com.groundhog.vendor.android.R.id.addBankAccountActivity));
        progressBar = (ProgressBar) findViewById(com.groundhog.vendor.android.R.id.progress_bar);

        accName.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        bankName.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        branchName.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        ifsc.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        //  confirmAccNum.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        type.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        save.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        accNameInputLayout.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        bankNameInputLayout.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        branchNameInputLayout.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        ifscInputLayout.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        accNumInputLayout.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        confirmAccNumInputLayout.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));
        // accNum.setTypeface(PlaythoraUtility.getFont(getApplicationContext(),RALEWAY_REGULAR));
        checkbox.setTypeface(PlaythoraUtility.getFont(getApplicationContext(), RALEWAY_REGULAR));


    }

    private void addBankAccountUI(View viewById) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(viewById instanceof EditText)) {
            viewById.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    PlaythoraUtility.hideSoftKeyboard(AddBankAccountActivity.this);
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (viewById instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) viewById).getChildCount(); i++) {
                View innerView = ((ViewGroup) viewById).getChildAt(i);
                addBankAccountUI(innerView);
            }
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                break;

        }
        return false;
    }
}
