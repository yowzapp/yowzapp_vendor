package com.groundhog.vendor.android.model;

/**
 * Created by hemanth on 2/12/16.
 */
public class BankAccountSuccess {
    private int id;
    private String account_name;
    private String bank_name;
    private String branch_name;
    private String account_number;
    private String ifsc_code;
    private int is_primary;

    public BankAccountSuccess(int id, String account_name, String bank_name, String branch_name,String account_number, String ifsc_code, int is_primary) {
        this.id = id;
        this.account_name=account_name;
        this.bank_name = bank_name;
        this.branch_name =branch_name;
        this.account_number = account_number;
        this.ifsc_code = ifsc_code;
        this.is_primary=is_primary;

    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAccount_name() {
        return account_name;
    }

    public void setAccount_name(String account_name) {
        this.account_name = account_name;
    }

    public String getBank_name() {
        return bank_name;
    }

    public void setBank_name(String bank_name) {
        this.bank_name = bank_name;
    }

    public String getBranch_name() {
        return branch_name;
    }

    public void setBranch_name(String branch_name) {
        this.branch_name = branch_name;
    }

    public String getAccount_number() {
        return account_number;
    }

    public void setAccount_number(String account_number) {
        this.account_number = account_number;
    }

    public String getIfsc_code() {
        return ifsc_code;
    }

    public void setIfsc_code(String ifsc_code) {
        this.ifsc_code = ifsc_code;
    }

    public int is_primary() {
        return is_primary;
    }

    public void setIs_primary(int is_primary) {
        this.is_primary = is_primary;
    }
}
