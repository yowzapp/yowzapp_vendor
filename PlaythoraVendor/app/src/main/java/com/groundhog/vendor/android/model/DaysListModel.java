package com.groundhog.vendor.android.model;

import java.util.List;

/**
 * Created by hemanth on 24/3/18.
 */

public class DaysListModel {
    private String status;
    private int statusCode;
    private String message;
    private List<DaysListSuccess> success;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DaysListSuccess> getSuccess() {
        return success;
    }

    public void setSuccess(List<DaysListSuccess> success) {
        this.success = success;
    }
}
