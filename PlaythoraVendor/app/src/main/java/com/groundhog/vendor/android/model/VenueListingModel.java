package com.groundhog.vendor.android.model;

import java.util.List;

/**
 * Created by vaishakha on 10/9/16.
 */
public class VenueListingModel {
    private String id;
    private String created;
    private String groundName;
    private String varified;
    private String groundPic;
    private String Games;

    private String status;
    private String message;
    private List<VenueListItems> success;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getGroundName() {
        return groundName;
    }

    public void setGroundName(String groundName) {
        this.groundName = groundName;
    }

    public String getVarified() {
        return varified;
    }

    public void setVarified(String varified) {
        this.varified = varified;
    }

    public String getGroundPic() {
        return groundPic;
    }

    public void setGroundPic(String groundPic) {
        this.groundPic = groundPic;
    }

    public String getGames() {
        return Games;
    }

    public void setGames(String games) {
        Games = games;
    }

    public LocationObj getLocation() {
        return location;
    }

    public void setLocation(LocationObj location) {
        this.location = location;
    }

    private LocationObj location;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<VenueListItems> getSuccess() {
        return success;
    }

    public void setSuccess(List<VenueListItems> success) {
        this.success = success;
    }
}
