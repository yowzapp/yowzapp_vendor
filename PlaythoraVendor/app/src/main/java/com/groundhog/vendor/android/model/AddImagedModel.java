package com.groundhog.vendor.android.model;

/**
 * Created by vaishakha on 14/9/16.
 */
public class AddImagedModel {
    private String images;

    public AddImagedModel(String images) {
        this.images = images;
    }

    public String getImages() {

        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }
}
