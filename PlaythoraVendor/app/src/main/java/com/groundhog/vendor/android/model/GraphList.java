package com.groundhog.vendor.android.model;

import java.util.List;

/**
 * Created by vaishakha on 10/2/17.
 */
public class GraphList {
    private String date;
    private List<Grounds> grounds;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<Grounds> getGrounds() {
        return grounds;
    }

    public void setGrounds(List<Grounds> grounds) {
        this.grounds = grounds;
    }

    public class Grounds{
        private String hex;
        private String name;
        private String amount;

        public String getHex() {
            return hex;
        }

        public void setHex(String hex) {
            this.hex = hex;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }
    }
}
